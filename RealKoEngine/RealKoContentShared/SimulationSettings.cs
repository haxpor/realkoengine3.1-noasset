﻿#region File Description
//-----------------------------------------------------------------------------
// SimulationSettings.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// Simulation settings will be applied and taken into effect when it starts.
    /// </summary>
    public class SimulationSettings
    {
        #region Nested signatures

        /// <summary>
        /// Time structure.
        /// </summary>
        public struct Time
        {
            /// <summary>
            /// Hour is in 24 hour format, no daylight saving is used here.
            /// It's in the range of [0,23]
            /// </summary>
            public int Hours;

            /// <summary>
            /// Accepted range is in [0,59].
            /// </summary>
            public int Minutes;

            /// <summary>
            /// Accepted range is in [0,59].
            /// </summary>
            public int Seconds;
        };

        /// <summary>
        /// Advanced time rate, used for setting the speed of time.
        /// </summary>
        /// <remarks>
        /// The following information is used in this equation.
        /// 
        /// time_add = (n-2) * DiffAdvancedSeconds + StartAdvancedSeconds
        /// 
        /// In which n is the the rate represents the x-rate to be used for other purpose, it starts from 2 as always.
        /// For n = 1, the engine will use the normal elapsed time from gameTime object of Game class, which is equal about 0.016 seconds or
        /// 16 ms.
        /// 
        /// For both of DiffAdvancedSeconds and StartAdvancedSeconds,
        /// we consider only the seconds part. The seconds-part doesn't need to be in [0,59], if you set the value to 120, then
        /// it means that the time will be advanced with the rate of 2 minutes per Game's update-call (which is once in about 
        /// 0.0167 seconds or 16 ms).
        /// 
        /// </remarks>
        public struct AdvancedTimeRate
        {
            /// <summary>
            /// See the equation above for more detial.
            /// </summary>
            public int StartAdvancedSeconds;

            /// <summary>
            /// See the equation above for more detial.
            /// </summary>
            public int DiffAdvancedSeconds;

            /// <summary>
            /// Set the current x-rate to be used when the simulation getting start.
            /// </summary>
            public int CurrentXRate;

            /// <summary>
            /// Duration of time in seconds, to change to texture D.
            /// </summary>
            public int DDuration;
        }

        /// <summary>
        /// Airport offset position.
        /// </summary>
        /// <remarks>This information won't be used in during processing.</remarks>
        public struct AirportOffsetPos
        {
            /// <summary>
            /// Whether or not the position is in model scale.
            /// </summary>
            public bool IsModelScale;

            /// <summary>
            /// Offset position measured following what is set in IsModelScale.
            /// </summary>
            public Vector3 Position;
        }

        /// <summary>
        /// Position of initial camera.
        /// </summary>
        public struct InitialCameraPos
        {
            /// <summary>
            /// Whether or not the position is rotated already.
            /// </summary>
            public bool IsPosRotated;

            /// <summary>
            /// Position.
            /// </summary>
            public Vector3 Position;
        }

        #endregion

        public Time StartTime;
        public AdvancedTimeRate AdvTimeRate;
        public bool IsSCMoveAdvancedTime;   //whether or not the SCMesh's movement will be affected by the advanced time.
                                            //this should be set at the compile, as changing in the runtime, we cannot do the smooth changing.
        public Vector3 Wind;        //wind's strength and direction (global settings for particle system, that they need to updated each frame.)
        public float WindChangeRate;    //how fast changing of wind will affect related systems in simulation
                                        //this value should be between [0.001,0.005] for our experiment, but will vary depend on the system used to
                                        // run the simulation
        public Vector3 LightningPreferredArea;  //the preferred area of lightning, anyway it will be little offseted by the engine,
                                                //just to give the best visual result. (The box analogous)
                                                //Refer to the document for more information.
        public float GlobalScale;   //will be applied for all axis
        public float MapToModelScale;
        public float AirplaneToMapScale;
        public bool IsUpdateOnline;
        public AirportOffsetPos AirportOffsetPosition;  // DON'T USE THIS FIELD, IT'S USED AS A PLACEHOLDER FOR REFERENCE IN ONE-TIME INTERNAL PROCESSING ONLY
        public Vector3 AirportOffsetRotation;  // in degrees
        public InitialCameraPos InitialCameraPosition;
    }

    /// <summary>
    /// Content type reader of SimulationSettings.
    /// </summary>
    public class SimulationSettingsReader : ContentTypeReader<SimulationSettings>
    {
        protected override SimulationSettings Read(ContentReader input, SimulationSettings existingInstance)
        {
            SimulationSettings simSettings = new SimulationSettings();
            
            simSettings.StartTime.Hours = input.ReadInt32();
            simSettings.StartTime.Minutes = input.ReadInt32();
            simSettings.StartTime.Seconds = input.ReadInt32();
            simSettings.AdvTimeRate.StartAdvancedSeconds = input.ReadInt32();
            simSettings.AdvTimeRate.DiffAdvancedSeconds = input.ReadInt32();
            simSettings.AdvTimeRate.CurrentXRate = input.ReadInt32();
            simSettings.AdvTimeRate.DDuration = input.ReadInt32();
            simSettings.IsSCMoveAdvancedTime = input.ReadBoolean();
            simSettings.Wind = input.ReadVector3();
            simSettings.WindChangeRate = input.ReadSingle();
            simSettings.LightningPreferredArea = input.ReadVector3();
            simSettings.GlobalScale = input.ReadSingle();
            simSettings.MapToModelScale = input.ReadSingle();
            simSettings.AirplaneToMapScale = input.ReadSingle();
            simSettings.IsUpdateOnline = input.ReadBoolean();
            simSettings.AirportOffsetPosition.IsModelScale = input.ReadBoolean();
            simSettings.AirportOffsetPosition.Position = input.ReadVector3();
            simSettings.AirportOffsetRotation = input.ReadVector3();
            simSettings.InitialCameraPosition.IsPosRotated = input.ReadBoolean();
            simSettings.InitialCameraPosition.Position = input.ReadVector3();

            return simSettings;
        }
    }
}
