﻿#region File Description
//-----------------------------------------------------------------------------
// AirportPointLightSource.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents point light in airport.
    /// </summary>
    public class AirportPointLightSource
    {
        public uint ID;
        public uint TypeID;
        public bool IsPosRotated;
        public Vector3 Position;
        public Vector4 LightColor4;
        public float SpecularIntensity;
        public float SpecularPower;
        public float Intensity;
        public float Radius;
    }

    /// <summary>
    /// Content type reader of airport's point light source asset.
    /// </summary>
    public class AirportPointLightSourceReader : ContentTypeReader<AirportPointLightSource>
    {
        protected override AirportPointLightSource Read(ContentReader input, AirportPointLightSource existingInstance)
        {
            AirportPointLightSource pl = new AirportPointLightSource();

            pl.ID = input.ReadUInt32();
            pl.TypeID = input.ReadUInt32();
            pl.IsPosRotated = input.ReadBoolean();
            pl.Position = input.ReadVector3();
            pl.LightColor4 = input.ReadVector4();
            pl.SpecularIntensity = input.ReadSingle();
            pl.SpecularPower = input.ReadSingle();
            pl.Intensity = input.ReadSingle();
            pl.Radius = input.ReadSingle();
            
            return pl;
        }
    }
}
