﻿#region File Description
//-----------------------------------------------------------------------------
// AirportAlphaMesh.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents the individual alpha mesh inside the airport model.
    /// </summary>
    /// <remarks>This class tends to be the information class only, caller need to create the wrapper class to represent its instance.
    /// The RealKo Engine already provided that one.
    /// 
    /// AirportMesh doesn't include the skydome, clouddome mesh (SCMesh) and light mesh.</remarks>
    public class AirportAlphaMesh
    {
        #region Nested Signatures

        /// <summary>
        /// Basic texture set for each of mesh.
        /// </summary>
        /// <remarks>This version is the reduced one from the original version found in AirportOpaqueMesh class.
        /// Due to the alpha stuff need only draw the with diffuse texture, thus no need for other one.</remarks>
        public struct TextureSet
        {
            public string Diffuse;
        }

        #endregion

        public uint ID;
        public uint TypeID;
        public string Name;

        public string ModelFilePath;    //do not incluide the suffix extension
        public int MeshIndex;
        public int BoneIndex;

        public TextureSet DayTextureSetFilePath;
        public TextureSet RainingTextureSetFilePath;
        public TextureSet SnowTextureSetFilePath;

        public string SrcBlend;         //string indicates the drawing operation for source pixel
        public string DestBlend;        //string indicates the drawing operation for destination pixel
    }

    /// <summary>
    /// Content type reader of airport mesh.
    /// </summary>
    public class AirportAlphaMeshReader : ContentTypeReader<AirportAlphaMesh>
    {
        protected override AirportAlphaMesh Read(ContentReader input, AirportAlphaMesh existingInstance)
        {
            AirportAlphaMesh am = new AirportAlphaMesh();
            am.ID = input.ReadUInt32();
            am.TypeID = input.ReadUInt32();
            am.Name = input.ReadString();
            am.ModelFilePath = input.ReadString();
            am.MeshIndex = input.ReadInt32();
            am.BoneIndex = input.ReadInt32();
            am.DayTextureSetFilePath.Diffuse = input.ReadString();
            am.RainingTextureSetFilePath.Diffuse = input.ReadString();
            am.SnowTextureSetFilePath.Diffuse = input.ReadString();
            am.SrcBlend = input.ReadString();
            am.DestBlend = input.ReadString();

            return am;
        }
    }
}
