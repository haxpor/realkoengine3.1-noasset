﻿#region File Description
//-----------------------------------------------------------------------------
// AirplaneTypeAsset.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents generic transportation objects.
    /// </summary>
    /// <remarks>This class tends to be the information class only, caller need to create the wrapper class to represent its instance.
    /// The RealKo Engine already provided that one.
    /// 
    /// This class will be used differently from the other mesh content as it will be used for furthur reference by other components.</remarks>
    public class TransportationObject
    {
        #region Nested Signatures

        /// <summary>
        /// Structure of light mesh indexes (mesh index and bone index)
        /// </summary>
        /// <remarks>Reason to use class-type is to reduce the copying overhead when use across the project. X-component is mesh index, and y-component is bone index.</remarks>
        public class LightMeshesIndexes
        {
            public Vector2 HeadLight;
            public Vector2 SignalLight;
            public Vector2 HeadGroundLight;
            public Vector2 SignalGroundLight;
        }

        /// <summary>
        /// Structure of body mesh indexes (mesh index and bone index)
        /// </summary>
        /// <remarks>Reason to use class-type is to reduce the copying overhead when use across the project. X-component is mesh index, and y-component is bone index.</remarks>
        public class BodyMeshesIndexes
        {
            public Vector2 Hull;
            public Vector2 Wheel;
        }

        /// <summary>
        /// Structure of the texture set.
        /// </summary>
        /// <remarks>Reason to use class-type is to reduce the copying overhead when use across the project.</remarks>
        public class TransportationObjectTextureSet
        {
            public string Diffuse;
            public string Specular;
            public string Normal;
            public string Emissive;
            public string XLight;
        }

        // Specify all the rotateable meshes here (allow up to 3 meshes)
        #region Rotateable Mesh
        /// <summary>
        /// Rotateable mesh.
        /// </summary>
        public class RotateableMesh
        {
            public int MeshIndex;
            public Vector3 RotateDirection;
            public bool IsPaused;
            public float SpeedFactor;
            public int BoneIndex;
        }

        #endregion

        #endregion

        public uint ID;
        public string Name;
        public string ModelFilePath;
        public LightMeshesIndexes LightMIndexes = new LightMeshesIndexes();
        public BodyMeshesIndexes BodyMIndexes = new BodyMeshesIndexes();
        public RotateableMesh RotMesh1 = new RotateableMesh();
        public RotateableMesh RotMesh2 = new RotateableMesh();
        public RotateableMesh RotMesh3 = new RotateableMesh();
        public RotateableMesh RotMesh4 = new RotateableMesh();
        public TransportationObjectTextureSet TextureSet = new TransportationObjectTextureSet();
    }

    /// <summary>
    /// Content type reader of airplane type asset.
    /// </summary>
    public class TransportationObjectReader : ContentTypeReader<TransportationObject>
    {
        protected override TransportationObject Read(ContentReader input, TransportationObject existingInstance)
        {
            TransportationObject tpopt = new TransportationObject();

            tpopt.ID = input.ReadUInt32();
            tpopt.Name = input.ReadString();
            tpopt.ModelFilePath = input.ReadString();

            // lights' indexes
            tpopt.LightMIndexes.HeadLight = input.ReadVector2();
            tpopt.LightMIndexes.SignalLight = input.ReadVector2();
            tpopt.LightMIndexes.HeadGroundLight = input.ReadVector2();
            tpopt.LightMIndexes.SignalGroundLight = input.ReadVector2();

            // body's indexes
            tpopt.BodyMIndexes.Hull = input.ReadVector2();
            tpopt.BodyMIndexes.Wheel = input.ReadVector2();

            tpopt.RotMesh1.MeshIndex = input.ReadInt32();
            tpopt.RotMesh1.RotateDirection = input.ReadVector3();
            tpopt.RotMesh1.IsPaused = input.ReadBoolean();
            tpopt.RotMesh1.SpeedFactor = input.ReadSingle();
            tpopt.RotMesh1.BoneIndex = input.ReadInt32();
            tpopt.RotMesh2.MeshIndex = input.ReadInt32();
            tpopt.RotMesh2.RotateDirection = input.ReadVector3();
            tpopt.RotMesh2.IsPaused = input.ReadBoolean();
            tpopt.RotMesh2.SpeedFactor = input.ReadSingle();
            tpopt.RotMesh2.BoneIndex = input.ReadInt32();
            tpopt.RotMesh3.MeshIndex = input.ReadInt32();
            tpopt.RotMesh3.RotateDirection = input.ReadVector3();
            tpopt.RotMesh3.IsPaused = input.ReadBoolean();
            tpopt.RotMesh3.SpeedFactor = input.ReadSingle();
            tpopt.RotMesh3.BoneIndex = input.ReadInt32();
            tpopt.RotMesh4.MeshIndex = input.ReadInt32();
            tpopt.RotMesh4.RotateDirection = input.ReadVector3();
            tpopt.RotMesh4.IsPaused = input.ReadBoolean();
            tpopt.RotMesh4.SpeedFactor = input.ReadSingle();
            tpopt.RotMesh4.BoneIndex = input.ReadInt32();

            tpopt.TextureSet.Diffuse = input.ReadString();
            tpopt.TextureSet.Specular = input.ReadString();
            tpopt.TextureSet.Normal = input.ReadString();
            tpopt.TextureSet.Emissive = input.ReadString();
            tpopt.TextureSet.XLight = input.ReadString();

            return tpopt;
        }
    }
}
