﻿#region File Description
//-----------------------------------------------------------------------------
// AirportLightMesh.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents the individual light mesh inside the airport model.
    /// </summary>
    /// <remarks>This class tends to be the information class only, caller need to create the wrapper class to represent its instance.
    /// The RealKo Engine already provided that one.</remarks>
    public class AirportLightMesh
    {
        public uint ID;
        public uint TypeID;
        public string Name;

        public string ModelFilePath;    //do not incluide the suffix extension
        public int MeshIndex;
        public int BoneIndex;

        public string LightTexture;     //only light texture is need, as it's the alpha stuff, no need to take the light calculation involved.

        public string SrcBlend;         //string indicates the drawing operation for source pixel
        public string DestBlend;        //string indicates the drawing operation for destination pixel
    }

    /// <summary>
    /// Content type reader of airport's light-mesh.
    /// </summary>
    public class AirportLightMeshReader : ContentTypeReader<AirportLightMesh>
    {
        protected override AirportLightMesh Read(ContentReader input, AirportLightMesh existingInstance)
        {
            AirportLightMesh alm = new AirportLightMesh();

            alm.ID = input.ReadUInt32();
            alm.TypeID = input.ReadUInt32();
            alm.Name = input.ReadString();
            alm.ModelFilePath = input.ReadString();
            alm.MeshIndex = input.ReadInt32();
            alm.BoneIndex = input.ReadInt32();
            alm.LightTexture = input.ReadString();
            alm.SrcBlend = input.ReadString();
            alm.DestBlend = input.ReadString();

            return alm;
        }
    }
}
