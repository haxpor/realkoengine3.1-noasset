﻿#region File Description
//-----------------------------------------------------------------------------
// AirportApproachLightMesh.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents the individual approach light mesh inside the airport model.
    /// </summary>
    /// <remarks>This class tends to be the information class only, caller need to create the wrapper class to represent its instance.
    /// The RealKo Engine already provided that one.</remarks>
    public class AirportApproachLightMesh
    {
        public uint ID;
        public uint TypeID;
        public string Name;

        public string ModelFilePath;    //do not incluide the suffix extension
        public int MeshIndex;
        public int BoneIndex;

        public string LightTexture;     //only light texture is need, as it's the alpha stuff, no need to take the light calculation involved.

        public float DelayTime;   //time used in delaying in between each pair of light, expressed in seconds but accurates in milliseconds.
        public int NumberOfStep;    //number of step used (actually it's the number of light pole.)
        public float MoveFactor;    //constant factor used to move the texture coordinate in simulating the moving light.

        public string SrcBlend;         //string indicates the drawing operation for source pixel
        public string DestBlend;        //string indicates the drawing operation for destination pixel
    }

    /// <summary>
    /// Content type reader of airport's approach light-mesh.
    /// </summary>
    public class AirportApproachLightMeshReader : ContentTypeReader<AirportApproachLightMesh>
    {
        protected override AirportApproachLightMesh Read(ContentReader input, AirportApproachLightMesh existingInstance)
        {
            AirportApproachLightMesh alm = new AirportApproachLightMesh();

            alm.ID = input.ReadUInt32();
            alm.TypeID = input.ReadUInt32();
            alm.Name = input.ReadString();
            alm.ModelFilePath = input.ReadString();
            alm.MeshIndex = input.ReadInt32();
            alm.BoneIndex = input.ReadInt32();
            alm.LightTexture = input.ReadString();
            alm.DelayTime = input.ReadSingle();
            alm.NumberOfStep = input.ReadInt32();
            alm.MoveFactor = input.ReadSingle();
            alm.SrcBlend = input.ReadString();
            alm.DestBlend = input.ReadString();

            return alm;
        }
    }
}
