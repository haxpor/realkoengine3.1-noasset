﻿#region File Description
//-----------------------------------------------------------------------------
// AirportOpaqueMesh.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// This class represents the individual opaque mesh inside the airport model.
    /// </summary>
    /// <remarks>This class tends to be the information class only, caller need to create the wrapper class to represent its instance.
    /// The RealKo Engine already provided that one.
    /// 
    /// AirportMesh doesn't include the skydome, clouddome mesh (SCMesh) and light mesh.</remarks>
    public class AirportOpaqueMesh
    {
        #region Nested Signatures

        /// <summary>
        /// Basic texture set for each of mesh.
        /// </summary>
        /// <remarks>In the real implementation, not all the textures are required.
        /// Use struct to reduce the overhead of creating the real class.</remarks>
        public struct TextureSet
        {
            public string Diffuse;
            public string Normal;
            public string Specular;
            public string Emissive;
        }

        #endregion

        public uint ID;
        public uint TypeID;
        public string Name;

        public string ModelFilePath;    //do not incluide the suffix extension
        public int MeshIndex;
        public int BoneIndex;

        public TextureSet DayTextureSetFilePath;
        public TextureSet RainingTextureSetFilePath;
        public TextureSet SnowTextureSetFilePath;
    }

    /// <summary>
    /// Content type reader of airport mesh.
    /// </summary>
    public class AirportOpaqueMeshReader : ContentTypeReader<AirportOpaqueMesh>
    {
        protected override AirportOpaqueMesh Read(ContentReader input, AirportOpaqueMesh existingInstance)
        {
            AirportOpaqueMesh am = new AirportOpaqueMesh();
            am.ID = input.ReadUInt32();
            am.TypeID = input.ReadUInt32();
            am.Name = input.ReadString();
            am.ModelFilePath = input.ReadString();
            am.MeshIndex = input.ReadInt32();
            am.BoneIndex = input.ReadInt32();
            am.DayTextureSetFilePath.Diffuse = input.ReadString();
            am.DayTextureSetFilePath.Normal = input.ReadString();
            am.DayTextureSetFilePath.Specular = input.ReadString();
            am.DayTextureSetFilePath.Emissive = input.ReadString();
            am.RainingTextureSetFilePath.Diffuse = input.ReadString();
            am.RainingTextureSetFilePath.Normal = input.ReadString();
            am.RainingTextureSetFilePath.Specular = input.ReadString();
            am.RainingTextureSetFilePath.Emissive = input.ReadString();
            am.SnowTextureSetFilePath.Diffuse = input.ReadString();
            am.SnowTextureSetFilePath.Normal = input.ReadString();
            am.SnowTextureSetFilePath.Specular = input.ReadString();
            am.SnowTextureSetFilePath.Emissive = input.ReadString();

            return am;
        }
    }
}
