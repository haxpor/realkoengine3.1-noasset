﻿#region File Description
//-----------------------------------------------------------------------------
// SCMesh.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RealKoContentShared
{
    /// <summary>
    /// SCMesh represents the skydome and clouddome mesh, and its texture set, which is the airport model.
    /// </summary>
    public class SCMesh
    {
        #region Nested Signatures

        /// <summary>
        /// Skydome and clouddome mesh's texture set.
        /// </summary>
        /// <remarks>Name of each field in this structure is designed to work individually for RealKo's artist-team.
        /// Texture A, B, C, and E are tend to be the daytime-sky environment texture. For texture D, tends to be used for
        ///  the raining, and snow time.</remarks>
        public struct SCTextureSet
        {
            public string TextureA;
            public string TextureB;
            public string TextureC;
            public string TextureD;
            public string TextureE;
        }

        #endregion

        public uint ID;
        public uint TypeID;
        public string Name;

        public string ModelFilePath;    //do not incluide the suffix extension
        public int MeshIndex;
        public int BoneIndex;

        public SCTextureSet TextureSetFilePath;
    }

    /// <summary>
    /// Content type reader of SCMesh.
    /// </summary>
    public class SCMeshReader : ContentTypeReader<SCMesh>
    {
        protected override SCMesh Read(ContentReader input, SCMesh existingInstance)
        {
            SCMesh scmesh = new SCMesh();

            scmesh.ID = input.ReadUInt32();
            scmesh.TypeID = input.ReadUInt32();
            scmesh.Name = input.ReadString();

            scmesh.ModelFilePath = input.ReadString();
            scmesh.MeshIndex = input.ReadInt32();
            scmesh.BoneIndex = input.ReadInt32();

            scmesh.TextureSetFilePath.TextureA = input.ReadString();
            scmesh.TextureSetFilePath.TextureB = input.ReadString();
            scmesh.TextureSetFilePath.TextureC = input.ReadString();
            scmesh.TextureSetFilePath.TextureD = input.ReadString();
            scmesh.TextureSetFilePath.TextureE = input.ReadString();

            return scmesh;
        }
    }
}
