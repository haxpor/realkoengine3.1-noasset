﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RealKoContentShared
{
    public class DerivedSimulationSettings : SimulationSettings
    {
        public Vector3 AirportRealMapOffsetPosition;
        public Vector3 AirportModelOffsetPosition;

        /// <summary>
        /// Make a copy of simulation's settings from s.
        /// </summary>
        /// <param name="s">Simulation settings to make a copy from.</param>
        public void CopyFrom(SimulationSettings s)
        {
            StartTime.Hours = s.StartTime.Hours;
            StartTime.Minutes = s.StartTime.Minutes;
            StartTime.Seconds = s.StartTime.Seconds;

            AdvTimeRate.StartAdvancedSeconds = s.AdvTimeRate.StartAdvancedSeconds;
            AdvTimeRate.DiffAdvancedSeconds = s.AdvTimeRate.DiffAdvancedSeconds;
            AdvTimeRate.CurrentXRate = s.AdvTimeRate.CurrentXRate;
            AdvTimeRate.DDuration = s.AdvTimeRate.DDuration;

            IsSCMoveAdvancedTime = s.IsSCMoveAdvancedTime;

            Wind = s.Wind;
            WindChangeRate = s.WindChangeRate;

            LightningPreferredArea = s.LightningPreferredArea;
            GlobalScale = s.GlobalScale;
            MapToModelScale = s.MapToModelScale;
            AirplaneToMapScale = s.AirplaneToMapScale;
            IsUpdateOnline = s.IsUpdateOnline;
            AirportOffsetPosition.IsModelScale = s.AirportOffsetPosition.IsModelScale;
            AirportOffsetPosition.Position = s.AirportOffsetPosition.Position;
            AirportOffsetRotation = s.AirportOffsetRotation;

            InitialCameraPosition.IsPosRotated = s.InitialCameraPosition.IsPosRotated;
            InitialCameraPosition.Position = s.InitialCameraPosition.Position;
        }
    }
}
