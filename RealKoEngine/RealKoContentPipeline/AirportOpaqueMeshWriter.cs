#region File Description
//-----------------------------------------------------------------------------
// AirportOpaqueMeshWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// AirportOpaqueMesh content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class AirportOpaqueMeshWriter : ContentTypeWriter<AirportOpaqueMesh>
    {
        protected override void Write(ContentWriter output, AirportOpaqueMesh value)
        {
            output.Write(value.ID);
            output.Write(value.TypeID);
            output.Write(value.Name);
            output.Write(value.ModelFilePath);
            output.Write(value.MeshIndex);
            output.Write(value.BoneIndex);
            output.Write(value.DayTextureSetFilePath.Diffuse);
            output.Write(value.DayTextureSetFilePath.Normal);
            output.Write(value.DayTextureSetFilePath.Specular);
            output.Write(value.DayTextureSetFilePath.Emissive);
            output.Write(value.RainingTextureSetFilePath.Diffuse);
            output.Write(value.RainingTextureSetFilePath.Normal);
            output.Write(value.RainingTextureSetFilePath.Specular);
            output.Write(value.RainingTextureSetFilePath.Emissive);
            output.Write(value.SnowTextureSetFilePath.Diffuse);
            output.Write(value.SnowTextureSetFilePath.Normal);
            output.Write(value.SnowTextureSetFilePath.Specular);
            output.Write(value.SnowTextureSetFilePath.Emissive);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(AirportOpaqueMeshReader).AssemblyQualifiedName;
        }
    }
}
