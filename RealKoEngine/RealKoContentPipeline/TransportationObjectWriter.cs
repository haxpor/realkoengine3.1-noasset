﻿#region File Description
//-----------------------------------------------------------------------------
// AirplaneTypeAssetWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// TransportationObject content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class TransportationObjectWriter : ContentTypeWriter<TransportationObject>
    {
        protected override void Write(ContentWriter output, TransportationObject value)
        {
            output.Write(value.ID);
            output.Write(value.Name);
            output.Write(value.ModelFilePath);
            output.Write(value.LightMIndexes.HeadLight);
            output.Write(value.LightMIndexes.SignalLight);
            output.Write(value.LightMIndexes.HeadGroundLight);
            output.Write(value.LightMIndexes.SignalGroundLight);
            output.Write(value.BodyMIndexes.Hull);
            output.Write(value.BodyMIndexes.Wheel);
            output.Write(value.RotMesh1.MeshIndex);
            output.Write(value.RotMesh1.RotateDirection);
            output.Write(value.RotMesh1.IsPaused);
            output.Write(value.RotMesh1.SpeedFactor);
            output.Write(value.RotMesh1.BoneIndex);
            output.Write(value.RotMesh2.MeshIndex);
            output.Write(value.RotMesh2.RotateDirection);
            output.Write(value.RotMesh2.IsPaused);
            output.Write(value.RotMesh2.SpeedFactor);
            output.Write(value.RotMesh2.BoneIndex);
            output.Write(value.RotMesh3.MeshIndex);
            output.Write(value.RotMesh3.RotateDirection);
            output.Write(value.RotMesh3.IsPaused);
            output.Write(value.RotMesh3.SpeedFactor);
            output.Write(value.RotMesh3.BoneIndex);
            output.Write(value.RotMesh4.MeshIndex);
            output.Write(value.RotMesh4.RotateDirection);
            output.Write(value.RotMesh4.IsPaused);
            output.Write(value.RotMesh4.SpeedFactor);
            output.Write(value.RotMesh4.BoneIndex);
            output.Write(value.TextureSet.Diffuse);
            output.Write(value.TextureSet.Specular);
            output.Write(value.TextureSet.Normal);
            output.Write(value.TextureSet.Emissive);
            output.Write(value.TextureSet.XLight);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(TransportationObjectReader).AssemblyQualifiedName;
        }
    }
}
