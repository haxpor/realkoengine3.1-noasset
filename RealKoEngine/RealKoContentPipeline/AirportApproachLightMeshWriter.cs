﻿#region File Description
//-----------------------------------------------------------------------------
// AirportApproachLightMeshWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// AirportApproachLightMesh content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class AirportApproachLightMeshWriter : ContentTypeWriter<AirportApproachLightMesh>
    {
        protected override void Write(ContentWriter output, AirportApproachLightMesh value)
        {
            output.Write(value.ID);
            output.Write(value.TypeID);
            output.Write(value.Name);
            output.Write(value.ModelFilePath);
            output.Write(value.MeshIndex);
            output.Write(value.BoneIndex);
            output.Write(value.LightTexture);
            output.Write(value.DelayTime);
            output.Write(value.NumberOfStep);
            output.Write(value.MoveFactor);
            output.Write(value.SrcBlend);
            output.Write(value.DestBlend);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(AirportApproachLightMeshReader).AssemblyQualifiedName;
        }
    }
}
