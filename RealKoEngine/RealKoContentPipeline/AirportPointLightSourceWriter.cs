﻿#region File Description
//-----------------------------------------------------------------------------
// AirportLightMeshWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// AirportPointLightSourceWriter content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class AirportPointLightSourceWriter : ContentTypeWriter<AirportPointLightSource>
    {
        protected override void Write(ContentWriter output, AirportPointLightSource value)
        {
            output.Write(value.ID);
            output.Write(value.TypeID);
            output.Write(value.IsPosRotated);
            output.Write(value.Position);
            output.Write(value.LightColor4);
            output.Write(value.SpecularIntensity);
            output.Write(value.SpecularPower);
            output.Write(value.Intensity);
            output.Write(value.Radius);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(AirportPointLightSourceReader).AssemblyQualifiedName;
        }
    }
}
