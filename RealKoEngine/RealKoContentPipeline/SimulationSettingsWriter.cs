﻿#region File Description
//-----------------------------------------------------------------------------
// SimulationSettingsWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// SimulationSettings content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class SimulationSettingsWriter : ContentTypeWriter<SimulationSettings>
    {
        protected override void Write(ContentWriter output, SimulationSettings value)
        {
            output.Write(value.StartTime.Hours);
            output.Write(value.StartTime.Minutes);
            output.Write(value.StartTime.Seconds);
            output.Write(value.AdvTimeRate.StartAdvancedSeconds);
            output.Write(value.AdvTimeRate.DiffAdvancedSeconds);
            output.Write(value.AdvTimeRate.CurrentXRate);
            output.Write(value.AdvTimeRate.DDuration);
            output.Write(value.IsSCMoveAdvancedTime);
            output.Write(value.Wind);
            output.Write(value.WindChangeRate);
            output.Write(value.LightningPreferredArea);
            output.Write(value.GlobalScale);
            output.Write(value.MapToModelScale);
            output.Write(value.AirplaneToMapScale);
            output.Write(value.IsUpdateOnline);
            output.Write(value.AirportOffsetPosition.IsModelScale);
            output.Write(value.AirportOffsetPosition.Position);
            output.Write(value.AirportOffsetRotation);
            output.Write(value.InitialCameraPosition.IsPosRotated);
            output.Write(value.InitialCameraPosition.Position);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(SimulationSettingsReader).AssemblyQualifiedName;
        }
    }
}
