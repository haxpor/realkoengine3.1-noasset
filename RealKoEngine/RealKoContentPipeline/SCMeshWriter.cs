﻿#region File Description
//-----------------------------------------------------------------------------
// SCMeshWriter.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using RealKoContentShared;

namespace RealKoContentPipeline
{
    /// <summary>
    /// SCMesh content type writer.
    /// 
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class SCMeshWriter : ContentTypeWriter<SCMesh>
    {
        protected override void Write(ContentWriter output, SCMesh value)
        {
            output.Write(value.ID);
            output.Write(value.TypeID);
            output.Write(value.Name);
            output.Write(value.ModelFilePath);
            output.Write(value.MeshIndex);
            output.Write(value.BoneIndex);
            output.Write(value.TextureSetFilePath.TextureA);
            output.Write(value.TextureSetFilePath.TextureB);
            output.Write(value.TextureSetFilePath.TextureC);
            output.Write(value.TextureSetFilePath.TextureD);
            output.Write(value.TextureSetFilePath.TextureE);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(SCMeshReader).AssemblyQualifiedName;
        }
    }
}
