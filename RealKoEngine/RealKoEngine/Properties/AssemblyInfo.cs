﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RealKoEngine")]
[assembly: AssemblyProduct("RealKoEngine")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("Wasin Thonkaew")]
[assembly: AssemblyCopyright("Copyright © Wasin Thonkaew 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("08761f02-de01-4fd7-b629-2705ef0d8940")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.2.0")]
[assembly: AssemblyFileVersionAttribute("1.0.2.0")]