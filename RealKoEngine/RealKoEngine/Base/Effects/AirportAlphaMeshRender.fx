//-----------------------------------------------------------------------------
// AirportAlphaMeshRender.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//AirportAlphaMeshRender.fx
//This effect file is used specically for drawing the airport's alpha meshes.
//The purpose of this is just to draw them out normally, no special effect.
//Note: No light will be taken into account thus we need such calculation, just route the color out.

#include "Includes.inc"

///////////////////////////////////////////////////////////////////////////////////////////
//base and target texture here is the diffuse texture.
//Don't be confused by the name of the variable!
texture2D baseTexture;
texture2D targetTexture;

//delta-textureset time (used for lerp between the baseTexture, and targetTexture)
//dTime is in range [0,1]
float dtTime;

sampler baseSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	MaxAnisotropy = 8;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <baseTexture>;
};

sampler targetSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	MaxAnisotropy = 8;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <targetTexture>;
};

struct PixelShaderInput
{
	float2 TextureCoords: TEXCOORD0;
};

float4 PixelShader(BasicPSInput input): COLOR0
{
	//Color
	//Lerping base and target texture
	//get color from baseTexture
	float4 baseColor = tex2D(baseSampler, input.TexCoords);
	//get color from targetTexture
	float4 targetColor = tex2D(targetSampler, input.TexCoords);

	//lerp from baseTexture to targetTexture (the dTime must be set properly)
	float4 color = lerp(baseColor, targetColor, dtTime);
	float oldAlpha = color.a;
	
	// fog
	if(fogEnabled)
	{
		color = fogFormulaePS(color, input.WorldPosition);
		color.a = oldAlpha;
	}
	
	color.rgb *= ambientLight;
	
	return color;
}

technique AirportAlphaMeshRender
{
	pass p0
	{
		VertexShader = compile vs_2_0 BasicVS();
		PixelShader = compile ps_2_0 PixelShader();
	}
}
