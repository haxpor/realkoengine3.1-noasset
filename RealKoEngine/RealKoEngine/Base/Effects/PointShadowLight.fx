//-----------------------------------------------------------------------------
// PointShadowLight.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to:	Catalin Zima		
//-----------------------------------------------------------------------------

#include "Includes.inc"

//color of the light
float3 LightColor;

//position of the light
float3 LightPosition;

//light intensity
float LightIntensity;

//light radius
float LightRadius;

//adjustment of the pixel
float2 adjust;

//depth bias when using the shadowing algorithm with different resolution
float depthBias;

//light's view and projection matrix
float4x4 lightView;
float4x4 lightProjection;

//diffuse color, and spcular intensity
texture colorMap;
//normal, and specular power
texture normalMap;
//depth
texture depthMap;
//shadow map
texture shadowMap;

//shape light map (these properties should be set externally everytime for each light)
bool isShapeLightMapEnabled;
texture shapeLightMap;

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler shadowMapSampler = sampler_state
{
	Texture = <shadowMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Point;
    MinFilter = Point;
    Mipfilter = None;
};

sampler shapeLightMapSampler = sampler_state
{
	Texture = <shapeLightMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Linear;
    MinFilter = Linear;
    Mipfilter = None;
};

struct VertexShaderInput
{
	float3 Position: POSITION0;
};

struct VertexShaderOutput
{
	float4 Position: POSITION0;
	float4 ScreenPosition: TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	//calculate the wvp matrix
	float4x4 wvp = mul(world, mul(view, projection) );
	
	//calculate the screen-position
	output.Position = mul( float4(input.Position, 1.0f), wvp);
	//make a copy of the screen-position
	output.ScreenPosition = output.Position;
	
	return output;
}

float4 PixelShaderFunction(VertexShaderOutput input): COLOR0
{
	//get the screen position
	input.ScreenPosition.xy /= input.ScreenPosition.w;
	
	//get the texture coordinate from the screen position
	//Note: The texture coordinate is in [0,1]*[0,1]
	//The screen coordinate is in [-1,1] * [1,-1]
	float2 texCoord = 0.5f * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1) - adjust;
	
	//get the normal data from the normal map
	float4 normalData = tex2D(normalMapSampler, texCoord);
	//transform normal back into [-1,1]
	float3 normal = 2.0f * normalData.xyz - 1.0f;
	//get specular power, by get in the range of [0,255]
	//this will be the local specular power, as we have the one in Includes.inc to multiply to as well
	float specularPower_local = normalData.a * 255;
	//get the specular intensity
	float specularIntensity = tex2D(colorMapSampler, texCoord).a;
	
	//get depth
	float depth = tex2D(depthMapSampler, texCoord).r;
	
	//calculate the screen space position
	float4 position;
	position.xy = input.ScreenPosition.xy;
	position.z = depth;
	position.w = 1.0f;
	//transform to the world space
	position = mul(position, invertedViewProjection);
	position /= position.w;
	
	float4 lpos = mul(mul(position, lightView), lightProjection);
	lpos /= lpos.w;
	float2 sTexC = float2(lpos.x/2.0f+ .5f, -lpos.y/2.0f+0.5f);

	//get the depth value from shadow map
	float SDepth = tex2D(shadowMapSampler, sTexC).r;
	
	//shadow flag
	bool isInShadow = SDepth <= lpos.z - depthBias;
	
	if(!isInShadow &&
		sTexC.x >= 0 && sTexC.x <= 1 && sTexC.y >= 0 && sTexC.y <= 1)
	{
		//calculate the light part
		//calculate the light vector
		float3 lightVector = LightPosition - position;
		
		//calculate the attenuation
		float attenuation = saturate(1.0f - length(lightVector) / LightRadius);
		
		//normalize the light vector
		lightVector = normalize(lightVector);
		
		//calculate the diffuse light
		float NdL = max(0, dot(normal, lightVector));
		float3 diffuseLight = NdL * LightColor.rgb;
		
		//calculate the reflection vector
		float3 reflectionVector = normalize(reflect(-lightVector, normal));
		//calculate the direction to camera vector
		float3 directionToCameraVector = normalize(cameraPosition - position);
		
		//calculate the specular light
		float specularLight = specularIntensity * pow( saturate(dot(reflectionVector, directionToCameraVector)), specularPower_local * specularPower);
		
		//get the result color
		float4 color =  attenuation * LightIntensity * float4(diffuseLight.rgb, specularLight);
		
		//shape with the shape light map (if necessary)
		if(isShapeLightMapEnabled)
			color *= tex2D(shapeLightMapSampler, sTexC).r;
			
		return color;
	}
	else
	{
		return float4(0,0,0,0);
	}
}

technique PointShadowLight
{
	pass P0
	{
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();
	}
}
