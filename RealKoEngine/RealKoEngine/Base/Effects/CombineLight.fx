//-----------------------------------------------------------------------------
// CombineLight.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//This effect is used to combine the color from the mesh's texture and the light map which drawed prior to this effect.

sampler sceneSampler : register (s0);
sampler lightSampler : register (s1);
sampler lightOcclusionSampler : register (s2);

// sun light intensity (ambient light)
float ambientLight;

float4 CombinePS(float2 texCoord: TEXCOORD0): COLOR0
{
	float3 diffuseColor = tex2D(sceneSampler, texCoord);
	float4 lightData = tex2D(lightSampler, texCoord);
	float lightOcclusion = tex2D(lightOcclusionSampler, texCoord).r;
	
	float3 lightColor = lightData.rgb;
	float specular = lightData.a;

	//check whether to taken light into effect
	//full specular intensity affect
	if(lightOcclusion == 0)
	{
		return float4((diffuseColor * lightColor) + specular + diffuseColor * ambientLight, 1);
	}
	//no light calculation affect
	else
		return float4(diffuseColor, 1);
}

technique CombineLight
{
	pass P0
	{
		PixelShader = compile ps_2_0 CombinePS();
	}
}