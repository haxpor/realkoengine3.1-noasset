//-----------------------------------------------------------------------------
// AirportOpaqueMeshRender.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//AirportOpaqueMeshRender effect file
//It is used to blend the color from the base, and target texture of airport's opaque mesh in different set of weather textureset.

#include "Includes.inc"

///////////////////////////////////////////////////////////////////////////////////////////
//base and target texture here is the diffuse texture.
//Don't be confused by the name of the variable!
texture2D baseTexture;
texture2D targetTexture;

//delta-textureset time (used for lerp between the baseTexture, and targetTexture)
//dTime is in range [0,1]
float dtTime;

sampler baseSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	MaxAnisotropy = 8;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <baseTexture>;
};

sampler targetSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	MaxAnisotropy = 8;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <targetTexture>;
};

///////////////////////////////////////////////////////////////////////////////////////////

struct RenderGBufferVertexOutput
{
	float4 Position: POSITION;
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 Depth: TEXCOORD3;
	float3x3 TangentToWorld: TEXCOORD4;
};

struct RenderGBufferPixelInput
{
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 Depth: TEXCOORD3;
	float3x3 TangentToWorld: TEXCOORD4;
};

struct RenderGBufferPixelOutput
{
	float4 Color: COLOR0;
	float4 Depth: COLOR1;
	float4 LightOcclusion: COLOR2;
	float4 Normal: COLOR3;
};

RenderGBufferVertexOutput RenderGBufferVS(float3 position: POSITION, float3 normal: NORMAL, float3 binormal: BINORMAL0, float3 tangent: TANGENT0
	, float2 texCoords: TEXCOORD)
{
	RenderGBufferVertexOutput output;
	
	//generate the wvp-matrix
	float4x4 wvp = mul( mul( world, view), projection);
	
	//get the position on screen
	output.Position = mul( float4(position, 1.0f), wvp);
	//get the depth information
	output.Depth = float2(output.Position.z, output.Position.w);
	
	//transform the position into world space
	float4 worldPosition = mul( float4( position, 1.0f), world);
	output.WorldPosition = worldPosition / worldPosition.w;
	
	// transform normal into world space (use this in pixel shader in case of the model doesn't have the separate normal map file)
	output.WorldNormal = mul(normal, world);
	
	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors
    output.TangentToWorld[0] = mul(tangent, world);
    output.TangentToWorld[1] = mul(binormal, world);
    output.TangentToWorld[2] = mul(normal, world);
	
	//copy the texture's coordinate
	output.TexCoords.x = texCoords.x * texCoordU;
	output.TexCoords.y = texCoords.y * texCoordV;
	
	return output;
}

RenderGBufferPixelOutput RenderGBufferPS(RenderGBufferPixelInput input)
{
	RenderGBufferPixelOutput output;

	//Color
	//Lerping base and target texture
	//get color from baseTexture
	float4 baseColor = tex2D(baseSampler, input.TexCoords);
	//get color from targetTexture
	float4 targetColor = tex2D(targetSampler, input.TexCoords);

	//lerp from baseTexture to targetTexture (the dTime must be set properly)
	float4 blendedColor = lerp(baseColor, targetColor, dtTime);
	
	// fog
	if(fogEnabled)
		blendedColor = fogFormulaePS(blendedColor, input.WorldPosition);
	
	//we get our final color
	output.Color = blendedColor;
	
	//set the specular intensity
	if(specularTextureEnabled)
	{
		output.Color.a = tex2D(specularSampler, input.TexCoords).r * specularIntensity;	
	}
	else
	{
		output.Color.a = specularIntensity;
	}

	//Normal
	//get the normal from texture if available
	if(normalTextureEnabled)
	{
		//get the normal data from the normal texture (of the model)
		float3 normal = tex2D(normalSampler, input.TexCoords).rgb;
		//transform into [-1,1]
		normal = 2.0f * normal - 1.0f;
		//transform it with TangentToWorld matrix
		normal = mul(normal, input.TangentToWorld);
		//normalize the result
		normal = normalize(normal);
		//convert back into [0,1]
		output.Normal.rgb = 0.5f * (normal + 1.0f);
	}
	else{
		//transform from [-1,1] to [0,1]
		output.Normal.rgb = 0.5f * (normalize(input.WorldNormal) + 1.0f);
	}
	//set the specular power
	output.Normal.a = specularPower;
	
	//Depth
	output.Depth = input.Depth.x / input.Depth.y;
	
	//set the light occlusion
	output.LightOcclusion = lightOcclusion;
	
	return output;
}

technique AirportOpaqueMeshBlendTextureSet
{
	pass p0
	{
		VertexShader = compile vs_2_0 RenderGBufferVS();
		PixelShader = compile ps_3_0 RenderGBufferPS();
	}
}

