//-----------------------------------------------------------------------------
// AntiAliasing.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
//-----------------------------------------------------------------------------

// This effect is used as post-processing to do anti-aliasing for deferred shading scene.

//sampler sceneSampler : register(s0);

// to be assigned
float2 PIXEL_SIZE;
float viewport_width;
float viewport_height;

// need to set this value in compile time
#define AA_TECHNIQUE 1

// ------------------------------------------------------
// -- DLAA (1) ------------------------------------------
// ------------------------------------------------------
#if (AA_TECHNIQUE == 1)
#include "AA/DLAA.h"
#endif

// -- not work yet --
// ------------------------------------------------------
// -- MLAA (2) ------------------------------------------
// ------------------------------------------------------
#if (AA_TECHNIQUE == 2)
#include "AA/MLAA.h"
#endif

// -- not work yet --
// ------------------------------------------------------
// -- SMAA (3) ------------------------------------------
// ------------------------------------------------------
#if (AA_TECHNIQUE == 3)
#include "AA/SMAA_Wrapper.h"
#endif