//-----------------------------------------------------------------------------
// SpotLight.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to:	Catalin Zima		
//-----------------------------------------------------------------------------

//Note that the following attributes will not be messed up within Includes.inc file.
// This scheme will separate the spot light to different file.
// Also note that the light source from spot light doesn't have any geometry, so the vertex transformation will not be performed, just route it to the pixel shader.

// SpotLight.fx effect.

//will use only partial of it
#include "Includes.inc"

//screen adjust
shared float2 adjust = 0.0f;

//properties of spotlight (the cone direction will be calculated from the vector of lightPosition and lightLookAtPos)
float3 lightPosition;
float3 lightConeLookAtPos;
float lightRange;
float3 lightColor;
float lightIntensity;
float lightConeAngle;
float lightConeDecay;

//textures set
//diffuse color, and specular intensity in the alpha channel
texture colorMap;
//normals, and specular power in the alpha channel
texture normalMap;
//depth
texture depthMap;

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Linear;
	MinFilter = Linear;
	MaxAnisotropy = 8;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = <normalMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = <depthMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct SpotLightVertexOutput
{
	float4 Position: POSITION0;
	float2 TexCoords: TEXCOORD0;
};

SpotLightVertexOutput SpotLightVS(float3 position: POSITION0, float2 texCoords: TEXCOORD0)
{
	SpotLightVertexOutput output;
	
	output.Position = float4(position, 1.0f);
	output.TexCoords = texCoords;
	
	return output;
}

float4 SpotLightPS(SpotLightVertexOutput input): COLOR0
{
	//get the normal data from normal map
	float4 normalData = tex2D(normalMapSampler, input.TexCoords);
	//transfrom back to [-1,1]
	float3 normal = 2.0f * normalData.rgb - 1.0f;
	//get the specular power, and convert it into [0,255]
	float specularPower_local = normalData.a * 255;
	
	//get specular intensity
	float specularIntensity_local = tex2D(colorMapSampler, input.TexCoords).a;
	
	//read depth
	float depthVal = tex2D(depthMapSampler, input.TexCoords).r;
	//create the screen position
	float4 position;
		//get x position by convert it from [0,1] to [-1,1]
	position.x = input.TexCoords.x * 2.0f - 1.0f;
		//same for y but with different sign
	position.y = -(input.TexCoords.y * 2.0f - 1.0f);
	position.z = depthVal;
	position.w = 1.0f;
	
	//trasform it into the world space
	position = mul(position, invertedViewProjection);
	position /= position.w;
		//now the position is in the world space
	
	//determine cone criteria
	float3 lightDirection = normalize(position - lightPosition);
	//calculate the condot
	float coneDot = dot(lightDirection, normalize(lightConeLookAtPos - lightPosition));
	bool isInLightCone = coneDot >= lightConeAngle;

	if(isInLightCone)
	{
		//calculate the attenuation of the spotlight
		float spotIntensity = pow(coneDot, lightConeDecay);
		//calculate the spotlight attennuation
		float spotAtt = saturate(1.0f - length(lightPosition - position)/lightRange); 
			
		//calculate the lambertian light 
		float NdL = max(0, dot(normal, -lightDirection));
		float3 diffuseLight = NdL * lightColor.rgb;
			
		//calculate the reflection vector
		float3 reflectVector = normalize(reflect(lightDirection, normal));
		//calculate the direction to camera
		float3 directionToCamera = normalize(cameraPosition - position);
		//calculate the specular light
		float specularLight = specularIntensity * specularIntensity_local * pow( saturate( dot(reflectVector, directionToCamera) ), specularPower_local * specularPower);
			
		//our result color
		float4 color = spotAtt * spotIntensity * lightIntensity * float4(diffuseLight.rgb, specularLight);
			
		//output the result color
		return color;
	}
	else
	{
		return float4(0,0,0,0);
	}
}

technique SpotLight
{
	pass p0
	{
		VertexShader = compile vs_2_0 SpotLightVS();
		PixelShader = compile ps_3_0 SpotLightPS();
	}
}