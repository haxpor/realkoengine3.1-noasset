//-----------------------------------------------------------------------------
// ClearGBuffer.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to: Catalin Zima
//-----------------------------------------------------------------------------

// Purpose is to clear the background color of GBuffer

struct VertexOutput
{
	float4 Position: POSITION0;
};

struct PixelOutput
{
	float4 Color: COLOR0;
	float4 Depth: COLOR1;
	float4 LightOcclusion: COLOR2;
	float4 Normal: COLOR3;
};

VertexOutput ClearGBufferVS(float3 position: POSITION0)
{
	VertexOutput output;
	
	//no need to apply the transformation anymore as we are already in the screen space
	output.Position = float4(position, 1);
	return output;
}

PixelOutput ClearGBufferPS(VertexOutput input)
{
	PixelOutput output;
	
	//clear color channel to black
	output.Color.rgb = 0.0f;
	//clear the specular intensity to zero
	output.Color.a = 0.0f;
	
	//set the normal to 0.5f (when transforming into [-1,1] we will get 0f)
	output.Normal.rgb = 0.5f;
	//clear the specular power to one
	output.Normal.a = 0.0f;
	
	//clear max depth by setting them to fartest
	output.Depth = 1.0f;
	
	//reset that all the object can be taken into light-calculation
	output.LightOcclusion = 0;
	
	return output;
}

technique ClearGBuffer
{
	pass p0
	{
		VertexShader = compile vs_1_1 ClearGBufferVS();
		PixelShader = compile ps_2_0 ClearGBufferPS();
	}
}