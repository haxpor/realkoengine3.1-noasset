//-----------------------------------------------------------------------------
// AirportAlphaLM2Render.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//AirportAlphaLM2Render.fx
//This effect file is used specifically for airport's light meshes 2 (LM2). They will be drawed with the alpha blending enabled.
//LM2 is just the same as the airplane's lm2 mesh, it's the signal light.
//The purpose of this effect is to simulate the blinking effect.

#include "Includes.inc"

//current time used for calculate the intensity of signal light (blinking effect)
float time;

///////////////////////////////////////////////////////////////////////////////////////////

struct PixelShaderInput
{
	float2 TextureCoords: TEXCOORD0;
};

float4 PixelShader(BasicPSInput input): COLOR0
{
	float4 color = tex2D(diffuseSampler, input.TexCoords);
	
	//calculate the light intensity used specially for the signal light (blinking effect)
	float intensity = sin(time);
	
	if(intensity < 0)
		intensity = 0;
	
	color *= intensity;
	
	return color;
}

technique AirplaneLM2Render
{
	pass p0
	{
		VertexShader = compile vs_1_1 BasicVS();
		PixelShader = compile ps_2_0 PixelShader();
	}
}
