//-----------------------------------------------------------------------------
// BasicMaterial.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

#include "Includes.inc"

technique Basic
{
	pass P0
	{
		AlphaBlendEnable = false;
		DestBlend = One;
		SrcBlend = One;
		BlendOp = ADD;
		
		VertexShader = compile vs_2_0 BasicVS();
		PixelShader = compile ps_3_0 BasicPS();
	}
}