//-----------------------------------------------------------------------------
// AirportAlphaLM1ApproachRender.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//AirportAlphaLM1ApproachRender.fx
//This effect file is used specifically for airport's light meshes 1 (LM1) with the special that it can move (dynamic, or tiling of the texture).
//They will be drawed with the alpha blending enabled.
//LM1 is just the same as the airplane's lm1 mesh, it's the static light.
//The purpose of this is just to draw them out normally, no special effect like blinking or else.

//Note: This effect is fixed with the amount to move, specially designed for approach light of the runway, each step is 0.08333.
//The algorithm use here is the step-approach, the external must send in the step to be multiplied with the move, thus doing this way, we will
// not be affected by the granulity of the time being sent in.

#include "Includes.inc"

//step for approach light (the diffuse map is wraped texture, so we can use step ranges from 1 to n, which n is the number of our light in the column.)
int step = 1;
//move factor
float moveFactor;
//movement or tiling of the texture (along the y-axis as always)
float2 move = float2(0,1);

///////////////////////////////////////////////////////////////////////////////////////////

struct PixelShaderInput
{
	float2 TextureCoords: TEXCOORD0;
};

float4 PixelShader(BasicPSInput input): COLOR0
{
	//tiling along the y-axis for simulating the approach light (the approach light is the model example for this effect file.)
	float4 color = tex2D(diffuseSampler, input.TexCoords + moveFactor * move * step);
	
	return color;
}

technique AirportLM1DynamicRender
{
	pass p0
	{
		VertexShader = compile vs_1_1 BasicVS();
		PixelShader = compile ps_2_0 PixelShader();
	}
}
