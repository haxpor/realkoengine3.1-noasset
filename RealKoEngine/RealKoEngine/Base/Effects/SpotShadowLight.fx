//-----------------------------------------------------------------------------
// SpotShadowLight.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to:	Catalin Zima		
//-----------------------------------------------------------------------------

//Note that the following attributes will not be messed up within Includes.inc file.
// This scheme will separate the spot light to different file.
// Also note that the light source from spot light doesn't have any geometry, so the vertex transformation will not be performed, just route it to the pixel shader.

// SpotShadowLight.fx effect file also contains the generating of shadow map function. And the drawing of the scene with the shadow map
//  involved.
// Note: Do not be confused the depthMap and shadowMap. (We use deferred shading here.)

//will use only partial of it
#include "Includes.inc"

//screen adjust
shared float2 adjust = 0.0f;

//properties of spotlight (the cone direction will be calculated from the vector of lightPosition and lightLookAtPos)
float3 lightPosition;
float3 lightConeLookAtPos;
float lightRange;
float3 lightColor;
float lightIntensity;
float lightConeAngle;
float lightConeDecay;

//depth bias when using the shadowing algorithm with different resolution
float depthBias;

//light's matrix
float4x4 lightView;
float4x4 lightProjection;

//textures set
//diffuse color, and specular intensity in the alpha channel
texture colorMap;
//normals, and specular power in the alpha channel
texture normalMap;
//depth
texture depthMap;
//shadow map
texture shadowMap;

//shape light map (these properties should be set externally everytime for each light)
bool isShapeLightMapEnabled;
texture shapeLightMap;

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = <normalMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = <depthMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler shadowMapSampler = sampler_state
{
	Texture = <shadowMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Point;
    MinFilter = Point;
    Mipfilter = None;
};

sampler shapeLightMapSampler = sampler_state
{
	Texture = <shapeLightMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Linear;
    MinFilter = Linear;
    Mipfilter = None;
};

// ####################
// Shadow Scene Draw
// ####################
struct SpotShadowLightVertexOutput
{
	float4 Position: POSITION0;
	float2 TexCoords: TEXCOORD0;
};

SpotShadowLightVertexOutput SpotShadowLightVS(float3 position: POSITION0, float2 texCoords: TEXCOORD0)
{
	SpotShadowLightVertexOutput output;
	
	output.Position = float4(position, 1.0f);
	output.TexCoords = texCoords;
	
	return output;
}

float4 SpotShadowLightPS(SpotShadowLightVertexOutput input): COLOR0
{
	//get the normal data from normal map
	float4 normalData = tex2D(normalMapSampler, input.TexCoords);
	//transfrom back to [-1,1]
	float3 normal = 2.0f * normalData.rgb - 1.0f;
	//get the specular power, and convert it into [0,255]
	float specularPower_local = normalData.a * 255;
	
	//get specular intensity
	float specularIntensity_local = tex2D(colorMapSampler, input.TexCoords).a;
	
	//read depth
	float depthVal = tex2D(depthMapSampler, input.TexCoords).r;
	//create the screen position
	float4 position;
		//get x position by convert it from [0,1] to [-1,1]
	position.x = input.TexCoords.x * 2.0f - 1.0f;
		//same for y but with different sign
	position.y = -(input.TexCoords.y * 2.0f - 1.0f);
	position.z = depthVal;
	position.w = 1.0f;
	
	//trasform it into the world space
	position = mul(position, invertedViewProjection);
	position /= position.w;
		//now the position is in the world space
		
	float4 lpos = mul(mul(position, lightView), lightProjection);
	lpos /= lpos.w;
	//float2 sTexC = float2((1+lpos.x)/2,1-(1+lpos.y)/2);
	float2 sTexC = float2(lpos.x/2.0f+ .5f, -lpos.y/2.0f+0.5f);
	
	//get the depth value from shadow map
	float SDepth = tex2D(shadowMapSampler, sTexC).r;
	//we will perform checking again at the if-statement next

	//shadow flag
	bool isInShadow = SDepth <= lpos.z - depthBias;
	
	//determine cone criteria
	float3 lightDirection = normalize(position - lightPosition);
	float coneDot = dot(lightDirection, normalize(lightConeLookAtPos - lightPosition));
	bool isInLightCone = coneDot >= lightConeAngle;
	
	if(!isInShadow && isInLightCone &&
	 sTexC.x >= 0 && sTexC.x <= 1 && sTexC.y >= 0 && sTexC.y <= 1)
	{
		//calculate the attenuation of the spotlight
		float spotIntensity = pow(coneDot, lightConeDecay);
		//calculate the spotlight attennuation
		float spotAtt = saturate(1.0f - length(lightPosition - position)/lightRange); 
		
		//calculate the lambertian light 
		float NdL = max(0, dot(normal, -lightDirection));
		float3 diffuseLight = NdL * lightColor.rgb;
		
		//calculate the reflection vector
		float3 reflectVector = normalize(reflect(lightDirection, normal));
		//calculate the direction to camera
		float3 directionToCamera = normalize(cameraPosition - position);
		//calculate the specular light
		float specularLight = specularIntensity * specularIntensity_local * pow( saturate( dot(reflectVector, directionToCamera) ), specularPower_local * specularPower);
		
		//our result color
		float4 color = spotAtt * spotIntensity * lightIntensity * float4(diffuseLight.rgb, specularLight);
		
		//shape with the shape light map (if necessary)
		if(isShapeLightMapEnabled)
			color *= tex2D(shapeLightMapSampler, sTexC).r;
		
		//output the result color
		return color;
	}
	else
	{
		//in the shadow
		return float4(0,0,0,0);
	}
}

technique SpotshadowLight
{
	pass p0
	{
		VertexShader = compile vs_2_0 SpotShadowLightVS();
		PixelShader = compile ps_3_0 SpotShadowLightPS();
	}
}