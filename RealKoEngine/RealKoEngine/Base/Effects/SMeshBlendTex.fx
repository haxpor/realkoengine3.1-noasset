//-----------------------------------------------------------------------------
// SMeshBlendTex.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to:	Catalin Zima		
//-----------------------------------------------------------------------------

//SMeshBlendTex effect file
//It is used to blend the color from the base, and target texture of skydome in particurlar time slot.
//Note: By separting skydome's effect and clouddome's effect from each other, so in the future we can provide and modify the detail without conflicting
// each other.

#include "Includes.inc"

// linear smoothing height
const float SKYDOME_LS_HEIGHT = 25 * 10;

///////////////////////////////////////////////////////////////////////////////////////////
texture2D baseTexture;
texture2D targetTexture;
texture2D textureD;
texture2D flashyTexture;

//whether or not to flash the sky texture.
bool isFlash;

//delta time (used for lerping between the baseTexture, and targetTexture)
//dTime is in range [0,1]
float dTime;

//delta time (used for lerping between the current blended textures, and texture D)
float textureD_dTime;


sampler baseSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <baseTexture>;
};

sampler targetSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <targetTexture>;
};

sampler textureDSampler = sampler_state
{
	MipFilter = None;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <textureD>;
};

sampler flashySampler = sampler_state
{
	MipFilter = None;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
	texture = <flashyTexture>;
};

///////////////////////////////////////////////////////////////////////////////////////////

struct RenderGBufferVertexOutput
{
	float4 Position: POSITION;
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 OriginalTexCoords: TEXCOORD3;
	float2 Depth: TEXCOORD4;
	float3x3 TangentToWorld: TEXCOORD5;
};

struct RenderGBufferPixelInput
{
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 OriginalTexCoords: TEXCOORD3;
	float2 Depth: TEXCOORD4;
	float3x3 TangentToWorld: TEXCOORD5;
};

struct RenderGBufferPixelOutput
{
	float4 Color: COLOR0;
	float4 Depth: COLOR1;
	float4 LightOcclusion: COLOR2;
	float4 Normal: COLOR3;
};

RenderGBufferVertexOutput RenderGBufferVS(float3 position: POSITION, float3 normal: NORMAL, float3 binormal: BINORMAL0, float3 tangent: TANGENT0
	, float2 texCoords: TEXCOORD)
{
	RenderGBufferVertexOutput output;
	
	//generate the wvp-matrix
	float4x4 wvp = mul( mul( world, view), projection);
	
	//get the position on screen
	output.Position = mul( float4(position, 1.0f), wvp);
	//get the depth information
	output.Depth = float2(output.Position.z, output.Position.w);
	
	//transform the position into world space
	float4 worldPosition = mul( float4( position, 1.0f), world);
	output.WorldPosition = worldPosition / worldPosition.w;
	
	// transform normal into world space (use this in pixel shader in case of the model doesn't have the separate normal map file)
	output.WorldNormal = mul(normal, world);
	
	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors
    output.TangentToWorld[0] = mul(tangent, world);
    output.TangentToWorld[1] = mul(binormal, world);
    output.TangentToWorld[2] = mul(normal, world);
    
    //keep our original texture coordinate
    output.OriginalTexCoords.x = texCoords.x;
    output.OriginalTexCoords.y = texCoords.y;
	
	//copy the texture's coordinate
	output.TexCoords.x = texCoords.x * texCoordU;
	output.TexCoords.y = texCoords.y * texCoordV;
	
	return output;
}

RenderGBufferPixelOutput RenderGBufferPS(RenderGBufferPixelInput input)
{
	RenderGBufferPixelOutput output;
	
	//Color
	//#1 Step one: Lerping base and target texture
	//get color from baseTexture
	float4 baseColor = tex2D(baseSampler, input.TexCoords);
	//get color from targetTexture
	float4 targetColor = tex2D(targetSampler, input.TexCoords);

	//lerp from baseTexture to targetTexture (the dTime must be set properly)
	float4 blendedColor = lerp(baseColor, targetColor, dTime);
	
	//#2 Step two: Lerping final of base and target textures, with texture D
	//Note: The following code section will try to minimize the power need in calcultion of GPU.
	//      Notice that we can furthur optimize the code, but doing so seem to be more cumbersome and more branching for GPU.
	if(textureD_dTime > 0.0){
		//get color from texture D
		float textureD_Color = tex2D(textureDSampler, input.TexCoords);
		
		blendedColor = lerp(blendedColor, textureD_Color, textureD_dTime);
		
		//safe to flash the sky texture here, as at this point we get the resulting blend
		//set it to pure white
		if(isFlash)
		{
			//blendedColor = float4(0.2,0.2,0.2,1);
			//read color from the flashy texture and modulate with our blendedColor
			//Note: Doing this we should have the black-tone sky texture.
			blendedColor += tex2D(flashySampler, input.OriginalTexCoords).r;
		}
	}
	
	// blend with scene
	if(input.WorldPosition.y <  SKYDOME_LS_HEIGHT)
	{
		float f = input.WorldPosition.y / SKYDOME_LS_HEIGHT;
		if(fogEnabled)
		{
			blendedColor = f * blendedColor + (1-f)*fogColor*ambientLight;
		}
		else
		{
			blendedColor = f * blendedColor + (1-f)*float4(1.0f, 1.0f, 1.0f, 1.0f)*ambientLight;
		}
	}
	
	output.Color = blendedColor;
	
	//set the specular intensity
	if(specularTextureEnabled)
	{
		output.Color.a = tex2D(specularSampler, input.TexCoords).r * specularIntensity;	
	}
	else
	{
		output.Color.a = specularIntensity;
	}

	//Normal
	//get the normal from texture if available
	if(normalTextureEnabled)
	{
		//get the normal data from the normal texture (of the model)
		float3 normal = tex2D(normalSampler, input.TexCoords).rgb;
		//transform into [-1,1]
		normal = 2.0f * normal - 1.0f;
		//transform it with TangentToWorld matrix
		normal = mul(normal, input.TangentToWorld);
		//normalize the result
		normal = normalize(normal);
		//convert back into [0,1]
		output.Normal.rgb = 0.5f * (normal + 1.0f);
	}
	else{
		//transform from [-1,1] to [0,1]
		output.Normal.rgb = 0.5f * (normalize(input.WorldNormal) + 1.0f);
	}
	//set the specular power
	output.Normal.a = specularPower;
	
	//Depth
	output.Depth = input.Depth.x / input.Depth.y;
	
	//set the light occlusion
	output.LightOcclusion = lightOcclusion;
	
	return output;
}

technique SkyBlendTex
{
	pass p0
	{
		VertexShader = compile vs_2_0 RenderGBufferVS();
		PixelShader = compile ps_2_0 RenderGBufferPS();
	}
}

