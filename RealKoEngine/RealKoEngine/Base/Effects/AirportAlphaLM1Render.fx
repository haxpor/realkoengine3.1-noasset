//-----------------------------------------------------------------------------
// AirportAlphaLM1Render.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//AirportAlphaLM1Render.fx
//This effect file is used specifically for airport's light meshes 1 (LM1). They will be drawed with the alpha blending enabled.
//LM1 is just the same as the airplane's lm1 mesh, it's the static light.
//The purpose of this is just to draw them out normally, no special effect like blinking or else.

#include "Includes.inc"

///////////////////////////////////////////////////////////////////////////////////////////

struct PixelShaderInput
{
	float2 TextureCoords: TEXCOORD0;
};

float4 PixelShader(BasicPSInput input): COLOR0
{
	float4 color = tex2D(diffuseSampler, input.TexCoords);
	
	// specifically for Donmueang Airport as its texture matches the need to lower the intensity of texture's color when x-coor is more than 0.5
	if (input.TexCoords.x > 0.5)
		color *= 0.2;
	
	return color;
}

technique AirportLM1Render
{
	pass p0
	{
		VertexShader = compile vs_1_1 BasicVS();
		PixelShader = compile ps_1_4 PixelShader();
	}
}
