// ------------------------------------------------------
// -- SMAA ----------------------------------------------
// ------------------------------------------------------
#define SMAA_PIXEL_SIZE float2(1.0/1024.0, 1.0/768.0)
#define SMAA_HLSL_3 1
#define SMAA_PRESET_HIGH 1
//float2 SMAA_PIXEL_SIZE;	// to be assigned

#include "AA/SMAA.h"

/**
 * Input vars and textures.
 */

texture2D colorTex2D;
texture2D depthTex2D;
texture2D edgesTex2D;
texture2D blendTex2D;
texture2D areaTex2D;
texture2D searchTex2D;

/**
 * DX9 samplers.
 */
sampler2D colorTex = sampler_state {
    Texture = <colorTex2D>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = true;
};

sampler2D colorTexG = sampler_state {
    Texture = <colorTex2D>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Linear; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = false;
};

sampler2D depthTex = sampler_state {
    Texture = <depthTex2D>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Linear; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = false;
};

sampler2D edgesTex = sampler_state {
    Texture = <edgesTex2D>;
    AddressU = Clamp; AddressV = Clamp;
    MipFilter = Linear; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = false;
};

sampler2D blendTex = sampler_state {
    Texture = <blendTex2D>;
    AddressU = Clamp; AddressV = Clamp;
    MipFilter = Linear; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = false;
};

sampler2D areaTex = sampler_state {
    Texture = <areaTex2D>;
    AddressU = Clamp; AddressV = Clamp; AddressW = Clamp;
    MipFilter = Linear; MinFilter = Linear; MagFilter = Linear;
    SRGBTexture = false;
};

sampler2D searchTex = sampler_state {
    Texture = <searchTex2D>;
    AddressU = Clamp; AddressV = Clamp; AddressW = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;
    SRGBTexture = false;
};

/**
 * Function wrappers
 */
SMAAEdgeDetection_VS_Output DX9_SMAAEdgeDetectionVS(in SMAAEdgeDetection_VS_Input v) {
    return SMAAEdgeDetectionVS(v);
}

SMAABlendingWeightCalculation_VS_Output DX9_SMAABlendingWeightCalculationVS(in SMAABlendingWeightCalculation_VS_Input v) {
    return SMAABlendingWeightCalculationVS(v);
}

SMAANeighborhoodBlending_VS_Output DX9_SMAANeighborhoodBlendingVS(in SMAANeighborhoodBlending_VS_Input v) {
    return SMAANeighborhoodBlendingVS(v);
}


float4 DX9_SMAALumaEdgeDetectionPS(in SMAAEdgeDetection_VS_Output v,
                                   uniform SMAATexture2D colorGammaTex) : COLOR0 {
    return SMAALumaEdgeDetectionPS(v, colorGammaTex);
}

float4 DX9_SMAAColorEdgeDetectionPS(in SMAAEdgeDetection_VS_Output v,
                                    uniform SMAATexture2D colorGammaTex) : COLOR0 {
    return SMAAColorEdgeDetectionPS(v, colorGammaTex);
}

float4 DX9_SMAADepthEdgeDetectionPS(in SMAAEdgeDetection_VS_Output v,
                                    uniform SMAATexture2D depthTex) : COLOR0 {
    return SMAADepthEdgeDetectionPS(v, depthTex);
}

float4 DX9_SMAABlendingWeightCalculationPS(in SMAABlendingWeightCalculation_VS_Output v,
										   uniform SMAATexture2D edgesTex, 
                                           uniform SMAATexture2D areaTex, 
                                           uniform SMAATexture2D searchTex
                                           ) : COLOR0 {
    return SMAABlendingWeightCalculationPS(v, edgesTex, areaTex, searchTex, 0);
}

float4 DX9_SMAANeighborhoodBlendingPS(in SMAANeighborhoodBlending_VS_Output v,
                                      uniform SMAATexture2D colorTex,
                                      uniform SMAATexture2D blendTex) : COLOR0 {
    return SMAANeighborhoodBlendingPS(v, colorTex, blendTex);
}


/**
 * Time for some techniques!
 */
technique LumaEdgeDetection {
    pass LumaEdgeDetection {
        VertexShader = compile vs_3_0 DX9_SMAAEdgeDetectionVS();
        PixelShader = compile ps_3_0 DX9_SMAALumaEdgeDetectionPS(colorTexG);
        ZEnable = false;        
        SRGBWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        // We will be creating the stencil buffer for later usage.
        StencilEnable = true;
        StencilPass = REPLACE;
        StencilRef = 1;
    }
}

technique ColorEdgeDetection {
    pass ColorEdgeDetection {
        VertexShader = compile vs_3_0 DX9_SMAAEdgeDetectionVS();
        PixelShader = compile ps_3_0 DX9_SMAAColorEdgeDetectionPS(colorTexG);
        ZEnable = false;        
        SRGBWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        // We will be creating the stencil buffer for later usage.
        StencilEnable = true;
        StencilPass = REPLACE;
        StencilRef = 1;
    }
}

technique DepthEdgeDetection {
    pass DepthEdgeDetection {
        VertexShader = compile vs_3_0 DX9_SMAAEdgeDetectionVS();
        PixelShader = compile ps_3_0 DX9_SMAADepthEdgeDetectionPS(depthTex);
        ZEnable = false;        
        SRGBWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        // We will be creating the stencil buffer for later usage.
        StencilEnable = true;
        StencilPass = REPLACE;
        StencilRef = 1;
    }
}

technique BlendWeightCalculation {
    pass BlendWeightCalculation {
        VertexShader = compile vs_3_0 DX9_SMAABlendingWeightCalculationVS();
        PixelShader = compile ps_3_0 DX9_SMAABlendingWeightCalculationPS(edgesTex, areaTex, searchTex);
        ZEnable = false;
        SRGBWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        // Here we want to process only marked pixels.
        StencilEnable = true;
        StencilPass = KEEP;
        StencilFunc = EQUAL;
        StencilRef = 1;
    }
}

technique NeighborhoodBlending {
    pass NeighborhoodBlending {
        VertexShader = compile vs_3_0 DX9_SMAANeighborhoodBlendingVS();
        PixelShader = compile ps_3_0 DX9_SMAANeighborhoodBlendingPS(colorTex, blendTex);
        ZEnable = false;
        SRGBWriteEnable = true;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        // Here we want to process all the pixels.
        StencilEnable = false;
    }
}