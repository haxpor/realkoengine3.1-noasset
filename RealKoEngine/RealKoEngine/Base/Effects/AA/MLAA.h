/**
 * Copyright (C) 2010 Jorge Jimenez (jorge@iryoku.com)
 * Copyright (C) 2010 Belen Masia (bmasia@unizar.es) 
 * Copyright (C) 2010 Jose I. Echevarria (joseignacioechevarria@gmail.com) 
 * Copyright (C) 2010 Fernando Navarro (fernandn@microsoft.com) 
 * Copyright (C) 2010 Diego Gutierrez (diegog@unizar.es)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the following statement:
 * 
 *       "Uses Jimenez's MLAA. Copyright (C) 2010 by Jorge Jimenez, Belen Masia,
 *        Jose I. Echevarria, Fernando Navarro and Diego Gutierrez."
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS 
 * IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are 
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the copyright holders.
 */


// Just for checking syntax at compile time


/**
 * Here we have an interesting define. In the last pass we make usage of 
 * bilinear filtering to avoid some lerps; however, bilinear filtering
 * in DX9, under DX9 hardware (but not in DX9 code running on DX10 hardware)
 * is done in gamma space, which gives sustantially worser results. So, this
 * flag allows to avoid the bilinear filter trick, changing it with some 
 * software lerps.
 *
 * So, to summarize, it is safe to use the bilinear filter trick when you are
 * using DX10 hardware on DX9. However, for the best results when using DX9
 * hardware, it is recommended comment this line.
 */

#define BILINEAR_FILTER_TRICK


/**
 * Input vars and textures.
 */
float MAX_SEARCH_STEPS = 16;    
float MAX_DISTANCE = 64;

float threshold;
float2 halfPixel;
texture2D colorTex;
texture2D depthTex;
texture2D edgesTex;
texture2D blendTex;
texture2D areaTex;

/**
 * DX9 samplers hell following this.
 */

sampler2D colorMap = sampler_state {
    Texture = <colorTex>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;    
};

sampler2D colorMapL = sampler_state {
    Texture = <colorTex>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Linear; MagFilter = Linear;    
};

sampler2D colorMapG = sampler_state {
    Texture = <colorTex>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;
    
};

sampler2D depthMap = sampler_state {
    Texture = <depthTex>;
    AddressU  = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;
    
};

sampler2D edgesMap = sampler_state {
    Texture = <edgesTex>;
    AddressU = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;
    
};

sampler2D edgesMapL = sampler_state {
    Texture = <edgesTex>;
    AddressU = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Linear; MagFilter = Linear;
    
};

sampler2D blendMap = sampler_state {
    Texture = <blendTex>;
    AddressU = Clamp; AddressV = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;
    
};

sampler2D areaMap = sampler_state {
    Texture = <areaTex>;
    AddressU = Clamp; AddressV = Clamp; AddressW = Clamp;
    MipFilter = Point; MinFilter = Point; MagFilter = Point;    
};


/**
 * Typical Multiply-Add operation to ease translation to assembly code.
 */

float4 mad(float4 m, float4 a, float4 b) {
    #if defined(XBOX)
    float4 result;
    asm {
        mad result, m, a, b
    };
    return result;
    #else
    return m * a + b;
    #endif
}


/**
 * This one just returns the first level of a mip map chain, which allow us to
 * avoid the nasty ddx/ddy warnings, even improving the performance a little 
 * bit.
 */

float4 tex2Dlevel0(sampler2D map, float2 texcoord) {
    return tex2Dlod(map, float4(texcoord, 0.0, 0.0));
}


/**
 * Same as above, this eases translation to assembly code;
 */

float4 tex2Doffset(sampler2D map, float2 texcoord, float2 offset) {
    #if defined(XBOX) && MAX_SEARCH_STEPS < 6
    float4 result;
    float x = offset.x;
    float y = offset.y;
    asm {
        tfetch2D result, texcoord, map, OffsetX = x, OffsetY = y
    };
    return result;
    #else
    return tex2Dlevel0(map, texcoord + PIXEL_SIZE * offset);
    #endif
}


/** 
 * Ok, we have the distance and both crossing edges, can you please return 
 * the float2 blending weights?
 */

float2 Area(float2 distance, float e1, float e2) {
     // * By dividing by areaSize - 1.0 below we are implicitely offsetting to
     //   always fall inside of a pixel
     // * Rounding prevents bilinear access precision problems
    float areaSize = MAX_DISTANCE * 5.0;
    float2 pixcoord = MAX_DISTANCE * round(4.0 * float2(e1, e2)) + distance;
    float2 texcoord = pixcoord / (areaSize - 1.0);
    return tex2Dlevel0(areaMap, texcoord).ra;
}


/**
 *  V E R T E X   S H A D E R S
 */
// - Pass Through Structure
struct PassThroughVS_Input
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float2 texcoord2 : TEXCOORD1;
};
struct PassThroughVS_Output
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float2 texcoord2 : TEXCOORD1;
	float4 vPosition : TEXCOORD2;
};

// Offset Structure
struct OffsetVS_Input
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
};
struct OffsetVS_Output
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float4 vPosition : TEXCOORD1;
	float4 offset[2] : TEXCOORD2;
};

PassThroughVS_Output PassThroughVS(in PassThroughVS_Input v) {
					
				   PassThroughVS_Output o = (PassThroughVS_Output)0;
				   
				   o.vPosition.x =  v.position.x - 2*halfPixel.x;
				   o.vPosition.y =  v.position.y + 2*halfPixel.y;
				   o.texcoord2 = v.texcoord2 ;//- halfPixel;
				   
				   // pass through the less
				   o.position = v.position;
				   o.texcoord = v.texcoord;
				   
				   return o;
}

OffsetVS_Output OffsetVS(in OffsetVS_Input v) {

	OffsetVS_Output o = (OffsetVS_Output)0;
	
	o.vPosition.x =  v.position.x - 2*halfPixel.x;
	o.vPosition.y =  v.position.y + 2*halfPixel.y;
	//texcoord = texcoord + halfPixel*2;
    o.offset[0] = v.texcoord.xyxy + PIXEL_SIZE.xyxy * float4(-1.0, 0.0, 0.0, -1.0);
    o.offset[1] = v.texcoord.xyxy + PIXEL_SIZE.xyxy * float4( 1.0, 0.0, 0.0,  1.0);
    
    // pass through the less
    o.position = v.position;
    o.texcoord = v.texcoord;
    
    return o;
}


/**
 *  1 S T   P A S S   ~   C O L O R   V E R S I O N
 */

float4 ColorEdgeDetectionPS(in OffsetVS_Output v) : COLOR0 {
    float3 weights = float3(0.2126,0.7152, 0.0722); // These ones are from the ITU-R Recommendation BT. 709

    /**
     * Luma calculation requires gamma-corrected colors:
     */
    float L = dot(tex2D(colorMapG, v.texcoord).rgb, weights);
    float Lleft = dot(tex2D(colorMapG, v.offset[0].xy).rgb, weights);
    float Ltop = dot(tex2D(colorMapG, v.offset[0].zw).rgb, weights);  
    float Lright = dot(tex2D(colorMapG, v.offset[1].xy).rgb, weights);
    float Lbottom = dot(tex2D(colorMapG, v.offset[1].zw).rgb, weights);

    float4 delta = abs(L.xxxx - float4(Lleft, Ltop, Lright, Lbottom));
    float4 edges = step(threshold.xxxx, delta);

    if (dot(edges, 1.0) == 0.0)
        discard;

    return edges;
}


/**
 *  1 S T   P A S S   ~   D E P T H   V E R S I O N
 */

float4 DepthEdgeDetectionPS(in OffsetVS_Output v) : COLOR0 {
    float D = tex2D(depthMap, v.texcoord).r;
    float Dleft = tex2D(depthMap, v.offset[0].xy).r;
    float Dtop  = tex2D(depthMap, v.offset[0].zw).r;
    float Dright = tex2D(depthMap, v.offset[1].xy).r;
    float Dbottom = tex2D(depthMap, v.offset[1].zw).r;

    float4 delta = abs(D.xxxx - float4(Dleft, Dtop, Dright, Dbottom));
    float4 edges = step(threshold.xxxx / 10.0, delta); // Dividing by 10 give us results similar to the color-based detection.

    if (dot(edges, 1.0) == 0.0)
        discard;

    return edges;
}


/**
 * Search functions for the 2nd pass.
 */

float SearchXLeft(float2 texcoord) {
	bool dummyRet = false;
    // We compare with 0.9 to prevent bilinear access precision problems.
    float i;
    float e = 0.0;
    for (i = -1.5; !dummyRet && i > -2.0 * MAX_SEARCH_STEPS; i -= 2.0) {
        e = tex2Doffset(edgesMapL, texcoord, float2(i, 0.0)).g;
        if (e < 0.9) dummyRet = true;
    }
    if(dummyRet)
		i += 2.0;
    return max(i + 1.5 - 2.0 * e, -2.0 * MAX_SEARCH_STEPS);
}

float SearchXRight(float2 texcoord) {
	bool dummyRet = false;
    float i;
    float e = 0.0;
    for (i = 1.5; !dummyRet && i < 2.0 * MAX_SEARCH_STEPS; i += 2.0) {
        e = tex2Doffset(edgesMapL, texcoord, float2(i, 0.0)).g;
        if (e < 0.9) dummyRet = true;
    }
    if(dummyRet)
		i -= 2.0;
    return min(i - 1.5 + 2.0 * e, 2.0 * MAX_SEARCH_STEPS);
}

float SearchYUp(float2 texcoord) {
	bool dummyRet = false;
    float i;
    float e = 0.0;
    for (i = -1.5; !dummyRet && i > -2.0 * MAX_SEARCH_STEPS; i -= 2.0) {
        e = tex2Doffset(edgesMapL, texcoord, float2(i, 0.0).yx).r;
        if (e < 0.9) dummyRet = true;
    }
    if(dummyRet)
		i += 2.0;
    return max(i + 1.5 - 2.0 * e, -2.0 * MAX_SEARCH_STEPS);
}

float SearchYDown(float2 texcoord) {
	bool dummyRet = false;
    float i;
    float e = 0.0;
    for (i = 1.5; i < 2.0 * MAX_SEARCH_STEPS; i += 2.0) {
        e = tex2Doffset(edgesMapL, texcoord, float2(i, 0.0).yx).r;
        if (e < 0.9) dummyRet = true;
    }
    if(dummyRet)
		i -= 2.0;
    return min(i - 1.5 + 2.0 * e, 2.0 * MAX_SEARCH_STEPS);
}


/**
 *  2 N D   P A S S
 */

float4 BlendWeightCalculationPS(in PassThroughVS_Output v) : COLOR0 {
    float4 areas = 0.0;

    float2 e = tex2D(edgesMap, v.texcoord2).rg;
    
    if (e.g) { // Edge at north

        // Search distances to the left and to the right:
        float2 d = float2(SearchXLeft(v.texcoord2), SearchXRight(v.texcoord2));

        // Now fetch the crossing edges. Instead of sampling between edgels, we
        // sample at -0.25, to be able to discern what value has each edgel:
        float4 coords = mad(float4(d.x, -0.25, d.y + 1.0, -0.25),
                            PIXEL_SIZE.xyxy, v.texcoord2.xyxy);
        float e1 = tex2Dlevel0(edgesMapL, coords.xy).r;
        float e2 = tex2Dlevel0(edgesMapL, coords.zw).r;

        // Ok, we know how this pattern looks like, now it is time for getting
        // the actual area:
        areas.rg = Area(abs(d), e1, e2);
    }
    
    if (e.r) { // Edge at west

        // Search distances to the top and to the bottom:
        float2 d = float2(SearchYUp(v.texcoord), SearchYDown(v.texcoord));

        // Now fetch the crossing edges (yet again):
        float4 coords = mad(float4(-0.005, d.x, -0.005, d.y + 1.0),
                            PIXEL_SIZE.xyxy, v.texcoord.xyxy);
        float e1 = tex2Dlevel0(edgesMapL, coords.xy).g;
        float e2 = tex2Dlevel0(edgesMapL, coords.zw).g;

        // Get the area for this direction:
        areas.ba = Area(abs(d), e1, e2);
    }

    return areas;
}


/**
 *  3 R D   P A S S
 */

float4 NeighborhoodBlendingPS(in OffsetVS_Output v) : COLOR0 {
    // Fetch the blending weights for current pixel:
    float4 topLeft = tex2D(blendMap, v.texcoord);
    float bottom = tex2D(blendMap, v.offset[1].zw).g;
    float right = tex2D(blendMap, v.offset[1].xy).a;
    float4 a = float4(topLeft.r, bottom, topLeft.b, right);

    // Up to 4 lines can be crossing a pixel (one in each edge). So, we perform
    // a weighted average, where the weight of each line is 'a' cubed, which
    // favors blending and works well in practice.
    float4 w = a * a * a;

	float4 C2 = tex2D(colorMap, v.texcoord );

    // There is some blending weight with a value greater than 0.0?
    float sum = dot(w, 1.0);
    if (sum < 1e-5)        
		discard;

    float4 color = 0.0;

    // Add the contributions of the possible 4 lines that can cross this pixel:            
		float4 C = tex2D(colorMap, v.texcoord );
        float4 Cleft = tex2D(colorMap, v.offset[0].xy);
        float4 Ctop = tex2D(colorMap, v.offset[0].zw);
        float4 Cright = tex2D(colorMap, v.offset[1].xy);
        float4 Cbottom = tex2D(colorMap, v.offset[1].zw);
        color = mad(lerp(C, Ctop, a.r), w.r, color);
        color = mad(lerp(C, Cbottom, a.g), w.g, color);
        color = mad(lerp(C, Cleft, a.b), w.b, color);
        color = mad(lerp(C, Cright, a.a), w.a, color);    

    // Normalize the resulting color and we are finished!
    return float4(color.x/sum,color.y/sum,color.z/sum,1); 
	//return float4(1,1,1,1); 
}


/**
 * Time for some techniques!
 */

technique ColorEdgeDetection {
    pass ColorEdgeDetection {
        VertexShader = compile vs_3_0 OffsetVS();
        PixelShader = compile ps_3_0 ColorEdgeDetectionPS();
        ZEnable = false;                
        AlphaBlendEnable = false;

    }
}

technique DepthEdgeDetection {
    pass DepthEdgeDetection {
        VertexShader = compile vs_3_0 OffsetVS();
        PixelShader = compile ps_3_0 DepthEdgeDetectionPS();
        ZEnable = false;                
        AlphaBlendEnable = false;

    }
}

technique BlendWeightCalculation {
    pass BlendWeightCalculation {
        VertexShader = compile vs_3_0 PassThroughVS();
        PixelShader = compile ps_3_0 BlendWeightCalculationPS();
        ZEnable = false;        
        AlphaBlendEnable = false;
        
    }
}

technique NeighborhoodBlending {
    pass NeighborhoodBlending {
        VertexShader = compile vs_3_0 OffsetVS();
        PixelShader = compile ps_3_0 NeighborhoodBlendingPS();
        ZEnable = false;        
        AlphaBlendEnable = false;

    }
}