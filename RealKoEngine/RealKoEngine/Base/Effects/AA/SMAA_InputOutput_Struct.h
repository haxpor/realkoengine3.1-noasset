// ---------------------------------------------------
// -- Pool of input / output structure ---------------
// ---------------------------------------------------
// : 1. SMAA Edge Detection
struct SMAAEdgeDetection_VS_Input
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
};
struct SMAAEdgeDetection_VS_Output
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float4 svPosition : TEXCOORD1;
	float4 offset[3]: TEXCOORD2;
};

// : SMAA Blending Weight Calculation
struct SMAABlendingWeightCalculation_VS_Input
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
};
struct SMAABlendingWeightCalculation_VS_Output
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float2 pixcoord : TEXCOORD1;
	float4 svPosition : TEXCOORD2;
	float4 offset[3] : TEXCOORD3;
};

// : SMAA Neighborhood Blending
struct SMAANeighborhoodBlending_VS_Input
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
};
struct SMAANeighborhoodBlending_VS_Output
{
	float4 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float4 svPosition : TEXCOORD1;
	float4 offset[2] : TEXCOORD2;
};



// ----------------------------------------------------------
// -- End of Pool of input / output structure ---------------
// ----------------------------------------------------------