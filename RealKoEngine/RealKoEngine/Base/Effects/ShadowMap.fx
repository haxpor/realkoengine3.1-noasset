//-----------------------------------------------------------------------------
// ShadowMap.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Thanks to:	Riemers		
//-----------------------------------------------------------------------------

// ShadowMap.fx effect file is used for drawing the opaque mesh object in order to generate the shadow map.

float4x4 world;
float4x4 lightView;
float4x4 lightProjection;

// -- Shadow Map -- //
struct ShadowMapVertexToPixel
{
	float4 Position	: POSITION;
	float2 Depth	: TEXCOORD0;
};

struct ShadowMapPixelToFrame
{
	float4 Color		: COLOR0;
};

ShadowMapVertexToPixel ShadowMapVertexShader(float4 inPos:POSITION)
{
	ShadowMapVertexToPixel Output;
	
	Output.Position = mul(inPos, mul( mul(world, lightView), lightProjection));

	Output.Depth.x = Output.Position.z;
	Output.Depth.y = Output.Position.w;
	
	return Output;
}

ShadowMapPixelToFrame ShadowMapPixelShader(ShadowMapVertexToPixel inPS)
{
	ShadowMapPixelToFrame Output;
	
	//based on how far from the light source with the direction it is facing too.
	Output.Color = inPS.Depth.x / inPS.Depth.y;
	
	return Output;
}

technique ShadowMap
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 ShadowMapVertexShader();
		PixelShader = compile ps_2_0 ShadowMapPixelShader();
	}
}