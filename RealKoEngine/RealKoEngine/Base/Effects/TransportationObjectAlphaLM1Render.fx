//-----------------------------------------------------------------------------
// TransportationObjectAlphaLM1Render.fx
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------

//TransportationObjectAlphaLM1Render.fx
//This effect file is used specifically for transportation object's light meshes 1 (LM1). They will be drawed with the alpha blending enabled.
//LM1 consists of head light, and head ground light.
//The purpose of this is just to draw them out normally, no special effect like blinking or else.

#include "Includes.inc"

///////////////////////////////////////////////////////////////////////////////////////////

struct PixelShaderInput
{
	float2 TextureCoords: TEXCOORD0;
};

float4 PixelShader(BasicPSInput input): COLOR0
{
	float4 color = tex2D(diffuseSampler, input.TexCoords);
	
	return color;
}

technique AirplaneLM1Render
{
	pass p0
	{
		VertexShader = compile vs_1_1 BasicVS();
		PixelShader = compile ps_1_1 PixelShader();
	}
}
