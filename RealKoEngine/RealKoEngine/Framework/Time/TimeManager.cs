﻿using System;
using Microsoft.Xna.Framework;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Time
{
    /// <summary>
    /// Time manager will manage and handle the virtual time calculation to be used in the simulation.
    /// </summary>
    public class TimeManager
    {
        /// <summary>
        /// Current virtual time used in the simualtion.
        /// </summary>
        protected TimeSpan currentVirtualTime;

        /// <summary>
        /// Virtual elapsedTime.
        /// </summary>
        protected TimeSpan virtualElapsedTime;

        /// <summary>
        /// Total time of simulation.
        /// </summary>
        /// <remarks>It adds the elapsed time each cycle. So it stores the total time of simulation.</remarks>
        protected TimeSpan simulationTotalTime;

        /// <summary>
        /// Whether or not this time manager will be updated online or offline.
        /// </summary>
        protected bool isUpdateOnline;

        #region Properties

        /// <summary>
        /// Current virtual time used in this simulation
        /// </summary>
        public TimeSpan CurrentVirtualTime
        {
            get { return currentVirtualTime; }
        }

        /// <summary>
        /// Get the virtual elapsed time calculated from the internal x-rate formulae.
        /// </summary>
        /// <remarks>User has the option whether or not to use this virtual elapsed time. This property is provided in order to correctly
        /// synchronized the time of simulation.</remarks>
        public TimeSpan VirtualElapsedTime
        {
            get { return virtualElapsedTime; }
        }

        /// <summary>
        /// Get the simulation total time accumulated within this TimeManager.
        /// </summary>
        /// <remarks>If the engine is stopped to update then simulationTotalTime will not be increased.</remarks>
        public TimeSpan SimulationTotalTime
        {
            get { return simulationTotalTime; }
        }

        /// <summary>
        /// Get or set the flag indicates whether this time manager will update online.
        /// </summary>
        public bool IsUpdateOnline
        {
            get { return isUpdateOnline; }
            set { isUpdateOnline = value; }
        }

        #endregion

        /// <summary>
        /// Create the time manager.
        /// </summary>
        public TimeManager(bool isUpdateOnline)
        {
            //set the update-online flag
            this.isUpdateOnline = isUpdateOnline;

            //create the instance of current virtual time
            currentVirtualTime = new TimeSpan(Engine.SimulationSettings.StartTime.Hours,
                Engine.SimulationSettings.StartTime.Minutes, Engine.SimulationSettings.StartTime.Seconds);
            //initialize the simulation total time
            //this is the start of time (so they are all zero)
            simulationTotalTime = new TimeSpan(0, 0, 0, 0, 0);
        }

        /// <summary>
        /// Synchronized the current virtual time with the external gameTime being sent from main application.
        /// </summary>
        /// <remarks>This method should be called only when this time manager's isUpdateOnline flag is not set. Other wrapper method will check the
        /// flag for us.</remarks>
        /// <param name="gameTime"></param>
        private void SyncTime(GameTime gameTime)
        {
            //create the addtion of virtual time
            //based on the current x-rate set in the engine (Refer to the document, or comment detail in the file "SimulationSettings.cs",
            // in the RealKoContentShared project.

            //the following equation is implemented follow the detail described in the document, and comment.
            if(Engine.SimulationSettings.AdvTimeRate.CurrentXRate > 1)
            {
                //calculate the total seconds need to be advanced
                int totalAdvSeconds = (Engine.SimulationSettings.AdvTimeRate.CurrentXRate - 2) * Engine.SimulationSettings.AdvTimeRate.DiffAdvancedSeconds
                    + Engine.SimulationSettings.AdvTimeRate.StartAdvancedSeconds;

                //create the timespan to be added to currentVirtualTime
                virtualElapsedTime = new TimeSpan(0, 0, totalAdvSeconds);

                //add to the current virtual time
                currentVirtualTime = currentVirtualTime.Add(virtualElapsedTime);
            }
            else
            {
                //reset the virual elapsed time to be equaled to gameTime's elapsedGameTime
                virtualElapsedTime = gameTime.ElapsedGameTime;

                //normally use the elasped time from the gameTime object
                currentVirtualTime = currentVirtualTime.Add(gameTime.ElapsedGameTime);
            }
        }

        /// <summary>
        /// Force to set the current virtual time.
        /// </summary>
        /// <param name="hours"></param>
        /// <param name="minutes"></param>
        /// <param name="seconds"></param>
        /// <remarks>This method allows for user to set the current time manually, after setting the time, the simulation will continue with the
        /// new time settings.
        /// This method is suit for the offline updating.</remarks>
        public void SetTime(int hours, int minutes, int seconds)
        {
            //this section of code is transparent to the user
            //set to the simulation settings of the engine.
            Engine.SimulationSettings.StartTime.Hours = hours;
            Engine.SimulationSettings.StartTime.Minutes = minutes;
            Engine.SimulationSettings.StartTime.Seconds = seconds;

            //create the new current virtual time
            currentVirtualTime = new TimeSpan(hours, minutes, seconds);
        }

        /// <summary>
        /// Force to set the current virtual time, and virtual elapsed time.
        /// </summary>
        /// <param name="hours"></param>
        /// <param name="minute"></param>
        /// <param name="seconds"></param>
        /// <param name="elaspedSeconds"></param>
        /// <param name="elaspedMilliseconds"></param>
        /// <remarks>This method is suit for the online updating, as the elapsedMilliseconds must be also taken into account.</remarks>
        public void SetTime(int hours, int minutes, int seconds, int elapsedSeconds, int elapsedMilliseconds)
        {
            //this section of code is transparent to the user
            //set to the simulation settings of the engine.
            Engine.SimulationSettings.StartTime.Hours = hours;
            Engine.SimulationSettings.StartTime.Minutes = minutes;
            Engine.SimulationSettings.StartTime.Seconds = seconds;

            //set the virtual elapsed time
            //doing this way, we can simulate the advancing of the time for entire simulation. (Other components may need it, so it's no hurt to
            // set this thing out.)
            virtualElapsedTime = new TimeSpan(0, 0, 0, elapsedSeconds, elapsedMilliseconds);

            //create the new current virtual time
            currentVirtualTime = new TimeSpan(hours, minutes, seconds);
        }

        /// <summary>
        /// Update this time manager.
        /// </summary>
        /// <remarks>This update() method is special so it will not implement RK_IUpdateable interface.</remarks>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime, bool isPaused)
        {
            if (!isPaused)
            {
                //increase the simulation total time (simulation total time will be separated from the gameTime from main class)
                simulationTotalTime = simulationTotalTime.Add(gameTime.ElapsedGameTime);

                //only call the SyncTime() method when it's offline update.
                if (!isUpdateOnline)
                    //sync time
                    SyncTime(gameTime);
            }
            else
                //reset the virtualElapsedTime
                virtualElapsedTime = TimeSpan.Zero;
        }
    }
}
