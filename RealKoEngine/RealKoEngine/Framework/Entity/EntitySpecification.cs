﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// Represent the specification of the entity itself.
    /// Such information as max and min speed of the entity.
    /// </summary>
    public static class EntitySpecification
    {
        /// <summary>
        /// Specification for airplane.
        /// </summary>
        public static class Airplane
        {
            public const float AIRPLANE_TURBINE_SPEED_LIMIT_1 = 0.01f;
            public const float AIRPLANE_TURBINE_SPEED_LIMIT_2 = 0.03f;
        }
    }
}
