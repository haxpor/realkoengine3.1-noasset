﻿#region File Description
//-----------------------------------------------------------------------------
// EntityIDSpecification.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// Define all the entity's related id.
    /// </summary>
    /// <remarks>All the entities will follow this specification.</remarks>
    public static class EntityIDSpecification
    {
        /// <summary>
        /// Defines all of individual entities that are standard to be existed in the game world.
        /// </summary>
        /// <remarks>For other entities, user need to implement manually.</remarks>
        public static class Entity
        {
            /// <summary>
            /// Airplane meshes entities-id.
            /// </summary>
            public static class Airplane
            {
                //Airplane
                public const uint AIRPLANE_MIN_ENTITY_ID = 1;
                public const uint AIRPLANE_MAX_ENTITY_ID = 5000;
            }

            /// <summary>
            /// Airport meshes entities-id.
            /// </summary>
            public static class Airport
            {
                //SkyDome/Clouddome meshes (SCMesh)
                public const uint SKYDOME_MESH_ENTITY_ID = 7000;
                public const uint CLOUDDOME_MESH_ENTITY_ID = 7001;

                //Airport opaque-meshes (exclude Skydome/Clouddome mesh)
                public const uint AIRPORT_OPAQUE_MESH_MIN_ENTITY_ID = 7002;
                public const uint AIRPORT_OPAQUE_MESH_MAX_ENTITY_ID = 9500;

                //Airport alpha-meshes
                public const uint AIRPORT_ALPHA_MESH_MIN_ENTITY_ID = 9501;
                public const uint AIRPORT_ALPHA_MESH_MAX_ENTITY_ID = 10000;

                //Airport's light-meshes
                public const uint AIRPORT_LIGHT_MESH_MIN_ENTITY_ID = 10001;
                public const uint AIRPORT_LIGHT_MESH_MAX_ENTITY_ID = 12000;
            }
        }

        /// <summary>
        /// Defines all of entity's type that are standard to be existed in the game world.
        /// </summary>
        /// <remarks>For other types, user need to implement manually.</remarks>
        public static class Type
        {
            /// <summary>
            /// Airplane type entities-id.
            /// </summary>
            public static class Airplane
            {
                //Airplane
                public const uint AIRPLANE_TYPE_MIN_ID = 1;
                public const uint AIRPLANE_TYPE_MAX_ID = 5000;
            }

            /// <summary>
            /// Airport mesh-type entites-id.
            /// </summary>
            public static class Airport
            {
                //Airport's meshes
                public const uint AIRPORT_OPAQUE_MESH_TYPE_ID = 5200000;
                public const uint AIRPORT_ALPHA_MESH_TYPE_ID = 5200001;

                //Airport's light meshes
                public const uint AIRPORT_STATIC_LIGHT_MESH_TYPE_ID = 5200002;
                public const uint AIRPORT_SIGNAL_LIGHT_MESH_TYPE_ID = 5200003;
                public const uint AIRPORT_APPROACH_LIGHT_MESH_TYPE_ID = 5200004;

                // Airport's light source
                public const uint AIRPORT_POINT_LIGHT_SOURCE_TYPE_ID = 5200005;
            }
        }
    }
}
