﻿#region File Description
//-----------------------------------------------------------------------------
// Entity3D.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Entity
{
    public class Entity3D: Entity, RK_IEntity3D
    {
        protected Vector3 scale = Vector3.One;
        protected Vector3 rotation = Vector3.Zero;
        protected Vector3 position = Vector3.Zero;
        protected BoundingSphere originalBoundingSphere;
        protected BoundingSphere boundingSphere;
        protected Matrix world = Matrix.Identity;
        protected bool isCulled;
        protected Vector3 headDirection;
        protected Vector3 rightDirection;

        #region Properties implemented from interface

        public Vector3 Scale
        {
            get { return scale; }
            set
            {
                scale = value;
            }
        }

        public Vector3 Rotation
        {
            get { return rotation; }
            set
            {
                rotation = value;
            }
        }

        public Vector3 Position
        {
            get { return position; }
            set
            {
                position = value;
            }
        }

        /// <summary>
        /// Original Bounding sphere cover the 3d entity.
        /// User can modify the bounding sphere as need to suit the application.
        /// </summary>
        /// <remarks>This is designed to store the original bounding sphere used for reference and furthure calculation.</remarks>
        public BoundingSphere OriginalBoundingSphere
        {
            get { return originalBoundingSphere; }
            set
            {
                originalBoundingSphere = value;
            }
        }

        /// <summary>
        /// Bounding sphere used to store the result of transformed of original bounding sphere.
        /// Used this value for calculation and checking for collision.
        /// </summary>
        public BoundingSphere BoundingSphere
        {
            get { return boundingSphere; }
            set
            {
                boundingSphere = value;
            }
        }

        /// <summary>
        /// World matrix.
        /// </summary>
        /// <remarks>The calculation of the world matrix is already performed from the scale, rotation, and position information.
        /// User can use this field to manually calculate the world matrix, if so must be performed after Update() method.</remarks>
        public Matrix World
        {
            get { return world; }
            set { world = value; }
        }

        /// <summary>
        /// Whether or not this entity3d is culled by the visibility algorithm.
        /// </summary>
        /// <remarks>This property tends to change every frame.
        /// This property is not required to used by the engine, as some of the object may not need to be culled due to some inconvenient.</remarks>
        public bool IsCulled
        {
            get { return isCulled; }
            set { isCulled = value; }
        }

        /// <summary>
        /// Get or set the head direction of this entity.
        /// </summary>
        /// <remarks>It will be used for the reference direction for its part to be rotated around, and other purposes.
        /// Note that it will be set automatically by other properties.</remarks>
        public Vector3 HeadDirection
        {
            get { return headDirection; }
            set
            {
                headDirection = value;
            }
        }

        /// <summary>
        /// Get or set the right direction of this entity.
        /// </summary>
        /// <remarks>It will be used for the reference direction for its part to be rotated around, and other purposes.
        /// Note that it will be set automatically by other properties.</remarks>
        public Vector3 RightDirection
        {
            get { return rightDirection; }
            set
            {
                rightDirection = value;
            }
        }

        #endregion

        /// <summary>
        /// Create new 3d entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeId"></param>
        /// <param name="name"></param>
        public Entity3D(uint id, uint typeId, string name)
            : base(id, typeId, name)
        {
            //initially set the head and right direction
            //[as complied by modeler]
            //also note that not all entities need these two properties, so the update() method will be implemented by the extended class
            headDirection = Vector3.UnitZ;
            rightDirection = Vector3.UnitX;
        }

        //* The update and draw methods will be implemented by the extended class, because of the variety of 3d entity in the game world.
        // Each model will be handle differently when drawing. (Either mesh-by-mesh, or auto-whole-model)
    }
}
