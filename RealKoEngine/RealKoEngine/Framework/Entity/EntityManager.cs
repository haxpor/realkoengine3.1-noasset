﻿#region File Description
//-----------------------------------------------------------------------------
// EntityManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// *EntityManager only be used for information retrieval and AI purpose only, not use this class for updating, or drawing on screen as
    ///  it will be redundant with Screen class that already handle these tasks.
    ///  
    /// </summary>
    /// <remarks>The identification of entities is in unsigned-integer data type.
    /// 
    /// This class was modified to be not only the generic class, but can handle specific asset type such as airplane, and it follows the specification
    /// from the EntityIDSpecification class. User need to consult the document for the specification found in this class.</remarks>
    public class EntityManager
    {
        /// <summary>
        /// Dictionary contains all the entities in the game world.
        /// </summary>
        protected Dictionary<uint, Entity> entityMap;

        #region Aiport Meshes

        /// <summary>
        /// List of the airport's opaque-meshes entities.
        /// </summary>
        protected List<AirportOpaqueMeshEntity> airportOpaqueEntityList;
        /// <summary>
        /// List of the airport's alpha-meshes entities.
        /// </summary>
        protected List<AirportAlphaMeshEntity> airportAlphaEntityList;

        /// <summary>
        /// List of the airport's static light-meshes entities.
        /// </summary>
        protected List<AirportStaticLightMeshEntity> airportStaticLightEntityList;
        /// <summary>
        /// List of the airport's signal light-meshes entities.
        /// </summary>
        protected List<AirportSignalLightMeshEntity> airportSignalLightEntityList;
        /// <summary>
        /// List of the airport's dynamic light-meshes entities.
        /// </summary>
        protected List<AirportApproachLightMeshEntity> airportDynamicLightEntityList;


        #endregion

        #region Airplane Entities

        /// <summary>
        /// List of the airplane entities.
        /// </summary>
        /// <remarks>This references to the same location of airplane entity as the entityMap does.</remarks>
        protected List<TransportationObjectEntity> airplaneEntityList;

        #endregion

        /// <summary>
        /// Create the EntityManager.
        /// </summary>
        public EntityManager()
        {
            entityMap = new Dictionary<uint, Entity>();

            airportOpaqueEntityList = new List<AirportOpaqueMeshEntity>();
            airportAlphaEntityList = new List<AirportAlphaMeshEntity>();

            airportStaticLightEntityList = new List<AirportStaticLightMeshEntity>();
            airportSignalLightEntityList = new List<AirportSignalLightMeshEntity>();
            airportDynamicLightEntityList = new List<AirportApproachLightMeshEntity>();

            airplaneEntityList = new List<TransportationObjectEntity>();
        }

        /// <summary>
        /// Register new entity.
        /// </summary>
        /// <param name="newEntity">New entity to be registered</param>
        public void RegisterEntity(Entity newEntity)
        {
            entityMap.Add(newEntity.ID, newEntity);

            //we will check the type not individual id
            //for airport's opaque-meshes entity
            if (newEntity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_OPAQUE_MESH_TYPE_ID)
                airportOpaqueEntityList.Add((AirportOpaqueMeshEntity)newEntity);
            //for airport's alpha-meshes entity
            else if (newEntity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_ALPHA_MESH_TYPE_ID)
                airportAlphaEntityList.Add((AirportAlphaMeshEntity)newEntity);

            //for airport's static light-meshes entity
            else if (newEntity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_STATIC_LIGHT_MESH_TYPE_ID)
                airportStaticLightEntityList.Add((AirportStaticLightMeshEntity)newEntity);
            else if (newEntity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_SIGNAL_LIGHT_MESH_TYPE_ID)
                airportSignalLightEntityList.Add((AirportSignalLightMeshEntity)newEntity);
            else if (newEntity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_APPROACH_LIGHT_MESH_TYPE_ID)
                airportDynamicLightEntityList.Add((AirportApproachLightMeshEntity)newEntity);

            //check whether the entity is airplane type, thus we also need to add it into the airplane list
            //see the document for the specification
            //Note: We also cast the entity into the airplane type.
            else if (newEntity.TypeId >= EntityIDSpecification.Type.Airplane.AIRPLANE_TYPE_MIN_ID &&
                newEntity.TypeId <= EntityIDSpecification.Type.Airplane.AIRPLANE_TYPE_MAX_ID)
                airplaneEntityList.Add((TransportationObjectEntity)newEntity);
        }

        /// <summary>
        /// Remove the specified entity.
        /// </summary>
        /// <param name="entity">Entity to be removed.</param>
        public void RemoveEntity(Entity entity)
        {
            entityMap.Remove(entity.ID);

            //we will check the type not individual id
            //for airport's opaque-meshes entity
            if (entity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_OPAQUE_MESH_TYPE_ID)
                airportOpaqueEntityList.Remove((AirportOpaqueMeshEntity)entity);
            //for aiport's alpha-meshes entity
            else if (entity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_ALPHA_MESH_TYPE_ID)
                airportAlphaEntityList.Remove((AirportAlphaMeshEntity)entity);

            //for airport's static light-meshes entity
            else if (entity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_STATIC_LIGHT_MESH_TYPE_ID)
                airportStaticLightEntityList.Remove((AirportStaticLightMeshEntity)entity);
            else if (entity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_SIGNAL_LIGHT_MESH_TYPE_ID)
                airportSignalLightEntityList.Remove((AirportSignalLightMeshEntity)entity);
            else if (entity.TypeId == EntityIDSpecification.Type.Airport.AIRPORT_APPROACH_LIGHT_MESH_TYPE_ID)
                airportDynamicLightEntityList.Remove((AirportApproachLightMeshEntity)entity);

            //check whether the entity is airplane type, thus we also need to delete it from the airplane list
            //see the document for the specification
            else if (entity.TypeId >= EntityIDSpecification.Type.Airplane.AIRPLANE_TYPE_MIN_ID &&
                entity.TypeId <= EntityIDSpecification.Type.Airplane.AIRPLANE_TYPE_MAX_ID)
                airplaneEntityList.Remove((TransportationObjectEntity)entity);
        }

        /// <summary>
        /// Clear all the entities.
        /// </summary>
        public void Clear()
        {
            entityMap.Clear();
            airportOpaqueEntityList.Clear();
            airportAlphaEntityList.Clear();
            airportStaticLightEntityList.Clear();
            airportSignalLightEntityList.Clear();
            airportDynamicLightEntityList.Clear();
            airplaneEntityList.Clear();
        }

        /// <summary>
        /// Get the Entity from its id.
        /// </summary>
        /// <param name="id">ID of Entity.</param>
        /// <returns></returns>
        public Entity GetEntityFrom(uint id)
        {
            //thus if the entity is in airplane type, it will return the same as the airplaneList will return
            return entityMap[id];
        }

        /// <summary>
        /// Get the airport's opaque entity list.
        /// </summary>
        /// <remarks>Use the return value of this method for information-retrival only.</remarks>
        /// <returns></returns>
        public List<AirportOpaqueMeshEntity> GetAirportOpaqueEntityList()
        {
            return airportOpaqueEntityList;
        }

        /// <summary>
        /// Get the airport's alpha entity list.
        /// </summary>
        /// <remarks>Use the return value of this method for information-retrival only.</remarks>
        /// <returns></returns>
        public List<AirportAlphaMeshEntity> GetAirportAlphaEntityList()
        {
            return airportAlphaEntityList;
        }

        /// <summary>
        /// Get the airport's static light entity list.
        /// </summary>
        /// <remarks>Use the return value of this method for information-retrival only.</remarks>
        /// <returns></returns>
        public List<AirportStaticLightMeshEntity> GetAirportStaticLightEntityList()
        {
            return airportStaticLightEntityList;
        }

        /// <summary>
        /// Get the airport's signal light entity list.
        /// </summary>
        /// <remarks>Use the return value of this method for information-retrival only.</remarks>
        /// <returns></returns>
        public List<AirportSignalLightMeshEntity> GetAirportSignalLightEntityList()
        {
            return airportSignalLightEntityList;
        }

        /// <summary>
        /// Get the airport's dynamic light entity list.
        /// </summary>
        /// <returns></returns>
        public List<AirportApproachLightMeshEntity> GetAirportApproachLightEntityList()
        {
            return airportDynamicLightEntityList;
        }

        /// <summary>
        /// Get the airplane entity list.
        /// </summary>
        /// <returns></returns>
        public List<TransportationObjectEntity> GetAirplaneEntityList()
        {
            return airplaneEntityList;
        }
    }
}
