﻿#region File Description
//-----------------------------------------------------------------------------
// EntityTypeID.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

namespace RealKo.Framework.Entity
{
    public class EntityTypeID
    {
        /// <summary>
        /// Root type of entity.
        /// It can be Entity2D, Entity3D, or both. In the case of both all the entities in the world will be drawed.
        /// </summary>
        public enum EntityRootType
        {
            Entity2D,
            Entity3D,
            All
        }
    }
}
