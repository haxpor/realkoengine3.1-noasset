﻿#region File Description
//-----------------------------------------------------------------------------
// Entity2D.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Entity
{
    public class Entity2D: Entity, RK_IEntity2D
    {
        protected Vector2 position;
        protected Vector2 dimension;
        protected Vector2 origin = Vector2.Zero;
        protected Vector2 scale = Vector2.One;
        protected string textureFilePath = null;
        protected Texture2D texture = null;
        protected Color tint = Color.White;
        protected SpriteBlendMode blendMode = SpriteBlendMode.None;

        #region Properties implemented from interface

        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
            }
        }

        public Vector2 Dimension
        {
            get { return dimension; }
            set
            {
                dimension = value;
            }
        }

        public Vector2 Origin
        {
            get { return origin; }
            set
            {
                origin = value;
            }
        }

        public Vector2 Scale
        {
            get { return scale; }
            set
            {
                scale = value;
            }
        }

        public string TextureFilePath
        {
            get { return textureFilePath; }
            set
            {
                textureFilePath = value;
                if (!string.IsNullOrEmpty(textureFilePath))
                {
                    //load the texture for this 2d entity
                    //if error occurs the exception will be throw and the app will terminate 
                    // (no need to catch the exception as the result behavior is the same.)
                    texture = Engine.Content.Load<Texture2D>(textureFilePath);
                }
            }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;
            }
        }

        public Color Tint
        {
            get { return tint; }
            set
            {
                tint = value;
            }
        }

        /// <summary>
        /// Control the current applied render state of this 2d entity.
        /// </summary>
        public SpriteBlendMode BlendMode
        {
            get { return blendMode; }
            set
            {
                blendMode = value;
            }
        }

        #endregion

        /// <summary>
        /// Create new 2d entity.
        /// </summary>
        /// <remarks>If user wants to modify some of entity's properties, he/she must do it manually after creation of entity.</remarks>
        /// <param name="id"></param>
        /// <param name="typeId"></param>
        /// <param name="name"></param>
        public Entity2D(uint id, uint typeId, string name)
            : base(id, typeId, name)
        {
        }

        /// <summary>
        /// Draw this Entity2D.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (Active && texture != null)
            {
                Engine.SpriteBatch.Begin(blendMode, SpriteSortMode.Immediate, SaveStateMode.None);
                Engine.SpriteBatch.Draw(texture, position, null, tint, 0.0f, origin, scale, SpriteEffects.None, 0);
                Engine.SpriteBatch.End();
            }
        }
    }
}
