﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IEntity2D.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Entity
{
    public interface RK_IEntity2D
    {
        //Properties signatures
        /// <summary>
        /// Position of quad-2d component.
        /// </summary>
        Vector2 Position
        {
            get;
            set;
        }

        /// <summary>
        /// Width and height of quad-2d component.
        /// </summary>
        Vector2 Dimension
        {
            get;
            set;
        }

        /// <summary>
        /// Scale of the quad-2d component.
        /// </summary>
        Vector2 Scale
        {
            get;
            set;
        }

        /// <summary>
        /// Origin of the quad-2d component.
        /// </summary>
        Vector2 Origin
        {
            get;
            set;
        }

        /// <summary>
        /// Texture file path of quad-2d component.
        /// </summary>
        string TextureFilePath
        {
            get;
            set;
        }

        /// <summary>
        /// Texture of quad-2d component.
        /// </summary>
        Texture2D Texture
        {
            get;
            set;
        }

        /// <summary>
        /// Tint color applied during the drawing process for this quad-2d component.
        /// </summary>
        Color Tint
        {
            get;
            set;
        }
    }
}
