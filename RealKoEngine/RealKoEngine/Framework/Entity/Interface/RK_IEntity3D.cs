﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IEntity3D.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Entity
{
    public interface RK_IEntity3D
    {
        //Properties signatures
        Vector3 Scale
        {
            get;
            set;
        }

        /// <summary>
        /// Amount of rotation angle (in radian) for each axis to be applied internally to matrix.
        /// </summary>
        /// <remarks>The sequence of to apply the rotation angle start from x-axis, y-axis, and z-axis.</remarks>
        Vector3 Rotation
        {
            get;
            set;
        }

        /// <summary>
        /// Position in 3d world-space.
        /// </summary>
        Vector3 Position
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set the original bounding sphere of this 3d entity.
        /// </summary>
        /// <remarks>Used to store the original value of bounding sphere of entity.</remarks>
        BoundingSphere OriginalBoundingSphere
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set the bounding sphere of this 3d entity.
        /// </summary>
        /// <remarks>Used to store the result of transformed of original bounding sphere.</remarks>
        BoundingSphere BoundingSphere
        {
            get;
            set;
        }

        /// <summary>
        /// World matrix transform.
        /// </summary>
        Matrix World
        {
            get;
        }
    }
}
