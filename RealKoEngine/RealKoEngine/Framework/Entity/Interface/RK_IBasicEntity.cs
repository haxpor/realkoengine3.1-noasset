﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IBasicEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace RealKo.Framework.Entity
{
    public interface RK_IBasicEntity
    {
        //Properties signatures
        /// <summary>
        /// ID represent the entity.
        /// </summary>
        /// <remarks>This is the readonly field.</remarks>
        uint ID
        {
            get;
        }

        /// <summary>
        /// Type Id represent the type of entity.
        /// </summary>
        /// <remarks>This is the readonly field.</remarks>
        uint TypeId
        {
            get;
        }

        /// <summary>
        /// Name of the entity.
        /// </summary>
        string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The activeness of this entity.
        /// Unactive entity will not be updated or drawed.
        /// </summary>
        bool Active
        {
            get;
            set;
        }
    }
}
