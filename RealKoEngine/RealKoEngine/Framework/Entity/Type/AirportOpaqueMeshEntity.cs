﻿#region File Description
//-----------------------------------------------------------------------------
// AirportOpaqueMeshEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    #region Specification of textureset code

    /// <summary>
    /// TextureSet code for changing the textureset.
    /// </summary>
    /// <remarks>We use the byte-type corresponding to the type used by our state code.
    /// Also note that byte-type is 8-bit unsigned integer.
    /// 
    /// Defined here if other need to use this information, they can allow to use it, but defined within this class to make others know that
    /// the TextureSetCode is specially designed to be used with the AirportOpaqueMeshEntity.</remarks>
    public enum TextureSetCode : byte
    {
        DayTextureSet,
        RainingTextureSet,
        SnowTextureSet
    }

    #endregion

    public class AirportOpaqueMeshEntity : Entity3D, RK_IShadowMapDrawable
    {
        #region Nested properties signatures

        /// <summary>
        /// Texture set for this AirportMeshEntity.
        /// </summary>
        /// <remarks>This class is designed to be consistent with the AirportMesh.TextureSet structure.
        /// This class will be updated follow that one.</remarks>
        public class AirportMeshTextureSet
        {
            public Texture2D Diffuse;
            public Texture2D Normal;
            public Texture2D Specular;
            public Texture2D Emissive;

            public AirportMeshTextureSet(AirportOpaqueMesh.TextureSet textureSet)
            {
                //load all the specified textures in texture set
                //also set the file path of each texture into texture's tag
                if(textureSet.Diffuse != "null")
                {
                    Diffuse = Engine.Content.Load<Texture2D>(textureSet.Diffuse);
                    Diffuse.Tag = (string)textureSet.Diffuse;
                }

                //optional
                if (textureSet.Normal != "null")
                {
                    Normal = Engine.Content.Load<Texture2D>(textureSet.Normal);
                    Normal.Tag = (string)textureSet.Normal;
                }

                //optional
                if (textureSet.Specular != "null")
                {
                    Specular = Engine.Content.Load<Texture2D>(textureSet.Specular);
                    Specular.Tag = (string)textureSet.Specular;
                }

                //optional
                if (textureSet.Emissive != "null")
                {
                    Emissive = Engine.Content.Load<Texture2D>(textureSet.Emissive);
                    Emissive.Tag = (string)textureSet.Emissive;
                }
            }
        }

        #endregion

        #region Associated variables for changing textureset

        /// <summary>
        /// Information of textureset code of base and target textureset.
        /// </summary>
        /// <remarks>Refer to the document for the list of code available.
        /// These two variables will be set externally, and will be taken into effect in the Update section of this entity.</remarks>
        protected TextureSetCode baseTextureSetCode;
        protected TextureSetCode targetTextureSetCode;

        #region Texturese code properties

        /// <summary>
        /// Get the base textureset code.
        /// </summary>
        public TextureSetCode BaseTextureSetCode
        {
            get { return baseTextureSetCode; }
        }

        /// <summary>
        /// Get or set the textureset code for target textureset code.
        /// </summary>
        /// <remarks>By changing this property, then it will affect the update section accordingly.
        /// If the set value for target textureset cannot be found with the corresponding texture, then no effect.
        /// This method also set the targetTexture automatically.</remarks>
        public TextureSetCode TargetTextureSetCode
        {
            get { return targetTextureSetCode; }
            set 
            {
                //if the value to be set to targetTextureSetCode is not the same as before then reset the dtTime, and dtCountingTime
                if (targetTextureSetCode != value)
                {
                    dtTime = 0;
                    dtCountingTime = 0;
                }

                //only set the targetTextureSetCode only if this AirportOpaqueMeshEntity has that corresponding textureset's diffuse texture.
                switch (value)
                {
                    case TextureSetCode.DayTextureSet:
                        //all mesh must at least have the diffuse texture
                        if (dayTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                    case TextureSetCode.RainingTextureSet:
                        if (rainingTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = rainingTextureSet.Diffuse;
                        }
                            //if not, then we set the texture back to the dayTextureSet
                        else if(dayTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                    case TextureSetCode.SnowTextureSet:
                        if (snowTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = snowTextureSet.Diffuse;
                        }
                            //if not, then we set the texture back to the dayTextureSet
                        else if (dayTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                }
            }
        }

        #endregion

        /// <summary>
        /// Texture of both base and target texture.
        /// </summary>
        /// <remarks>The texture here is diffuse texture as we don't want to blend another type of texture along the way.
        /// Only the diffuse texture is taken into account here.</remarks>
        protected Texture2D baseTexture;
        protected Texture2D targetTexture;

        /// <summary>
        /// Counting time used for calculating the dtTime.
        /// </summary>
        protected float dtCountingTime;

        /// <summary>
        /// Delta-Textureset time.
        /// </summary>
        /// <remarks>This variable will be used for calculating the blending between two textures.</remarks>
        protected float dtTime;

        #endregion

        protected string modelFilePath;
        protected int meshIndex;
        protected int boneIndex;

        protected AirportMeshTextureSet dayTextureSet;
        protected AirportMeshTextureSet rainingTextureSet;
        protected AirportMeshTextureSet snowTextureSet;

        protected Material material;
        protected Material lm1Material;
        protected Model model;

        /// <summary>
        /// Whether or not to enable the emissive map applied for this mesh.
        /// </summary>
        protected bool isLightTurnOn;

        #region Properties

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the mesh index represented this AirportMesh.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex; }
        }

        /// <summary>
        /// Get the bone index represented this AirportMesh.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }

        /// <summary>
        /// Get the day texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet DayTextureSet
        {
            get { return dayTextureSet; }
        }

        /// <summary>
        /// Get the raining texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet RainingTextureSet
        {
            get { return rainingTextureSet; }
        }

        /// <summary>
        /// Get the snow texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet SnowTextureSet
        {
            get { return snowTextureSet; }
        }

        /// <summary>
        /// Get the material used by this AirportMesh.
        /// </summary>
        public Material Material
        {
            get { return material; }
        }

        /// <summary>
        /// Get the model used by this AirportMesh.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        /// <summary>
        /// Get or set the whether or not to turn on the light for this mesh.
        /// </summary>
        /// <remarks>If the emissive map for this mesh is not available, then trying to set this property will not be effect.</remarks>
        public bool IsLightTurnOn
        {
            get { return isLightTurnOn; }
            set { isLightTurnOn = value; }
        }

        /// <summary>
        /// Get the delta-textureset time.
        /// </summary>
        /// <remarks>Use this property for the information-debugging purpose only.</remarks>
        public float DtTime
        {
            get { return dtTime; }
        }

        #endregion

        /// <summary>
        /// Create the airport mesh entity with the neccessary information from AirportMesh.
        /// </summary>
        /// <param name="amInfo"></param>
        public AirportOpaqueMeshEntity(AirportOpaqueMesh amInfo)
            : base(amInfo.ID, amInfo.TypeID, amInfo.Name)
        {
            //setup all properties
            modelFilePath = amInfo.ModelFilePath;
            meshIndex = amInfo.MeshIndex;
            boneIndex = amInfo.BoneIndex;

            //load all the associated texture sets
            dayTextureSet = new AirportMeshTextureSet(amInfo.DayTextureSetFilePath);
            rainingTextureSet = new AirportMeshTextureSet(amInfo.RainingTextureSetFilePath);
            snowTextureSet = new AirportMeshTextureSet(amInfo.SnowTextureSetFilePath);

            //load the model and set its bounding sphere
            model = Engine.Content.Load<Model>(amInfo.ModelFilePath);
            originalBoundingSphere = model.Meshes[meshIndex].BoundingSphere;

            //create the material, it's the drawer for this mesh
            //Note: The effect of the engine's render system is already load.
            // This effect is the same as the one used by scene class.
            material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/AirportOpaqueMeshRender"));
            material.CacheOriginalBoneTransforms(model);
            //material.SpecularIntensity = 0.05f;

            //set the default texture set to diffuse
            //diffuse map as base, other as optional
            material.DiffuseTexture = (string)dayTextureSet.Diffuse.Tag;
            //optional
            if(dayTextureSet.Specular != null)
                material.SpecularTexture = (string)dayTextureSet.Specular.Tag;
            if(dayTextureSet.Normal != null)
                material.NormalTexture = (string)dayTextureSet.Normal.Tag;
            if (dayTextureSet.Emissive != null)
                material.EmissiveTexture = (string)dayTextureSet.Emissive.Tag;

            //apply the global scale
            scale *= Engine.SimulationSettings.GlobalScale;

            //create the alpha-mesh material
            //LM1
            lm1Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/AirportAlphaLM1Render"));
            lm1Material.CacheOriginalBoneTransforms(model);
            //set the stuff of changing the textureset as initial stage
            //set the state code to day-textureset-code as default for both base and target textureSet code.
            baseTextureSetCode = TextureSetCode.DayTextureSet;
            targetTextureSetCode = TextureSetCode.DayTextureSet;

            //set both base and target textureset texture to day textureset.
            //Note: If we changed the code for base, and target textureset code, then we must manually change the texture for each of them too.
            baseTexture = dayTextureSet.Diffuse;
            targetTexture = dayTextureSet.Diffuse;

            //set the dtCountingTime, and dttime to zero
            //Note: This is the enhanced version as it is designed for step-up calculation.
            dtCountingTime = 0;
            dtTime = 0;
        }

        /// <summary>
        /// Update the changing of textureset its related stuff.
        /// </summary>
        private void UpdateTextureSet(GameTime gameTime)
        {
            //make any change if only if the baseTextureSetCode is not the same as targetTexureSetCode
            //the targetTextureSetCode will be set or modified externally.
            //Note: The following "if-statement" will limit the calculation load, by process only when the changing need to be performed.
            // If it's done then no need to do anything.
            if (baseTextureSetCode != targetTextureSetCode)
            {
                //**the setting of targetTextureSetCode, and targetTexture already be done externally before entering into this point.

                //increase the dtCountingTime (it's based on the engine's time.)
                dtCountingTime += (float)Engine.TimeManager.VirtualElapsedTime.TotalSeconds;

                //calculate the dtTime (correspond to the changing-to-textureD duration, see the document and config file for more information.)
                dtTime = dtCountingTime / Engine.SimulationSettings.AdvTimeRate.DDuration;

                //clamping it (in GPU we don't clamping it again, so it requires to do it here.)
                //this is the stepup process, so we must reponse to the end point properly
                if (dtTime > 1.0f)
                {
                    //we also set the other associated textures to the target textureset.
                    //See the algorithm inside, by doing this way, if the new targetTextureSet doesn't have some of the associated textures, then
                    // it will remainly in use the baseTextureSet's associated textures.
                    switch (targetTextureSetCode)
                    {
                        case TextureSetCode.DayTextureSet:
                            //specular
                            if (dayTextureSet.Specular != null)
                            {
                                material.SpecularTexture = (string)dayTextureSet.Specular.Tag;
                            }

                            if (dayTextureSet.Normal != null)
                            {
                                material.NormalTexture = (string)dayTextureSet.Normal.Tag;
                            }

                            if (dayTextureSet.Emissive != null)
                            {
                                material.EmissiveTexture = (string)dayTextureSet.Emissive.Tag;
                            }
                            break;
                        case TextureSetCode.RainingTextureSet:
                            //specular
                            if (rainingTextureSet.Specular != null)
                            {
                                material.SpecularTexture = (string)rainingTextureSet.Specular.Tag;
                            }
                            else if(dayTextureSet.Specular != null)
                            {
                                material.SpecularTexture = (string)dayTextureSet.Specular.Tag;
                            }

                            //normal
                            if (rainingTextureSet.Normal != null)
                            {
                                material.NormalTexture = (string)rainingTextureSet.Normal.Tag;
                            }
                            else if(dayTextureSet.Normal != null)
                            {
                                material.NormalTexture = (string)dayTextureSet.Normal.Tag;
                            }

                            //emissive
                            if (rainingTextureSet.Emissive != null)
                            {
                                material.EmissiveTexture = (string)rainingTextureSet.Emissive.Tag;
                            }
                            else if(dayTextureSet.Emissive != null)
                            {
                                material.EmissiveTexture = (string)dayTextureSet.Emissive.Tag;
                            }
                            break;
                        case TextureSetCode.SnowTextureSet:
                            //specular
                            if (snowTextureSet.Specular != null)
                            {
                                material.SpecularTexture = (string)snowTextureSet.Specular.Tag;
                            }
                            else if(dayTextureSet.Specular != null)
                            {
                                material.SpecularTexture = (string)dayTextureSet.Specular.Tag;
                            }

                            //normal
                            if (snowTextureSet.Normal != null)
                            {
                                material.NormalTexture = (string)snowTextureSet.Normal.Tag;
                            }
                            else if (dayTextureSet.Normal != null)
                            {
                                material.NormalTexture = (string)dayTextureSet.Normal.Tag;
                            }

                            //emissive
                            if (snowTextureSet.Emissive != null)
                            {
                                material.EmissiveTexture = (string)snowTextureSet.Emissive.Tag;
                            }
                            else if (dayTextureSet.Emissive != null)
                            {
                                material.EmissiveTexture = (string)dayTextureSet.Emissive.Tag;
                            }
                            break;
                    }

                    //set the baseTextureSetCode to targetTextureSetCode
                    baseTextureSetCode = targetTextureSetCode;
                    //set baseTexture to targetTexture
                    baseTexture = targetTexture;
                    //reset dtTime to zero
                    dtTime = 0;
                    //reset dtCountingTime to zero
                    dtCountingTime = 0;
                }
            }
        }

        /// <summary>
        /// Update this AirportMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the world matrix
                world = Matrix.CreateScale(scale) *
                        Matrix.CreateTranslation(position) *
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);
                        

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);

                //update the changing of textureset and its related stuff
                UpdateTextureSet(gameTime);
            }
        }

        /// <summary>
        /// Draw this AirportMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                //set the parameters inside the effect file accordingly
                material.Effect.Parameters["dtTime"].SetValue(dtTime);
                material.Effect.Parameters["baseTexture"].SetValue(baseTexture);
                material.Effect.Parameters["targetTexture"].SetValue(targetTexture);

                material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
            }
        }

        /// <summary>
        /// Draw the alpha meshes of the airport.
        /// </summary>
        /// <remarks>This should be called after the deferred shading process has been finished.
        /// Also note that this is the special method for airport mesh as the class says it's the opaque mesh, but it can draw the alpha mesh too.
        /// *Because of the approach we use to reduce the memory load issue, thus by not separating another mesh (alpha mesh) just to draw with the apply of
        ///  emissive map, then we must draw it from here.</remarks>
        /// <param name="gameTime"></param>
        public void DrawAlpha(GameTime gameTime)
        {
            if (active)
            {
                if (isLightTurnOn && dayTextureSet.Emissive != null)
                {
                    //For light originated from window of the airport's meshes
                    //set the emissive texture
                    lm1Material.DiffuseTexture = (string)dayTextureSet.Emissive.Tag;
                    //draw the window mesh
                    //Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = true;
                    lm1Material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
                }
            }
        }

        /// <summary>
        /// Draw the depth scene into the external routing of shadow render target.
        /// </summary>
        /// <param name="gameTime"></param>
        public void DrawShadowMap(GameTime gameTime)
        {
            //just copy the code from the DrawOpaque() method, and add the last argument of shadowmap effect from RenderSystem.
            if (active)
            {
                //no need to resend anything as it's in the same time (same gameTime at the moment)
                material.DrawBasic(gameTime, world, model, meshIndex, boneIndex, Engine.RenderSystem.ShadowMapDrawEffect);
            }
        }
    }
}
