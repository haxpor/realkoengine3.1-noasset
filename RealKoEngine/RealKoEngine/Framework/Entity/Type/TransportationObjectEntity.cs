﻿#region File Description
//-----------------------------------------------------------------------------
// AirplaneEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// TransporationObjectEntity represents the entity instance of any transportation object throughout the simulation.
    /// </summary>
    public class TransportationObjectEntity: Entity3D, RK_IShadowMapDrawable
    {
        #region Nested properties signatures

        /// <summary>
        /// Texture set for this TransportationObjectEntity.
        /// </summary>
        /// <remarks>This class is designed to be consistent with the SCMesh.SCTextureSet structure.
        /// This class will be updated follow that one.</remarks>
        public class TransportationObjectTextureSet
        {
            public Texture2D Diffuse;
            public Texture2D Specular;
            public Texture2D Normal;
            public Texture2D Emissive;
            public Texture2D XLight;

            /// <summary>
            /// Load all the textures associated with this airplane type.
            /// </summary>
            /// <param name="textureSet"></param>
            public TransportationObjectTextureSet(TransportationObject.TransportationObjectTextureSet textureSet)
            {
                //load all the specified textures in texture set
                //also set the file path of each texture into texture's tag
                if (textureSet.Diffuse != "null")
                {
                    Diffuse = Engine.Content.Load<Texture2D>(textureSet.Diffuse);
                    Diffuse.Tag = (string)textureSet.Diffuse;
                }

                if (textureSet.Specular != "null")
                {
                    Specular = Engine.Content.Load<Texture2D>(textureSet.Specular);
                    Specular.Tag = (string)textureSet.Specular;
                }

                if (textureSet.Normal != "null")
                {
                    Normal = Engine.Content.Load<Texture2D>(textureSet.Normal);
                    Normal.Tag = (string)textureSet.Normal;
                }

                if (textureSet.Emissive != "null")
                {
                    Emissive = Engine.Content.Load<Texture2D>(textureSet.Emissive);
                    Emissive.Tag = (string)textureSet.Emissive;
                }

                if (textureSet.XLight != "null")
                {
                    XLight = Engine.Content.Load<Texture2D>(textureSet.XLight);
                    XLight.Tag = (string)textureSet.XLight;
                }
            }
        }

        #endregion

        protected string modelFilePath;
        protected TransportationObjectTextureSet textureSet;

        protected Material opaqueMaterial;
        protected Material lm1Material;
        protected Material lm2Material;
        protected Model model;

        protected bool isWheelPullOut;
        protected bool isWindowLightTurnOn;
        protected bool isHeadLightTurnOn;
        protected bool isSignalLightTurnOn;
        protected bool isHeadGroungLightTurnOn;
        protected bool isSignalGroundLightTurnOn;

        // Copy variables for rotation direction of rotateable mesh 1-3
        private Vector3 rotMesh_RotDir1; private float rotMesh_speed1 = 0.0f;
        private Vector3 rotMesh_RotDir2; private float rotMesh_speed2 = 0.0f;
        private Vector3 rotMesh_RotDir3; private float rotMesh_speed3 = 0.0f;
        private Vector3 rotMesh_RotDir4; private float rotMesh_speed4 = 0.0f;
        private bool rotMesh_isPaused1;
        private bool rotMesh_isPaused2;
        private bool rotMesh_isPaused3;
        private bool rotMesh_isPaused4;

        //value used to simulate the uniqueness of each airplane's signal light
        protected float signalLightUncertainty;

        //just for convenience
        protected TransportationObject tranObj_copy;

        // temp for internal update
        private Matrix rotationMat = Matrix.Identity;

        #region Properties

        /// <summary>
        /// Get the transportation object information attached to this airplane.
        /// </summary>
        /// <remarks>Do not modify any value of this properties. Use it for information purpose only.</remarks>
        public TransportationObject TransportationObjectInfo
        {
            get { return tranObj_copy; }
        }

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the texture set that this TransportationObjectEntity used.
        /// </summary>
        public TransportationObjectTextureSet TextureSet
        {
            get { return textureSet; }
        }

        /// <summary>
        /// Get the opaque material used by this AirplaneEntity.
        /// </summary>
        public Material OpaqueMaterial
        {
            get { return opaqueMaterial; }
        }

        /// <summary>
        /// Get the model used by this AirplaneEntity.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        /// <summary>
        /// Get or set the wheel-pull-out flag.
        /// </summary>
        public bool IsWheelPullOut
        {
            get { return isWheelPullOut; }
            set { isWheelPullOut = value; }
        }

        /// <summary>
        /// Get or set the window-light-turn-on flag.
        /// </summary>
        public bool IsWindowLightTurnOn
        {
            get { return isWindowLightTurnOn; }
            set { isWindowLightTurnOn = value; }
        }

        /// <summary>
        /// Get or set the head-light-turn-on flag.
        /// </summary>
        public bool IsHeadLightTurnOn
        {
            get { return isHeadLightTurnOn; }
            set { isHeadLightTurnOn = value; }
        }

        /// <summary>
        /// Get or set the signal-light-turn-on flag.
        /// </summary>
        public bool IsSignalLightTurnOn
        {
            get { return isSignalLightTurnOn; }
            set { isSignalLightTurnOn = value; }
        }

        /// <summary>
        /// Get or set the head ground light turn-on flag.
        /// </summary>
        public bool IsHeadGroundLightTurnOn
        {
            get { return isHeadGroungLightTurnOn; }
            set { isHeadGroungLightTurnOn = value; }
        }

        /// <summary>
        /// Get or set the signal ground light turn-on flag.
        /// </summary>
        public bool IsSignalGroundLightTurnOn
        {
            get { return isSignalGroundLightTurnOn; }
            set { isSignalGroundLightTurnOn = value; }
        }

        /// <summary>
        /// Get the signal light uncertainty.
        /// </summary>
        public float SignalLightUncertainty
        {
            get { return signalLightUncertainty; }
        }

        /// <summary>
        /// Get or set the paused flag of rotateable mesh 1.
        /// </summary>
        public bool IsRotMesh1_Paused
        {
            get { return rotMesh_isPaused1; }
            set {
                if (rotMesh_isPaused1 != value)
                {
                    rotMesh_isPaused1 = value;
                    if (rotMesh_isPaused1)
                        rotMesh_speed1 = 0.0f;
                    else
                        rotMesh_speed1 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
                }
            }
        }

        /// <summary>
        /// Get or set the paused flag of rotateable mesh 2.
        /// </summary>
        public bool IsRotMesh2_Paused
        {
            get { return rotMesh_isPaused2; }
            set
            {
                if (rotMesh_isPaused2 != value)
                {
                    rotMesh_isPaused2 = value;
                    if (rotMesh_isPaused2)
                        rotMesh_speed2 = 0.0f;
                    else
                        rotMesh_speed2 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
                }
            }
        }

        /// <summary>
        /// Get or set the paused flag of rotateable mesh 3.
        /// </summary>
        public bool IsRotMesh3_Paused
        {
            get { return rotMesh_isPaused3; }
            set
            {
                if (rotMesh_isPaused3 != value)
                {
                    rotMesh_isPaused3 = value;
                    if (rotMesh_isPaused3)
                        rotMesh_speed3 = 0.0f;
                    else
                        rotMesh_speed3 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
                }
            }
        }

        /// <summary>
        /// Get or set the paused flag of rotateable mesh 4.
        /// </summary>
        public bool IsRotMesh4_Paused
        {
            get { return rotMesh_isPaused4; }
            set
            {
                if (rotMesh_isPaused4 != value)
                {
                    rotMesh_isPaused4 = value;
                    if (rotMesh_isPaused4)
                        rotMesh_speed4 = 0.0f;
                    else
                        rotMesh_speed4 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
                }
            }
        }

        #endregion

        /// <summary>
        /// Create the TransportationObjectEntity.
        /// </summary>
        /// <param name="id">ID defined for this airplane entity.</param>
        /// <param name="apt">Information required to created this AirplaneEntity</param>
        public TransportationObjectEntity(uint id, TransportationObject tranObj)
            : base(id, tranObj.ID, tranObj.Name)
        {
            //make the copy of airplane type information
            //Note: AirplaneTypeAsset is the pure reference type, thus no overhead here (just addressing)
            tranObj_copy = tranObj;

            //copy the rotateable direction to our variables
            //Note: -1 is the dummy variable here
            if (tranObj_copy.RotMesh1.MeshIndex != -1)
            {
                rotMesh_RotDir1 = tranObj_copy.RotMesh1.RotateDirection;

                if (tranObj_copy.RotMesh1.IsPaused)
                    rotMesh_speed1 = 0.0f;
                else
                    rotMesh_speed1 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
            }
            if (tranObj_copy.RotMesh2.MeshIndex != -1)
            {
                rotMesh_RotDir2 = tranObj_copy.RotMesh2.RotateDirection;

                if (tranObj_copy.RotMesh2.IsPaused)
                    rotMesh_speed2 = 0.0f;
                else
                    rotMesh_speed2 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
            }
            if (tranObj_copy.RotMesh3.MeshIndex != -1)
            {
                rotMesh_RotDir3 = tranObj_copy.RotMesh3.RotateDirection;

                if (tranObj_copy.RotMesh3.IsPaused)
                    rotMesh_speed3 = 0.0f;
                else
                    rotMesh_speed3 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
            }
            if (tranObj_copy.RotMesh4.MeshIndex != -1)
            {
                rotMesh_RotDir4 = tranObj_copy.RotMesh4.RotateDirection;

                if (tranObj_copy.RotMesh4.IsPaused)
                    rotMesh_speed4 = 0.0f;
                else
                    rotMesh_speed4 = EntitySpecification.Airplane.AIRPLANE_TURBINE_SPEED_LIMIT_1;
            }

            //setup all properties
            modelFilePath = tranObj_copy.ModelFilePath;
            textureSet = new TransportationObjectTextureSet(tranObj_copy.TextureSet);
            
            //load all the assets
            //if the model is loaded multiple times then the content manager will return the reference to the same one.
            model = Engine.Content.Load<Model>(modelFilePath);

            //set the bounding sphere cover the model (exclude the light meshes)
            //first create the array of indexes consists of the body mesh (this is the fixed size due to the specification of the asset configuration from the engine)
            //Note: Refer to the document for more information of the specification
            int[] meshIndexes = { (int)tranObj_copy.BodyMIndexes.Hull.X, (int)tranObj_copy.BodyMIndexes.Wheel.X };

            originalBoundingSphere = XNAUtil.CreateBoundingSphereFrom(model, meshIndexes);

            //set the scale accordingly (just try this value out)
            scale *= Engine.SimulationSettings.GlobalScale * Engine.SimulationSettings.AirplaneToMapScale;

            //create the material, it's the drawer for this mesh
            opaqueMaterial = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/TransportationObjectOpaqueMeshRender"));
             // cache original bone transforms
            opaqueMaterial.CacheOriginalBoneTransforms(model);

            //## FIXED Version of code, 15 Sept 2009, 4:13 PM. ##
            //Note: Use this version of code instead.
            //Diffuse
            if (textureSet.Diffuse != null)
                opaqueMaterial.DiffuseTexture = (string)textureSet.Diffuse.Tag;
            else
                opaqueMaterial.DiffuseTexture = null;

            //Specular
            if(textureSet.Specular != null)
                opaqueMaterial.SpecularTexture = (string)textureSet.Specular.Tag;
            else
                opaqueMaterial.SpecularTexture = null;

            //Normal
            if(textureSet.Normal != null)
                opaqueMaterial.NormalTexture = (string)textureSet.Normal.Tag;
            else
                opaqueMaterial.NormalTexture = null;

            //Emissive
            if(textureSet.Emissive != null)
                opaqueMaterial.EmissiveTexture = (string)textureSet.Emissive.Tag;
            else
                opaqueMaterial.EmissiveTexture = null;

            //create the alpha-mesh material
            //LM1
            lm1Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/TransportationObjectAlphaLM1Render"));
            lm1Material.CacheOriginalBoneTransforms(model);
            //LM2
            lm2Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/TransportationObjectAlphaLM2Render"));
            lm2Material.CacheOriginalBoneTransforms(model);
            //set the texture for lm2 immediately as only one texture existed.
            if (textureSet.XLight != null)
                lm2Material.DiffuseTexture = (string)textureSet.XLight.Tag;
            else
                lm2Material.DiffuseTexture = null;

            //set the uncertainty of signal light's periodically blinking
            signalLightUncertainty = XNAUtil.RandomBetween(2.5f, 3.5f);
        }

        /// <summary>
        /// Update this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the rotation matrix
                rotationMat =  Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

                //calculate the world matrix
                world = Matrix.CreateScale(scale) *
                        rotationMat * 
                        Matrix.CreateTranslation(position);

                //apply to head direction (apply only if the rotation for all axises are not zero)
                if (rotation.X != 0.0f && rotation.Y != 0.0f && rotation.Z != 0.0f)
                {
                    headDirection = Vector3.Transform(headDirection, rotationMat);
                    rightDirection = Vector3.Transform(rightDirection, rotationMat);
                }

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
            }
        }

        /// <summary>
        /// Draw this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if(active)
                //draw opaque meshes
                DrawOpaque(gameTime);
        }

        /// <summary>
        /// Draw the opaque meshes of the airplane.
        /// </summary>
        /// <remarks>This will be called automatically by the AirplaneEntity class.</remarks>
        /// <param name="gameTime"></param>
        private void DrawOpaque(GameTime gameTime)
        {
            if (active)
            {
                //draw all the body meshes index
                //draw the hull mesh
                opaqueMaterial.DrawBasic(gameTime, world, model, (int)tranObj_copy.BodyMIndexes.Hull.X, (int)tranObj_copy.BodyMIndexes.Hull.Y);

                //draw the wheel mesh
                if (isWheelPullOut && (int)tranObj_copy.BodyMIndexes.Wheel.X != -1)
                    opaqueMaterial.DrawBasic(gameTime, world, model, (int)tranObj_copy.BodyMIndexes.Wheel.X, (int)tranObj_copy.BodyMIndexes.Wheel.Y);

                //draw the rotateable meshes
                if (tranObj_copy.RotMesh1.MeshIndex != -1)
                {
                    /*opaqueMaterial.DrawBasic(gameTime, 
                        world * 
                        Matrix.CreateTranslation(-position) *
                        Matrix.CreateFromAxisAngle(Vector3.Transform(rotMesh_RotDir1, rotationMat),
                           rotMesh_speed1 * tranObj_copy.RotMesh1.SpeedFactor * 
                           (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds) *
                            Matrix.CreateTranslation(position),     
                        model, tranObj_copy.RotMesh1.MeshIndex, tranObj_copy.RotMesh1.BoneIndex);*/

                    //Vector3 pos = new Vector3(54.09f, 34.926f, 71.836f);
                    //Vector3 v = pos * scale;
                    //Vector3 vNeg = -pos * scale;

                    // -- working code to rotate mesh around itself --
                    opaqueMaterial.DrawBasicMeshTransform(gameTime,
                        world,
                        Matrix.CreateFromAxisAngle(rotMesh_RotDir1,
                           rotMesh_speed1 * tranObj_copy.RotMesh1.SpeedFactor *
                           (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds)
                        , model, tranObj_copy.RotMesh1.MeshIndex, tranObj_copy.RotMesh1.BoneIndex);
                }

                if (tranObj_copy.RotMesh2.MeshIndex != -1)
                {
                    opaqueMaterial.DrawBasicMeshTransform(gameTime,
                        world,
                        Matrix.CreateFromAxisAngle(rotMesh_RotDir2,
                           rotMesh_speed2 * tranObj_copy.RotMesh2.SpeedFactor *
                           (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds)
                        , model, tranObj_copy.RotMesh2.MeshIndex, tranObj_copy.RotMesh2.BoneIndex);
                }

                if (tranObj_copy.RotMesh3.MeshIndex != -1)
                {
                    opaqueMaterial.DrawBasicMeshTransform(gameTime,
                        world,
                        Matrix.CreateFromAxisAngle(rotMesh_RotDir3,
                           rotMesh_speed3 * tranObj_copy.RotMesh3.SpeedFactor *
                           (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds)
                        , model, tranObj_copy.RotMesh3.MeshIndex, tranObj_copy.RotMesh3.BoneIndex);
                }

                if (tranObj_copy.RotMesh4.MeshIndex != -1)
                {
                    opaqueMaterial.DrawBasicMeshTransform(gameTime,
                        world,
                        Matrix.CreateFromAxisAngle(rotMesh_RotDir4,
                           rotMesh_speed4 * tranObj_copy.RotMesh4.SpeedFactor *
                           (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds)
                        , model, tranObj_copy.RotMesh4.MeshIndex, tranObj_copy.RotMesh4.BoneIndex);
                }

                //Matrix.CreateTranslation(-position) *
                //Matrix.CreateFromAxisAngle(Vector3.Transform(Vector3.UnitZ, rotationMat), 
                //    0.001f * (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds) *
                //Matrix.CreateTranslation(position), model, tranObj_copy.BodyMIndex.Wheel, boneIndex);

                //insert the following code into the line above to simulate the rotation around the arbitrary point
                //* Matrix.CreateTranslation(-position) *
                //Matrix.CreateFromAxisAngle(rightDirection, 0.001f * (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds) *
                //Matrix.CreateTranslation(position)
            }
        }

        /// <summary>
        /// Draw the alpha meshes of the airplane.
        /// </summary>
        /// <remarks>This should be called after the deferred shading process has been finished.</remarks>
        /// <param name="gameTime"></param>
        public void DrawAlpha(GameTime gameTime)
        {
            if (active)
            {
                if (isWindowLightTurnOn && textureSet.Emissive != null)
                {
                    //cull them too
                    Engine.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

                    //For window mesh
                    //set the emissive texture
                    lm1Material.DiffuseTexture = (string)textureSet.Emissive.Tag;
                    //reuse the same mesh here
                    lm1Material.DrawBasic(gameTime, world, model, (int)tranObj_copy.BodyMIndexes.Hull.X, (int)tranObj_copy.BodyMIndexes.Hull.Y);
                }

                if (isHeadLightTurnOn && textureSet.XLight != null && (int)tranObj_copy.LightMIndexes.HeadLight.X != -1)
                {
                    //set the xlight texture
                    lm1Material.DiffuseTexture = (string)textureSet.XLight.Tag;
                    //draw the head light mesh
                    Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    lm1Material.DrawBasic(gameTime, world, model, (int)tranObj_copy.LightMIndexes.HeadLight.X, (int)tranObj_copy.LightMIndexes.HeadLight.Y);
                }

                if (isSignalLightTurnOn && textureSet.XLight != null && (int)tranObj_copy.LightMIndexes.SignalLight.X != -1)
                {
                    //set the xlight texture
                    lm2Material.DiffuseTexture = (string)textureSet.XLight.Tag;
                    //draw the head light mesh
                    Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    //set the current time used for blinking effect
                    lm2Material.Effect.Parameters["time"].SetValue((float)Engine.TimeManager.SimulationTotalTime.TotalSeconds * signalLightUncertainty);
                    lm2Material.DrawBasic(gameTime, world, model, (int)tranObj_copy.LightMIndexes.SignalLight.X, (int)tranObj_copy.LightMIndexes.SignalLight.Y);

                    //also taken the ground into account
                    if (isSignalGroundLightTurnOn && (int)tranObj_copy.LightMIndexes.SignalGroundLight.X != -1)
                    {
                        //draw the signal ground light
                        lm2Material.DrawBasic(gameTime, world, model, (int)tranObj_copy.LightMIndexes.SignalGroundLight.X, (int)tranObj_copy.LightMIndexes.SignalGroundLight.Y);
                    }
                }

                if (isHeadGroungLightTurnOn && textureSet.XLight != null && (int)tranObj_copy.LightMIndexes.HeadGroundLight.X != -1)
                {
                    //set the xlight texture
                    lm1Material.DiffuseTexture = (string)textureSet.XLight.Tag;
                    //draw the head light mesh
                    Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    lm1Material.DrawBasic(gameTime, world, model, (int)tranObj_copy.LightMIndexes.HeadGroundLight.X, (int)tranObj_copy.LightMIndexes.HeadGroundLight.Y);
                }
            }
        }

        /// <summary>
        /// Draw the depth scene into the external routing of shadow render target.
        /// </summary>
        /// <param name="gameTime"></param>
        public void DrawShadowMap(GameTime gameTime)
        {
            //just copy the code from the DrawOpaque() method, and add the last argument of shadowmap effect from RenderSystem.
            if (active)
            {
                //no need to resend anything as it's in the same time (same gameTime at the moment)

                //draw all the body meshes index
                //draw the hull mesh
                opaqueMaterial.DrawBasic(gameTime, world, model, (int)tranObj_copy.BodyMIndexes.Hull.X, (int)tranObj_copy.BodyMIndexes.Hull.Y, Engine.RenderSystem.ShadowMapDrawEffect);

                //draw the wheel mesh
                if (isWheelPullOut && (int)tranObj_copy.BodyMIndexes.Wheel.X != -1)
                    opaqueMaterial.DrawBasic(gameTime, world, model, (int)tranObj_copy.BodyMIndexes.Wheel.X, (int)tranObj_copy.BodyMIndexes.Wheel.Y, Engine.RenderSystem.ShadowMapDrawEffect);
            }
        }
    }
}
