﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Entity.Type.Manager
{
    public enum FogFormulae { LINEAR = 1, EXPO, EXPO2 };
    public enum FogPreset { DEFAULT, VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH};

    public class FogSettingManager
    {
        Effect baseMaterial;

        // Linear Fog
        private float fogNear;
        private float fogFar;
        private float fogAltitudeScale;
        private float fogThinning;

        // Exponential Fog
        private float fogDensity;

        // -- Control --
        private FogFormulae fogFormulae;
        private Color fogColor;
        private bool fogEnabled;

        #region Properties

        /// <summary>
        /// Get or set fog near.
        /// </summary>
        public float FogNear
        {
            get { return fogNear; }
            set
            {
                fogNear = value;
                setParameter("fogNear", fogNear);
            }
        }

        /// <summary>
        /// Get or set fog far.
        /// </summary>
        public float FogFar
        {
            get { return fogFar; }
            set
            {
                fogFar = value;
                setParameter("fogFar", fogFar);
            }
        }

        /// <summary>
        /// Get or set fog altitude scale.
        /// </summary>
        public float FogAltitudeScale
        {
            get { return fogAltitudeScale; }
            set
            {
                fogAltitudeScale = value;
                setParameter("fogAltitudeScale", fogAltitudeScale);
            }
        }

        /// <summary>
        /// Get or set fog thinning.
        /// </summary>
        public float FogThinning
        {
            get { return fogThinning; }
            set
            {
                fogThinning = value;
                setParameter("fogThinning", fogThinning);
            }
        }

        /// <summary>
        /// Get or set fog density.
        /// </summary>
        public float FogDensity
        {
            get { return fogDensity; }
            set
            {
                fogDensity = value;
                setParameter("fogDensity", fogDensity);
            }
        }

        /// <summary>
        /// Get or set fog formulae.
        /// </summary>
        public FogFormulae FogFormulae
        {
            get { return fogFormulae; }
            set
            {
                fogFormulae = value;
                setParameter("fogFormulae", fogFormulae);
            }
        }

        /// <summary>
        /// Get or set fog color.
        /// </summary>
        public Color FogColor
        {
            get { return fogColor; }
            set
            {
                fogColor = value;
                setParameter("fogColor", fogColor);
            }
        }

        /// <summary>
        /// Get or set fog enabled.
        /// </summary>
        public bool FogEnabled
        {
            get { return fogEnabled; }
            set
            {
                fogEnabled = value;
                setParameter("fogEnabled", fogEnabled);
            }
        }

        #endregion Properties

        /// <summary>
        /// Create a new fog setting.
        /// </summary>
        /// <param name="baseMaterial"></param>
        public FogSettingManager(Effect baseMaterial)
        {
            this.baseMaterial = baseMaterial;
        }

        /// <summary>
        /// Init with default setting.
        /// </summary>
        public void InitDefault()
        {
            InitWithPreset(FogPreset.DEFAULT);
        }

        /// <summary>
        /// Init with specified setting.
        /// </summary>
        /// <param name="preset">Setting numb</param>
        public void InitWithPreset(FogPreset preset)
        {
            // optimized for short building
            if (preset == FogPreset.DEFAULT)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.06f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
            else if (preset == FogPreset.VERY_LOW)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.001f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
            else if (preset == FogPreset.LOW)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.01f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
            else if (preset == FogPreset.MEDIUM)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.07f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
            else if (preset == FogPreset.HIGH)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.12f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
            else if (preset == FogPreset.VERY_HIGH)
            {
                FogNear = 1;
                FogFar = 20;
                FogAltitudeScale = 1;
                FogThinning = 1;
                FogDensity = 0.2f;
                FogColor = Color.White;
                FogEnabled = true;

                // use EXPO
                FogFormulae = FogFormulae.EXPO;
            }
        }

        #region Set parameters Methods for various data types

        /// <summary>
        /// Set fog parameter in float type.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="value"></param>
        private void setParameter(string param, float value)
        {
            baseMaterial.Parameters[param].SetValue(value);
        }

        /// <summary>
        /// Set fog parameters in boolean type.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="value"></param>
        private void setParameter(string param, bool value)
        {
            baseMaterial.Parameters[param].SetValue(value);
        }

        /// <summary>
        /// Set fog parameter in Color type.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="value"></param>
        private void setParameter(string param, Color value)
        {
            baseMaterial.Parameters[param].SetValue(value.ToVector4());
        }

        /// <summary>
        /// Set fog parameter in FogFormulae type.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="value"></param>
        private void setParameter(string param, FogFormulae value)
        {
            baseMaterial.Parameters[param].SetValue((int)value);
        }

        #endregion Set parameters Methods for various data types
    }
}
