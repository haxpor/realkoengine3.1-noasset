﻿#region File Description
//-----------------------------------------------------------------------------
// SCMeshManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Entity.Type;
using RealKo.Framework.Graphics;
using RealKo.Framework.Graphics.Misc;
using RealKo.Framework.Helper.Util;

namespace RealKo.Framework.Entity.Type.Manager
{
    /// <summary>
    /// SCMeshManager is designed to manage the updating and drawing process of both skydome-mesh, and clouddome-mesh.
    /// It's very important that skydome and clouddome contain only 1 mesh for each of them.
    /// </summary>
    /// <remarks>User should not override this class unless you know what you're doing.
    /// Also this class reference to the engine class.
    /// 
    /// *This class corresponds to the TimeManager's hour offset. This class will follow the change in TimeManager class.</remarks>
    public class SCMeshManager: RK_IPreInitialize, RK_IUpdateable, RK_IDrawable
    {
        #region Skydome and clouddome mesh entity

        /// <summary>
        /// Skydome entity.
        /// </summary>
        protected SCMeshEntity skyDome;

        /// <summary>
        /// Clouddome entity.
        /// </summary>
        protected SCMeshEntity cloudDome;

        #region Properties

        /// <summary>
        /// Get the skydome entity.
        /// </summary>
        public SCMeshEntity SkyDome
        {
            get { return skyDome; }
        }

        /// <summary>
        /// Get the clouddome entity.
        /// </summary>
        public SCMeshEntity CloudDome
        {
            get { return cloudDome; }
        }

        #endregion

        #endregion

        #region Textures stuff

        //The instance of texture2d will be grabbed from the SCMeshEntity itself. (Thus this is the underlying system.)

        protected Texture2D sBaseTexture;
        protected Texture2D sTargetTexture;

        protected Texture2D cBaseTexture;
        protected Texture2D cTargetTexture;

        protected Texture2D sDTexture;
        protected Texture2D cDTexture;

        #endregion

        #region Time stuff

        /// <summary>
        /// Number indicates for which timeslot the currentTime is in.
        /// </summary>
        protected int timeSlot;

        /// <summary>
        /// Time proportion in single time slot.
        /// </summary>
        protected float dTime;

        #region Properties of time stuff
        /// <summary>
        /// Get the current time slot.
        /// </summary>
        public int TimeSlot
        {
            get { return timeSlot; }
        }

        /// <summary>
        /// Proportion of current time compared to the beginning of the time in current time slot.
        /// </summary>
        public float DeltaTime
        {
            get { return dTime; }
        }

        #endregion

        #endregion

        #region Internal properties for changing process

        //These are the intensities used when blending together 4 daytime textures.
        //These values come from the experiment.
        float intensityA = 0.15f;
        float intensityB = 0.67f;
        float intensityC = 1.0f;
        float intensityE = 0.4f;

        // ambient light
        float ambientLight = 1.0f;

        /// <summary>
        /// Get ambient light, or sun light intensity.
        /// </summary>
        public float AmbientLight
        {
            get { return ambientLight; }
        }

        #endregion

        #region Texture D stuff

        /// <summary>
        /// Current accumulated time used to calculate the delta time.
        /// </summary>
        float changingTextureD_countingTime = 0f;

        /// <summary>
        /// Current delta time of changing to texture D.
        /// </summary>
        /// <remarks>The clamping process will be handled in the InternalUpdate() method.
        /// This measures in seconds.</remarks>
        float changingTextureD_dTime = 0.0f;

        /// <summary>
        /// Begin the process of changing to texture D.
        /// 
        /// Only the change that differ from current value will be taken into effect.
        /// </summary>
        /// <remarks>Default is false.
        /// Do not change this value directly, change through BeginChangeToTextureD property instead.</remarks>
        bool isBeginChangeToTextureD = false;

        /// <summary>
        /// Flag indicates whether or not, the process will step up.
        /// If it's false, then it's the step down process.
        /// </summary>
        /// <remarks>Default is false.</remarks>
        bool isChangeToTextureD_StepUp = false;

        #region Properties of Texture D stuff

        /// <summary>
        /// Get the current texture d's delta time.
        /// </summary>
        public float DDeltaTime
        {
            get { return changingTextureD_dTime; }
        }

        /// <summary>
        /// Get or set the flag indicates whether to begin changing to texture D.
        /// </summary>
        public bool BeginChangeToTextureD
        {
            get { return isBeginChangeToTextureD; }
            set
            {
                if (isBeginChangeToTextureD != value)
                {
                    if (isBeginChangeToTextureD == false)
                    {
                        //then it's the step up process
                        isChangeToTextureD_StepUp = true;

                        isBeginChangeToTextureD = true;
                    }
                    else if (isBeginChangeToTextureD == true)
                    {
                        //then it's the step down process
                        isChangeToTextureD_StepUp = false;

                        isBeginChangeToTextureD = false;
                    }
                }
            }
        }

        /// <summary>
        /// Get whether or not the changing process to texture D is the step up process.
        /// </summary>
        public bool IsChangeToTextureD_StepUp
        {
            get { return isChangeToTextureD_StepUp; }
        }

        #endregion

        #endregion

        #region Lighting flash

        /// <summary>
        /// Define the states involving in lighting-flash natural algorithm.
        /// </summary>
        public enum FlashPatternTimeState
        {
            DuringPatternDataLifeActive,
            DuringTimeTilNextPatternData
        }

        #region FlashPatternData definition
        /// <summary>
        /// Flash pattern data represents the information for each of the pattern data in lighting-flash natural algorithm.
        /// </summary>
        public class FlashPatternData
        {
            /// <summary>
            /// Time in seconds (accurate in milliseconds) for this pattern data to be in active.
            /// </summary>
            public float TimeLifeActive;

            /// <summary>
            /// Time in seconds (accurate in milliseconds) before the next pattern data can be put in progress.
            /// </summary>
            public float TimeTilNextPatternData;

            /// <summary>
            /// Possibility to flash the sky. The range must be in [0.0, 1.0].
            /// </summary>
            public float ChanceToFlash;

            /// <summary>
            /// Flashy texture's index used in lighting-flash natural algorithm.
            /// </summary>
            public int FlashyTextureIndex;

            /// <summary>
            /// Create the flash pattern data.
            /// </summary>
            /// <param name="timeLifeActive"></param>
            /// <param name="timeTilNextPatternData"></param>
            /// <param name="chaneToFlash"></param>
            /// <param name="flashyTextureIndex"></param>
            public FlashPatternData(float timeLifeActive, float timeTilNextPatternData, float chaneToFlash, int flashyTextureIndex)
            {
                TimeLifeActive = timeLifeActive;
                TimeTilNextPatternData = timeTilNextPatternData;
                ChanceToFlash = chaneToFlash;
                FlashyTextureIndex = flashyTextureIndex;
            }
        }
        #endregion

        #region Variables used for light-flash natural algorithm
        /// <summary>
        /// Current state of the lighting-flash natural algorithm.
        /// </summary>
        protected FlashPatternTimeState currentFlashState;

        /// <summary>
        /// Current counting time will be reused for counting the time for both 'during the pattern's life time', and 'during the time til next pattern'.
        /// </summary>
        /// <remarks>It's accurate nearly in milliseconds.</remarks>
        protected float currentFlashCountingTime;

        /// <summary>
        /// Current flash pattern data index.
        /// </summary>
        /// <remarks>We can use this information to get the information directly from the array.</remarks>
        protected int currentFlashPatternDataIndex;

        /// <summary>
        /// Array of flash pattern data.
        /// </summary>
        protected FlashPatternData[] flashPatterns;

        /// <summary>
        /// Whether this cycle need to flash the sky or not.
        /// </summary>
        protected bool isFlashThisCycle;

        /// <summary>
        /// Flashy textures used in the lighting-flashes natural algorithm.
        /// </summary>
        protected Texture2D[] flashyTextures;
        #endregion

        #endregion

        #region Lightning Bolt

        /// <summary>
        /// Define the states involving in lightning-bolt natural algorithm.
        /// </summary>
        public enum LightningBoltPatternTimeState
        {
            DuringPatternDataLifeActive,
            DuringTimeTilNextPatternData
        }

        #region LightningPatternData definition
        /// <summary>
        /// Lightning pattern data represents the information for each of the pattern data in lightning-bolt natural algorithm.
        /// </summary>
        public class LightningBoltPatternData
        {
            /// <summary>
            /// Time in seconds (accurate in milliseconds) for this pattern data to be in active.
            /// </summary>
            public float TimeLifeActive;

            /// <summary>
            /// Time in seconds (accurate in milliseconds) before the next pattern data can be put in progress.
            /// </summary>
            public float TimeTilNextPatternData;

            /// <summary>
            /// Possibility for lightning to be occurred. The range must be in [0.0, 1.0].
            /// </summary>
            public float ChanceToLightning;

            /// <summary>
            /// Create the lightning-bolt pattern data.
            /// </summary>
            /// <param name="timeLifeActive"></param>
            /// <param name="timeTilNextPatternData"></param>
            /// <param name="ChanceToLightning"></param>
            public LightningBoltPatternData(float timeLifeActive, float timeTilNextPatternData, float chanceToLightning)
            {
                TimeLifeActive = timeLifeActive;
                TimeTilNextPatternData = timeTilNextPatternData;
                ChanceToLightning = chanceToLightning;
            }
        }
        #endregion

        #region Variables used for lightning-bolt natural algorithm
        /// <summary>
        /// Current state of the lightning-bolt natural algorithm.
        /// </summary>
        protected LightningBoltPatternTimeState currentLightningBoltState;

        /// <summary>
        /// Current counting time will be reused for counting the time for both 'during the pattern's life time', and 'during the time til next pattern'.
        /// </summary>
        /// <remarks>It's accurate nearly in milliseconds.</remarks>
        protected float currentLightningBoltCountingTime;

        /// <summary>
        /// Current lightning-bolt pattern data index.
        /// </summary>
        /// <remarks>We can use this information to get the information directly from the array.</remarks>
        protected int currentLightningBoltPatternDataIndex;

        /// <summary>
        /// Array of lightning-bolt pattern data.
        /// </summary>
        protected LightningBoltPatternData[] lightningBoltPatterns;

        /// <summary>
        /// Whether or not the lightning-bolt is started.
        /// </summary>
        protected bool isLightningBoltStarted;

        /// <summary>
        /// Lightning bolt.
        /// </summary>
        protected LightningBolt lightningBolt;

        
        #region Lighting Bolt exposed
        /// <summary>
        /// Lightning exposed by this SCMeshManager.
        /// </summary>
        /// <remarks>Normally the RenderSystem works with this property automatically.
        /// User has no need to involve this property into implementation.</remarks>
        public LightningBolt LightningBolt
        {
            get { return lightningBolt; }
        }

        /// <summary>
        /// Whether or not the lightning bolt has started.
        /// </summary>
        /// <remarks>RenderSystem will use this property to indicate whether or not to take the time in processing the lightning's properties.</remarks>
        public bool IsLightningBoltStarted
        {
            get { return isLightningBoltStarted; }
        }
        #endregion

        #endregion

        #endregion

        /// <summary>
        /// Create the SCMeshManager from the specified skydom-mesh and clouddome-mesh.
        /// </summary>
        /// <param name="skydome"></param>
        /// <param name="clouddome"></param>
        public SCMeshManager(SCMeshEntity skyDome, SCMeshEntity cloudDome)
        {
            //set the SCMeshEntity
            this.skyDome = skyDome;
            this.cloudDome = cloudDome;

            //initialize
            PreInitialize();
        }

        /// <summary>
        /// Initialize the lighting-flash algorithm's stuff.
        /// </summary>
        private void PreInitializeLightingFlashAlgorithm()
        {
            //create our pattern data
            flashPatterns = new FlashPatternData[5];
            lightningBoltPatterns = new LightningBoltPatternData[5];
            //create the flashy textures
            flashyTextures = new Texture2D[7];
            flashyTextures[0] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy01");
            flashyTextures[1] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy02");
            flashyTextures[2] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy03");
            flashyTextures[3] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy04");
            flashyTextures[4] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy05");
            flashyTextures[5] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy06");
            flashyTextures[6] = Engine.Content.Load<Texture2D>("Base/Textures/SC/SP_ENV_Flashy07");

            //create and set individual flash pattern data
            //Note: The flashy textures set here are tended to be changed later on. So it's not that trivial for setting textur here.
            //conservative flash
            flashPatterns[0] = new FlashPatternData(1.25f, 3, 0.5f, XNAUtil.Random.Next(flashyTextures.Length));
            //high flash
            flashPatterns[1] = new FlashPatternData(0.8f, 5, 0.7f, XNAUtil.Random.Next(flashyTextures.Length));
            //low flash
            flashPatterns[2] = new FlashPatternData(1, 2, 0.1f, XNAUtil.Random.Next(flashyTextures.Length));
            //very low flash
            flashPatterns[3] = new FlashPatternData(2, 2, 0.08f, XNAUtil.Random.Next(flashyTextures.Length));
            flashPatterns[4] = new FlashPatternData(0.75f, 4, 0.65f, XNAUtil.Random.Next(flashyTextures.Length));

            //create and set the individual lightning-bolt pattern data
            lightningBoltPatterns[0] = new LightningBoltPatternData(0.35f, 4, 0.6f);
            lightningBoltPatterns[1] = new LightningBoltPatternData(0.30f, 5, 0.5f);
            lightningBoltPatterns[2] = new LightningBoltPatternData(0.28f, 4, 0.7f);
            lightningBoltPatterns[3] = new LightningBoltPatternData(0.45f, 6, 0.6f);
            lightningBoltPatterns[4] = new LightningBoltPatternData(0.56f, 3, 0.65f);

            //preset the flashy's stuff
            currentFlashState = FlashPatternTimeState.DuringPatternDataLifeActive;
            currentFlashCountingTime = 0f;
            //random the start index of lighting-flash pattern data to be used
            currentFlashPatternDataIndex = XNAUtil.Random.Next(flashPatterns.Length);
            
            //preset the lightning-bolt's stuff
            currentLightningBoltState = LightningBoltPatternTimeState.DuringPatternDataLifeActive;
            currentLightningBoltCountingTime = 0f;
            //random the start index of lightning-bolt pattern data to be used
            currentLightningBoltPatternDataIndex = XNAUtil.Random.Next(lightningBoltPatterns.Length);
            //create the lightning bolt
            LightningDescriptor ld = new LightningDescriptor();
            ld.AnimationFramerate = 9f;
            ld.InteriorColor = Color.White;
            ld.ExteriorColor = Color.WhiteSmoke;
            ld.GlowIntensity = 1f;
            ld.Topology.Clear();
            ld.Topology.Add(LightningSubdivisionOp.JitterAndFork);
            ld.Topology.Add(LightningSubdivisionOp.JitterAndFork);
            ld.Topology.Add(LightningSubdivisionOp.Jitter);
            ld.Topology.Add(LightningSubdivisionOp.Jitter);
            ld.Topology.Add(LightningSubdivisionOp.JitterAndFork);
            ld.Topology.Add(LightningSubdivisionOp.Jitter);
            //ld.Topology.Add(LightningSubdivisionOp.JitterAndFork);
            lightningBolt = new LightningBolt(Vector3.Zero, Vector3.One, ld);
        }

        /// <summary>
        /// Initialize this manager.
        /// </summary>
        public void PreInitialize()
        {
            //both skydome and clouddome will not be taken into light-calculation
            cloudDome.Material.LightOcclusion = 1.0f;
            skyDome.Material.LightOcclusion = 1.0f;

            //set the properties of skydome and clouddome
            cloudDome.Material.TextureCoordU = 3f;
            cloudDome.Material.TextureCoordV = 3f;

            skyDome.Material.TextureCoordU = 3f;
            skyDome.Material.TextureCoordV = 1f;

            //scale it to the global scale
            skyDome.Scale = new Vector3(Engine.SimulationSettings.GlobalScale);
            cloudDome.Scale = skyDome.Scale;

            //first set the timeslot to the wrong value
            timeSlot = -1;

            // set initial ambient light to 1.0
            ambientLight = 1.0f;

            //create the texture render target used to hold the texture from blending-result of base, and target texture for both
            //of skydome and clouddome
            int backBufferWidth = Engine.GraphicsDevice.PresentationParameters.BackBufferWidth;
            int backBufferHeight = Engine.GraphicsDevice.PresentationParameters.BackBufferHeight;

            //set the texture D (as the texture D will not be changed each frame)
            sDTexture = skyDome.TextureSet.TextureD;
            cDTexture = cloudDome.TextureSet.TextureD;

            //set to the GPU
            skyDome.Material.Effect.Parameters["textureD"].SetValue(sDTexture);
            cloudDome.Material.Effect.Parameters["textureD"].SetValue(cDTexture);

            PreInitializeLightingFlashAlgorithm();
        }

        /// <summary>
        /// Update all the related stuff of this SCMeshManager.
        /// 
        /// Update detail
        /// -Determine the current time which what slot is in.
        /// 
        ///     4    #    1
        ///          #
        /// ###################
        ///          #
        ///     3    #    2
        ///     
        /// The image above reflects the usual wall clock.
        /// 
        /// The north pole starts from 0.00 HR, and each slot occupies 6 hours before changing to another slot.
        /// Determining-Algorithm is based on the hour part of current time.
        /// 
        /// Also this method will find the properly base, and target texture and set them to be used in the real simulation.
        /// </summary>
        private void InternalUpdate()
        {
            //find which slot we are in
            //Note: We use the time in the time manager,
            int hours = Engine.TimeManager.CurrentVirtualTime.Hours;

            //the following is the fixed time-slot detail (used the greater-than-equal and less-than scheme)
            //if change is need, then change the following code
            if (hours >= 0 && hours < 6)
            {
                //set the time slot
                timeSlot = 1;

                //set the skydome's texture set
                sBaseTexture = skyDome.TextureSet.TextureA;
                sTargetTexture = skyDome.TextureSet.TextureB;

                //set the clouddome's texture set
                cBaseTexture = cloudDome.TextureSet.TextureA;
                cTargetTexture = cloudDome.TextureSet.TextureB;

                //calculate the total-difference-minutes
                float dTMinutes = hours * 60f + Engine.TimeManager.CurrentVirtualTime.Minutes + 
                    Engine.TimeManager.CurrentVirtualTime.Seconds / 60f + Engine.TimeManager.CurrentVirtualTime.Milliseconds / 60000.0f;

                //calculate the delta time
                dTime = dTMinutes / 360.0f;

                //lerp the sun light's intensity
                ambientLight = MathHelper.Lerp(intensityA, intensityB, dTime);
            }
            else if (hours >= 6 && hours < 12)
            {
                //set the time slot
                timeSlot = 2;

                //set the skydome's texture set
                sBaseTexture = skyDome.TextureSet.TextureB;
                sTargetTexture = skyDome.TextureSet.TextureC;

                //set the clouddome's texture set
                cBaseTexture = cloudDome.TextureSet.TextureB;
                cTargetTexture = cloudDome.TextureSet.TextureC;

                //calculate the total-difference-minutes
                float dTMinutes = (hours - 6f) * 60f + Engine.TimeManager.CurrentVirtualTime.Minutes +
                    Engine.TimeManager.CurrentVirtualTime.Seconds / 60f + Engine.TimeManager.CurrentVirtualTime.Milliseconds / 60000.0f;

                //calculate the delta time
                dTime = dTMinutes / 360.0f;

                //lerp the sun light's intensity
                ambientLight = MathHelper.Lerp(intensityB, intensityC, dTime);
            }
            else if (hours >= 12 && hours < 18)
            {
                //set the time slot
                timeSlot = 3;

                //set the skydome's texture set
                sBaseTexture = skyDome.TextureSet.TextureC;
                sTargetTexture = skyDome.TextureSet.TextureE;

                //set the clouddome's texture set
                cBaseTexture = cloudDome.TextureSet.TextureC;
                cTargetTexture = cloudDome.TextureSet.TextureE;

                //calculate the total-difference-minutes
                float dTMinutes = (hours - 12f) * 60f + Engine.TimeManager.CurrentVirtualTime.Minutes +
                    Engine.TimeManager.CurrentVirtualTime.Seconds / 60f + Engine.TimeManager.CurrentVirtualTime.Milliseconds / 60000.0f;

                //calculate the delta time
                dTime = dTMinutes / 360.0f;

                //lerp the sun light's intensity
                ambientLight = MathHelper.Lerp(intensityC, intensityE, dTime);
            }
            else if(hours >= 18 && hours < 24)
            {
                //set the time slot
                timeSlot = 4;

                //set the skydome's texture set
                sBaseTexture = skyDome.TextureSet.TextureE;
                sTargetTexture = skyDome.TextureSet.TextureA;

                //set the clouddome's texture set
                cBaseTexture = cloudDome.TextureSet.TextureE;
                cTargetTexture = cloudDome.TextureSet.TextureA;

                //calculate the total-difference-minutes
                float dTMinutes = (hours - 18f) * 60f + Engine.TimeManager.CurrentVirtualTime.Minutes +
                    Engine.TimeManager.CurrentVirtualTime.Seconds / 60f + Engine.TimeManager.CurrentVirtualTime.Milliseconds / 60000.0f;

                //calculate the delta time
                dTime = dTMinutes / 360.0f;

                //lerp the sun light's intensity
                ambientLight = MathHelper.Lerp(intensityE, intensityA, dTime);
            }

            //The part of ddelta time can be separated from the normal daytime calculation
            //calculate the delta time of changing into texture D
            //Note: The calculations here are not expensive so we continue doing, thus this benefit us for simple implementation.
            //It's based on engine's time.
            if (isChangeToTextureD_StepUp)
                changingTextureD_countingTime += (float)Engine.TimeManager.VirtualElapsedTime.TotalSeconds;
            else
                changingTextureD_countingTime -= (float)Engine.TimeManager.VirtualElapsedTime.TotalSeconds;

            changingTextureD_dTime = changingTextureD_countingTime / Engine.SimulationSettings.AdvTimeRate.DDuration;

            //clamping it (in GPU we don't clamping it again, so it requires to do it here.)
            if (changingTextureD_dTime < 0.0f)
            {
                changingTextureD_countingTime = 0.0f;
                changingTextureD_dTime = 0.0f;
            }
            else if (changingTextureD_dTime > 1.0f)
            {
                changingTextureD_countingTime = Engine.SimulationSettings.AdvTimeRate.DDuration;
                changingTextureD_dTime = 1.0f;
            }
        }

        /// <summary>
        /// Update the lighting-flash natural algorithm.
        /// </summary>
        /// <remarks>We should rely on the virtual time from time manager, as the sky or cloud should be changed based on the speed of current time
        ///  which is virtual elapsed time in time manager.</remarks>
        private void InternalLightingFlashAlgorithmUpdate()
        {
            //update only when we have completely change into the textureD
            if (changingTextureD_dTime == 1.0f)
            {
                //update based on state
                if (currentFlashState == FlashPatternTimeState.DuringPatternDataLifeActive)
                {
                    if (currentFlashCountingTime > flashPatterns[currentFlashPatternDataIndex].TimeLifeActive)
                    {
                        //reset the counting time
                        currentFlashCountingTime = 0;
                        //change state
                        currentFlashState = FlashPatternTimeState.DuringTimeTilNextPatternData;
                    }
                    else
                    {
                        //increase counting time (based on the virtual time from time manager)
                        currentFlashCountingTime += (float)(Engine.TimeManager.VirtualElapsedTime.TotalSeconds);
                        //calculate the chance whether this cycle, it's time to flash or not
                        if (XNAUtil.Random.NextDouble() < flashPatterns[currentFlashPatternDataIndex].ChanceToFlash)
                            isFlashThisCycle = true;
                        else
                            isFlashThisCycle = false;
                    }
                }
                else if (currentFlashState == FlashPatternTimeState.DuringTimeTilNextPatternData)
                {
                    //clear the sky to the normal texture
                    isFlashThisCycle = false;

                    if (currentFlashCountingTime > flashPatterns[currentFlashPatternDataIndex].TimeTilNextPatternData)
                    {
                        //reset the counting time
                        currentFlashCountingTime = 0;
                        //random the next flash pattern data index
                        currentFlashPatternDataIndex = XNAUtil.Random.Next(flashPatterns.Length);
                        //random the flash texture to be set to the prior randomized flashPattern
                        flashPatterns[currentFlashPatternDataIndex].FlashyTextureIndex = XNAUtil.Random.Next(flashyTextures.Length);
                        //change state
                        currentFlashState = FlashPatternTimeState.DuringPatternDataLifeActive;
                    }
                    else
                    {
                        //increase counting time (based on the virtual time from time manager)
                        currentFlashCountingTime += (float)(Engine.TimeManager.VirtualElapsedTime.TotalSeconds);
                    }
                }
            }
        }

        /// <summary>
        /// Update the lightning-bolt natural algorithm.
        /// </summary>
        /// <remarks>We should rely on the virtual time from time manager, as the sky or cloud should be changed based on the speed of current time
        ///  which is virtual elapsed time in time manager.</remarks>
        private void InternalLightningBoltAlgorithmUpdate(GameTime gameTime)
        {
            //update only when we have completely change into the textureD
            if (changingTextureD_dTime == 1.0f)
            {
                //update based on state
                if (currentLightningBoltState == LightningBoltPatternTimeState.DuringPatternDataLifeActive)
                {
                    if (currentLightningBoltCountingTime > lightningBoltPatterns[currentLightningBoltPatternDataIndex].TimeLifeActive)
                    {
                        //reset the counting time
                        currentLightningBoltCountingTime = 0;
                        //change state
                        currentLightningBoltState = LightningBoltPatternTimeState.DuringTimeTilNextPatternData;
                    }
                    else
                    {
                        //increase counting time (based on the virtual time from time manager)
                        currentLightningBoltCountingTime += (float)(Engine.TimeManager.VirtualElapsedTime.TotalSeconds);
                        //only calculate the chance if currently lightning bolt is not actively drawed
                        if (!isLightningBoltStarted)
                        {
                            //the flag tends to change once each frame. (So no need to update it again if it's false.)
                            if (XNAUtil.Random.NextDouble() < lightningBoltPatterns[currentLightningBoltPatternDataIndex].ChanceToLightning)
                            {
                                isLightningBoltStarted = true;

                                //randomize source point
                                lightningBolt.Source = new Vector3(
                                    XNAUtil.RandomUnitDirection() * 
                                    XNAUtil.RandomBetween(60, Engine.SimulationSettings.LightningPreferredArea.X),
                                    
                                    (float)XNAUtil.Random.NextDouble() * 20f + Engine.SimulationSettings.LightningPreferredArea.Y,
                                    
                                    XNAUtil.RandomUnitDirection() * 
                                    XNAUtil.RandomBetween(60, Engine.SimulationSettings.LightningPreferredArea.Z));

                                //random the destination point
                                lightningBolt.Destination = new Vector3(lightningBolt.Source.X + XNAUtil.RandomUnitDirection() * 10f *
                                    (float)XNAUtil.Random.NextDouble(), 0f,
                                    lightningBolt.Source.Z + XNAUtil.RandomUnitDirection() * 10f * (float)XNAUtil.Random.NextDouble());
                            }
                        }

                        //update the lightning-bolt
                        lightningBolt.Update(gameTime);

                        //update the lightning-bolt only if its state is started.
                        //if (isLightningBoltStarted)
                        //{
                            //random the destination point
                            //lightningBolt.Destination = new Vector3(lightningBolt.Source.X + XNAUtil.RandomUnitDirection() * 10f *
                            //    (float)XNAUtil.Random.NextDouble(), 0f,
                            //    lightningBolt.Source.Z + XNAUtil.RandomUnitDirection() * 10f * (float)XNAUtil.Random.NextDouble());
                            //lightningBolt.Update(gameTime);
                        //}
                    }
                }
                else if (currentLightningBoltState == LightningBoltPatternTimeState.DuringTimeTilNextPatternData)
                {
                    //clear the started-flag of the lightning-bolt
                    isLightningBoltStarted = false;

                    if (currentLightningBoltCountingTime > lightningBoltPatterns[currentLightningBoltPatternDataIndex].TimeTilNextPatternData)
                    {
                        //reset the counting time
                        currentLightningBoltCountingTime = 0;
                        //random the next lightning pattern data index
                        currentLightningBoltPatternDataIndex = XNAUtil.Random.Next(lightningBoltPatterns.Length);
                        //change state
                        currentLightningBoltState = LightningBoltPatternTimeState.DuringPatternDataLifeActive;
                    }
                    else
                    {
                        //increase counting time (based on the virtual time from time manager)
                        currentLightningBoltCountingTime += (float)(Engine.TimeManager.VirtualElapsedTime.TotalSeconds);
                    }
                }
            }
        }

        /// <summary>
        /// Update all the containing entities and its related functionality.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            //update all the related stuff
            InternalUpdate();
            InternalLightingFlashAlgorithmUpdate();
            InternalLightningBoltAlgorithmUpdate(gameTime);

            //update skydome and clouddome
            skyDome.Update(gameTime);
            cloudDome.Update(gameTime);
        }

        /// <summary>
        /// Draw all the containing entities and its related functionality.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Draw(GameTime gameTime)
        {
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = false;
            Engine.GraphicsDevice.RenderState.DepthBufferEnable = false;
            Engine.GraphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;

            //set the delta time, the base, and target texture for skydome
            skyDome.Material.Effect.Parameters["dTime"].SetValue(dTime);
            skyDome.Material.Effect.Parameters["baseTexture"].SetValue(sBaseTexture);
            skyDome.Material.Effect.Parameters["targetTexture"].SetValue(sTargetTexture);
            skyDome.Material.Effect.Parameters["textureD_dTime"].SetValue(changingTextureD_dTime);
            skyDome.Material.Effect.Parameters["isFlash"].SetValue(isFlashThisCycle);
            //bound the bandwidth by updating only when isFlashThisCycle is set this frame.
            if(isFlashThisCycle)
                skyDome.Material.Effect.Parameters["flashyTexture"].SetValue(flashyTextures[flashPatterns[currentFlashPatternDataIndex].FlashyTextureIndex]); 

            skyDome.Draw(gameTime);

            //blend the cloud with sky
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = true;

            //set the delta time, the base, and target texture for clouddome
            cloudDome.Material.Effect.Parameters["dTime"].SetValue(dTime);
            cloudDome.Material.Effect.Parameters["baseTexture"].SetValue(cBaseTexture);
            cloudDome.Material.Effect.Parameters["targetTexture"].SetValue(cTargetTexture);
            cloudDome.Material.Effect.Parameters["textureD_dTime"].SetValue(changingTextureD_dTime);

            //whether the SCMesh's movement will be affected by the advanced of time
            if (Engine.SimulationSettings.IsSCMoveAdvancedTime)
                cloudDome.Material.Effect.Parameters["time"].SetValue((float)(Engine.TimeManager.CurrentVirtualTime.TotalSeconds / 500.0f));
            else
                cloudDome.Material.Effect.Parameters["time"].SetValue((float)(gameTime.TotalGameTime.TotalSeconds / 500.0f));

            cloudDome.Draw(gameTime);

            //restore back the render states
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = false;
            Engine.GraphicsDevice.RenderState.DepthBufferEnable = true;
            Engine.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
        }
    }
}
