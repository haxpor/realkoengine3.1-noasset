﻿#region File Description
//-----------------------------------------------------------------------------
// AirplaneTypeAssetInfoManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RealKoContentShared;

namespace RealKo.Framework.Entity.Type.Manager
{
    /// <summary>
    /// Transportation Object type asset information manager manages and hold the information of the transportation object type in the pool, 
    /// ready for other components to reference to them and used.
    /// </summary>
    public class TransportationObjectInfoManager
    {
        protected Dictionary<uint, TransportationObject> transObjTypes;

        /// <summary>
        /// Create the airplane type asset manager.
        /// </summary>
        public TransportationObjectInfoManager()
        {
            transObjTypes = new Dictionary<uint, TransportationObject>();
        }

        /// <summary>
        /// Add new transportation object into the list.
        /// </summary>
        /// <remarks>The key used for adding new entry is taken from the transportation object's information itself.</remarks>
        public void AddNewTransportationObject(TransportationObject transObj)
        {
            transObjTypes.Add(transObj.ID, transObj);
        }

        /// <summary>
        /// Get the transportation object from the specified id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TransportationObject GetTransportationObjectFrom(uint id)
        {
            return transObjTypes[id];
        }
    }
}
