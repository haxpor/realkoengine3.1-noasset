﻿#region File Description
//-----------------------------------------------------------------------------
// AirportApproachLightMeshEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// Represent the individual of airport's approach light mesh.
    /// </summary>
    /// <remarks>This type of light is specially designed to be used with approach light in the airport model.</remarks>
    public class AirportApproachLightMeshEntity : Entity3D
    {
        protected string modelFilePath;
        protected int meshIndex;
        protected int boneIndex;

        protected Material lm1Material;
        protected Model model;

        protected Blend srcBlend;
        protected Blend destBlend;

        //whether or not the light is turned on
        protected bool isTurnOn;

        #region Variables used in the approach light's algorithm
        // Step concerning
        protected int originalNumberOfStep; //this will be set from the constructor
        protected int step = 0;

        // Time concerning
        protected float delay;  //this will be set from the constructor
        protected float countingTime = 0;

        // Move factor for texture coordinate
        protected float moveFactor; //this will be set from the constructor
        #endregion

        #region Properties

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the mesh index represented this AirportStaticLightMeshEntity.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex; }
        }

        /// <summary>
        /// Get the bone index represented this AirportStaticLightMeshEntity.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }

        /// <summary>
        /// Get the model used by this AirplaneEntity.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        /// <summary>
        /// Get or set the turn on-flag of this airport light.
        /// </summary>
        public bool IsTurnOn
        {
            get { return isTurnOn; }
            set { isTurnOn = value; }
        }

        /// <summary>
        /// Get the source blend used for drawing this mesh.
        /// </summary>
        public Blend SrcBlend
        {
            get { return srcBlend; }
        }

        /// <summary>
        /// Get the destination blend used for drawing this mesh.
        /// </summary>
        public Blend DestBlend
        {
            get { return destBlend; }
        }

        /// <summary>
        /// Number of step used in the approach light's algorithm.
        /// </summary>
        public int NumberOfStep
        {
            get { return originalNumberOfStep; }
        }

        /// <summary>
        /// Delay time between each pair of lights.
        /// </summary>
        public float DelayTime
        {
            get { return delay; }
        }

        /// <summary>
        /// Move factor used in the approach light's algorithm.
        /// </summary>
        public float MoveFactor
        {
            get { return moveFactor; }
        }

        #endregion

        /// <summary>
        /// Create new airport's approach light-mesh entity.
        /// </summary>
        /// <remarks>This type of light is speicially designed for approach light in the airport.</remarks>
        /// <param name="id"></param>
        /// <param name="lightMesh"></param>
        public AirportApproachLightMeshEntity(AirportApproachLightMesh lightMesh)
            : base(lightMesh.ID, lightMesh.TypeID, lightMesh.Name)
        {
            //set the properties to be used in the approach light's algorithm
            originalNumberOfStep = lightMesh.NumberOfStep;
            delay = lightMesh.DelayTime;
            moveFactor = lightMesh.MoveFactor;

            //setup all properties
            modelFilePath = lightMesh.ModelFilePath;
            meshIndex = lightMesh.MeshIndex;
            boneIndex = lightMesh.BoneIndex;
            srcBlend = XNAUtil.GetBlendCodeFrom(lightMesh.SrcBlend);
            destBlend = XNAUtil.GetBlendCodeFrom(lightMesh.DestBlend);

            //load all the assets
            //if the model is loaded multiple times then the content manager will return the reference to the same one.
            model = Engine.Content.Load<Model>(modelFilePath);

            //get our original bounding sphere
            originalBoundingSphere = model.Meshes[meshIndex].BoundingSphere;

            //set the scale accordingly (just try this value out)
            scale *= Engine.SimulationSettings.GlobalScale;

            //create the alpha-mesh material
            //LM1
            lm1Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/AirportAlphaLM1ApproachRender"));
            lm1Material.CacheOriginalBoneTransforms(model);
            //set the texture
            lm1Material.DiffuseTexture = lightMesh.LightTexture;
        }

        /// <summary>
        /// Update this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the world matrix
                world = Matrix.CreateScale(scale) *
                        Matrix.CreateTranslation(position) *
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
            }
        }

        /// <summary>
        /// Draw this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                if (isTurnOn)
                {
                    //draw the light mesh
                    Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    lm1Material.Effect.Parameters["step"].SetValue(step);
                    lm1Material.Effect.Parameters["moveFactor"].SetValue(moveFactor);
                    lm1Material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);

                    //increase the counting time by the gameTime's total seconds (accurates in milliseconds.)
                    countingTime += (float)(Engine.TimeManager.VirtualElapsedTime.TotalSeconds);
                    if (countingTime > delay)
                    {
                        //increase and round the step for this approach light mesh
                        step++;
                        step %= originalNumberOfStep + 1;

                        //reset the counting time
                        countingTime = 0.0f;
                    }
                }
            }
        }
    }
}
