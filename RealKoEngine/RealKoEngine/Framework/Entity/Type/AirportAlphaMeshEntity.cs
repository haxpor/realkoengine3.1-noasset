﻿#region File Description
//-----------------------------------------------------------------------------
// AirportAlphaMeshEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    public class AirportAlphaMeshEntity : Entity3D
    {
        #region Nested properties signatures

        /// <summary>
        /// Texture set for this AirportMeshEntity.
        /// </summary>
        /// <remarks>This class is designed to be consistent with the AirportMesh.TextureSet structure.
        /// This class will be updated follow that one.
        /// 
        /// This is the reduced version from the original one found in AirportOpaqueMeshEntity class.</remarks>
        public class AirportMeshTextureSet
        {
            public Texture2D Diffuse;

            public AirportMeshTextureSet(AirportAlphaMesh.TextureSet textureSet)
            {
                //load all the specified textures in texture set
                //also set the file path of each texture into texture's tag
                if (textureSet.Diffuse != "null")
                {
                    Diffuse = Engine.Content.Load<Texture2D>(textureSet.Diffuse);
                    Diffuse.Tag = (string)textureSet.Diffuse;
                }
            }
        }

        #endregion

        #region Associated variables for changing textureset

        /// <summary>
        /// Information of textureset code of base and target textureset.
        /// </summary>
        /// <remarks>Refer to the document for the list of code available.
        /// These two variables will be set externally, and will be taken into effect in the Update section of this entity.</remarks>
        protected TextureSetCode baseTextureSetCode;
        protected TextureSetCode targetTextureSetCode;

        #region Texturese code properties

        /// <summary>
        /// Get the base textureset code.
        /// </summary>
        public TextureSetCode BaseTextureSetCode
        {
            get { return baseTextureSetCode; }
        }

        /// <summary>
        /// Get or set the textureset code for target textureset code.
        /// </summary>
        /// <remarks>By changing this property, then it will affect the update section accordingly.
        /// This method also set the targetTexture automatically.</remarks>
        public TextureSetCode TargetTextureSetCode
        {
            get { return targetTextureSetCode; }
            set
            {
                //if the value to be set to targetTextureSetCode is not the same as before then reset the dtTime, and dtCountingTime
                if (targetTextureSetCode != value)
                {
                    dtTime = 0;
                    dtCountingTime = 0;
                }

                //only set the targetTextureSetCode only if this AirportOpaqueMeshEntity has that corresponding textureset's diffuse texture.
                switch (value)
                {
                    case TextureSetCode.DayTextureSet:
                        //all mesh must at least have the diffuse texture
                        if (dayTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                    case TextureSetCode.RainingTextureSet:
                        if (rainingTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = rainingTextureSet.Diffuse;
                        }
                            //if not, then we set the texture back to the dayTextureSet
                        else
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                    case TextureSetCode.SnowTextureSet:
                        if (snowTextureSet.Diffuse != null)
                        {
                            targetTextureSetCode = value;
                            targetTexture = snowTextureSet.Diffuse;
                        }
                            //if not, then we set the texture back to the dayTextureSet
                        else
                        {
                            targetTextureSetCode = value;
                            targetTexture = dayTextureSet.Diffuse;
                        }
                        break;
                }
            }
        }

        #endregion

        /// <summary>
        /// Texture of both base and target texture.
        /// </summary>
        /// <remarks>The texture here is diffuse texture as we don't want to blend another type of texture along the way.
        /// Only the diffuse texture is taken into account here.</remarks>
        protected Texture2D baseTexture;
        protected Texture2D targetTexture;

        /// <summary>
        /// Counting time used for calculating the dtTime.
        /// </summary>
        protected float dtCountingTime;

        /// <summary>
        /// Delta-Textureset time.
        /// </summary>
        /// <remarks>This variable will be used for calculating the blending between two textures.</remarks>
        protected float dtTime;

        #endregion

        protected string modelFilePath;
        protected int meshIndex;
        protected int boneIndex;

        protected AirportMeshTextureSet dayTextureSet;
        protected AirportMeshTextureSet rainingTextureSet;
        protected AirportMeshTextureSet snowTextureSet;

        protected Material material;
        protected Model model;

        protected Blend srcBlend;
        protected Blend destBlend;

        #region Properties

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the mesh index represented this AirportMesh.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex; }
        }

        /// <summary>
        /// Get the bone index represented this AirportMesh.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }

        /// <summary>
        /// Get the day texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet DayTextureSet
        {
            get { return dayTextureSet; }
        }

        /// <summary>
        /// Get the raining texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet RainingTextureSet
        {
            get { return rainingTextureSet; }
        }

        /// <summary>
        /// Get the snow texture set of this AirportMesh.
        /// </summary>
        public AirportMeshTextureSet SnowTextureSet
        {
            get { return snowTextureSet; }
        }

        /// <summary>
        /// Get the material used by this AirportMesh.
        /// </summary>
        public Material Material
        {
            get { return material; }
        }

        /// <summary>
        /// Get the model used by this AirportMesh.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        /// <summary>
        /// Get the source blend used for drawing this mesh.
        /// </summary>
        public Blend SrcBlend
        {
            get { return srcBlend; }
        }

        /// <summary>
        /// Get the destination blend used for drawing this mesh.
        /// </summary>
        public Blend DestBlend
        {
            get { return destBlend; }
        }

        #endregion

        /// <summary>
        /// Create the airport mesh entity with the neccessary information from AirportMesh.
        /// </summary>
        /// <param name="amInfo"></param>
        public AirportAlphaMeshEntity(AirportAlphaMesh amInfo)
            : base(amInfo.ID, amInfo.TypeID, amInfo.Name)
        {
            //setup all properties
            modelFilePath = amInfo.ModelFilePath;
            meshIndex = amInfo.MeshIndex;
            boneIndex = amInfo.BoneIndex;

            //load all the associated texture sets
            dayTextureSet = new AirportMeshTextureSet(amInfo.DayTextureSetFilePath);
            rainingTextureSet = new AirportMeshTextureSet(amInfo.RainingTextureSetFilePath);
            snowTextureSet = new AirportMeshTextureSet(amInfo.SnowTextureSetFilePath);

            srcBlend = XNAUtil.GetBlendCodeFrom(amInfo.SrcBlend);
            destBlend = XNAUtil.GetBlendCodeFrom(amInfo.DestBlend);

            //load the model and set its bounding sphere
            model = Engine.Content.Load<Model>(amInfo.ModelFilePath);
            originalBoundingSphere = model.Meshes[meshIndex].BoundingSphere;

            //create the material, it's the drawer for this mesh
            //Note: The effect of the engine's render system is already load.
            // This effect is the same as the one used by scene class.
            material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/AirportAlphaMeshRender"));
            material.CacheOriginalBoneTransforms(model);
            //material.SpecularIntensity = 0.05f;

            //set the default texture set to diffuse
            material.DiffuseTexture = (string)dayTextureSet.Diffuse.Tag;

            //apply the global scale
            scale *= Engine.SimulationSettings.GlobalScale;

            //set the stuff of changing the textureset as initial stage
            //set the state code to day-textureset-code as default for both base and target textureSet code.
            baseTextureSetCode = TextureSetCode.DayTextureSet;
            targetTextureSetCode = TextureSetCode.DayTextureSet;

            //set both base and target textureset texture to day textureset.
            //Note: If we changed the code for base, and target textureset code, then we must manually change the texture for each of them too.
            baseTexture = dayTextureSet.Diffuse;
            targetTexture = dayTextureSet.Diffuse;

            //set the dtCountingTime, and dttime to zero
            //Note: This is the enhanced version as it is designed for step-up calculation.
            dtCountingTime = 0;
            dtTime = 0;
        }

        /// <summary>
        /// Update the changing of textureset its related stuff.
        /// </summary>
        private void UpdateTextureSet(GameTime gameTime)
        {
            //make any change if only if the baseTextureSetCode is not the same as targetTexureSetCode
            //the targetTextureSetCode will be set or modified externally.
            //Note: The following "if-statement" will limit the calculation load, by process only when the changing need to be performed.
            // If it's done then no need to do anything.
            if (baseTextureSetCode != targetTextureSetCode)
            {
                //**the setting of targetTextureSetCode, and targetTexture already be done externally before entering into this point.

                //increase the dtCountingTime (it's based on the engine's time.)
                dtCountingTime += (float)Engine.TimeManager.VirtualElapsedTime.TotalSeconds;

                //calculate the dtTime (correspond to the changing-to-textureD duration, see the document and config file for more information.)
                dtTime = dtCountingTime / Engine.SimulationSettings.AdvTimeRate.DDuration;

                //clamping it (in GPU we don't clamping it again, so it requires to do it here.)
                //this is the stepup process, so we must reponse to the end point properly
                if (dtTime > 1.0f)
                {
                    //set the baseTextureSetCode to targetTextureSetCode
                    baseTextureSetCode = targetTextureSetCode;
                    //set baseTexture to targetTexture
                    baseTexture = targetTexture;
                    //reset dtTime to zero
                    dtTime = 0;
                    //reset dtCountingTime to zero
                    dtCountingTime = 0;
                }
            }
        }

        /// <summary>
        /// Update this AirportMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the world matrix
                world = Matrix.CreateScale(scale) *
                         Matrix.CreateTranslation(position) *
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);

                //update the changing of textureset and its related stuff
                UpdateTextureSet(gameTime);
            }
        }

        /// <summary>
        /// Draw this AirportMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                //set the parameters inside the effect file accordingly
                material.Effect.Parameters["dtTime"].SetValue(dtTime);
                material.Effect.Parameters["baseTexture"].SetValue(baseTexture);
                material.Effect.Parameters["targetTexture"].SetValue(targetTexture);

                material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
            }
        }
    }
}
