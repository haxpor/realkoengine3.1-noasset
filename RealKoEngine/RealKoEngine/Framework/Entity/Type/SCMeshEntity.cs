﻿#region File Description
//-----------------------------------------------------------------------------
// SCMeshEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKo.Framework.Exception;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    public class SCMeshEntity: Entity3D
    {
        #region Nested properties signatures

        /// <summary>
        /// Texture set for this SCMeshEntity.
        /// </summary>
        /// <remarks>This class is designed to be consistent with the SCMesh.SCTextureSet structure.
        /// This class will be updated follow that one.</remarks>
        public class SCMeshTextureSet
        {
            public Texture2D TextureA;
            public Texture2D TextureB;
            public Texture2D TextureC;
            public Texture2D TextureD;
            public Texture2D TextureE;

            /// <summary>
            /// Create the SCMesh's texture set by loading all the specified textures with Engine's ContentManager.
            /// </summary>
            /// <param name="textureSet"></param>
            public SCMeshTextureSet(SCMesh.SCTextureSet textureSet)
            {
                //load all the specified textures in texture set
                //also set the file path of each texture into texture's tag
                TextureA = Engine.Content.Load<Texture2D>(textureSet.TextureA);
                TextureA.Tag = (string)textureSet.TextureA;

                TextureB = Engine.Content.Load<Texture2D>(textureSet.TextureB);
                TextureB.Tag = (string)textureSet.TextureB;

                TextureC = Engine.Content.Load<Texture2D>(textureSet.TextureC);
                TextureC.Tag = (string)textureSet.TextureC;

                TextureD = Engine.Content.Load<Texture2D>(textureSet.TextureD);
                TextureD.Tag = (string)textureSet.TextureD;

                TextureE = Engine.Content.Load<Texture2D>(textureSet.TextureE);
                TextureE.Tag = (string)textureSet.TextureE;
            }
        }

        #endregion

        protected string modelFilePath;
        protected int meshIndex;
        protected int boneIndex;
        protected SCMeshTextureSet textureSet;

        protected Material material;
        protected Model model;

        #region Properties

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the mesh index represented this SCMeshEntity.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex; }
        }

        /// <summary>
        /// Get the bone index represented this SCMeshEntity.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }

        /// <summary>
        /// Get the texture set that this SCMeshEntity used.
        /// </summary>
        public SCMeshTextureSet TextureSet
        {
            get { return textureSet; }
        }

        /// <summary>
        /// Get the material used by this SCMeshEntity.
        /// </summary>
        public Material Material
        {
            get { return material; }
        }

        /// <summary>
        /// Get the model used by this SCMeshEntity.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        #region Override change textureset

        /// <summary>
        /// Get or set the TextureA used by this SCMeshEntity.
        /// </summary>
        public string TextureA
        {
            get { return (string)textureSet.TextureA.Tag; }
            set
            {
                if (value != null && value != "" && value != "null")
                {
                    textureSet.TextureA = Engine.Content.Load<Texture2D>(value);
                    textureSet.TextureA.Tag = value;
                }
            }
        }

        /// <summary>
        /// Get or set the TextureB used by this SCMeshEntity.
        /// </summary>
        public string TextureB
        {
            get { return (string)textureSet.TextureB.Tag; }
            set
            {
                if (value != null && value != "" && value != "null")
                {
                    textureSet.TextureB = Engine.Content.Load<Texture2D>(value);
                    textureSet.TextureB.Tag = value;
                }
            }
        }

        /// <summary>
        /// Get or set the TextureC used by this SCMeshEntity.
        /// </summary>
        public string TextureC
        {
            get { return (string)textureSet.TextureC.Tag; }
            set
            {
                if (value != null && value != "" && value != "null")
                {
                    textureSet.TextureC = Engine.Content.Load<Texture2D>(value);
                    textureSet.TextureC.Tag = value;
                }
            }
        }

        /// <summary>
        /// Get or set the TextureD used by this SCMeshEntity.
        /// </summary>
        public string TextureD
        {
            get { return (string)textureSet.TextureD.Tag; }
            set
            {
                if (value != null && value != "" && value != "null")
                {
                    textureSet.TextureD = Engine.Content.Load<Texture2D>(value);
                    textureSet.TextureD.Tag = value;
                }
            }
        }

        /// <summary>
        /// Get or set the TextureE used by this SCMeshEntity.
        /// </summary>
        public string TextureE
        {
            get { return (string)textureSet.TextureE.Tag; }
            set
            {
                if (value != null && value != "" && value != "null")
                {
                    textureSet.TextureE = Engine.Content.Load<Texture2D>(value);
                    textureSet.TextureE.Tag = value;
                }
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Create the SCMesh (Skydome and clouddome mesh) entity.
        /// </summary>
        /// <param name="scMeshInfo">Information required to created this SCMeshEntity.</param>
        public SCMeshEntity(SCMesh scMeshInfo)
            : base(scMeshInfo.ID, scMeshInfo.TypeID, scMeshInfo.Name)
        {
            //setup all properties
            modelFilePath = scMeshInfo.ModelFilePath;
            meshIndex = scMeshInfo.MeshIndex;
            boneIndex = scMeshInfo.BoneIndex;
            textureSet = new SCMeshTextureSet(scMeshInfo.TextureSetFilePath);
            
            //load all the assets
            //if the model is loaded multiple times then the content manager will return the reference to the same one.
            model = Engine.Content.Load<Model>(modelFilePath);
            //set the bounding sphere cover this mesh
            originalBoundingSphere = model.Meshes[meshIndex].BoundingSphere;

            //create the material, it's the drawer for this mesh
            //for SCMesh uses the separate effect file for each one, so we need to check its id first
            //(check the ID paper for reference, the following code is fixed with entity's id.)
            if (scMeshInfo.ID == EntityIDSpecification.Entity.Airport.SKYDOME_MESH_ENTITY_ID)
                material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/SMeshBlendTex"));
            else if (scMeshInfo.ID == EntityIDSpecification.Entity.Airport.CLOUDDOME_MESH_ENTITY_ID)
                material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/CMeshBlendTex"));
            else
                //error occur
                throw new SCMeshNotFoundException();

            // cache bone transforms
            material.CacheOriginalBoneTransforms(model);

            //initially set the default texture to textureA
            material.DiffuseTexture = (string)textureSet.TextureA.Tag;
        }

        /// <summary>
        /// Update this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the world matrix
                world =  Matrix.CreateScale(scale) *
                         Matrix.CreateTranslation(position) *
                         Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
            }
        }

        /// <summary>
        /// Draw this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                //draw the mesh
                material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
            }
        }
    }
}
