﻿#region File Description
//-----------------------------------------------------------------------------
// AirportStaticLightMeshEntity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework.Graphics;
using RealKo.Framework.Helper.Util;
using RealKoContentShared;

namespace RealKo.Framework.Entity
{
    /// <summary>
    /// Represent the individual of airport's static light mesh.
    /// </summary>
    public class AirportStaticLightMeshEntity : Entity3D
    {
        protected string modelFilePath;
        protected int meshIndex;
        protected int boneIndex;

        protected Material lm1Material;
        protected Model model;

        protected Blend srcBlend;
        protected Blend destBlend;

        //whether or not the light is turned on
        protected bool isTurnOn;

        #region Properties

        /// <summary>
        /// Get the model file path string.
        /// </summary>
        public string ModelFilePath
        {
            get { return modelFilePath; }
        }

        /// <summary>
        /// Get the mesh index represented this AirportStaticLightMeshEntity.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex; }
        }

        /// <summary>
        /// Get the bone index represented this AirportStaticLightMeshEntity.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }

        /// <summary>
        /// Get the model used by this AirplaneEntity.
        /// </summary>
        /// <remarks>If the multiple-load occurred, then the same reference to model will be returned.</remarks>
        public Model Model
        {
            get { return model; }
        }

        /// <summary>
        /// Get or set the turn on-flag of this airport light.
        /// </summary>
        public bool IsTurnOn
        {
            get { return isTurnOn; }
            set { isTurnOn = value; }
        }

        /// <summary>
        /// Get the source blend used for drawing this mesh.
        /// </summary>
        public Blend SrcBlend
        {
            get { return srcBlend; }
        }

        /// <summary>
        /// Get the destination blend used for drawing this mesh.
        /// </summary>
        public Blend DestBlend
        {
            get { return destBlend; }
        }

        #endregion

        /// <summary>
        /// Create new airport's static light-mesh entity.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lightMesh"></param>
        public AirportStaticLightMeshEntity(AirportLightMesh lightMesh)
            : base(lightMesh.ID, lightMesh.TypeID, lightMesh.Name)
        {
            //setup all properties
            modelFilePath = lightMesh.ModelFilePath;
            meshIndex = lightMesh.MeshIndex;
            boneIndex = lightMesh.BoneIndex;
            srcBlend = XNAUtil.GetBlendCodeFrom(lightMesh.SrcBlend);
            destBlend = XNAUtil.GetBlendCodeFrom(lightMesh.DestBlend);

            //load all the assets
            //if the model is loaded multiple times then the content manager will return the reference to the same one.
            model = Engine.Content.Load<Model>(modelFilePath);

            //get our original bounding sphere
            originalBoundingSphere = model.Meshes[meshIndex].BoundingSphere;

            //set the scale accordingly (just try this value out)
            scale *= Engine.SimulationSettings.GlobalScale;

            //create the alpha-mesh material
            //LM1
            lm1Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/AirportAlphaLM1Render"));
            lm1Material.CacheOriginalBoneTransforms(model);
            //set the texture
            lm1Material.DiffuseTexture = lightMesh.LightTexture;
        }

        /// <summary>
        /// Update this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                //calculate the world matrix
                world = Matrix.CreateScale(scale) *
                        Matrix.CreateTranslation(position) *
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

                //update its bounding sphere too
                boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
            }
        }

        /// <summary>
        /// Draw this SCMesh.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                if (isTurnOn)
                {
                    //draw the light mesh
                    Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    lm1Material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
                }
            }
        }
    }
}
