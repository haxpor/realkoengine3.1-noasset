﻿#region File Description
//-----------------------------------------------------------------------------
// Entity.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Entity
{
    public class Entity: RK_IBasicEntity, RK_IUpdateable, RK_IDrawable
    {
        protected uint id;
        protected uint typeId;
        protected string name;
        protected bool active;

        #region Properties implemented from interface

        /// <summary>
        /// Get or set the id of this entity.
        /// </summary>
        /// <remarks>This is the readonly field.</remarks>
        public uint ID
        {
            get { return id; }
        }

        /// <summary>
        /// Get or set the type id of this entity.
        /// </summary>
        /// <remarks>This is the readonly field.</remarks>
        public uint TypeId
        {
            get { return typeId; }
        }

        /// <summary>
        /// Get or set the name of this entity.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Get or set the activeness of this entity.
        /// Unactive entity will not be updated or drawed.
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        #endregion

        /// <summary>
        /// Create new entity.
        /// </summary>
        /// <remarks>Refer to the ID-List reference document that came with the engine.</remarks>
        /// <param name="id">ID of the new entity.</param>
        /// <param name="typeId"></param>
        /// <param name="name"></param>
        public Entity(uint id, uint typeId, string name)
        {
            this.id = id;
            this.typeId = typeId;
            this.name = name;
            this.active = true;
        }

        /// <summary>
        /// Update the entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            //do nothing here
        }

        /// <summary>
        /// Draw the entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Draw(GameTime gameTime)
        {
            //do nothing here
        }
    }
}
