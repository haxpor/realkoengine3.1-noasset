﻿#region File Description
//-----------------------------------------------------------------------------
// Engine.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Entity.Type.Manager;
using RealKo.Framework.Service;
using RealKo.Framework.Time;
using RealKo.Framework.Helper;
using RealKo.Framework.Helper.Util;
using RealKo.Framework.State;
using RealKoContentShared;

namespace RealKo.Framework
{
    public static class Engine
    {
        #region DTTRt specification

        /// <summary>
        /// Number indicates which dttRt to be used in the rendering process, if draw-to-texture is enabled.
        /// </summary>
        public enum DTTRtNum
        {
            NONE,
            ONE,
            TWO,
            THREE,
            FOUR
        }

        #endregion

        #region Basic stuff

        /// <summary>
        /// GraphicsDevice that the engine is using.
        /// </summary>
        /// <remarks>If integrated this engine with win-form app, then user need to create the graphics device manually and set it to this field.</remarks>
        public static GraphicsDevice GraphicsDevice;

        /// <summary>
        /// SpriteBatch that the engine is using.
        /// </summary>
        public static SpriteBatch SpriteBatch;

        /// <summary>
        /// Content manager of this engine.
        /// </summary>
        public static ContentManager Content;

        /// <summary>
        /// Services provider of this engine.
        /// </summary>
        public static RKServiceContainer Services;

        /// <summary>
        /// All information about simulation settings.
        /// </summary>
        public static DerivedSimulationSettings SimulationSettings;

        /// <summary>
        /// Base material that this engine used.
        /// </summary>
        public static Effect BaseMaterial;

        /// <summary>
        /// Current state infomation of the engine.
        /// </summary>
        public static GameState State;

        #endregion

        #region Main Component System

        /// <summary>
        /// Deferred shading rendering system.
        /// </summary>
        public static DeferredShadingRenderer RenderSystem;

        /// <summary>
        /// Deferred screen manager of this engine.
        /// </summary>
        public static ScreenManager DeferredScreenManager;
        /// <summary>
        /// Text2 screen manager of this engine.
        /// </summary>
        public static ScreenManager Text2ScreenManager;

        /// <summary>
        /// Entity manager of this engine.
        /// </summary>
        public static EntityManager EntityManager;

        /// <summary>
        /// Camera manager of this engine.
        /// </summary>
        public static CameraManager CameraManager;

        /// <summary>
        /// Time manager of this engine.
        /// </summary>
        /// <remarks>This component is original designed to work with SCMeshManager, but can be adapted to work with other components.</remarks>
        public static TimeManager TimeManager;

        #endregion

        #region Misc components

        public static FogSettingManager FogSettingManager;

        #endregion Misc components

        /// <summary>
        /// The current GameTime.
        /// </summary>
        public static GameTime GameTime;

        /// <summary>
        /// Whether the Engine has been initialized yet.
        /// </summary>
        public static bool IsInitialized = false;

        #region Status of loading config files

        /// <summary>
        /// Whether the Engine has loaded the SCMesh config file.
        /// </summary>
        public static bool IsSCMeshConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the Airport's opaque meshes-config file.
        /// </summary>
        public static bool IsAirportOpaqueMeshesConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the Airport's alpha meshes-config file.
        /// </summary>
        public static bool IsAirportAlphaMeshesConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the Airport's light meshes-config file.
        /// </summary>
        public static bool IsAirportLightMeshesConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the simulation's settings config file.
        /// </summary>
        public static bool IsSimulationSettingsConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the transportation object config file.
        /// </summary>
        public static bool IsTransportationObjectConfigLoaded = false;

        /// <summary>
        /// Whether the Engine has loaded the point light source config file.
        /// </summary>
        public static bool IsPointLightSourceConfigLoaded = false;

        #endregion

        #region Entity Manager

        /// <summary>
        /// Manager of skydome and clouddome.
        /// This component will control the effect of daytime.
        /// </summary>
        public static SCMeshManager SCMeshManager;

        /// <summary>
        /// Manager of transportation object types.
        /// This component just manages, and hold the information about airplane type.
        /// </summary>
        public static TransportationObjectInfoManager TransportationObjectInfoManager;

        #endregion

        #region Draw-to-Texture (DTT) stuffs

        /// <summary>
        /// DTTRt 1.
        /// </summary>
        public static RenderTarget2D DTTRt1;
        /// <summary>
        /// DTTRt 2.
        /// </summary>
        public static RenderTarget2D DTTRt2;
        /// <summary>
        /// DTTRt 3.
        /// </summary>
        public static RenderTarget2D DTTRt3;
        /// <summary>
        /// DTTRt 4.
        /// </summary>
        public static RenderTarget2D DTTRt4;

        /// <summary>
        /// Current number of code that DTTRt is active in draw-call.
        /// </summary>
        public static DTTRtNum CurrentDTTRtActive;

        #endregion

        #region Airport's position and rotation
        // Do not modified this value, use offsetAirport() method instead.
        public static Vector3 AirportOffseted_Position;
        // Do not modified this value, use offsetAirport() method instead.
        public static Vector3 AirportOffseted_Rotation;
        #endregion

        /// <summary>
        /// Initializes the engine.
        /// The path must be in absolute path.
        /// </summary>
        /// <param name="GraphicsDeviceService">GraphicsDevice.</param>
        /// <param name="simulationConfigFilePath">Simulation config file path.</param>
        /// <param name="scMeshesConfigFilePath">Skydome/Clouddome-mesh config file path.</param>
        /// <param name="airportOpaqueMeshesConfigFilePath">Airport-opaque-mesh config file path.</param>
        /// <param name="airportAlphaMeshesConfigFilePath">Airport-alpha-mesh config file.</param>
        /// <param name="airportLightMeshesConfigFilePath">Airport-light-mesh config file.</param>
        /// <param name="airportApproachLightMeshesConfigFilePath">Airport-approach-light-mesh config file.</param>
        /// <param name="airplaneTypeAssetConfigFilePath">Airplane-type-asset config file path.</param>
        /// <param name="pointLightSourceConfigFilePath">Point light source config file path.</param>
        /// <param name="transportationConfigFilePath">Transportation config file path.</param>
        /// <remarks>Accepts null for empty or none config file for particular ones.</remarks>
        public static void SetupEngine(IGraphicsDeviceService GraphicsDeviceService,
            string simulationConfigFilePath,
            string scMeshesConfigFilePath,
            string airportOpaqueMeshesConfigFilePath,
            string airportAlphaMeshesConfigFilePath,
            string airportLightMeshesConfigFilePath,
            string airportApproachLightMeshesConfigFilePath,
            string pointLightSourceConfigFilePath,
            string transportationConfigFilePath)
        {
            // Setup the GraphicsDevice and SpriteBatch
            Engine.GraphicsDevice = GraphicsDeviceService.GraphicsDevice;
            Engine.SpriteBatch = new SpriteBatch(GraphicsDeviceService.GraphicsDevice);

            //create the services provider
            Engine.Services = new RKServiceContainer();
            Engine.Services.AddService(typeof(IGraphicsDeviceService), GraphicsDeviceService);
            Engine.Content = new ContentManager(Engine.Services, "");
            //load the default base material
            Engine.BaseMaterial = Engine.Content.Load<Effect>("Base/Effects/BasicMaterial");

            //set up the dttRts
            SetupDTTRts();

            //initilize the state
            //NOTE: User need to set the state manually after call to SetupEngine()
            State = new GameState();

            //load the simulation settings config file
            //this must be done before TimeManager will be created
            LoadSimulationSettingsConfig(simulationConfigFilePath);

            //initialize all the main components of the engine
            //although they're static, but for the first time we must create its instance.
            RenderSystem = new DeferredShadingRenderer(Engine.GraphicsDevice, Engine.Content, Engine.SpriteBatch);
            DeferredScreenManager = new ScreenManager();
            Text2ScreenManager = new ScreenManager();
            EntityManager = new EntityManager();
            CameraManager = new CameraManager();
            TimeManager = new TimeManager(SimulationSettings.IsUpdateOnline);

            // initialize all misc components of the engine
            FogSettingManager = new FogSettingManager(Engine.BaseMaterial);
            FogSettingManager.InitDefault();

            //SCMeshConfig and AirportMeshes config are required to be loaded after RenderSystem, and EntityManager are loaded.
            LoadSCMeshConfig(scMeshesConfigFilePath);
            LoadAirportMeshesConfig(airportOpaqueMeshesConfigFilePath, airportAlphaMeshesConfigFilePath, airportLightMeshesConfigFilePath,
                airportApproachLightMeshesConfigFilePath);
            LoadPointLightSourceConfig(pointLightSourceConfigFilePath);
            LoadTransportationObjectConfig(transportationConfigFilePath);

            // ready to do offseting
            offsetAirport(Engine.SimulationSettings.AirportModelOffsetPosition, Engine.SimulationSettings.AirportOffsetRotation);

            //set that the engine is initialized successfully
            Engine.IsInitialized = true;
        }

        /// <summary>
        /// Setup all the draw-to-texture render targets, dttRts.
        /// </summary>
        /// <remarks>This method must be called after the graphics device has been initialized, and created.</remarks>
        private static void SetupDTTRts()
        {
            //they're all preserved usage as we will route it again for drawing the alpha stuff on top of it.
            //Note: The resolution for DTTRt is scale down for 4 times (2 times down for width, and 2 times down for height)
            // But only for DTTRt1 is preserved for main drawing buffer (same width and height)
            DTTRt1 = new RenderTarget2D(Engine.GraphicsDevice, Engine.GraphicsDevice.PresentationParameters.BackBufferWidth,
                Engine.GraphicsDevice.PresentationParameters.BackBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
            DTTRt2 = new RenderTarget2D(Engine.GraphicsDevice, Engine.GraphicsDevice.PresentationParameters.BackBufferWidth / 2,
                Engine.GraphicsDevice.PresentationParameters.BackBufferHeight / 2, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
            DTTRt3 = new RenderTarget2D(Engine.GraphicsDevice, Engine.GraphicsDevice.PresentationParameters.BackBufferWidth / 2,
                Engine.GraphicsDevice.PresentationParameters.BackBufferHeight / 2, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
            DTTRt4 = new RenderTarget2D(Engine.GraphicsDevice, Engine.GraphicsDevice.PresentationParameters.BackBufferWidth / 2,
                Engine.GraphicsDevice.PresentationParameters.BackBufferHeight / 2, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
        }

        /// <summary>
        /// Pre-offset the position and rotation angle of all the airport's meshes.
        /// Note that this method will be taken into effect only if the engine has been initialized first.
        /// </summary>
        /// <remarks>This method will set the position value of each mesh in the airport model, and let each mesh's update method takes care
        /// when the update time has come.
        /// 
        /// Also this method is extensive on loading thus do not call this method too often. It should be called only once in the initialization
        /// stage.</remarks>
        /// <param name="offsetPosition">Position in vector3 to offset all the meshes of airport model.</param>
        private static void offsetAirport(Vector3 offsetPosition, Vector3 offsetRotation)
        {
            // scale and convert to radians
            offsetPosition *= Engine.SimulationSettings.GlobalScale;
            offsetRotation = XNAUtil.ToRadians(ref offsetRotation);

            //save it to our airport's position and rotation first
            AirportOffseted_Position = -offsetPosition;
            AirportOffseted_Rotation = -offsetRotation;

            if (IsSCMeshConfigLoaded)
            {
                //sc meshes (must be existed both)
                SCMeshEntity skyDome = (SCMeshEntity)Engine.EntityManager.GetEntityFrom(7000);
                skyDome.Position = AirportOffseted_Position;
                skyDome.Rotation = AirportOffseted_Rotation;
                SCMeshEntity cloudDome = (SCMeshEntity)Engine.EntityManager.GetEntityFrom(7001);
                cloudDome.Position = AirportOffseted_Position;
                cloudDome.Rotation = AirportOffseted_Rotation;
            }

            //airport alpha meshes
            foreach (AirportAlphaMeshEntity mesh in Engine.EntityManager.GetAirportAlphaEntityList())
            {
                mesh.Position = AirportOffseted_Position;
                mesh.Rotation = AirportOffseted_Rotation;
            }

            //airport opaque meshes
            foreach (AirportOpaqueMeshEntity mesh in Engine.EntityManager.GetAirportOpaqueEntityList())
            {
                mesh.Position = AirportOffseted_Position;
                mesh.Rotation = AirportOffseted_Rotation;
            }

            //airport approach light meshes
            foreach (AirportApproachLightMeshEntity mesh in Engine.EntityManager.GetAirportApproachLightEntityList())
            {
                mesh.Position = AirportOffseted_Position;
                mesh.Rotation = AirportOffseted_Rotation;
            }

            //airport signal light meshes
            foreach (AirportSignalLightMeshEntity mesh in Engine.EntityManager.GetAirportSignalLightEntityList())
            {
                mesh.Position = AirportOffseted_Position;
                mesh.Rotation = AirportOffseted_Rotation;
            }

            //airport static light meshes
            foreach (AirportStaticLightMeshEntity mesh in Engine.EntityManager.GetAirportStaticLightEntityList())
            {
                mesh.Position = AirportOffseted_Position;
                mesh.Rotation = AirportOffseted_Rotation;
            }
        }

        /// <summary>
        /// Load the simulation's settings config file.
        /// </summary>
        /// <param name="filepath"></param>
        private static void LoadSimulationSettingsConfig(string filepath)
        {
            //load the simulation's settings
            SimulationSettings temp = Engine.Content.Load<SimulationSettings>(filepath);

            // Create derived simulation settings
            SimulationSettings = new DerivedSimulationSettings();
            // make a copy
            SimulationSettings.CopyFrom(temp);
            // calculate for derived simulation settings
            if (SimulationSettings.AirportOffsetPosition.IsModelScale)
            {
                SimulationSettings.AirportModelOffsetPosition = SimulationSettings.AirportOffsetPosition.Position;
                SimulationSettings.AirportRealMapOffsetPosition = SimulationSettings.MapToModelScale * SimulationSettings.AirportModelOffsetPosition;
            }
            else
            {
                SimulationSettings.AirportRealMapOffsetPosition = SimulationSettings.AirportOffsetPosition.Position;
                SimulationSettings.AirportModelOffsetPosition = SimulationSettings.AirportRealMapOffsetPosition / SimulationSettings.MapToModelScale;
            }

            //set the status of this load-type
            IsSimulationSettingsConfigLoaded = true;
        }

        /// <summary>
        /// Load the airport's meshes config files.
        /// </summary>
        /// <param name="opaqueMeshesFilePath"></param>
        /// <param name="alphaMeshesFilePath"></param>
        private static void LoadAirportMeshesConfig(string opaqueMeshesFilePath, string alphaMeshesFilePath, string lightMeshesFilePath,
            string approachLightMeshesFilePath)
        {
            //load all the type of airport's meshes
            LoadAirportOpaqueMeshesConfig(opaqueMeshesFilePath);
            LoadAirportAlphaMeshesConfig(alphaMeshesFilePath);
            LoadAirportLightMeshesConfig(lightMeshesFilePath, approachLightMeshesFilePath);
        }

        /// <summary>
        /// Load the airport's opaque meshes-config file.
        /// </summary>
        /// <param name="opaqueMeshesFilePath"></param>
        /// <remarks>Mandatory</remarks>
        private static void LoadAirportOpaqueMeshesConfig(string opaqueMeshesFilePath)
        {
            //load the AirportMesh into list
            List<AirportOpaqueMesh> amList = Engine.Content.Load<List<AirportOpaqueMesh>>(opaqueMeshesFilePath);
            //traverse through the list to
            // create the AirportMesh entity
            // and register to the entity manager
            foreach (AirportOpaqueMesh amInfo in amList)
            {
                //create new entity
                AirportOpaqueMeshEntity amEntity = new AirportOpaqueMeshEntity(amInfo);
                //register to the entity manager
                Engine.EntityManager.RegisterEntity(amEntity);
            }

            //set the status of this load-type
            IsAirportOpaqueMeshesConfigLoaded = true;
        }

        /// <summary>
        /// Load the airport's alpha meshes-config file.
        /// </summary>
        /// <param name="alphaMeshesFilePath"></param>
        private static void LoadAirportAlphaMeshesConfig(string alphaMeshesFilePath)
        {
            if (alphaMeshesFilePath == null || alphaMeshesFilePath == "null")
                IsAirportAlphaMeshesConfigLoaded = false;
            else
            {
                //load the AirportMesh into list
                List<AirportAlphaMesh> amList = Engine.Content.Load<List<AirportAlphaMesh>>(alphaMeshesFilePath);
                //traverse through the list to
                // create the AirportMesh entity
                // and register to the entity manager
                foreach (AirportAlphaMesh amInfo in amList)
                {
                    //create new entity
                    AirportAlphaMeshEntity amEntity = new AirportAlphaMeshEntity(amInfo);
                    //register to the entity manager
                    Engine.EntityManager.RegisterEntity(amEntity);
                }

                //set the status of this load-type
                IsAirportAlphaMeshesConfigLoaded = true;
            }
        }

        /// <summary>
        /// Load the airport's light meshes-config file.
        /// </summary>
        /// <remarks>Load all the related type of light-mesh.</remarks>
        /// <param name="lightMeshesFilePath"></param>
        private static void LoadAirportLightMeshesConfig(string lightMeshesFilePath, string approachLightMeshesFilePath)
        {
            if (lightMeshesFilePath != null && lightMeshesFilePath != "null")
            {
                //load the AirportLightMesh into list (only for static, and signal light mesh)
                List<AirportLightMesh> amList = Engine.Content.Load<List<AirportLightMesh>>(lightMeshesFilePath);
                //traverse through the list to
                // create the AirportLightMesh entity
                // and register to the entity manager
                foreach (AirportLightMesh almInfo in amList)
                {
                    //determine the type of light mesh first, then create the entity according to that type
                    if (almInfo.TypeID == EntityIDSpecification.Type.Airport.AIRPORT_STATIC_LIGHT_MESH_TYPE_ID)
                    {
                        //create the static light entity
                        AirportStaticLightMeshEntity almEntity = new AirportStaticLightMeshEntity(almInfo);
                        //register to the entity manager
                        Engine.EntityManager.RegisterEntity(almEntity);
                    }
                    else if (almInfo.TypeID == EntityIDSpecification.Type.Airport.AIRPORT_SIGNAL_LIGHT_MESH_TYPE_ID)
                    {
                        //create the signal light entity
                        AirportSignalLightMeshEntity almEntity = new AirportSignalLightMeshEntity(almInfo);
                        //register to the entity manager
                        Engine.EntityManager.RegisterEntity(almEntity);
                    }
                }

                //set the status of this load-type
                IsAirportAlphaMeshesConfigLoaded = true;
            }

            if (approachLightMeshesFilePath != null && approachLightMeshesFilePath != "null")
            {
                //load the AirportLightMesh into list (only for approach light mesh)
                List<AirportApproachLightMesh> apmList = Engine.Content.Load<List<AirportApproachLightMesh>>(approachLightMeshesFilePath);
                foreach (AirportApproachLightMesh apm in apmList)
                {
                    if (apm.TypeID == EntityIDSpecification.Type.Airport.AIRPORT_APPROACH_LIGHT_MESH_TYPE_ID)
                    {
                        //create the approach light entity
                        AirportApproachLightMeshEntity apmEntity = new AirportApproachLightMeshEntity(apm);
                        //register to the entity manager
                        Engine.EntityManager.RegisterEntity(apmEntity);
                    }
                }

                //set the status of this load-type
                IsAirportAlphaMeshesConfigLoaded = true;
            }
        }

        /// <summary>
        /// Load the airport's SCMesh config file.
        /// </summary>
        /// <param name="filepath">Null or "null" to bypass this config</param>
        private static void LoadSCMeshConfig(string filepath)
        {
            if (filepath == null || filepath == "null")
                IsSCMeshConfigLoaded = false;
            else
            {
                //load the SCMesh into list
                List<SCMesh> scMeshList = Engine.Content.Load<List<SCMesh>>(filepath);
                //traverse through the list to
                // create the SCMesh entity
                // and register to the entity manager
                foreach (SCMesh scMeshInfo in scMeshList)
                {
                    //create new entity
                    SCMeshEntity scMeshEntity = new SCMeshEntity(scMeshInfo);
                    //register to the entity manager
                    Engine.EntityManager.RegisterEntity(scMeshEntity);
                }

                //create the SCMeshManager
                //refer to the entity's identification paper
                SCMeshManager = new SCMeshManager(
                    (SCMeshEntity)Engine.EntityManager.GetEntityFrom(EntityIDSpecification.Entity.Airport.SKYDOME_MESH_ENTITY_ID),
                    (SCMeshEntity)Engine.EntityManager.GetEntityFrom(EntityIDSpecification.Entity.Airport.CLOUDDOME_MESH_ENTITY_ID));

                //set the status of this load-type
                IsSCMeshConfigLoaded = true;
            }
        }

        /// <summary>
        /// Load transportation object config file.
        /// </summary>
        /// <param name="filepath">Null or "null" to bypass this config</param>
        private static void LoadTransportationObjectConfig(string filepath)
        {
            if (filepath == null || filepath == "null")
                IsTransportationObjectConfigLoaded = false;
            else
            {
                //initialize the airplane type manager
                TransportationObjectInfoManager = new TransportationObjectInfoManager();

                //load the AirplaneTypeAsset into list
                List<TransportationObject> transObjs = Engine.Content.Load<List<TransportationObject>>(filepath);

                //traverse though the list and add each one into the AirplaneTypeAssetInfoManager's list
                foreach (TransportationObject obj in transObjs)
                {
                    TransportationObjectInfoManager.AddNewTransportationObject(obj);
                }

                //set the status of this load-type
                IsTransportationObjectConfigLoaded = true;
            }
        }

        /// <summary>
        /// Load point light source config file.
        /// </summary>
        /// <param name="filepath">Null or "null" to bypass this config.</param>
        private static void LoadPointLightSourceConfig(string filepath)
        {
            if (filepath == null || filepath == "null")
                IsPointLightSourceConfigLoaded = false;
            else
            {
                List<AirportPointLightSource> pls = Engine.Content.Load<List<AirportPointLightSource>>(filepath);

                // traverse through the list and create then add point light into engine
                foreach (AirportPointLightSource pl in pls)
                {
                    // if position is already rotated, then we ignore offset of rotation
                    Engine.RenderSystem.PointLights.Add(new PointLight(XNAUtil.OffsetPoint(pl.Position / Engine.SimulationSettings.MapToModelScale, true, pl.IsPosRotated), new Color(pl.LightColor4), 
                        pl.SpecularIntensity, pl.SpecularPower, pl.Intensity, pl.Radius));
                }

                IsPointLightSourceConfigLoaded = true;
            }
        }

        /// <summary>
        /// Update the engine.
        /// </summary>
        /// <remarks>Just only call to this method, the engine will do all the job for us.</remarks>
        /// <param name="gameTime"></param>
        public static void Update(GameTime gameTime, bool isPaused)
        {
            //update time manager (TimeManager has the internal update which takes isPaused as the parameter)
            TimeManager.Update(gameTime, isPaused);

            if (!isPaused)
            {
                //SCMesh
                if (IsSCMeshConfigLoaded)
                    SCMeshManager.Update(gameTime);

                //update the render system
                RenderSystem.Update(gameTime);
                // update parameters to base material
                Engine.BaseMaterial.Parameters["ambientLight"].SetValue(Engine.SCMeshManager.AmbientLight);

                #region Individual Update
                //TODO: Update all the individual mesh here
                //individual update for the must-update-draw meshes (airport meshes and scmesh)
                //airport's opaque meshes
                foreach (AirportOpaqueMeshEntity aom in EntityManager.GetAirportOpaqueEntityList())
                {
                    aom.Update(gameTime);
                }
                //airport's alpha meshes
                foreach (AirportAlphaMeshEntity alm in EntityManager.GetAirportAlphaEntityList())
                {
                    alm.Update(gameTime);
                }
                //airport's static light meshes
                foreach (AirportStaticLightMeshEntity a in Engine.EntityManager.GetAirportStaticLightEntityList())
                {
                    a.Update(gameTime);
                }
                //airport's signal light meshes
                foreach (AirportSignalLightMeshEntity a in Engine.EntityManager.GetAirportSignalLightEntityList())
                {
                    a.Update(gameTime);
                }
                //airport's dynamic light meshes
                foreach (AirportApproachLightMeshEntity a in Engine.EntityManager.GetAirportApproachLightEntityList())
                {
                    a.Update(gameTime);
                }
                //ENDTODO
                #endregion

                //update all the scenes in each screen reside in the all of screen managers
                DeferredScreenManager.Update(gameTime);
                Text2ScreenManager.Update(gameTime);
            }
        }

        /// <summary>
        /// Draw all the content reside in the game world.
        /// </summary>
        /// <remarks>This method also has the option to enable/disable the drawing of text2 screen. So those debugging information will not
        /// be appeared on top of the binocular scene. (Grab the keyword from working :))</remarks>
        /// <param name="gameTime"></param>
        /// 
        /// <param name="isDrawToTexture"></param>
        /// <param name="dttRtNum"></param>
        /// <param name="isText2ScreenDrawed"></param>
        public static void Draw(GameTime gameTime, bool isDrawToTexture, DTTRtNum dttRtNum, bool isText2ScreenDrawed)
        {
            //set the draw option accordingly
            Engine.RenderSystem.IsDrawToTexture = isDrawToTexture;
            Engine.CurrentDTTRtActive = dttRtNum;

            //send in the dttRt to RenderSystem
            if (isDrawToTexture)
            {
                //choose which dttRt to send to RenderSystem
                switch (dttRtNum)
                {
                    case DTTRtNum.ONE:
                        Engine.RenderSystem.DttRt = DTTRt1;
                        break;
                    case DTTRtNum.TWO:
                        Engine.RenderSystem.DttRt = DTTRt2;
                        break;
                    case DTTRtNum.THREE:
                        Engine.RenderSystem.DttRt = DTTRt3;
                        break;
                    case DTTRtNum.FOUR:
                        Engine.RenderSystem.DttRt = DTTRt4;
                        break;
                }
            }

            //begin the rendering process
            RenderSystem.BeginDraw(gameTime);

            //TODO: Draw all the individual opaque mesh here
            //Note: All the stuff here must be in opaque.

            //SCMesh
            if(IsSCMeshConfigLoaded)
                SCMeshManager.Draw(gameTime);
            //airport's opaque meshes
            foreach (AirportOpaqueMeshEntity aom in EntityManager.GetAirportOpaqueEntityList())
            {
                aom.Draw(gameTime);
            }
            //ENDTODO

            //draw the scenes (inside the screen)
            DeferredScreenManager.Draw(gameTime);

            //end the rendering process
            //Note: By ending this drawing process, next the DeferredShadingRenderer will draw the alpha stuff immediately.
            RenderSystem.EndDraw(gameTime);

            //if need to draw the text2 screen
            if (isText2ScreenDrawed)
            {
                //if need to draw-to-texture (dttRt involved)
                if (isDrawToTexture)
                {
                    //choose which dttRt to send to RenderSystem
                    switch (dttRtNum)
                    {
                        case DTTRtNum.ONE:
                            Engine.GraphicsDevice.SetRenderTarget(0, DTTRt1);
                            Text2ScreenManager.Draw(gameTime);
                            Engine.GraphicsDevice.SetRenderTarget(0, null);

                            break;
                        case DTTRtNum.TWO:
                            Engine.GraphicsDevice.SetRenderTarget(0, DTTRt2);
                            Text2ScreenManager.Draw(gameTime);
                            Engine.GraphicsDevice.SetRenderTarget(0, null);
                            break;
                        case DTTRtNum.THREE:
                            Engine.GraphicsDevice.SetRenderTarget(0, DTTRt3);
                            Text2ScreenManager.Draw(gameTime);
                            Engine.GraphicsDevice.SetRenderTarget(0, null);
                            break;
                        case DTTRtNum.FOUR:
                            Engine.GraphicsDevice.SetRenderTarget(0, DTTRt4);
                            Text2ScreenManager.Draw(gameTime);
                            Engine.GraphicsDevice.SetRenderTarget(0, null);
                            break;
                    }
                }
                else
                {
                    //time to render the textures/text screen on top of deferred screen
                    Text2ScreenManager.Draw(gameTime);
                }
            }
        }
    }
}
