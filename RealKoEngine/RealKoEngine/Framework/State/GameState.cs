﻿#region File Description
//-----------------------------------------------------------------------------
// GameState.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

namespace RealKo.Framework.State
{
    /// <summary>
    /// Current state information, user should create the state information manually to suit the application developed.
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// Current state will be set across the game by interested classes.
        /// </summary>
        public int CurrentState;

        /// <summary>
        /// This field should be implemented as need.
        /// </summary>
        public int PreviousState;
    }
}
