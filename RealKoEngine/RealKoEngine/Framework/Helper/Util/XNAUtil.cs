﻿#region File Description
//-----------------------------------------------------------------------------
// XNAUtil.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Helper.Util
{
    public static class XNAUtil
    {
        /// <summary>
        /// Random object shared for used.
        /// Use this object to random the number directly or use the helper functions provided.
        /// </summary>
        public static Random Random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Positive and negative direction to be used for general purpose.
        /// </summary>
        private static int[] signs = { -1, 1 };

        /// <summary>
        /// Random the floating-point type between min and max, and return it.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>Random value between min and max.</returns>
        public static float RandomBetween(float min, float max)
        {
            return min + (float)Random.NextDouble() * (max - min);
        }

        /// <summary>
        /// Random the unit of direction.
        /// The direction can be in either negative or positive direction.
        /// </summary>
        /// <returns>Normalized direction, it can be -1, or 1.</returns>
        public static int RandomUnitDirection()
        {
            return signs[Random.Next(signs.Length)];
        }

        /// <summary>
        /// Random the direction away from the center of the circle.
        /// Ranges from 0-360 degrees.
        /// </summary>
        /// <returns>Direction in 2-axis defined the vector point away from the center of the circle.</returns>
        public static Vector2 RandomCircleDirection()
        {
            float radians = RandomBetween(0, MathHelper.TwoPi);
            return new Vector2((float)Math.Cos(radians), (float)Math.Sin(radians));
        }

        /// <summary>
        /// Transform the bounding sphere with the specified world matrix.
        /// </summary>
        /// <param name="boundingSphere"></param>
        /// <param name="world"></param>
        /// <returns>Transformed boundingsphere.</returns>
        public static BoundingSphere TransformBoundingSphere(BoundingSphere boundingSphere, Matrix world)
        {
            Vector3 trans;
            Vector3 scale;
            Quaternion quaternion;
            world.Decompose(out scale, out quaternion, out trans);

            float maxScale = scale.X;
            if (maxScale < scale.Y)
                maxScale = scale.Y;
            if (maxScale < scale.Z)
                maxScale = scale.Z;

            float transformedSphereRadius = boundingSphere.Radius * maxScale;
            Vector3 transformedSphereCenter = Vector3.Transform(boundingSphere.Center, world);

            BoundingSphere transformedBoundingSphere = new BoundingSphere(transformedSphereCenter, transformedSphereRadius);
            return transformedBoundingSphere;
        }

        /// <summary>
        /// Create the coarse-cover bounding sphere of the given model.
        /// </summary>
        /// <remarks>The bounding sphere will cover all of the meshes of the model.</remarks>
        /// <param name="model">The model to be used to create the boundingsphere.</param>
        /// <returns></returns>
        public static BoundingSphere CreateBoundingSphereFrom(Model model)
        {
            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            return completedBoundingSphere;
        }

        /// <summary>
        /// Create the bounding sphere to cover all the mesh specified by indexes array.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="indexes"></param>
        /// <returns></returns>
        public static BoundingSphere CreateBoundingSphereFrom(Model model, int[] indexes)
        {
            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            //check first if the indexes's length is greater than 0
            if (indexes.Length > 0)
            {
                //loop through the indexes array, and get the mesh from the specified index
                foreach (int i in indexes)
                {
                    // checking
                    if (i >= 0)
                    {
                        ModelMesh mesh = model.Meshes[i];

                        //get the bounding sphere from the current mesh
                        BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                        //merge the bounding sphere together
                        completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
                    }
                }
            }

            return completedBoundingSphere;
        }

        /// <summary>
        /// Get the blending code from the blending string.
        /// </summary>
        /// <remarks>If the blending string cannot be found and mapped correctly then this will return Blend.One.
        /// Also the "Default" means Blend.One.</remarks>
        /// <param name="blendStr"></param>
        /// <returns></returns>
        public static Blend GetBlendCodeFrom(string blendStr)
        {
            switch (blendStr)
            {
                case "Default": 
                    return Blend.One;
                case "SourceAlpha":
                    return Blend.SourceAlpha;
                case "DestinationAlpha":
                    return Blend.DestinationAlpha;
                case "One":
                    return Blend.One;
                case "InverseSourceAlpha":
                    return Blend.InverseSourceAlpha;
                case "InverseDestinationAlpha":
                    return Blend.InverseDestinationAlpha;
                default:
                    return Blend.One;
            }
        }

        /// <summary>
        /// Perform the checking of angle (in radians) between two vectors.
        /// If the calculated angle is less than the specified tangle, then return true. Otherwise return false.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="tangle">Threshold angle to be checked.</param>
        public static bool AngleCheck(Vector3 v1, Vector3 v2, float tangle)
        {
            if (Vector3.Dot(v1, v2) >= tangle)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get radians equivalent of the Vector3 in degrees.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3 ToRadians(ref Vector3 v)
        {
            return new Vector3(MathHelper.ToRadians(v.X), MathHelper.ToRadians(v.Y), MathHelper.ToRadians(v.Z));
        }

        /// <summary>
        /// Transform a specified point with the offset position and rotation of airport.
        /// </summary>
        /// <param name="pos">Position to offset regarding to airport's offeting (both position and rotation)</param>
        /// <returns></returns>
        public static Vector3 OffsetPoint(Vector3 pos)
        {
            return OffsetPoint(pos, false, false);
        }

        /// <summary>
        /// Transform a specified point with the offset position and rotation of airport.
        /// </summary>
        /// <param name="pos">Position to offset regarding to airport's offeting (both position and rotation)</param>
        /// <param name="ignoreAirportOffsetPos">Whether or not to ignore the airport's offet position.</param>
        /// <returns></returns>
        public static Vector3 OffsetPoint(Vector3 pos, bool ignoreAirportOffsetPos)
        {
            return OffsetPoint(pos, ignoreAirportOffsetPos, false);
        }

        /// <summary>
        /// Transform a specified point with the offset position and rotation of airport.
        /// </summary>
        /// <param name="pos">Position to offset regarding to airport's offeting (both position and rotation)</param>
        /// <param name="ignoreAirportOffsetPos">Whether or not to ignore the airport's offet position.</param>
        /// <param name="ignoreAirportRotationPos">Whether or not to ignore the airport's rotation position.</param>
        /// <returns></returns>
        public static Vector3 OffsetPoint(Vector3 pos, bool ignoreAirportOffsetPos, bool ignoreAirportRotationPos)
        {
            Vector3 cpos = pos;
            Vector3 radians = ToRadians(ref Engine.SimulationSettings.AirportOffsetRotation);

            if (!ignoreAirportOffsetPos)
                cpos -= Engine.SimulationSettings.AirportModelOffsetPosition;
            if(!ignoreAirportRotationPos)
                cpos = Vector3.Transform(cpos, Quaternion.CreateFromYawPitchRoll(-radians.Y, -radians.X, -radians.Z));
            cpos *= Engine.SimulationSettings.GlobalScale;
            return cpos;
        }
    }
}
