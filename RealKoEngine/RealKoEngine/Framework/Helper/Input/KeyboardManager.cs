﻿#region File Description
//-----------------------------------------------------------------------------
// KeyboardManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Helper.Input
{
    /// <summary>
    /// Manage and handle all the key input.
    /// </summary>
    /// <remarks>No multiple key input assignment is available now, just plain simple system.</remarks>
    public class KeyboardManager: RK_IUpdateable
    {
        KeyboardState currKs, preKs;

        /// <summary>
        /// Get the current keyboard state.
        /// </summary>
        public KeyboardState CurrentKeyState
        {
            get { return currKs; }
        }

        /// <summary>
        /// Get the previous keyboard state.
        /// </summary>
        public KeyboardState PreKeyState
        {
            get { return preKs; }
        }

        /// <summary>
        /// Create keyboard manager.
        /// </summary>
        public KeyboardManager()
        {
            //init all the state to the same
            currKs = Keyboard.GetState();
            preKs = Keyboard.GetState();
        }

        /// <summary>
        /// Update the keyboard manager
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            //update the states of keyboard
            preKs = currKs;
            currKs = Keyboard.GetState();
        }

        //The following should be called before Update() method, to properly check the keyboard input.
        #region Key checking

        /// <summary>
        /// Return if the specified key is initially pressed.
        /// </summary>
        /// <param name="key"></param>
        public bool OnInitialKeyDown(Keys key)
        {
            if (currKs.IsKeyDown(key) && preKs.IsKeyUp(key))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Return if the specified key is pressed.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool OnKeyDown(Keys key)
        {
            if (currKs.IsKeyDown(key))
                return true;
            else
                return false;
        }

        #endregion
    }
}
