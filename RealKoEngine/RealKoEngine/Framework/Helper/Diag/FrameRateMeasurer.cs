﻿#region File Description
//-----------------------------------------------------------------------------
// FrameRateMeasurer.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Helper.Diag
{
    /// <summary>
    /// FrameRateMeasurer class.
    /// It will measure the framerate for the current XNA-App (Not integrated with win-form application).
    /// </summary>
    /// <remarks>This is not the standard class for RealKo Engine, used this class for convenient usage only.</remarks>
    public class FrameRateMeasurer
    {
        //framerate properties
        int frameCount = 0;
        float elapsedFrameTime = 0f;
        float frameInterval = 200f;
        float frameRate = 0f;

        SpriteFont font;
        SpriteBatch batch;

        /// <summary>
        /// Get the current calculated framerate.
        /// </summary>
        public float FrameRate
        {
            get { return frameRate; }
        }

        /// <summary>
        /// Create frame rate measurer.
        /// </summary>
        /// <param name="font"></param>
        /// <param name="batch"></param>
        public FrameRateMeasurer(SpriteFont font, SpriteBatch batch)
        {
            this.font = font;
            this.batch = batch;
        }

        /// <summary>
        /// Calculate the frame rate of the current time.
        /// </summary>
        /// <param name="gameTime"></param>
        public void CalculateFrameRate(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedRealTime.TotalMilliseconds;

            frameCount++;
            //update current time
            elapsedFrameTime += elapsedTime;

            if (elapsedFrameTime > frameInterval)
            {
                //calculate framerate (in unit of frame-per-second)
                frameRate = ((float)frameCount * 1000.0f) / elapsedFrameTime;

                frameCount = 0;
                elapsedFrameTime = 0;
            }
        }

        /// <summary>
        /// Calculate the frame rate of the current time, and draw it on the screen.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="drawPos"></param>
        public void CalculateFrameRateAndDrawIt(GameTime gameTime, Vector2 drawPos)
        {
            // calculate the framerate
            CalculateFrameRate(gameTime);

            // take the result to draw on screen
            //draw it (to safely draw thing on screen, turn on the SaveSate)
            batch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);
            batch.DrawString(font, "Framerate: " + (int)frameRate, drawPos, Color.White);
            batch.End();
        }
    }
}
