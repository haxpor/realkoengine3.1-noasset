﻿#region File Description
//-----------------------------------------------------------------------------
// CameraManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace RealKo.Framework.Helper
{
    /// <summary>
    /// Camera manager is general purpose of usage for any view perspective in the scene.
    /// </summary>
    /// <remarks>Camera manager will not attempt to set the parameters inside the GPU when the property is set. This is due to we design this
    /// class to be used extensively throughout the implementation by the user.</remarks>
    public class CameraManager
    {
        /// <summary>
        /// Position of the camera.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Field of view of the camera in radian.
        /// </summary>
        public float FieldOfView;

        /// <summary>
        /// Up vector defined for this camera.
        /// </summary>
        public Vector3 Up;

        /// <summary>
        /// LookAt vector defined for this camera.
        /// </summary>
        public Vector3 LookAt;

        /// <summary>
        /// View matrix applied for this camera.
        /// </summary>
        public Matrix View;

        /// <summary>
        /// Projection matrix applied for this camera.
        /// </summary>
        public Matrix Projection;

        /// <summary>
        /// Inverted view-projection matrix.
        /// </summary>
        public Matrix InvertedViewProjection;

        /// <summary>
        /// Near clipping plane's distance of this camera.
        /// </summary>
        public float NearClip;

        /// <summary>
        /// Far clipping plane's distance of this camera.
        /// </summary>
        public float FarClip;

        /// <summary>
        /// View frustum of this camera.
        /// </summary>
        public BoundingFrustum ViewFrustum
        {
            get { return new BoundingFrustum(View * Projection); }
        }

        /// <summary>
        /// Create the camera manager with the default value for each field.
        /// </summary>
        public CameraManager()
        {
            Position = Vector3.Zero;
            FieldOfView = MathHelper.ToRadians(90);
            Up = Vector3.Up;
            LookAt = -Vector3.UnitZ;
            View = Matrix.Identity;
            Projection = Matrix.Identity;
            InvertedViewProjection = Matrix.Identity;
            NearClip = 0.1f;
            FarClip = 10000f;
        }

        /// <summary>
        /// Copy all the properties of this camera manager into the specified manager.
        /// </summary>
        /// <remarks>User desires to implement his/her own camera to view in different perspective of the scene, should use this method to copy
        ///  all the properties into user's own camera manager object.</remarks>
        /// <param name="manager"></param>
        public void CopyTo(CameraManager manager)
        {
            manager.Position = Position;
            manager.FieldOfView = FieldOfView;
            manager.Up = Up;
            manager.LookAt = LookAt;
            manager.View = View;
            manager.Projection = Projection;
            manager.InvertedViewProjection = InvertedViewProjection;
            manager.NearClip = NearClip;
            manager.FarClip = FarClip;
        }

        #region Helper methods (The following section will not affect the main core processing of this manager.)

        /// <summary>
        /// Calculate the view matrix representing this camera.
        /// </summary>
        /// <remarks>This method will provide the flexibility of calculating the view matrix. Use has the option to calculate externally and manually
        /// or just use this method to help perform the task.</remarks>
        /// <param name="cameraPosition"></param>
        /// <param name="cameraTarget"></param>
        /// <param name="cameraUpVector"></param>
        /// <returns></returns>
        public Matrix CalculateViewMatrix(Vector3 cameraPosition, Vector3 cameraTarget, Vector3 cameraUpVector)
        {
            return Matrix.CreateLookAt(cameraPosition, cameraTarget, cameraUpVector);
        }

        /// <summary>
        /// Calculate the projection matrix representing this camera.
        /// </summary>
        /// <remarks>This method will provide the flexibility of calculating the projection matrix. Use has the option to calculate externally and manually
        /// or just use this method to help perform the task.</remarks>
        /// <param name="aspectRatio"></param>
        /// <returns></returns>
        public Matrix CalculateProjectionMatrix(float aspectRatio)
        {
            return Matrix.CreatePerspectiveFieldOfView(FieldOfView, aspectRatio, NearClip, FarClip);
        }

        /// <summary>
        /// Calculate the inverted view-projection matrix representing this camera.
        /// </summary>
        /// <remarks>This method will provide the flexibility of calculating the inverted view-projection matrix. Use has the option to calculate externally and manually
        /// or just use this method to help perform the task.</remarks>
        /// <returns></returns>
        public Matrix CalculateInvertedViewProjectionMatrix()
        {
            return Matrix.Invert(View * Projection);
        }

        #endregion
    }
}
