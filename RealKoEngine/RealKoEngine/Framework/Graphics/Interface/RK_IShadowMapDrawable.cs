﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IShadowMapDrawable.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// All the opaque meshe entity that would be able to cast shadow, should implement this interface but must routing the ShadowMapDrawEffect
    ///  from the RenderSystem too.
    /// </summary>
    public interface RK_IShadowMapDrawable
    {
        //generate the shadow map (depth map)
        void DrawShadowMap(GameTime gameTime);
    }
}