﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IDrawable.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace RealKo.Framework.Graphics
{
    public interface RK_IDrawable
    {
        void Draw(GameTime gameTime);
    }
}
