﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IPreInitialize.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

namespace RealKo.Framework.Graphics
{
    public interface RK_IPreInitialize
    {
        void PreInitialize();
    }
}
