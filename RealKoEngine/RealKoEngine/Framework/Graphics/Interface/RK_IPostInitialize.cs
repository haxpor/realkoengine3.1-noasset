﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IPostInitialize.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

namespace RealKo.Framework.Graphics
{
    public interface RK_IPostInitialize
    {
        void PostInitialize();
    }
}
