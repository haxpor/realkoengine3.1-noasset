﻿#region File Description
//-----------------------------------------------------------------------------
// RK_IUpdateable.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace RealKo.Framework.Graphics
{
    public interface RK_IUpdateable
    {
        void Update(GameTime gameTime);
    }
}
