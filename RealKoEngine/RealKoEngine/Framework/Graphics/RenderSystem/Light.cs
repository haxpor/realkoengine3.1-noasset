﻿#region File Description
//-----------------------------------------------------------------------------
// Light.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// Note: All of the classes reside in this class file are intented to be used for information purpose only. Itself doesn't have to contain the
//  working algorithm or functionality of something. That work must be done externally via other sub-system that integrated or use this class.

namespace RealKo.Framework.Graphics
{
    #region Base class for shadow light

    /// <summary>
    /// Light base class used for provide the basic information that all the shadow light should have.
    /// </summary>
    public class LightBase
    {
        /// <summary>
        /// The enablility of the shadow light.
        /// </summary>
        /// <remarks>By set/unset this value properly, the performance boost can be achieved.</remarks>
        public bool IsEnabled = true;
    }

    #endregion

    #region Specification for normal light-type

    /// <summary>
    /// Directional light.
    /// </summary>
    public class DirectionalLight: LightBase
    {
        /// <summary>
        /// Direction of light looking to.
        /// </summary>
        public Vector3 Direction;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Intensity of this light.
        /// </summary>
        /// <remarks>Intensity must be in [0,1], as it will be applied in the pixel shader.(all the color is </remarks>
        public float Intensity;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Create the directional light.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="color"></param>
        /// <param name="intensity"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower">specularPower</param>
        public DirectionalLight(Vector3 direction, Color color, float intensity, float specularIntensity, float specularPower)
            : base()
        {
            Direction = direction;
            Color = color;
            Intensity = intensity;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
        }
    }

    /// <summary>
    /// Point light.
    /// </summary>
    public class PointLight : LightBase
    {
        /// <summary>
        /// Position of light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Intensity of this point light.
        /// </summary>
        public float Intensity;

        /// <summary>
        /// Radius the effect point light.
        /// </summary>
        public float Radius;

        /// <summary>
        /// Create a new point light.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="color"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower"></param>
        /// <param name="intensity"></param>
        /// <param name="radius"></param>
        public PointLight(Vector3 position, Color color, float specularIntensity, float specularPower, float intensity, float radius)
            : base()
        {
            Position = position;
            Color = color;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
            Intensity = intensity;
            Radius = radius;
        }
    }

    /// <summary>
    /// Spot light.
    /// </summary>
    /// <remarks>DO NOT USE THIS CLASS. It's not in the stable state.</remarks>
    public class SpotLight : LightBase
    {
        /// <summary>
        /// Position of the directional shadow light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Look at position of the directional shadow light.
        /// </summary>
        public Vector3 LookAtPos;

        /// <summary>
        /// Range of the spotlight.
        /// </summary>
        public float Range;

        /// <summary>
        /// Cone angle in radians.
        /// </summary>
        public float ConeAngle;

        /// <summary>
        /// Cone decay.
        /// </summary>
        public float ConeDecay;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Intensity of this ight.
        /// </summary>
        public float Intensity;

        /// <summary>
        /// Create the new directional shadow light.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lookAtPos"></param>
        /// <param name="range"></param>
        /// <param name="coneAngle"></param>
        /// <param name="color"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower"></param>
        /// <param name="intensity"></param>
        public SpotLight(Vector3 position, Vector3 lookAtPos, float range,float coneAngle, float coneDecay, Color color,
            float specularIntensity, float specularPower, float intensity)
            : base()
        {
            Position = position;
            LookAtPos = lookAtPos;
            Range = range;
            ConeAngle = coneAngle;
            ConeDecay = coneDecay;
            Color = color;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
            Intensity = intensity;
        }
    }

    #endregion

    #region Specification for shadow light-type

    /// <summary>
    /// Directional shadow light.
    /// </summary>
    public class DirectionalShadowLight : LightBase
    {
        /// <summary>
        /// Position of the light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Look at position of the light.
        /// </summary>
        public Vector3 LookAtPos;

        /// <summary>
        /// Range of the spotlight.
        /// </summary>
        public float Range;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Intensity of this light.
        /// </summary>
        /// <remarks>Intensity must be in [0,1], as it will be applied in the pixel shader.(all the color is </remarks>
        public float Intensity;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Shape light texture.
        /// Setting this value to null tells the engine that this light will not be shaped by texture.
        /// </summary>
        /// <remarks>Texture should be reference type, just to reduce the load.</remarks>
        public Texture2D ShapeLightTexture;

        /// <summary>
        /// View matrix represents this light.
        /// </summary>
        public Matrix View;

        /// <summary>
        /// Projection matrix represents this light.
        /// </summary>
        public Matrix Projection;

        /// <summary>
        /// Create the directional light.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lookAtPos"></param>
        /// <param name="range"></param>
        /// <param name="color"></param>
        /// <param name="intensity"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public DirectionalShadowLight(Vector3 position, Vector3 lookAtPos, float range,
            Color color, float intensity, float specularIntensity, float specularPower, Texture2D shapeLightTexture,
            Matrix view, Matrix projection)
            : base()
        {
            Position = position;
            LookAtPos = lookAtPos;
            Range = range;
            Color = color;
            Intensity = intensity;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
            ShapeLightTexture = shapeLightTexture;
            View = view;
            Projection = projection;
        }
    }

    /// <summary>
    /// Point shadow light.
    /// </summary>
    /// <remarks>It's just the enhanced version of the normal point light, in which it can cast the shadow on the model.</remarks>
    public class PointShadowLight : LightBase
    {
        /// <summary>
        /// The flags indicates whether this point light will cast the shadow on following view-direction.
        /// The default value for all the flags is true.
        /// </summary>
        /// <remarks>All of the flags must be set manually by the user.</remarks>
        public bool IsShadowForUpView = true;
        public bool IsShadowForDownView = true;
        public bool IsShadowForFrontView = true;
        public bool IsShadowForBackView = true;
        public bool IsShadowForLeftView = true;
        public bool IsShadowForRightView = true;

        /// <summary>
        /// Position of light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Intensity of this point shadow light.
        /// </summary>
        public float Intensity;

        /// <summary>
        /// Shape light texture.
        /// Setting this value to null tells the engine that this light will not be shaped by texture.
        /// </summary>
        /// <remarks>Texture should be reference type, just to reduce the load.</remarks>
        public Texture2D ShapeLightTexture;

        /// <summary>
        /// Radius the effect point shadow light.
        /// </summary>
        public float Radius;

        /// <summary>
        /// Projection matrix of this point shadow light.
        /// </summary>
        public Matrix Projection;

        /// <summary>
        /// Create a new point light.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="color"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower"></param>
        /// <param name="intensity"></param>
        /// <param name="radius"></param>
        public PointShadowLight(Vector3 position, Color color, float specularIntensity, float specularPower, float intensity,
            Texture2D shapeLightTexture, float radius)
            : base()
        {
            Position = position;
            Color = color;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
            Intensity = intensity;
            ShapeLightTexture = shapeLightTexture;
            Radius = radius;
            //creat the projection matrix (also increase the farplane a little, so it won't be clipped in unnatural way.)
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 1f, 0.1f, Radius + 10f);
            //Projection = Matrix.CreateOrthographic(10,10, 0.1f, Radius);
        }
    }

    /// <summary>
    /// Spotshadow light.
    /// </summary>
    /// <remarks>Spotlight light that used for calculating the shadow map.</remarks>
    public class SpotShadowLight : LightBase
    {
        /// <summary>
        /// Position of the directional shadow light.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Look at position of the directional shadow light.
        /// </summary>
        public Vector3 LookAtPos;

        /// <summary>
        /// Range of the spotlight.
        /// </summary>
        public float Range;

        /// <summary>
        /// Cone angle in radians.
        /// </summary>
        public float ConeAngle;

        /// <summary>
        /// Cone decay.
        /// </summary>
        public float ConeDecay;

        /// <summary>
        /// Color of light.
        /// </summary>
        public Color Color;

        /// <summary>
        /// Specular intensity of light.
        /// </summary>
        public float SpecularIntensity;

        /// <summary>
        /// Specular power of light.
        /// </summary>
        public float SpecularPower;

        /// <summary>
        /// Intensity of this ight.
        /// </summary>
        public float Intensity;

        /// <summary>
        /// Shape light texture.
        /// Setting this value to null tells the engine that this light will not be shaped by texture.
        /// </summary>
        /// <remarks>Texture should be reference type, just to reduce the load.</remarks>
        public Texture2D ShapeLightTexture;

        /// <summary>
        /// View matrix represents this light.
        /// </summary>
        public Matrix View;

        /// <summary>
        /// Projection matrix represents this light.
        /// </summary>
        public Matrix Projection;

        /// <summary>
        /// Create the new directional shadow light.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lookAtPos"></param>
        /// <param name="range"></param>
        /// <param name="coneAngle"></param>
        /// <param name="color"></param>
        /// <param name="specularIntensity"></param>
        /// <param name="specularPower"></param>
        /// <param name="intensity"></param>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        public SpotShadowLight(Vector3 position, Vector3 lookAtPos, float range,float coneAngle, float coneDecay, Color color,
            float specularIntensity, float specularPower, float intensity, Texture2D shapeLightTexture,
            Matrix view, Matrix projection)
            : base()
        {
            Position = position;
            LookAtPos = lookAtPos;
            Range = range;
            ConeAngle = coneAngle;
            ConeDecay = coneDecay;
            Color = color;
            SpecularIntensity = specularIntensity;
            SpecularPower = specularPower;
            Intensity = intensity;
            ShapeLightTexture = shapeLightTexture;
            View = view;
            Projection = projection;
        }
    }

    #endregion
}
