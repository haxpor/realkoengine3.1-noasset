﻿#region File Description
//-----------------------------------------------------------------------------
// Material.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKoContentShared;

//use another name for more clear of usage (to avoid conflict)
using ContentShared = RealKoContentShared.AirportOpaqueMesh;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// This material class is designed to work quickly with the base effect file that will be included by other effect files.
    /// It means that this material class will set the basic paramters for that basic effect file, but also allow user to access the effect and
    ///  manually set the parameter in the extended effect file, thus avoid create new material class for every extended effect file.
    /// </summary>
    public class Material
    {
        protected GraphicsDevice graphicsDevice;
        protected ContentManager content;
        /// <summary>
        /// Effect file first send to this class will be copied and all its values will be initially set with SetValue() method.
        /// </summary>
        protected Effect materialEffect;

        //cache of the model bone matrix
        Matrix[] absBoneTransforms = null;
        Matrix[] originalBoneTransforms = null;

        //textured being used throughout the effect
        protected Texture2D diffuseTexture = null;
        protected Texture2D specularTexture = null;
        protected Texture2D normalTexture = null;
        protected Texture2D emissiveTexture = null;

        //params set via effect's parameters
        protected string diffuseTextureName = string.Empty;
        protected string specularTextureName = string.Empty;
        protected string normalTextureName = string.Empty;
        protected string emissiveTextureName = string.Empty;
        protected float specularIntensity = 1f;
        protected float specularPower = 1f;
        protected Vector4 specularColor = Vector4.One;
        protected Vector4 colorValue;
        protected float textureCoordU = 1.0f;
        protected float textureCoordV = 1.0f;
        protected float lightOcclusion = 0f;    //refer to the engine's document for detail.

        //Effect's params
        protected EffectParameter materialColorValueEffectParam;
        protected EffectParameter diffuseTextureEffectParam;
        protected EffectParameter diffuseTextureEnabledEffectParam;
        protected EffectParameter specularTextureEffectParam;
        protected EffectParameter specularTextureEnabledEffectParam;
        protected EffectParameter specularIntensityEffectParam;
        protected EffectParameter specularPowerEffectParam;
        protected EffectParameter specularColorEffectParam;
        protected EffectParameter normalTextureEffectParam;
        protected EffectParameter normalTextureEnabledEffectParam;
        protected EffectParameter emissiveTextureEffectParam;
        protected EffectParameter emissiveTextureEnabledEffectParam;
        protected EffectParameter textureCoordUEffectParam;
        protected EffectParameter textureCoordVEffectParam;
        protected EffectParameter lightOcclusionEffectParam;
        protected EffectParameter worldEffectParam;

        #region Mesh-by-Mesh technique

        // Mesh-by-Mesh drawing technique
        // Note: The following variables are thread-safe and mapped to each object to draw, thus no conflict will be occurred.
        //Current model's mesh and meshpart (these objects will be set in BeginBatchDraw() method.
        protected ModelMesh currentMesh;
        protected ModelMeshPart currentMeshPart;
        //Bone information of mesh
        protected ModelBone currentBone;
        protected Matrix currentParentTransform;

        #endregion

        #region Properties

        /// <summary>
        /// Get the base effect that this material is using.
        /// </summary>
        /// <remarks>For extended effect file, access this field and manually set the appropriate parameters.</remarks>
        public Effect Effect
        {
            get { return materialEffect; }
        }

        #endregion

        public Material(GraphicsDevice graphicsDevice, ContentManager content, Effect effect)
        {
            //play safe here
            if (graphicsDevice == null)
                throw new ArgumentNullException("graphicsDevice");
            this.graphicsDevice = graphicsDevice;

            if (content == null)
                throw new ArgumentNullException("content");
            this.content = content;

            if (effect == null)
                throw new ArgumentNullException("effect");
            //clone the effect for this material
            materialEffect = effect.Clone(graphicsDevice);

            //get the effect's params (reduce the string look-up)
            materialColorValueEffectParam = materialEffect.Parameters["materialColor"];
            diffuseTextureEnabledEffectParam = materialEffect.Parameters["diffuseTextureEnabled"];
            diffuseTextureEffectParam = materialEffect.Parameters["diffuseTexture"];
            specularTextureEffectParam = materialEffect.Parameters["specularTexture"];
            specularTextureEnabledEffectParam = materialEffect.Parameters["specularTextureEnabled"];
            specularIntensityEffectParam = materialEffect.Parameters["specularIntensity"];
            specularPowerEffectParam = materialEffect.Parameters["specularPower"];
            specularColorEffectParam = materialEffect.Parameters["specularColor"];
            normalTextureEffectParam = materialEffect.Parameters["normalTexture"];
            normalTextureEnabledEffectParam = materialEffect.Parameters["normalTextureEnabled"];
            emissiveTextureEffectParam = materialEffect.Parameters["emissiveTexture"];
            emissiveTextureEnabledEffectParam = materialEffect.Parameters["emissiveTextureEnabled"];
            textureCoordUEffectParam = materialEffect.Parameters["texCoordU"];
            textureCoordVEffectParam = materialEffect.Parameters["texCoordV"];
            lightOcclusionEffectParam = materialEffect.Parameters["lightOcclusion"];
            worldEffectParam = materialEffect.Parameters["world"];
        }

        #region Material Properties

        /// <summary>
        /// Set or get the diffuse texture.
        /// </summary>
        public string DiffuseTexture
        {
            set
            {
                if (value == null)
                {
                    diffuseTextureName = value;
                    diffuseTexture = null;
                    diffuseTextureEnabledEffectParam.SetValue(false);
                    diffuseTextureEffectParam.SetValue((Texture)null);
                }
                else if(diffuseTextureName != value)
                {
                    diffuseTextureName = value;
                    diffuseTexture = content.Load<Texture2D>(diffuseTextureName);
                    diffuseTextureEnabledEffectParam.SetValue(true);
                    diffuseTextureEffectParam.SetValue(diffuseTexture);
                }
            }

            get { return diffuseTextureName; }
        }

        /// <summary>
        /// Set or get the specular texture.
        /// </summary>
        public string SpecularTexture
        {
            set
            {
                if (value == null)
                {
                    specularTextureName = value;
                    specularTexture = null;
                    specularTextureEnabledEffectParam.SetValue(false);
                    specularTextureEffectParam.SetValue((Texture2D)null);
                }
                else if(specularTextureName != value)
                {
                    specularTextureName = value;
                    specularTexture = content.Load<Texture2D>(specularTextureName);
                    specularTextureEnabledEffectParam.SetValue(true);
                    specularTextureEffectParam.SetValue(specularTexture);
                }
            }

            get { return specularTextureName; }
        }

        /// <summary>
        /// Set or get the normal texture.
        /// </summary>
        public string NormalTexture
        {
            set
            {
                if (value == null)
                {
                    normalTextureName = value;
                    normalTexture = null;
                    normalTextureEnabledEffectParam.SetValue(false);
                    normalTextureEffectParam.SetValue((Texture2D)null);
                }
                else if(normalTextureName != value)
                {
                    normalTextureName = value;
                    normalTexture = content.Load<Texture2D>(normalTextureName);
                    normalTextureEnabledEffectParam.SetValue(true);
                    normalTextureEffectParam.SetValue(normalTexture);
                }
            }

            get { return normalTextureName; }
        }

        /// <summary>
        /// Set or get the emissive texture.
        /// </summary>
        public string EmissiveTexture
        {
            set
            {
                if (value == null)
                {
                    emissiveTextureName = value;
                    emissiveTexture = null;
                    emissiveTextureEnabledEffectParam.SetValue(false);
                    emissiveTextureEffectParam.SetValue((Texture2D)null);
                }
                else if (emissiveTextureName != value)
                {
                    emissiveTextureName = value;
                    emissiveTexture = content.Load<Texture2D>(emissiveTextureName);
                    emissiveTextureEnabledEffectParam.SetValue(true);
                    emissiveTextureEffectParam.SetValue(emissiveTexture);
                }
            }

            get { return emissiveTextureName; }
        }

        /// <summary>
        /// Set or get the specular intensity.
        /// </summary>
        public float SpecularIntensity
        {
            set
            {
                specularIntensity = value;

                specularIntensityEffectParam.SetValue(specularIntensity);
            }

            get { return specularIntensity; }
        }

        /// <summary>
        /// Set or get the specular power.
        /// </summary>
        public float SpecularPower
        {
            set {  
                specularPower = value;
                specularPowerEffectParam.SetValue(specularPower);
            }

            get { return specularPower; }
        }

        /// <summary>
        /// Set or get the specular color.
        /// </summary>
        /// <remarks>This property will not be used for deferred shading rendering system, but include here in case adapting to another system.</remarks>
        public Vector4 SpecularColor
        {
            set { 
                specularColor = value;
                specularColorEffectParam.SetValue(specularColor);
            }
            get { return specularColor; }
        }

        /// <summary>
        /// Set or get the material's color.
        /// </summary>
        /// <remarks>If not set, will use white color.</remarks>
        public Vector4 MaterialColor
        {
            set
            {
                colorValue = value;
                //if colorValue is valid, we send the new value to GPU (default is white)
                if (colorValue != null)
                {
                    materialColorValueEffectParam.SetValue(colorValue);
                }
            }
            get { return colorValue; }
        }

        /// <summary>
        /// Set or get the texture's coordinate U.
        /// </summary>
        /// <remarks>If not set, then will use 1.0.</remarks>
        public float TextureCoordU
        {
            set
            {
                if (value > 0.0f)
                {
                    textureCoordU = value;
                    textureCoordUEffectParam.SetValue(textureCoordU);
                }
            }

            get { return textureCoordU; }
        }

        /// <summary>
        /// Set or get the texture's coordinate V.
        /// </summary>
        /// <remarks>If not set, then will use 1.0.</remarks>
        public float TextureCoordV
        {
            set
            {
                if (value > 0.0f)
                {
                    textureCoordV = value;
                    textureCoordVEffectParam.SetValue(textureCoordV);
                }
            }

            get { return textureCoordV; }
        }

        /// <summary>
        /// Set or get the light occlusion value.
        /// </summary>
        /// <remarks>Light occlusion, 0 means all lights can be taken into effect, otherwise no lights can be taken into effect.</remarks>
        public float LightOcclusion
        {
            set
            {
                lightOcclusion = value;
                lightOcclusionEffectParam.SetValue(lightOcclusion);
            }

            get { return lightOcclusion; }
        }

        #endregion

        /// <summary>
        /// Cache the orignal bone transforms.
        /// </summary>
        /// <param name="model">Model to cache its original bone transforms.</param>
        public void CacheOriginalBoneTransforms(Model model)
        {
            // auto clear previous cache
            if (originalBoneTransforms != null)
                // clear
                ClearOriginalBoneTransformsCache();

            // allocate new space
            originalBoneTransforms = new Matrix[model.Bones.Count];

            int i=0;
            foreach (ModelBone bone in model.Bones)
            {
                originalBoneTransforms[i++] = bone.Transform;
            }

            // caching absolute bone transform for static transformation
            // note: for dynamic transformation, it will update up on the newly cached data
            absBoneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(absBoneTransforms);
        }

        /// <summary>
        /// Clear original bone transform cache.
        /// </summary>
        public void ClearOriginalBoneTransformsCache()
        {
            originalBoneTransforms = null;
            absBoneTransforms = null;
        }

        /// <summary>
        /// Draw the individual mesh from the specified meshIndex.
        /// Use this method if there is no need to modify the current texture applied to the drawing process.
        /// </summary>
        /// <remarks>The texture material is already set prior to this call, so we only need to draw the geomatry.
        /// Also this method assumes that the desire mesh contains only 1 mesh-part, so only first mesh-part will be applied to drawing process.
        /// The transformation applied here is taken into account only for external transformation, the internal transformation of model will
        ///  be handle automatically.</remarks>
        /// <param name="gameTime"></param>
        /// <param name="model"></param>
        /// <param name="world"></param>
        /// <param name="model"></param>
        /// <param name="meshIndex"></param>
        /// <param name="boneIndex"></param>
        public void DrawBasic(GameTime gameTime, Matrix world, Model model, int meshIndex, int boneIndex)
        {
            currentMesh = model.Meshes[meshIndex];

            foreach (ModelMeshPart currentMeshPart in currentMesh.MeshParts)
            {
                //set up the geometry buffer and its properties
                graphicsDevice.Vertices[0].SetSource(currentMesh.VertexBuffer, currentMeshPart.StreamOffset, currentMeshPart.VertexStride);
                graphicsDevice.Indices = currentMesh.IndexBuffer;
                graphicsDevice.VertexDeclaration = currentMeshPart.VertexDeclaration;

                // for static transform -> benefit from caching
                worldEffectParam.SetValue(absBoneTransforms[model.Meshes[meshIndex].ParentBone.Index] * world);

                //[THIS SECTION OF CODE CAN BE COPIED AND USED IN ANOTHER PART]
                //set the world matrix for this model
                //we will concern entirely of the bone issue, thus it's safe to iterate all the parent-bone and apply them with
                // the current mesh's transform.
                //Note: By doing this way, we can reduce the calculation load that might be happen when we use CopyAbsoluteBoneTransformsTo() method
                // of model class.
                /*if (model.Bones[boneIndex] != null)
                {
                    currentBone = model.Bones[boneIndex].Parent;

                    //multi level of bone
                    if (currentBone != null)
                    {
                        currentParentTransform = currentBone.Transform;
                        while (currentBone.Parent != null)
                        {
                            currentParentTransform = currentBone.Parent.Transform * currentParentTransform;
                            currentBone = currentBone.Parent;
                        }
                        worldEffectParam.SetValue(currentParentTransform * model.Bones[boneIndex].Transform * world);
                    }
                    //one level of bone
                    else
                    {
                        worldEffectParam.SetValue(model.Bones[boneIndex].Transform * world);
                    }
                }
                else
                {
                    //no bone at all
                    worldEffectParam.SetValue(world);
                }*/
                //[END OF THE SECTION]

                //begin effect
                materialEffect.Begin();

                //begin the Ambient pass
                materialEffect.CurrentTechnique.Passes[0].Begin();

                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, currentMeshPart.BaseVertex, 0,
                    currentMeshPart.NumVertices, currentMeshPart.StartIndex,
                    currentMeshPart.PrimitiveCount);

                materialEffect.CurrentTechnique.Passes[0].End();

                materialEffect.End();
            }
        }

        /// <summary>
        /// Draw the individual mesh from the specified meshIndex and texture.
        /// </summary>
        /// <remarks>The texture material can be set at the time of calling this method.
        /// Also this method assumes that the desire mesh contains only 1 mesh-part, so only first mesh-part will be applied to drawing process.
        /// The transformation applied here is taken into account only for external transformation, the internal transformation of model will
        ///  be handle automatically.</remarks>
        /// <param name="gameTime"></param>
        /// <param name="world"></param>
        /// <param name="model"></param>
        /// <param name="meshIndex"></param>
        /// <param name="boneIndex"></param>
        /// <param name="textureSet">Texture set contains 'diffuse', 'normal', and 'specular' texture.</param>
        public void DrawBasic(GameTime gameTime, Matrix world, Model model, int meshIndex, int boneIndex, ContentShared.TextureSet textureSet)
        {
            currentMesh = model.Meshes[meshIndex];

            foreach (ModelMeshPart currentMeshPart in currentMesh.MeshParts)
            {
                //set up the geometry buffer and its properties
                graphicsDevice.Vertices[0].SetSource(currentMesh.VertexBuffer, currentMeshPart.StreamOffset, currentMeshPart.VertexStride);
                graphicsDevice.Indices = currentMesh.IndexBuffer;
                graphicsDevice.VertexDeclaration = currentMeshPart.VertexDeclaration;

                // for static transform -> benefit from caching
                worldEffectParam.SetValue(absBoneTransforms[model.Meshes[meshIndex].ParentBone.Index] * world);

                //[THIS SECTION OF CODE CAN BE COPIED AND USED IN ANOTHER PART]
                //set the world matrix for this model
                //we will concern entirely of the bone issue, thus it's safe to iterate all the parent-bone and apply them with
                // the current mesh's transform.
                //Note: By doing this way, we can reduce the calculation load that might be happen when we use CopyAbsoluteBoneTransformsTo() method
                // of model class.
                /*currentBone = model.Bones[boneIndex].Parent;
                currentParentTransform = currentBone.Transform;
                while (currentBone.Parent != null)
                {
                    currentParentTransform = currentBone.Parent.Transform * currentParentTransform;
                    currentBone = currentBone.Parent;
                }
                worldEffectParam.SetValue(currentParentTransform * model.Bones[boneIndex].Transform * world);*/
                //[END OF THE SECTION]

                //set the textures (only send the new data to GPU if each one is not the same as before)
                DiffuseTexture = textureSet.Diffuse;
                NormalTexture = textureSet.Normal;
                SpecularTexture = textureSet.Specular;

                //begin effect
                materialEffect.Begin();

                //begin the Ambient pass
                materialEffect.CurrentTechnique.Passes[0].Begin();

                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, currentMeshPart.BaseVertex, 0,
                    currentMeshPart.NumVertices, currentMeshPart.StartIndex,
                    currentMeshPart.PrimitiveCount);

                materialEffect.CurrentTechnique.Passes[0].End();

                materialEffect.End();
            }
        }

        /// <summary>
        /// Draw the individual mesh from the specified meshIndex following the specified effect.
        /// Use this method if there is no need to modify the current texture applied to the drawing process.
        /// 
        /// Note: User that need to draw special purpose of effect's draw call, should call this method and specify the effect to be used in
        ///  drawing process at the last argument.
        ///  Also any in need prior to draw this entity, can be set before calling this method.
        /// </summary>
        /// <remarks>The texture material is already set prior to this call, so we only need to draw the geomatry.
        /// Also this method assumes that the desire mesh contains only 1 mesh-part, so only first mesh-part will be applied to drawing process.
        /// The transformation applied here is taken into account only for external transformation, the internal transformation of model will
        ///  be handle automatically.</remarks>
        /// <param name="gameTime"></param>
        /// <param name="model"></param>
        /// <param name="world"></param>
        /// <param name="model"></param>
        /// <param name="meshIndex"></param>
        /// <param name="boneIndex"></param>
        /// <param name="effect"></param>
        public void DrawBasic(GameTime gameTime, Matrix world, Model model, int meshIndex, int boneIndex, Effect effect)
        {
            currentMesh = model.Meshes[meshIndex];

            foreach (ModelMeshPart currentMeshPart in currentMesh.MeshParts)
            {
                //set up the geometry buffer and its properties
                graphicsDevice.Vertices[0].SetSource(currentMesh.VertexBuffer, currentMeshPart.StreamOffset, currentMeshPart.VertexStride);
                graphicsDevice.Indices = currentMesh.IndexBuffer;
                graphicsDevice.VertexDeclaration = currentMeshPart.VertexDeclaration;

                // for static transform -> benefit from caching
                worldEffectParam.SetValue(absBoneTransforms[model.Meshes[meshIndex].ParentBone.Index] * world);

                //[THIS SECTION OF CODE CAN BE COPIED AND USED IN ANOTHER PART]
                //set the world matrix for this model
                //we will concern entirely of the bone issue, thus it's safe to iterate all the parent-bone and apply them with
                // the current mesh's transform.
                //Note: By doing this way, we can reduce the calculation load that might be happen when we use CopyAbsoluteBoneTransformsTo() method
                // of model class.
                /*if (model.Bones[boneIndex] != null)
                {
                    currentBone = model.Bones[boneIndex].Parent;

                    //multi level of bone
                    if (currentBone != null)
                    {
                        currentParentTransform = currentBone.Transform;
                        while (currentBone.Parent != null)
                        {
                            currentParentTransform = currentBone.Parent.Transform * currentParentTransform;
                            currentBone = currentBone.Parent;
                        }
                        effect.Parameters["world"].SetValue(currentParentTransform * model.Bones[boneIndex].Transform * world);
                    }
                    //one level of bone
                    else
                    {
                        effect.Parameters["world"].SetValue(model.Bones[boneIndex].Transform * world);
                    }
                }
                else
                {
                    //no bone at all
                    effect.Parameters["world"].SetValue(world);
                }*/
                //[END OF THE SECTION]

                //begin effect
                effect.Begin();

                //begin the Ambient pass
                effect.CurrentTechnique.Passes[0].Begin();

                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, currentMeshPart.BaseVertex, 0,
                    currentMeshPart.NumVertices, currentMeshPart.StartIndex,
                    currentMeshPart.PrimitiveCount);

                effect.CurrentTechnique.Passes[0].End();

                effect.End();
            }
        }

        /// <summary>
        /// Draw the individual mesh from the specified meshIndex.
        /// Use this method if there is no need to modify the current texture applied to the drawing process but with a need to apply
        ///     transformation to the specified mesh index.
        /// Must set original bone transforms first with CacheOriginalBoneTransforms().
        /// </summary>
        /// <remarks>The texture material is already set prior to this call, so we only need to draw the geomatry.
        /// Also this method assumes that the desire mesh contains only 1 mesh-part, so only first mesh-part will be applied to drawing process.
        /// The transformation applied here is taken into account only for external transformation, the internal transformation of model will
        ///  be handle automatically.</remarks>
        /// <param name="gameTime"></param>
        /// <param name="model"></param>
        /// <param name="world"></param>
        /// <param name="model"></param>
        /// <param name="meshIndex"></param>
        /// <param name="boneIndex"></param>
        public void DrawBasicMeshTransform(GameTime gameTime, Matrix world, Matrix mesh, Model model, int meshIndex, int boneIndex)
        {
            currentMesh = model.Meshes[meshIndex];

            foreach (ModelMeshPart currentMeshPart in currentMesh.MeshParts)
            {
                //set up the geometry buffer and its properties
                graphicsDevice.Vertices[0].SetSource(currentMesh.VertexBuffer, currentMeshPart.StreamOffset, currentMeshPart.VertexStride);
                graphicsDevice.Indices = currentMesh.IndexBuffer;
                graphicsDevice.VertexDeclaration = currentMeshPart.VertexDeclaration;

                // apply bone transform to mesh
                Matrix selfBone = mesh * originalBoneTransforms[boneIndex];
                model.Bones[boneIndex].Transform = selfBone;

                // discard the previuos absolute bone-transform, and update with new one
                absBoneTransforms = null;
                absBoneTransforms = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(absBoneTransforms);

                worldEffectParam.SetValue(absBoneTransforms[model.Meshes[meshIndex].ParentBone.Index] * world);

                // reset back bone-transform to original
                model.Bones[boneIndex].Transform = originalBoneTransforms[boneIndex];

                //begin effect
                materialEffect.Begin();

                //begin the Ambient pass
                materialEffect.CurrentTechnique.Passes[0].Begin();

                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, currentMeshPart.BaseVertex, 0,
                    currentMeshPart.NumVertices, currentMeshPart.StartIndex,
                    currentMeshPart.PrimitiveCount);

                materialEffect.CurrentTechnique.Passes[0].End();

                materialEffect.End();
            }
        }
    }
}
