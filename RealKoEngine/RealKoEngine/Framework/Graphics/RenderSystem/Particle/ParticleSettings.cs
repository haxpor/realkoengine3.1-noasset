#region File Description
//-----------------------------------------------------------------------------
// ParticleSettings.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace RealKo.Framework.Graphics.Particle
{
    /// <summary>
    /// Settings class describes all the tweakable options used
    /// to control the appearance of a particle system.
    /// </summary>
    public class ParticleSettings
    {
        // Name of the texture used by this particle system.
        public string TextureName = null;


        // Maximum number of particles that can be displayed at one time.
        public int MaxParticles = 100;


        // How long these particles will last.
        public TimeSpan Duration = TimeSpan.FromSeconds(1);


        // If greater than zero, some particles will last a shorter time than others.
        public float DurationRandomness = 0;


        // Controls how much particles are influenced by the velocity of the object
        // which created them. You can see this in action with the explosion effect,
        // where the flames continue to move in the same direction as the source
        // projectile. The projectile trail particles, on the other hand, set this
        // value very low so they are less affected by the velocity of the projectile.
        public float EmitterVelocitySensitivity = 1;


        // Range of values controlling how much X and Z axis velocity to give each
        // particle. Values for individual particles are randomly chosen from somewhere
        // between these limits.
        public float MinHorizontalVelocity = 0;
        public float MaxHorizontalVelocity = 0;

        // Whether or not, each frame the horizontal velocity's direction will be randomed
        public bool IsHorizontalVelocityDirectionRandomed = true;


        // Range of values controlling how much Y axis velocity to give each particle.
        // Values for individual particles are randomly chosen from somewhere between
        // these limits.
        public float MinVerticalVelocity = 0;
        public float MaxVerticalVelocity = 0;


        // Direction and strength of the gravity effect. Note that this can point in any
        // direction, not just down! The fire effect points it upward to make the flames
        // rise, and the smoke plume points it sideways to simulate wind.
        // Note: Implemented specially for RealKo Engine, at the first time Gravity will be set to Wind (setting from simulation config file)
        //       But any particle system can have different Gravity, set individually to suit the particular style of each particle system's effect.
        public Vector3 Gravity = Vector3.Zero;

        // Boolean indicates whether or not this particle effect will be affected from the change of wind
        // Note: For the creation of the particle system's instance, user still need to specify the Gravity manually if desire other value
        //       other than global wind from simulation config file.
        //       This value will be used externally from the deferred shading rendering system, to check whether or not to update it.
        public bool IsAffectedByWindChanging = true;


        // Controls how the particle velocity will change over their lifetime. If set
        // to 1, particles will keep going at the same speed as when they were created.
        // If set to 0, particles will come to a complete stop right before they die.
        // Values greater than 1 make the particles speed up over time.
        public float EndVelocity = 1;


        // Range of values controlling the particle color and alpha. Values for
        // individual particles are randomly chosen from somewhere between these limits.
        public Color MinColor = Color.White;
        public Color MaxColor = Color.White;


        // Range of values controlling how fast the particles rotate. Values for
        // individual particles are randomly chosen from somewhere between these
        // limits. If both these values are set to 0, the particle system will
        // automatically switch to an alternative shader technique that does not
        // support rotation, and thus requires significantly less GPU power. This
        // means if you don't need the rotation effect, you may get a performance
        // boost from leaving these values at 0.
        public float MinRotateSpeed = 0;
        public float MaxRotateSpeed = 0;


        // Range of values controlling how big the particles are when first created.
        // Values for individual particles are randomly chosen from somewhere between
        // these limits.
        public float MinStartSize = 100;
        public float MaxStartSize = 100;


        // Range of values controlling how big particles become at the end of their
        // life. Values for individual particles are randomly chosen from somewhere
        // between these limits.
        public float MinEndSize = 100;
        public float MaxEndSize = 100;

        // Alpha blending settings.
        // These combination can be sumed up to create a brand new visual effect.
        public BlendFunction BlendFunction = BlendFunction.Add;
        public Blend SourceBlend = Blend.SourceAlpha;
        public Blend DestinationBlend = Blend.InverseSourceAlpha;

        // Blend factor, used if settings the SourceBlend, or DestinationBlend to use the BlendFactor
        public Color BlendFactor = Color.White;

        // Pointsprite max size
        // Set this value with careful, as it can hurt your performance
        // Note: Fog can benefit from setting this value high
        public float PointSizeMax = 256;
    }
}
