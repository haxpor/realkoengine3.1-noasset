﻿#region File Description
//-----------------------------------------------------------------------------
// FogParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics.Particle
{
    public class FogParticleSystem : ParticleSystem
    {
        public FogParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Base/Textures/SP_FX_Volfog";

            settings.MaxParticles = 250;

            //should have the reasonable amount of life time (avoid suddenly fade)
            settings.Duration = TimeSpan.FromSeconds(100);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 0;

            settings.MinVerticalVelocity = 0;
            settings.MaxVerticalVelocity = 0;

            //treat wind as the gravity here
            settings.Gravity = Vector3.Zero;
            settings.IsAffectedByWindChanging = false;
            settings.IsHorizontalVelocityDirectionRandomed = false; //ensure no penalty hit at all for randoming

            settings.EndVelocity = 0.75f;

            settings.MinRotateSpeed = -0.12f;
            settings.MaxRotateSpeed = 0.12f;

            settings.MinStartSize = 1024;
            settings.MaxStartSize = 1024;

            settings.MinEndSize = 1024;
            settings.MaxEndSize = 1024;

            settings.MinColor = new Color(150, 141, 137);
            settings.MaxColor = new Color(133, 124, 120);

            settings.SourceBlend = Blend.One;
            settings.DestinationBlend = Blend.InverseSourceColor;
            //use bigger resolution for texture, thus reducing the number of particles to be created
            //and can achieve stunting visual effect
            settings.PointSizeMax = 1024;
        }
    }
}
