﻿#region File Description
//-----------------------------------------------------------------------------
// SnowParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics.Particle
{
    public class SnowParticleSystem : ParticleSystem
    {
        public SnowParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Base/Textures/SP_FX_Snowflake";

            settings.MaxParticles = 8000;

            settings.Duration = TimeSpan.FromSeconds(7);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 1;

            settings.MinVerticalVelocity = -3;
            settings.MaxVerticalVelocity = -2;

            //treat wind as the gravity here
            settings.Gravity = Engine.SimulationSettings.Wind;

            settings.EndVelocity = 0.75f;

            settings.MinRotateSpeed = -5;
            settings.MaxRotateSpeed = 5;

            settings.MinStartSize = 1;
            settings.MaxStartSize = 1;

            settings.MinEndSize = 0.25f;
            settings.MaxEndSize = 0.5f;

            settings.MinColor = Color.Snow;
            settings.MaxColor = Color.White;

            settings.SourceBlend = Blend.One;
            settings.DestinationBlend = Blend.One;
            settings.BlendFunction = BlendFunction.Max;
        }
    }
}
