﻿#region File Description
//-----------------------------------------------------------------------------
// DuskParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics.Particle
{
    public class DuskParticleSystem : ParticleSystem
    {
        public DuskParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Base/Textures/SP_FX_Dusk";

            settings.MaxParticles = 1500;

            //should have the reasonable amount of life time (avoid suddenly fade)
            settings.Duration = TimeSpan.FromSeconds(5);

            settings.MinHorizontalVelocity = 5;
            settings.MaxHorizontalVelocity = 15;

            settings.MinVerticalVelocity = 0;
            settings.MaxVerticalVelocity = 0;

            //treat wind as the gravity here
            //only get the x-component of wind
            //if need this particle system to follow the wind, user need to do it manually and externally
            settings.Gravity = new Vector3(Engine.SimulationSettings.Wind.X, 0, 0);
            settings.IsAffectedByWindChanging = false;
            //only one direction to go
            settings.IsHorizontalVelocityDirectionRandomed = false;

            settings.EndVelocity = 1f;

            settings.MinRotateSpeed = -3f;
            settings.MaxRotateSpeed = 3f;

            settings.MinStartSize = 256;
            settings.MaxStartSize = 256;

            settings.MinEndSize = 256;
            settings.MaxEndSize = 256;

            settings.MinColor = new Color(213, 192, 145);
            settings.MaxColor = new Color(213, 192, 145);

            settings.SourceBlend = Blend.One;
            settings.DestinationBlend = Blend.InverseSourceColor;
        }
    }
}
