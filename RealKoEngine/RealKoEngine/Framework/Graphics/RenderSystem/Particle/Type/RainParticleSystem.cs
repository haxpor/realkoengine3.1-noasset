﻿#region File Description
//-----------------------------------------------------------------------------
// RainParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics.Particle
{
    public class RainParticleSystem : ParticleSystem
    {
        public RainParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Base/Textures/SP_FX_Raindrop";

            settings.MaxParticles = 8000;

            settings.Duration = TimeSpan.FromSeconds(3);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 0;

            settings.MinVerticalVelocity = -45;
            settings.MaxVerticalVelocity = -55;

            //treat wind as the gravity here
            settings.Gravity = new Vector3(0,-10,0);
            settings.IsAffectedByWindChanging = false;
            settings.IsHorizontalVelocityDirectionRandomed = false;

            settings.EndVelocity = 1f;

            settings.MinRotateSpeed = 0;
            settings.MaxRotateSpeed = 0;

            settings.MinStartSize = 10;
            settings.MaxStartSize = 10;

            settings.MinEndSize = 10;
            settings.MaxEndSize = 10;

            settings.SourceBlend = Blend.BlendFactor;
            settings.DestinationBlend = Blend.One;

            settings.BlendFactor = new Color(0.25f, 0.25f, 0.25f, 1.0f);
        }
    }
}
