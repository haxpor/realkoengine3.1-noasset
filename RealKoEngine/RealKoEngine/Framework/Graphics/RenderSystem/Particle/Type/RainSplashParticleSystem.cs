﻿#region File Description
//-----------------------------------------------------------------------------
// RainSplashParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics.Particle
{
    public class RainSplashParticleSystem : ParticleSystem
    {
        public RainSplashParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Base/Textures/SP_FX_Rainsplash";

            settings.MaxParticles = 10000;

            settings.Duration = TimeSpan.FromSeconds(0.125f);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 0;

            settings.MinVerticalVelocity = 0;
            settings.MaxVerticalVelocity = 0;

            //treat wind as the gravity here
            settings.Gravity = Vector3.Zero;
            settings.IsAffectedByWindChanging = false;
            settings.IsHorizontalVelocityDirectionRandomed = false;

            settings.EndVelocity = 1f;

            settings.MinRotateSpeed = 0;
            settings.MaxRotateSpeed = 0;

            settings.MinStartSize = 0.06f;
            settings.MaxStartSize = 0.07f;

            settings.MinEndSize = 0.01f;
            settings.MaxEndSize = 0.05f;

            settings.SourceBlend = Blend.SourceAlpha;
            settings.DestinationBlend = Blend.One;
        }
    }
}
