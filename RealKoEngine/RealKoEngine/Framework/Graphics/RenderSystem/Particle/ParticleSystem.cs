#region File Description
//-----------------------------------------------------------------------------
// ParticleSystem.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Accompanied by: Microsoft, Creators Club
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace RealKo.Framework.Graphics.Particle
{
    /// <summary>
    /// The main component in charge of displaying particles.
    /// </summary>
    public abstract class ParticleSystem
    {
        #region Fields

        // Graphics device reference from the game class.
        protected GraphicsDevice graphicsDevice;

        // Settings class controls the appearance and animation of this particle system.
        protected ParticleSettings settings = new ParticleSettings();

        // For loading the effect and particle texture.
        protected ContentManager content;

        // Custom effect for drawing point sprite particles. This computes the particle
        // animation entirely in the vertex shader: no per-particle CPU work required!
        protected Effect particleEffect;

        // Name representing this particle system.
        protected string name;

        // Depth for this particle system.
        // Greater means draw later, lower means draw first.
        protected int depth;

        // Shortcuts for accessing frequently changed effect parameters.
        protected EffectParameter effectViewParameter;
        protected EffectParameter effectProjectionParameter;
        protected EffectParameter effectViewportHeightParameter;
        protected EffectParameter effectTimeParameter;
        protected EffectParameter effectDurationParameter;
        protected EffectParameter effectDurationRandomnessParameter;
        protected EffectParameter effectGravityParameter;
        protected EffectParameter effectEndVelocityParameter;
        protected EffectParameter effectMinColorParameter;
        protected EffectParameter effectMaxColorParameter;
        protected EffectParameter effectRotationSpeedParameter;
        protected EffectParameter effectStartSizeParameter;
        protected EffectParameter effectEndSizeParameter;
        protected EffectParameter effectTextureParameter;

        #region Misc Properties

        /// <summary>
        /// Get or set the name representing this particle system.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Get or set depth value for this particle system.
        /// </summary>
        public int Depth
        {
            get { return depth; }
            set { depth = value; }
        }

        #endregion

        #region Properties that can be changed during runtime

        /// <summary>
        /// Get or set the duration of individual particle of this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public TimeSpan Duration
        {
            get { return settings.Duration; }
            set
            {
                settings.Duration = value;
                effectDurationParameter.SetValue((float)settings.Duration.TotalSeconds);
            }
        }

        /// <summary>
        /// Get or set the duration randomness of this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public float DurationRandomness
        {
            get { return settings.DurationRandomness; }
            set
            {
                settings.DurationRandomness = value;
                effectDurationRandomnessParameter.SetValue(settings.DurationRandomness);
            }
        }

        /// <summary>
        /// Get or set the gravity affected to this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.
        /// This is the only one of global settings from simulation config file, that all the particle system need to update.
        /// This method will check first whether the change of data resides in GPU is need, if so it will resend new data to GPU.</remarks>
        public Vector3 Gravity
        {
            get { return settings.Gravity; }
            set
            {
                if (settings.Gravity != value)
                {
                    //lerping the gravity
                    Vector3 lerpedGravity;
                    lerpedGravity.X = MathHelper.Lerp(settings.Gravity.X, value.X, Engine.SimulationSettings.WindChangeRate);
                    lerpedGravity.Y = MathHelper.Lerp(settings.Gravity.Y, value.Y, Engine.SimulationSettings.WindChangeRate);
                    lerpedGravity.Z = MathHelper.Lerp(settings.Gravity.Z, value.Z, Engine.SimulationSettings.WindChangeRate);

                    //set the new lerped to current gravity
                    settings.Gravity = lerpedGravity;

                    effectGravityParameter.SetValue(settings.Gravity);
                }
            }
        }

        /// <summary>
        /// Get or set the boolean indicated whether or not this particle system will be affected by the changing of wind.
        /// </summary>
        /// <remarks>This property doesn't involve sending itself data to GPU, but affect the its behavior.</remarks>
        public bool IsAffectedByWindChanging
        {
            get { return settings.IsAffectedByWindChanging; }
            set
            {
                settings.IsAffectedByWindChanging = value;
            }
        }

        /// <summary>
        /// Get or set the end velocity for each individual particle of this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public float EndVelocity
        {
            get { return settings.EndVelocity; }
            set
            {
                settings.EndVelocity = value;
                effectEndVelocityParameter.SetValue(settings.EndVelocity);
            }
        }

        /// <summary>
        /// Get or set the minimum color for each individual particle of this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public Color MinColor
        {
            get { return settings.MinColor; }
            set
            {
                settings.MinColor = value;
                effectMinColorParameter.SetValue(settings.MinColor.ToVector4());
            }
        }

        /// <summary>
        /// Get or set the maximum color for each individual particle of this particle system.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public Color MaxColor
        {
            get { return settings.MaxColor; }
            set
            {
                settings.MaxColor = value;
                effectMaxColorParameter.SetValue(settings.MaxColor.ToVector4());
            }
        }

        /// <summary>
        /// Get or set 'min and max rotated speed-packed data'.
        /// 
        /// Min rotation speed is in x component of Vector2.
        /// Max rotation speed is in y component of Vector2.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public Vector2 MinMaxRotateSpeed
        {
            get { return new Vector2(settings.MinRotateSpeed, settings.MaxRotateSpeed); }
            set
            {
                settings.MinRotateSpeed = value.X;
                settings.MaxRotateSpeed = value.Y;

                effectRotationSpeedParameter.SetValue(new Vector2(settings.MinRotateSpeed, settings.MaxRotateSpeed));
            }
        }

        /// <summary>
        /// Get or set 'min and max start size-packed data'.
        /// 
        /// Min start size is in x component of Vector2.
        /// Max start size is in y component of Vector2.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public Vector2 MinMaxStartSize
        {
            get { return new Vector2(settings.MinStartSize, settings.MaxStartSize); }
            set
            {
                settings.MinStartSize = value.X;
                settings.MaxStartSize = value.Y;

                effectStartSizeParameter.SetValue(new Vector2(settings.MinStartSize, settings.MaxStartSize));
            }
        }

        /// <summary>
        /// Get or set 'min and max end size-packed data'.
        /// 
        /// Min end size is in x component of Vector2.
        /// Max end size is in y component of Vector2.
        /// </summary>
        /// <remarks>See ParticleSettings class for more detail.</remarks>
        public Vector2 MinMaxEndSize
        {
            get { return new Vector2(settings.MinEndSize, settings.MaxEndSize); }
            set
            {
                settings.MinEndSize = value.X;
                settings.MaxEndSize = value.Y;

                effectEndSizeParameter.SetValue(new Vector2(settings.MinEndSize, settings.MaxEndSize));
            }
        }

        /// <summary>
        /// Texture used for this particle system.
        /// </summary>
        /// <remarks>The texture can be changed during runtime.</remarks>
        public string Texture
        {
            get { return settings.TextureName; }
            set
            {
                //reduce the overhead of loading asset
                if (settings.TextureName != value)
                {
                    settings.TextureName = value;
                    effectTextureParameter.SetValue(content.Load<Texture2D>(settings.TextureName));
                }
            }
        }

        /// <summary>
        /// Get or set the minimum horizontal velocity of this particle system.
        /// </summary>
        /// <remarks>This property doesn't send any data to GPU again.</remarks>
        public float MinHorizontalVelocity
        {
            get { return settings.MinHorizontalVelocity; }
            set
            {
                settings.MinHorizontalVelocity = value;
            }
        }

        /// <summary>
        /// Get or set whether or not the horizontal velocity's direction need to be randomed.
        /// </summary>
        /// <remarks>This property doesn't send any data to GPU again.</remarks>
        public bool IsHorizontalVelocityDirectionRandomed
        {
            get { return settings.IsHorizontalVelocityDirectionRandomed; }
            set
            {
                settings.IsHorizontalVelocityDirectionRandomed = value;
            }
        }

        /// <summary>
        /// Get or set the maximum horizontal velocity of this particle system.
        /// </summary>
        /// <remarks>This property doesn't send any data to GPU again.</remarks>
        public float MaxHorizontalVelocity
        {
            get { return settings.MaxHorizontalVelocity; }
            set
            {
                settings.MaxHorizontalVelocity = value;
            }
        }

        /// <summary>
        /// Get or set the minimum vertical velocity of this particle system.
        /// </summary>
        /// <remarks>This property doesn't send any data to GPU again.</remarks>
        public float MinVerticalVelocity
        {
            get { return settings.MinVerticalVelocity; }
            set
            {
                settings.MinVerticalVelocity = value;
            }
        }

        /// <summary>
        /// Get or set the maximum vertical velocity of this particle system.
        /// </summary>
        /// <remarks>This property doesn't send any data to GPU again.</remarks>
        public float MaxVerticalVelocity
        {
            get { return settings.MaxVerticalVelocity; }
            set
            {
                settings.MaxVerticalVelocity = value;
            }
        }

        #endregion

        // An array of particles, treated as a circular queue.
        ParticleVertex[] particles;


        // A vertex buffer holding our particles. This contains the same data as
        // the particles array, but copied across to where the GPU can access it.
        DynamicVertexBuffer vertexBuffer;


        // Vertex declaration describes the format of our ParticleVertex structure.
        VertexDeclaration vertexDeclaration;


        // The particles array and vertex buffer are treated as a circular queue.
        // Initially, the entire contents of the array are free, because no particles
        // are in use. When a new particle is created, this is allocated from the
        // beginning of the array. If more than one particle is created, these will
        // always be stored in a consecutive block of array elements. Because all
        // particles last for the same amount of time, old particles will always be
        // removed in order from the start of this active particle region, so the
        // active and free regions will never be intermingled. Because the queue is
        // circular, there can be times when the active particle region wraps from the
        // end of the array back to the start. The queue uses modulo arithmetic to
        // handle these cases. For instance with a four entry queue we could have:
        //
        //      0
        //      1 - first active particle
        //      2 
        //      3 - first free particle
        //
        // In this case, particles 1 and 2 are active, while 3 and 4 are free.
        // Using modulo arithmetic we could also have:
        //
        //      0
        //      1 - first free particle
        //      2 
        //      3 - first active particle
        //
        // Here, 3 and 0 are active, while 1 and 2 are free.
        //
        // But wait! The full story is even more complex.
        //
        // When we create a new particle, we add them to our managed particles array.
        // We also need to copy this new data into the GPU vertex buffer, but we don't
        // want to do that straight away, because setting new data into a vertex buffer
        // can be an expensive operation. If we are going to be adding several particles
        // in a single frame, it is faster to initially just store them in our managed
        // array, and then later upload them all to the GPU in one single call. So our
        // queue also needs a region for storing new particles that have been added to
        // the managed array but not yet uploaded to the vertex buffer.
        //
        // Another issue occurs when old particles are retired. The CPU and GPU run
        // asynchronously, so the GPU will often still be busy drawing the previous
        // frame while the CPU is working on the next frame. This can cause a
        // synchronization problem if an old particle is retired, and then immediately
        // overwritten by a new one, because the CPU might try to change the contents
        // of the vertex buffer while the GPU is still busy drawing the old data from
        // it. Normally the graphics driver will take care of this by waiting until
        // the GPU has finished drawing inside the VertexBuffer.SetData call, but we
        // don't want to waste time waiting around every time we try to add a new
        // particle! To avoid this delay, we can specify the SetDataOptions.NoOverwrite
        // flag when we write to the vertex buffer. This basically means "I promise I
        // will never try to overwrite any data that the GPU might still be using, so
        // you can just go ahead and update the buffer straight away". To keep this
        // promise, we must avoid reusing vertices immediately after they are drawn.
        //
        // So in total, our queue contains four different regions:
        //
        // Vertices between firstActiveParticle and firstNewParticle are actively
        // being drawn, and exist in both the managed particles array and the GPU
        // vertex buffer.
        //
        // Vertices between firstNewParticle and firstFreeParticle are newly created,
        // and exist only in the managed particles array. These need to be uploaded
        // to the GPU at the start of the next draw call.
        //
        // Vertices between firstFreeParticle and firstRetiredParticle are free and
        // waiting to be allocated.
        //
        // Vertices between firstRetiredParticle and firstActiveParticle are no longer
        // being drawn, but were drawn recently enough that the GPU could still be
        // using them. These need to be kept around for a few more frames before they
        // can be reallocated.

        int firstActiveParticle;
        int firstNewParticle;
        int firstFreeParticle;
        int firstRetiredParticle;


        // Store the current time, in seconds.
        float currentTime;


        // Count how many times Draw has been called. This is used to know
        // when it is safe to retire old particles back into the free list.
        int drawCounter;


        // Shared random number generator.
        static Random random = new Random();


        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        protected ParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
        {
            this.graphicsDevice = graphicsDevice;
            this.content = content;
            this.name = name;

            Initialize();
        }


        /// <summary>
        /// Initializes the component.
        /// </summary>
        public void Initialize()
        {
            InitializeSettings(settings);

            particles = new ParticleVertex[settings.MaxParticles];

            // prepare the effect and vertex buffer
            LoadParticleEffect();

            vertexDeclaration = new VertexDeclaration(graphicsDevice,
                                                      ParticleVertex.VertexElements);

            // Create a dynamic vertex buffer.
            int size = ParticleVertex.SizeInBytes * particles.Length;

            vertexBuffer = new DynamicVertexBuffer(graphicsDevice, size,
                                                   BufferUsage.WriteOnly |
                                                   BufferUsage.Points);
        }


        /// <summary>
        /// Derived particle system classes should override this method
        /// and use it to initalize their tweakable settings.
        /// </summary>
        protected abstract void InitializeSettings(ParticleSettings settings);


        /// <summary>
        /// Helper for loading and initializing the particle effect.
        /// </summary>
        void LoadParticleEffect()
        {
            Effect effect = content.Load<Effect>("Base/Effects/ParticleEffect");

            // If we have several particle systems, the content manager will return
            // a single shared effect instance to them all. But we want to preconfigure
            // the effect with parameters that are specific to this particular
            // particle system. By cloning the effect, we prevent one particle system
            // from stomping over the parameter settings of another.
            
            particleEffect = effect.Clone(graphicsDevice);

            EffectParameterCollection parameters = particleEffect.Parameters;

            // Look up shortcuts for parameters that change every frame.
            effectViewParameter = parameters["View"];
            effectProjectionParameter = parameters["Projection"];
            effectViewportHeightParameter = parameters["ViewportHeight"];
            effectTimeParameter = parameters["CurrentTime"];
            effectDurationParameter = parameters["Duration"];
            effectDurationRandomnessParameter = parameters["DurationRandomness"];
            effectGravityParameter = parameters["Gravity"];
            effectEndVelocityParameter = parameters["EndVelocity"];
            effectMinColorParameter = parameters["MinColor"];
            effectMaxColorParameter = parameters["MaxColor"];
            effectRotationSpeedParameter = parameters["RotateSpeed"];
            effectStartSizeParameter = parameters["StartSize"];
            effectEndSizeParameter = parameters["EndSize"];
            effectTextureParameter = parameters["Texture"];

            // Set the value of parameters accordingly
            effectDurationParameter.SetValue((float)settings.Duration.TotalSeconds);
            effectDurationRandomnessParameter.SetValue(settings.DurationRandomness);
            effectGravityParameter.SetValue(settings.Gravity);
            effectEndVelocityParameter.SetValue(settings.EndVelocity);
            effectMinColorParameter.SetValue(settings.MinColor.ToVector4());
            effectMaxColorParameter.SetValue(settings.MaxColor.ToVector4());

            effectRotationSpeedParameter.SetValue(
                new Vector2(settings.MinRotateSpeed, settings.MaxRotateSpeed));
            
            effectStartSizeParameter.SetValue(
                new Vector2(settings.MinStartSize, settings.MaxStartSize));
            
            effectEndSizeParameter.SetValue(
                new Vector2(settings.MinEndSize, settings.MaxEndSize));

            // Load the particle texture, and set it onto the effect.
            Texture2D texture = content.Load<Texture2D>(settings.TextureName);

            effectTextureParameter.SetValue(texture);

            // Choose the appropriate effect technique. If these particles will never
            // rotate, we can use a simpler pixel shader that requires less GPU power.
            string techniqueName;

            if ((settings.MinRotateSpeed == 0) && (settings.MaxRotateSpeed == 0))
                techniqueName = "NonRotatingParticles";
            else
                techniqueName = "RotatingParticles";

            particleEffect.CurrentTechnique = particleEffect.Techniques[techniqueName];
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the particle system.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            if (gameTime == null)
                throw new ArgumentNullException("gameTime");

            currentTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            RetireActiveParticles();
            FreeRetiredParticles();

            // If we let our timer go on increasing for ever, it would eventually
            // run out of floating point precision, at which point the particles
            // would render incorrectly. An easy way to prevent this is to notice
            // that the time value doesn't matter when no particles are being drawn,
            // so we can reset it back to zero any time the active queue is empty.

            if (firstActiveParticle == firstFreeParticle)
                currentTime = 0;

            if (firstRetiredParticle == firstActiveParticle)
                drawCounter = 0;
        }


        /// <summary>
        /// Helper for checking when active particles have reached the end of
        /// their life. It moves old particles from the active area of the queue
        /// to the retired section.
        /// </summary>
        void RetireActiveParticles()
        {
            float particleDuration = (float)settings.Duration.TotalSeconds;

            while (firstActiveParticle != firstNewParticle)
            {
                // Is this particle old enough to retire?
                float particleAge = currentTime - particles[firstActiveParticle].Time;

                if (particleAge < particleDuration)
                    break;

                // Remember the time at which we retired this particle.
                particles[firstActiveParticle].Time = drawCounter;

                // Move the particle from the active to the retired queue.
                firstActiveParticle++;

                if (firstActiveParticle >= particles.Length)
                    firstActiveParticle = 0;
            }
        }


        /// <summary>
        /// Helper for checking when retired particles have been kept around long
        /// enough that we can be sure the GPU is no longer using them. It moves
        /// old particles from the retired area of the queue to the free section.
        /// </summary>
        void FreeRetiredParticles()
        {
            while (firstRetiredParticle != firstActiveParticle)
            {
                // Has this particle been unused long enough that
                // the GPU is sure to be finished with it?
                int age = drawCounter - (int)particles[firstRetiredParticle].Time;

                // The GPU is never supposed to get more than 2 frames behind the CPU.
                // We add 1 to that, just to be safe in case of buggy drivers that
                // might bend the rules and let the GPU get further behind.
                if (age < 3)
                    break;

                // Move the particle from the retired to the free queue.
                firstRetiredParticle++;

                if (firstRetiredParticle >= particles.Length)
                    firstRetiredParticle = 0;
            }
        }
        
        /// <summary>
        /// Draws the particle system.
        /// </summary>
        /// <remarks>Allow user to extend the drawing's functionality.</remarks>
        public void Draw(GameTime gameTime)
        {
            GraphicsDevice device = graphicsDevice;

            //set the camera's view and projection matrix
            SetCamera();

            // Restore the vertex buffer contents if the graphics device was lost.
            if (vertexBuffer.IsContentLost)
            {
                vertexBuffer.SetData(particles);
            }

            // If there are any particles waiting in the newly added queue,
            // we'd better upload them to the GPU ready for drawing.
            if (firstNewParticle != firstFreeParticle)
            {
                AddNewParticlesToVertexBuffer();
            }

            // If there are any active particles, draw them now!
            if (firstActiveParticle != firstFreeParticle)
            {
                SetParticleRenderStates(device.RenderState);

                // Set an effect parameter describing the viewport size. This is needed
                // to convert particle sizes into screen space point sprite sizes.
                effectViewportHeightParameter.SetValue(device.Viewport.Height);

                // Set an effect parameter describing the current time. All the vertex
                // shader particle animation is keyed off this value.
                effectTimeParameter.SetValue(currentTime);

                // Set the particle vertex buffer and vertex declaration.
                device.Vertices[0].SetSource(vertexBuffer, 0,
                                             ParticleVertex.SizeInBytes);

                device.VertexDeclaration = vertexDeclaration;

                // Activate the particle effect.
                particleEffect.Begin();

                foreach (EffectPass pass in particleEffect.CurrentTechnique.Passes)
                {
                    pass.Begin();

                    if (firstActiveParticle < firstFreeParticle)
                    {
                        // If the active particles are all in one consecutive range,
                        // we can draw them all in a single call.
                        device.DrawPrimitives(PrimitiveType.PointList,
                                              firstActiveParticle,
                                              firstFreeParticle - firstActiveParticle);
                    }
                    else
                    {
                        // If the active particle range wraps past the end of the queue
                        // back to the start, we must split them over two draw calls.
                        device.DrawPrimitives(PrimitiveType.PointList,
                                              firstActiveParticle,
                                              particles.Length - firstActiveParticle);

                        if (firstFreeParticle > 0)
                        {
                            device.DrawPrimitives(PrimitiveType.PointList,
                                                  0,
                                                  firstFreeParticle);
                        }
                    }

                    pass.End();
                }

                particleEffect.End();

                // Reset a couple of the more unusual renderstates that we changed,
                // so as not to mess up any other subsequent drawing.
                device.RenderState.PointSpriteEnable = false;
                device.RenderState.DepthBufferWriteEnable = true;
            }

            drawCounter++;

            //restore the renderstate to normal
            RestoreRenderStates(device.RenderState);
        }


        /// <summary>
        /// Helper for uploading new particles from our managed
        /// array to the GPU vertex buffer.
        /// </summary>
        void AddNewParticlesToVertexBuffer()
        {
            int stride = ParticleVertex.SizeInBytes;

            if (firstNewParticle < firstFreeParticle)
            {
                // If the new particles are all in one consecutive range,
                // we can upload them all in a single call.
                vertexBuffer.SetData(firstNewParticle * stride, particles,
                                     firstNewParticle,
                                     firstFreeParticle - firstNewParticle,
                                     stride, SetDataOptions.NoOverwrite);
            }
            else
            {
                // If the new particle range wraps past the end of the queue
                // back to the start, we must split them over two upload calls.
                vertexBuffer.SetData(firstNewParticle * stride, particles,
                                     firstNewParticle,
                                     particles.Length - firstNewParticle,
                                     stride, SetDataOptions.NoOverwrite);

                if (firstFreeParticle > 0)
                {
                    vertexBuffer.SetData(0, particles,
                                         0, firstFreeParticle,
                                         stride, SetDataOptions.NoOverwrite);
                }
            }

            // Move the particles we just uploaded from the new to the active queue.
            firstNewParticle = firstFreeParticle;
        }


        /// <summary>
        /// Helper for setting the renderstates used to draw particles.
        /// </summary>
        void SetParticleRenderStates(RenderState renderState)
        {
            // Enable point sprites.
            renderState.PointSpriteEnable = true;
            renderState.PointSizeMax = settings.PointSizeMax;

            // Set the alpha blend mode.
            renderState.AlphaBlendEnable = true;
            renderState.AlphaBlendOperation = settings.BlendFunction;
            renderState.BlendFactor = settings.BlendFactor;
            renderState.SourceBlend = settings.SourceBlend;
            renderState.DestinationBlend = settings.DestinationBlend;

            // Set the alpha test mode.
            renderState.AlphaTestEnable = true;
            renderState.AlphaFunction = CompareFunction.Greater;
            renderState.ReferenceAlpha = 0;

            // Enable the depth buffer (so particles will not be visible through
            // solid objects like the ground plane), but disable depth writes
            // (so particles will not obscure other particles).
            renderState.DepthBufferEnable = true;
            renderState.DepthBufferWriteEnable = false;
        }

        /// <summary>
        /// Restore the renderstates used to draw particles.
        /// </summary>
        /// <remarks>Failing to call this method will result in the unexpect graphics result.</remarks>
        /// <param name="renderState"></param>
        void RestoreRenderStates(RenderState renderState)
        {
            // Enable point sprites.
            renderState.PointSpriteEnable = false;
            // Set default maximum point size to 256 (as it is used in particle system only)
            renderState.PointSizeMax = 256;

            // Set the alpha blend mode.
            renderState.AlphaBlendEnable = false;
            renderState.AlphaBlendOperation = BlendFunction.Add;
            renderState.BlendFactor = Color.White;
            renderState.SourceBlend = Blend.One;
            renderState.DestinationBlend = Blend.One;

            // Set the alpha test mode.
            renderState.AlphaTestEnable = false;
            renderState.AlphaFunction = CompareFunction.Always;

            // Set the depth buffer to be enabled again, and also the depth write to enable too
            renderState.DepthBufferEnable = true;
            renderState.DepthBufferWriteEnable = true;
        }


        #endregion

        #region Public Methods


        /// <summary>
        /// Sets the camera view and projection matrices
        /// that will be used to draw this particle system.
        /// </summary>
        /// <remarks>This method is automatically called by the engine. It used the information from engine's camera manager.</remarks>
        private void SetCamera()
        {
            effectViewParameter.SetValue(Engine.CameraManager.View);
            effectProjectionParameter.SetValue(Engine.CameraManager.Projection);
        }


        /// <summary>
        /// Adds a new particle to the system.
        /// </summary>
        public void AddParticle(Vector3 position, Vector3 velocity)
        {
            // Figure out where in the circular queue to allocate the new particle.
            int nextFreeParticle = firstFreeParticle + 1;

            if (nextFreeParticle >= particles.Length)
                nextFreeParticle = 0;

            // If there are no free particles, we just have to give up.
            if (nextFreeParticle == firstRetiredParticle)
                return;

            // Adjust the input velocity based on how much
            // this particle system wants to be affected by it.
            velocity *= settings.EmitterVelocitySensitivity;

            // Add in some random amount of horizontal velocity.
            float horizontalVelocity = MathHelper.Lerp(settings.MinHorizontalVelocity,
                                                       settings.MaxHorizontalVelocity,
                                                       (float)random.NextDouble());

            double horizontalAngle = 0;
            
            // If the direction need to be randomed.
            if (settings.IsHorizontalVelocityDirectionRandomed)
                horizontalAngle = random.NextDouble() * MathHelper.TwoPi;

            velocity.X += horizontalVelocity * (float)Math.Cos(horizontalAngle);
            velocity.Z += horizontalVelocity * (float)Math.Sin(horizontalAngle);

            // Add in some random amount of vertical velocity.
            velocity.Y += MathHelper.Lerp(settings.MinVerticalVelocity,
                                          settings.MaxVerticalVelocity,
                                          (float)random.NextDouble());

            // Choose four random control values. These will be used by the vertex
            // shader to give each particle a different size, rotation, and color.
            Color randomValues = new Color((byte)random.Next(255),
                                           (byte)random.Next(255),
                                           (byte)random.Next(255),
                                           (byte)random.Next(255));

            // Fill in the particle vertex structure.
            particles[firstFreeParticle].Position = position;
            particles[firstFreeParticle].Velocity = velocity;
            particles[firstFreeParticle].Random = randomValues;
            particles[firstFreeParticle].Time = currentTime;

            firstFreeParticle = nextFreeParticle;
        }


        #endregion
    }
}
