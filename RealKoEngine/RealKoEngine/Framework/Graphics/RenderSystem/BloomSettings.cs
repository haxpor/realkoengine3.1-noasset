﻿#region File Description
//-----------------------------------------------------------------------------
// BloomSettings.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System.Collections.Generic;

namespace RealKo.Framework.Graphics
{
    public class BloomSetting
    {
        #region Bloom settings field

        /// <summary>
        /// Name of this bloom settings.
        /// </summary>
        public BloomPresetSetting.BloomPreset Name;

        /// <summary>
        /// Amount of blur to be applied.
        /// </summary>
        /// <remarks>Blur algorithm is in the simple form.</remarks>
        public float BlurAmount;

        /// <summary>
        /// Bloom threshold value.
        /// </summary>
        public float BloomThreshold;

        /// <summary>
        /// Scene intensity value.
        /// </summary>
        public float SceneIntensity;

        /// <summary>
        /// Scene saturation value.
        /// </summary>
        public float SceneSaturation;

        /// <summary>
        /// Bloom intensity value.
        /// </summary>
        public float BloomIntensity;

        /// <summary>
        /// Bloom saturation value.
        /// </summary>
        public float BloomSaturation;

        #endregion

        /// <summary>
        /// Create the bloom settings
        /// </summary>
        public BloomSetting(BloomPresetSetting.BloomPreset name, float blurAmount, float threshold, float sceneIntensity, float sceneSaturation, float bloomIntensity, float bloomSaturation)
        {
            Name = name;
            BlurAmount = blurAmount;
            BloomThreshold = threshold;

            SceneIntensity = sceneIntensity;
            SceneSaturation = sceneSaturation;
            BloomIntensity = bloomIntensity;
            BloomSaturation = bloomSaturation;
        }
    }

    /// <summary>
    /// Use this bloom preset settings to quickly set the bloom's properties.
    /// </summary>
    public static class BloomPresetSetting
    {
        /// <summary>
        /// Dictionary for bloomsettings to achieve the fast retrival of O(1).
        /// </summary>
        static Dictionary<string, BloomSetting> bloomDict = new Dictionary<string, BloomSetting>();

        #region Settings signatures

        public enum BloomPreset
        {
            Default,
            Soft,
            Desaturated,
            Saturated,
            Blurry,
            Subtle,
            InUse
        };

        #endregion

        #region Default bloom preset settings

        /// <summary>
        /// Bloom preset settings
        /// </summary>
        static BloomSetting[] BloomPresetSettings = 
        {
            new BloomSetting(BloomPreset.Default, 0.008f, 0.25f, 1f, 1f, 1.25f, 1f),
            new BloomSetting(BloomPreset.Soft, 0.008f, 0f, 1f, 1f, 1f, 1f),
            new BloomSetting(BloomPreset.Desaturated, 0.008f, 0.5f, 1f, 1f, 2f, 0f),
            new BloomSetting(BloomPreset.Saturated, 0.008f, 0.25f, 1f, 0f, 2f, 2f),
            new BloomSetting(BloomPreset.Blurry, 0.008f, 0f, 0.1f, 1f, 1f, 1f),
            new BloomSetting(BloomPreset.Subtle, 0.008f, 0.5f, 1f, 1f, 1f, 1f),
            new BloomSetting(BloomPreset.Subtle, 0.002f, 0.5f, 1f, 1f, 0.6f, 1f)
        };

        /// <summary>
        /// Get the bloom preset setting from the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Selected bloom settings from the specified name.</returns>
        public static BloomSetting GetPresetSetting(BloomPreset name)
        {
            //for the first retrival
            if (bloomDict.Count == 0)
            {
                bloomDict.Add(BloomPreset.Default.ToString(), BloomPresetSettings[0]);
                bloomDict.Add(BloomPreset.Soft.ToString(), BloomPresetSettings[1]);
                bloomDict.Add(BloomPreset.Desaturated.ToString(), BloomPresetSettings[2]);
                bloomDict.Add(BloomPreset.Saturated.ToString(), BloomPresetSettings[3]);
                bloomDict.Add(BloomPreset.Blurry.ToString(), BloomPresetSettings[4]);
                bloomDict.Add(BloomPreset.Subtle.ToString(), BloomPresetSettings[5]);
                bloomDict.Add(BloomPreset.InUse.ToString(), BloomPresetSettings[6]);
            }

            //return the bloom settting
            return bloomDict[name.ToString()];
        }

        #endregion
    }
}
