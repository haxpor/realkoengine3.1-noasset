#region File Description
//-----------------------------------------------------------------------------
// LightningVertex.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Accompanied by: Catalin Zima
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RealKo.Framework.Graphics.Misc
{
    struct LightningVertex
    {
        public Vector3 Position;
        public Vector2 TextureCoordinates;
        public Vector2 ColorGradient;
        
        // Describe the layout of this vertex structure.
        public static readonly VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3,
                                    VertexElementMethod.Default,
                                    VertexElementUsage.Position, 0),
            new VertexElement(0, 12, VertexElementFormat.Vector2,
                                     VertexElementMethod.Default,
                                     VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(0, 20, VertexElementFormat.Vector2,
                                     VertexElementMethod.Default,
                                     VertexElementUsage.TextureCoordinate, 1),
        };
        // Describe the size of this vertex structure.
        public const int SizeInBytes = 28;
    }
}
