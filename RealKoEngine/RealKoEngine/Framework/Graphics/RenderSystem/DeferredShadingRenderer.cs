﻿#region File Description
//-----------------------------------------------------------------------------
// DeferredShadingRenderer.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
// Accompanied by: Catalin Zima
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics.Particle;
using RealKo.Framework.Entity;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// Deferred render sytem required the reference with the engine, if use this class externally, user must modify this class.
    /// 
    /// </summary>
    public class DeferredShadingRenderer: RK_IUpdateable
    {
        /// <summary>
        /// GraphicsDevice from game class.
        /// </summary>
        GraphicsDevice graphicsDevice;

        /// <summary>
        /// ContentManager from game class.
        /// </summary>
        ContentManager content;

        /// <summary>
        /// SpriteBatch from game class.
        /// </summary>
        SpriteBatch spriteBatch;

        #region 3D Effect filess
        // Used to clear the buffer to appropriate value inside the buffer
        Effect clearGBufferEffect;
        Effect renderGBufferEffect;
        Effect combineLightEffect;
        Effect shadowMapDrawEffect;

        //normal light effect
        Effect directionalLightEffect;
        Effect pointLightEffect;
        Effect spotLightEffect;

        //shadow light effect
        Effect directionalShadowLightEffect;
        Effect pointShadowLightEffect;
        Effect spotShadowLightEffect;

        /// <summary>
        /// The effect file that each of the scene, or material need to draw their own geometry.
        /// </summary>
        public Effect RenderEffect
        {
            get { return renderGBufferEffect; }
        }

        /// <summary>
        /// Get the shadow-map-draw effect to be used in routing the drawing process of each entity.
        /// </summary>
        public Effect ShadowMapDrawEffect
        {
            get { return shadowMapDrawEffect; }
        }

        #endregion

        #region Post-processing
        private bool isPostProcessingEnabled = true;

        /// <summary>
        /// Get or set whether to enable post-processing.
        /// </summary>
        public bool PostProcessingEnabled
        {
            get { return isPostProcessingEnabled; }
            set { isPostProcessingEnabled = value; }
        }
        #endregion Post-processing

        #region Anti-Aliasing

        public enum AATechnique { DLAA, SMAA, MLAA };

        private bool isAppliedAntiAliasing = true;
        private RenderTarget2D antialiasingEdgeMaskTarget;
        private RenderTarget2D antialiasingTarget;
        private Effect antialiasingEffect;
        private AATechnique antialiasingTechnique = AATechnique.DLAA;

        // -- for mlaa --
        private Texture2D mlaaAreaTex;
        private RenderTarget2D mlaaEdgeTexTarget;
        private RenderTarget2D mlaaBlendTexTarget;

        // -- for smaa --
        private Texture2D smaaAreaTex;
        private Texture2D smaaSearchTex;
        private RenderTarget2D smaaEdgeTexTarget;
        private DepthStencilBuffer smaaEdgeTexDepthStencilBuffer;
        private RenderTarget2D smaaBlendTexTarget;

        /// <summary>
        /// Get or set applying anti-aliasing.
        /// </summary>
        public bool ApplyAntiAliasing
        {
            get { return isAppliedAntiAliasing; }
            set { isAppliedAntiAliasing = value; }
        }

        #endregion Anti-Aliasing

        #region Bloom Post-processing

        Effect bloomExtractEffect;
        Effect bloomBlurEffect;
        Effect combineFinalEffect;

        RenderTarget2D bloomExtractTarget;
        RenderTarget2D bloomBlurTarget;

        /// <summary>
        /// Current bloom settings applied for this deferred shading renderer.
        /// </summary>
        /// <remarks>Default is subtle.</remarks>
        BloomSetting bloomSetting = BloomPresetSetting.GetPresetSetting(BloomPresetSetting.BloomPreset.InUse);
        /// <summary>
        /// Get or set the current bloom setting applied for this deferred shading renderer.
        /// </summary>
        public BloomPresetSetting.BloomPreset BloomSetting
        {
            get { return bloomSetting.Name; }
            set
            {
                bloomSetting = BloomPresetSetting.GetPresetSetting(value);
            }
        }

        /// <summary>
        /// Set or get whether to apply bloom effect for the deferred rendering.
        /// </summary>
        /// <remarks>If unset this value, the bloom post-processing's setting will not be taken into effect.</remarks>
        bool isBloomApplied = true;
        public bool ApplyBloom
        {
            get { return isBloomApplied; }
            set { isBloomApplied = value; }
        }

        #endregion

        #region Associate render targets

        /// <summary>
        /// All associate render-targets for GBuffer.
        /// </summary>
        RenderTarget2D colorRt;
        RenderTarget2D depthRt;
        RenderTarget2D lightOcclusionRt;
        RenderTarget2D normalRt;

        RenderTarget2D lightMapRt;
        RenderTarget2D combinedLightRt;

        RenderTarget2D shadowMapRt;

        //For DTTRt 1
        DepthStencilBuffer oldDepthBuffer;
        DepthStencilBuffer depthBuffer;

        #region Depth properties

        /// <summary>
        /// Get the old depthbuffer.
        /// </summary>
        /// <remarks>After finishing set the depthbuffer, user need to return to the old depthbuffer by setting to this one.</remarks>
        public DepthStencilBuffer OldDepthBuffer
        {
            get { return oldDepthBuffer; }
        }

        /// <summary>
        /// Get the currently used depthbuffer.
        /// </summary>
        /// <remarks>Use this field to draw user's customed scene on top of the normal scene or DTTRtn.</remarks>
        public DepthStencilBuffer DepthBuffer
        {
            get { return depthBuffer; }
        }

        #endregion

        #endregion

        #region Lights (Directional light, and Point light)

        //normal uncasting shadow lights
        public List<DirectionalLight> DirectionalLights;
        public List<PointLight> PointLights;
        public List<SpotLight> Spotlights;

        //casting shadow light
        public List<DirectionalShadowLight> DirectionalShadowLights;
        public List<PointShadowLight> PointShadowLights;
        public List<SpotShadowLight> SpotShadowLights;

        #endregion

        //Other scenes integrated into deferred shading
        #region Misc Scenes

        #region Particles scene

        /// <summary>
        /// Particle systems manager.
        /// </summary>
        public ParticleSystemsManager ParticleSystemsManager;

        #endregion

        #endregion

        #region Draw onto texture and its stuff (option)
        /// <summary>
        /// Whether or not to draw to texture.
        /// </summary>
        /// <remarks>By enabling this option, the DeferredShadingRenderer will draw onto the texture and can be use for multilayer-draw.</remarks>
        bool isDrawToTexture = false;
        /// <summary>
        /// Reference-type of render target, sent into from Engine Class.
        /// </summary>
        /// <remarks>Refer to the document for more information about the mechanism of draw-to-texture.</remarks>
        RenderTarget2D dttRt;

        #region Properties
        /// <summary>
        /// Get or set the flag indicates whether or not to draw to texture.
        /// </summary>
        public bool IsDrawToTexture
        {
            get { return isDrawToTexture; }
            set { isDrawToTexture = value; }
        }
        /// <summary>
        /// Get or set the draw-to-texture render target.
        /// </summary>
        /// <remarks>User must not use this field, unless knowing what to do.
        /// This field should be set only by the Engine Class automatically.</remarks>
        public RenderTarget2D DttRt
        {
            get { return dttRt; }
            set { dttRt = value; }
        }

        #endregion

        #endregion

        #region Shadowing stuffs

        /// <summary>
        /// Depth bias when using the shadowing algorithm with different resolution
        /// </summary>
        /// <remarks>The default value is suited for the screen resolution of 1280 * 1024.</remarks>
        float shadowDepthBias = 0.0001f;

        /// <summary>
        /// Get or set the shadow depth bias to be used for rendering the shadow process.
        /// </summary>
        public float ShadowDepthBias
        {
            get { return shadowDepthBias; }
            set { shadowDepthBias = value; }
        }

        #endregion

        #region Taking screenshot
        private ResolveTexture2D screenshot;
        private static int screenshotNumber = 0;
        #endregion Taking screenshot

        /// <summary>
        /// Quad renderer used for clearing the buffer inside the GBuffer.
        /// </summary>
        QuadRenderer quadRenderer;

        /// <summary>
        /// Screen adjustment for the transformed screen coordinate.
        /// </summary>
        Vector2 screenAdjustment = Vector2.Zero;
        public Vector2 ScreenAdjustment
        {
            get { return screenAdjustment; }
            set { screenAdjustment = value; }
        }

        /// <summary>
        /// Sphere model used to draw the point light.
        /// </summary>
        Model sphereModel;

        /// <summary>
        /// Cone model used to draw the spotlight.
        /// </summary>
        Model coneModel;

        //#debug for deferred shadow #
        RenderTarget2D dsRt;
        RenderTarget2D lightMapBlurredRt;

        public DeferredShadingRenderer(GraphicsDevice graphicsDevice, ContentManager content, SpriteBatch spriteBatch)
        {
            this.graphicsDevice = graphicsDevice;
            this.content = content;
            this.spriteBatch = spriteBatch;

            //create the list of lights
            DirectionalLights = new List<DirectionalLight>();
            PointLights = new List<PointLight>();
            Spotlights = new List<SpotLight>();

            DirectionalShadowLights = new List<DirectionalShadowLight>();
            PointShadowLights = new List<PointShadowLight>();
            SpotShadowLights = new List<SpotShadowLight>();

            //create the quad renderer
            quadRenderer = new QuadRenderer(this.graphicsDevice);

            //load the the associate effect files
            clearGBufferEffect = this.content.Load<Effect>("Base/Effects/ClearGBuffer");
            renderGBufferEffect = this.content.Load<Effect>("Base/Effects/RenderGBuffer");
            directionalLightEffect = this.content.Load<Effect>("Base/Effects/DirectionalLight");
            pointLightEffect = this.content.Load<Effect>("Base/Effects/PointLight");
            spotLightEffect = this.content.Load<Effect>("Base/Effects/Spotlight");
            combineLightEffect = this.content.Load<Effect>("Base/Effects/CombineLight");
            shadowMapDrawEffect = this.content.Load<Effect>("Base/Effects/ShadowMap");

            //all the effect of shadow light
            directionalShadowLightEffect = this.content.Load<Effect>("Base/Effects/DirectionalShadowLight");
            pointShadowLightEffect = this.content.Load<Effect>("Base/Effects/PointShadowLight");
            spotShadowLightEffect = this.content.Load<Effect>("Base/Effects/SpotShadowLight");

            // antialiasing
            antialiasingEffect = this.content.Load<Effect>("Base/Effects/AntiAliasing");

            //load effect
            bloomExtractEffect = this.content.Load<Effect>("Base/Effects/BloomExtract");
            bloomBlurEffect = this.content.Load<Effect>("Base/Effects/BloomBlur");
            combineFinalEffect = this.content.Load<Effect>("Base/Effects/CombineFinal");

            //load the unit sphere
            sphereModel = this.content.Load<Model>("Base/Meshes/sphere");
            coneModel = this.content.Load<Model>("Base/Meshes/cone");

            //initialize all of the render-targets
            int backBufferWidth = graphicsDevice.PresentationParameters.BackBufferWidth;
            int backBufferHeight = graphicsDevice.PresentationParameters.BackBufferHeight;

            //set the screen adjustment
            screenAdjustment.X = 0.5f / backBufferWidth;
            screenAdjustment.Y = 0.5f / backBufferHeight;

            colorRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            normalRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            depthRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Single, RenderTargetUsage.PreserveContents);
            lightOcclusionRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Single, RenderTargetUsage.PreserveContents);

            lightMapRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
            lightMapBlurredRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            combinedLightRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);

            // anti-aliasing render target
            antialiasingEdgeMaskTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            antialiasingTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);

            // -- preset for smaa
            if (antialiasingTechnique == AATechnique.SMAA)
            {
                smaaAreaTex = this.content.Load<Texture2D>("Base/Effects/AA/AreaTexDX9");
                smaaSearchTex = this.content.Load<Texture2D>("Base/Effects/AA/SearchTex");
                smaaEdgeTexTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
                smaaBlendTexTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
                smaaEdgeTexDepthStencilBuffer = new DepthStencilBuffer(smaaEdgeTexTarget.GraphicsDevice, smaaEdgeTexTarget.Width, smaaEdgeTexTarget.Height, DepthFormat.Depth24Stencil8);

                antialiasingEffect.Parameters["areaTex2D"].SetValue(smaaAreaTex);
                antialiasingEffect.Parameters["searchTex2D"].SetValue(smaaSearchTex);
                //antialiasingEffect.Parameters["SMAA_PIXEL_SIZE"].SetValue(new Vector2(1.0f / graphicsDevice.Viewport.Width,
                //    1.0f / graphicsDevice.Viewport.Height));
            }
            // -- preset for mlaa
            else if (antialiasingTechnique == AATechnique.MLAA)
            {
                mlaaAreaTex = this.content.Load<Texture2D>("Base/Effects/AA/AreaMap129");
                mlaaEdgeTexTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
                mlaaBlendTexTarget = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);

                antialiasingEffect.Parameters["areaTex"].SetValue(mlaaAreaTex);
                antialiasingEffect.Parameters["halfPixel"].SetValue(new Vector2(0.5f / backBufferWidth, 0.5f / backBufferHeight));
            }
            // -- preset for all
            // set shared variables to antialiasing effect first
            antialiasingEffect.Parameters["viewport_width"].SetValue(graphicsDevice.Viewport.Width);
            antialiasingEffect.Parameters["viewport_height"].SetValue(graphicsDevice.Viewport.Height);
            antialiasingEffect.Parameters["PIXEL_SIZE"].SetValue(new Vector2(1.0f / backBufferWidth, 1.0f / backBufferHeight));

            //create the bloom extract, and bloom blur target with the dimension half of the back buffer (avoid the fillrate)
            bloomExtractTarget = new RenderTarget2D(graphicsDevice, backBufferWidth / 2, backBufferHeight / 2, 1, SurfaceFormat.Color);
            bloomBlurTarget = new RenderTarget2D(graphicsDevice, backBufferWidth / 2, backBufferHeight / 2, 1, SurfaceFormat.Color);

            //set the depth buffer to be the same as the one used in depth render target of deferred shading
            depthBuffer = new DepthStencilBuffer(depthRt.GraphicsDevice, depthRt.Width, depthRt.Height, depthRt.GraphicsDevice.DepthStencilBuffer.Format);

            //create the shadow map render target
            shadowMapRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Single);
            dsRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Single);

            // screenshot
            screenshot = new ResolveTexture2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, graphicsDevice.PresentationParameters.BackBufferFormat);

            //creation section for external scene
            //create new particle systems manager
            ParticleSystemsManager = new ParticleSystemsManager();
        }

        /// <summary>
        /// Set the render-target to be ready for drawing on for GBuffer.
        /// </summary>
        private void SetGBuffer()
        {
            //the order ot the render target is also important in term of rendering
            //due to we integrated the particle system with this deferred shading system, but the particle system is based on pointsprite,
            //so no need for normal to be mapped with each particle as we don't need that real lighting result. We just take the normal 
            //render target to last, thus in the effect file which requires to write the output for COLORn semantic in continuos can be achieved.
            graphicsDevice.SetRenderTarget(0, colorRt);
            graphicsDevice.SetRenderTarget(1, depthRt);
            graphicsDevice.SetRenderTarget(2, lightOcclusionRt);
            graphicsDevice.SetRenderTarget(3, normalRt);
        }

        /// <summary>
        /// Resolve all the render-targets in GBuffer.
        /// </summary>
        private void ResolveGBufer()
        {
            graphicsDevice.SetRenderTarget(0, null);
            graphicsDevice.SetRenderTarget(1, null);
            graphicsDevice.SetRenderTarget(2, null);
            graphicsDevice.SetRenderTarget(3, null);
        }

        /// <summary>
        /// Clear the buffer of all the render-targets in GBuffer.
        /// </summary>
        private void ClearGBuffer()
        {
            clearGBufferEffect.Begin();
            clearGBufferEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            clearGBufferEffect.CurrentTechnique.Passes[0].End();
            clearGBufferEffect.End();
        }

        /// <summary>
        /// Update the deferred rendering system.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            //**Only for particle scene is concerned here, as it is the most suitable to be handle directly by DeferredShadingRenderer class.
            //update all the external scenes
            //TODO: Implementer has to decide and know for all external scenes to be updated on top here
            if (ParticleSystemsManager.NumberOfParticleSystem > 0)
            {
                //traverse all the particle system
                foreach (ParticleSystem p in ParticleSystemsManager.ParticleSystems)
                {
                    //check whether changing the wind will also affect this particle system
                    if (p.IsAffectedByWindChanging)
                    {
                        //update the global settings from config file (through engine)
                        //refer to the document for more information
                        //Note: Some particle systems, in which defined the constant Gravity inside the class themselves, will not be affected
                        //      totally by the wind.
                        p.Gravity = Engine.SimulationSettings.Wind;
                    }

                    //draw
                    p.Update(gameTime);
                }
            }
            //ENDTODO
        }

        /// <summary>
        /// Prepare the deferred rendering process.
        /// </summary>
        /// <remarks>Draw part will be handled by the outside.</remarks>
        /// <param name="gameTime"></param>
        public void BeginDraw(GameTime gameTime)
        {
            //set the proper depth-test function
            graphicsDevice.RenderState.DepthBufferFunction = CompareFunction.LessEqual;

            //set render targets
            SetGBuffer();

            //cache the current depth buffer
            oldDepthBuffer = graphicsDevice.DepthStencilBuffer;

            //set our custom depth buffer
            graphicsDevice.DepthStencilBuffer = depthBuffer;

            //clear render targets
            ClearGBuffer();

            //clear out our depth buffer
            graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Black, 1.0f, 1);
        }

        /// <summary>
        /// End the deferred rendering process.
        /// In fact, end draw will finish the deferred rendering pass by drawing the lights, and draw scene with or without bloom post-processing.
        /// </summary>
        /// <remarks>Draw part will be handled by the outside.</remarks>
        /// <param name="gameTime"></param>
        public void EndDraw(GameTime gameTime)
        {
            //resolve all render targets
            ResolveGBufer();

            //set the depth buffer back to old one
            graphicsDevice.DepthStencilBuffer = oldDepthBuffer;

            // draw all lights
            DrawLights(gameTime);

            //draw on top of the result from lighting target
            graphicsDevice.SetRenderTarget(0, combinedLightRt);

            //set the depth buffer for the less of drawing
            graphicsDevice.DepthStencilBuffer = depthBuffer;

            #region Drawing airport's alpha meshes (alpha that needs depth writing enabled)

            //Airport's alpha meshes
            //Note: Airport alpha meshes should not be drawed while the depthWriteEnable is true, due to we include both fog-mesh, and tree-mesh
            // to be in the alpha meshes of airport, thus if it's enabled then the trees will look junky.
            // But the above problem can be solved by providing the option in the config file, but we neglect to do so as it's cumbersome for now.
            if (Engine.EntityManager.GetAirportAlphaEntityList().Count > 0)
            {
                // Pass 1
                graphicsDevice.RenderState.DepthBufferEnable = true;
                graphicsDevice.RenderState.DepthBufferWriteEnable = true;
                graphicsDevice.RenderState.AlphaTestEnable = true;
                graphicsDevice.RenderState.AlphaFunction = CompareFunction.GreaterEqual;
                graphicsDevice.RenderState.ReferenceAlpha = 200;
                graphicsDevice.RenderState.CullMode = CullMode.None;


                //traverse through the airport's alpha meshes entity list, and draw each one
                foreach (AirportAlphaMeshEntity am in Engine.EntityManager.GetAirportAlphaEntityList())
                {
                    //draw the mesh
                    am.Draw(gameTime);
                }

                // Pass 2
                // Note: This pass may not need for simple shape mesh, and texture.
                /*graphicsDevice.RenderState.DepthBufferEnable = true;
                graphicsDevice.RenderState.DepthBufferWriteEnable = false;
                graphicsDevice.RenderState.AlphaTestEnable = true;
                graphicsDevice.RenderState.AlphaFunction = CompareFunction.Less;
                graphicsDevice.RenderState.ReferenceAlpha = 200;
                graphicsDevice.RenderState.CullMode = CullMode.None;

                graphicsDevice.RenderState.AlphaBlendEnable = true;

                //traverse through the airport's alpha meshes entity list, and draw each one
                foreach (AirportAlphaMeshEntity am in Engine.EntityManager.GetAirportAlphaEntityList())
                {
                    //set the drawing option
                    graphicsDevice.RenderState.SourceBlend = am.SrcBlend;
                    graphicsDevice.RenderState.DestinationBlend = am.DestBlend;

                    //draw the mesh
                    am.Draw(gameTime);
                }*/

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            graphicsDevice.RenderState.AlphaTestEnable = false;

            #endregion Drawing airport's alpha meshes (alpha that needs depth writing enabled)

            #region Drawing all other alpha stuffs here
            //TODO: Draw the external scene on top of the deferred shading scene here.
            //Note: The order of scene to be drawed here is pretty mess, we must be in manually order the draw call.
            // This section should be revised soon.

            //set the render states before hands
            graphicsDevice.RenderState.AlphaBlendEnable = true;
            graphicsDevice.RenderState.DepthBufferEnable = true;
            graphicsDevice.RenderState.DepthBufferWriteEnable = false;
            graphicsDevice.RenderState.BlendFunction = BlendFunction.Add;
            graphicsDevice.RenderState.CullMode = CullMode.None;

            //Draw the airport's light originated from the windows of each mesh (if need)
            if (Engine.EntityManager.GetAirportOpaqueEntityList().Count > 0)
            {
                //traverse through the airport's opaque-meshes entity list, and draw each one
                foreach (AirportOpaqueMeshEntity a in Engine.EntityManager.GetAirportOpaqueEntityList())
                {
                    //set the drawing option
                    graphicsDevice.RenderState.SourceBlend = Blend.One;
                    graphicsDevice.RenderState.DestinationBlend = Blend.One;

                    //draw the alpha stuff
                    a.DrawAlpha(gameTime);
                }

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            //Draw airport's static light-meshes
            if (Engine.EntityManager.GetAirportStaticLightEntityList().Count > 0)
            {
                //traverse through the airport's static light-meshes entity list, and draw each one
                foreach (AirportStaticLightMeshEntity a in Engine.EntityManager.GetAirportStaticLightEntityList())
                {
                    //set the drawing option
                    graphicsDevice.RenderState.SourceBlend = a.SrcBlend;
                    graphicsDevice.RenderState.DestinationBlend = a.DestBlend;

                    //if any alpha mesh need to be drawed at this time, the internal system of the airplane entity itself will determine and do
                    //the job for us
                    a.Draw(gameTime);
                }

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            //Draw airport's dynamic light-meshes
            if (Engine.EntityManager.GetAirportApproachLightEntityList().Count > 0)
            {
                //traverse through the airport's dynamic light-meshes entity list, and draw each one
                foreach (AirportApproachLightMeshEntity a in Engine.EntityManager.GetAirportApproachLightEntityList())
                {
                    //set the drawing option
                    graphicsDevice.RenderState.SourceBlend = a.SrcBlend;
                    graphicsDevice.RenderState.DestinationBlend = a.DestBlend;

                    //if any alpha mesh need to be drawed at this time, the internal system of the airplane entity itself will determine and do
                    //the job for us
                    a.Draw(gameTime);
                }

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            // old place for rendering airport's alpha meshes

            //Draw the airplane's alpha mesh
            if (Engine.EntityManager.GetAirplaneEntityList().Count > 0)
            {
                //set the render states accordingly
                //Note: For airplane, the renderstates for drawing are fixed.
                graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                graphicsDevice.RenderState.DestinationBlend = Blend.One;

                //traverse through the airplane entity list, and draw each one
                foreach (TransportationObjectEntity a in Engine.EntityManager.GetAirplaneEntityList())
                {
                    if (!a.IsCulled)
                    {
                        //if any alpha mesh need to be drawed at this time, the internal system of the airplane entity itself will determine and do
                        //the job for us
                        a.DrawAlpha(gameTime);
                    }
                }

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            //Draw airport's signal light-meshes
            if (Engine.EntityManager.GetAirportSignalLightEntityList().Count > 0)
            {
                //traverse through the airport's signal light-meshes entity list, and draw each one
                foreach (AirportSignalLightMeshEntity a in Engine.EntityManager.GetAirportSignalLightEntityList())
                {
                    //set the drawing option
                    graphicsDevice.RenderState.SourceBlend = a.SrcBlend;
                    graphicsDevice.RenderState.DestinationBlend = a.DestBlend;

                    //if any alpha mesh need to be drawed at this time, the internal system of the airplane entity itself will determine and do
                    //the job for us
                    a.Draw(gameTime);
                }

                //*the render states will be properly set automatically within the particle system class
                //so no need to do anything here
            }

            //restore the culling mode
            graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            //Note: All the particle systems should be drawed after all the solid objects are drawed. This is quite important as the particle itself
            //can be able to pass the depth-test.
            //draw the particle system
            if (ParticleSystemsManager.NumberOfParticleSystem > 0)
            {
                //traverse all the particle system
                foreach (ParticleSystem p in ParticleSystemsManager.ParticleSystems)
                {
                    //draw
                    p.Draw(gameTime);
                }
            }

            //ENDTODO
            #endregion

            graphicsDevice.SetRenderTarget(0, null);

            //set the depth buffer back to old one
            graphicsDevice.DepthStencilBuffer = oldDepthBuffer;

            #region Special drawing (still alpha scene)

            if (Engine.IsSCMeshConfigLoaded)
            {
                //draw the lighting-bolt of the rainny effect (call to GenerateTexture() method of LightningBolt class),
                // due to it involved in the underlying draw-system, so we can apply the depth buffer currently in used.
                //Note: We still use the current depth buffer.
                if (Engine.SCMeshManager.IsLightningBoltStarted)
                {
                    #region Set our renderstates ready to draw the lightning
                    graphicsDevice.RenderState.DepthBufferEnable = true;
                    graphicsDevice.RenderState.DepthBufferWriteEnable = false;
                    #endregion

                    Engine.SCMeshManager.LightningBolt.GenerateTexture(gameTime, Matrix.Identity, Engine.CameraManager.View, Engine.CameraManager.Projection);
                    //now combine it with the combinedLightRt
                    Engine.SCMeshManager.LightningBolt.Draw(gameTime, combinedLightRt);

                    //restore our renderstates
                    #region Restore our renderstates
                    // Enable point sprites.
                    graphicsDevice.RenderState.PointSpriteEnable = false;
                    // Set default maximum point size to 256 (as it is used in particle system only)
                    graphicsDevice.RenderState.PointSizeMax = 256;

                    // Set the alpha blend mode.
                    graphicsDevice.RenderState.AlphaBlendEnable = false;
                    graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
                    graphicsDevice.RenderState.BlendFactor = Color.White;
                    graphicsDevice.RenderState.SourceBlend = Blend.One;
                    graphicsDevice.RenderState.DestinationBlend = Blend.One;

                    // Set the alpha test mode.
                    graphicsDevice.RenderState.AlphaTestEnable = false;
                    graphicsDevice.RenderState.AlphaFunction = CompareFunction.Always;

                    // Set the depth buffer to be enabled again, and also the depth write to enable too
                    graphicsDevice.RenderState.DepthBufferEnable = true;
                    graphicsDevice.RenderState.DepthBufferWriteEnable = true;
                    #endregion
                }
            }

            #endregion

            //draw the bloom applied, or just normal scene
            if (isPostProcessingEnabled)
                DrawPostProcessing(gameTime, combinedLightRt);
            else
                DrawNormalScene(gameTime, combinedLightRt);

            //restore our renderstates
            //Note: this is the last chance to restore the renderstates and use in the next drawing process.
            #region Restore our renderstates
            // Enable point sprites.
            graphicsDevice.RenderState.PointSpriteEnable = false;
            // Set default maximum point size to 256 (as it is used in particle system only)
            graphicsDevice.RenderState.PointSizeMax = 256;

            // Set the alpha blend mode.
            graphicsDevice.RenderState.AlphaBlendEnable = false;
            graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
            graphicsDevice.RenderState.BlendFactor = Color.White;
            graphicsDevice.RenderState.SourceBlend = Blend.One;
            graphicsDevice.RenderState.DestinationBlend = Blend.One;

            // Set the alpha test mode.
            graphicsDevice.RenderState.AlphaTestEnable = false;
            graphicsDevice.RenderState.AlphaFunction = CompareFunction.Always;

            // Set the depth buffer to be enabled again, and also the depth write to enable too
            graphicsDevice.RenderState.DepthBufferEnable = true;
            graphicsDevice.RenderState.DepthBufferWriteEnable = true;
            #endregion
        }

        /// <summary>
        /// Draw all the lights that will effect the scene.
        /// </summary>
        /// <param name="gameTime"></param>
        private void DrawLights(GameTime gameTime)
        {
            //#1 LIGHT MAP
            //render the light into light render target
            graphicsDevice.SetRenderTarget(0, lightMapRt);
            //clear all components to 0 (0,0,0,0)
            graphicsDevice.Clear(Color.TransparentBlack);

            //also enable the lightOcclusion render target in case of particular light to write onto the sky/cloud dome
            //this render target is preserved, so we can safely draw on top of it
            //graphicsDevice.SetRenderTarget(1, lightOcclusionRt);

            graphicsDevice.RenderState.AlphaBlendEnable = true;
            //add those color
            graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
            graphicsDevice.RenderState.DestinationBlend = Blend.One;
            graphicsDevice.RenderState.SourceBlend = Blend.One;
            //use the same operation for alpha channel
            graphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;

            //draw the directional lights from the list
            foreach (DirectionalLight d in DirectionalLights)
            {
                if(d.IsEnabled)
                    DrawDirectionalLight(d);
            }

            //draw the pointlights from the list
            foreach (PointLight p in PointLights)
            {
                if(p.IsEnabled)
                    DrawPointLight(p);
            }

            //draw the spotlights from the list
            foreach (SpotLight s in Spotlights)
            {
                if(s.IsEnabled)
                    DrawSpotlight(s);
            }

            //restore the alphablending
            graphicsDevice.RenderState.AlphaBlendEnable = false;

            //resolve all render targets
            graphicsDevice.SetRenderTarget(0, null);
            //graphicsDevice.SetRenderTarget(1, null);

            #region Draw shadow lights
            //##DEBUG##
            //Debug for the calculating of shadow on the scene.

            //Directional shadow light
            foreach (DirectionalShadowLight dsLight in DirectionalShadowLights)
            {
                if (dsLight.IsEnabled)
                {
                    //Generate the shadow map phrase
                    //draw onto the shadow map render target
                    graphicsDevice.SetRenderTarget(0, shadowMapRt);
                    //clear to zero distance (in order to limit the effect of outrange of shadow mapping)
                    graphicsDevice.Clear(Color.TransparentBlack);

                    //set the view, and projection matrix for the light's field-of-view (to shadow map effect)
                    //this shadowMap effect will be routed into the DrawShadowMap method of each entity.
                    shadowMapDrawEffect.Parameters["lightView"].SetValue(dsLight.View);
                    shadowMapDrawEffect.Parameters["lightProjection"].SetValue(dsLight.Projection);

                    graphicsDevice.RenderState.AlphaBlendEnable = false;
                    graphicsDevice.RenderState.DepthBufferEnable = true;
                    graphicsDevice.RenderState.DepthBufferWriteEnable = true;

                    foreach (AirportOpaqueMeshEntity aom in Engine.EntityManager.GetAirportOpaqueEntityList())
                    {
                        aom.DrawShadowMap(gameTime);
                    }

                    foreach (TransportationObjectEntity a in Engine.EntityManager.GetAirplaneEntityList())
                    {
                        a.DrawShadowMap(gameTime);
                    }

                    graphicsDevice.SetRenderTarget(0, null);

                    //set the appropriate render targets
                    graphicsDevice.RenderState.AlphaBlendEnable = true;
                    //add those color
                    graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
                    graphicsDevice.RenderState.DestinationBlend = Blend.One;
                    graphicsDevice.RenderState.SourceBlend = Blend.One;
                    //use the same operation for alpha channel
                    graphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;

                    //draw on top of the light render target
                    graphicsDevice.SetRenderTarget(0, lightMapRt);
                    DrawDirectionalShadowLight(dsLight);
                    graphicsDevice.SetRenderTarget(0, null);
                }
            }

            //Point shadow light
            foreach (PointShadowLight psLight in PointShadowLights)
            {
                //We list all of the methods here due to we don't want to separate the method into another method in this RenderSystem Class.
                //Note: Assume that we use the coordinate in which x-axis points right, y-axis points up, and z-axis points toward to camera.
                // If use another coordinate-setting-up we need to modify the following code to match that coordinate too.
                #region Methods for drawing the shadow map, and shadow light.

                //temp variable for this block of code
                Vector3 lookDirection;


                //we will go deep into the 6 views of point shadow light
                if (psLight.IsShadowForBackView)
                {
                    //Generate the shadow map phrase
                    //draw onto the shadow map render target
                    graphicsDevice.SetRenderTarget(0, shadowMapRt);
                    //clear to zero distance (in order to limit the effect of outrange of shadow mapping)
                    graphicsDevice.Clear(Color.TransparentBlack);

                    //set the view, and projection matrix for the light's field-of-view (to shadow map effect)
                    //this shadowMap effect will be routed into the DrawShadowMap method of each entity.
                    //in this case we need to recalculate the view matrix
                    lookDirection = new Vector3(1.0f, 0, 0);
                    Matrix view = Matrix.CreateLookAt(psLight.Position, psLight.Position + lookDirection, new Vector3(0,1,0));
                    shadowMapDrawEffect.Parameters["lightView"].SetValue(view);
                    shadowMapDrawEffect.Parameters["lightProjection"].SetValue(psLight.Projection);

                    graphicsDevice.RenderState.AlphaBlendEnable = false;
                    graphicsDevice.RenderState.DepthBufferEnable = true;
                    graphicsDevice.RenderState.DepthBufferWriteEnable = true;

                    foreach (AirportOpaqueMeshEntity aom in Engine.EntityManager.GetAirportOpaqueEntityList())
                    {
                        aom.DrawShadowMap(gameTime);
                    }

                    foreach (TransportationObjectEntity a in Engine.EntityManager.GetAirplaneEntityList())
                    {
                        a.DrawShadowMap(gameTime);
                    }

                    graphicsDevice.SetRenderTarget(0, null);

                    //set the appropriate render targets
                    graphicsDevice.RenderState.AlphaBlendEnable = true;
                    //add those color
                    graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
                    graphicsDevice.RenderState.DestinationBlend = Blend.One;
                    graphicsDevice.RenderState.SourceBlend = Blend.One;
                    //use the same operation for alpha channel
                    graphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;

                    //draw on top of the light render target
                    graphicsDevice.SetRenderTarget(0, lightMapRt);
                    DrawPointShadowLight(psLight);
                    graphicsDevice.SetRenderTarget(0, null);
                }

                #endregion
            }

            //Spot shadow light
            foreach (SpotShadowLight ssLight in SpotShadowLights)
            {
                if (ssLight.IsEnabled)
                {
                    //Generate the shadow map phrase
                    //draw onto the shadow map render target
                    graphicsDevice.SetRenderTarget(0, shadowMapRt);
                    //clear to zero distance (in order to limit the effect of outrange of shadow mapping)
                    graphicsDevice.Clear(Color.TransparentBlack);

                    //set the view, and projection matrix for the light's field-of-view (to shadow map effect)
                    shadowMapDrawEffect.Parameters["lightView"].SetValue(ssLight.View);
                    shadowMapDrawEffect.Parameters["lightProjection"].SetValue(ssLight.Projection);

                    graphicsDevice.RenderState.AlphaBlendEnable = false;
                    graphicsDevice.RenderState.DepthBufferEnable = true;
                    graphicsDevice.RenderState.DepthBufferWriteEnable = true;

                    foreach (AirportOpaqueMeshEntity aom in Engine.EntityManager.GetAirportOpaqueEntityList())
                    {
                        aom.DrawShadowMap(gameTime);
                    }

                    foreach (TransportationObjectEntity a in Engine.EntityManager.GetAirplaneEntityList())
                    {
                        a.DrawShadowMap(gameTime);
                    }

                    graphicsDevice.SetRenderTarget(0, null);

                    //set the appropriate render targets
                    graphicsDevice.RenderState.AlphaBlendEnable = true;
                    //add those color
                    graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
                    graphicsDevice.RenderState.DestinationBlend = Blend.One;
                    graphicsDevice.RenderState.SourceBlend = Blend.One;
                    //use the same operation for alpha channel
                    graphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;

                    //draw on top of the light render target
                    graphicsDevice.SetRenderTarget(0, lightMapRt);
                    //graphicsDevice.Clear(Color.TransparentBlack);
                    DrawSpotshadowLight(ssLight);
                    graphicsDevice.SetRenderTarget(0, null);
                }
            }

            //##ENDDEBUG##
            #endregion Draw shadow lights

            #region Blur light map
            //blur the lightmap (thus blur the shadow edge a bit)
            //Note: The amount to blur should not be too high as it will blur the light to radiate too much.
            graphicsDevice.SetRenderTarget(0, lightMapBlurredRt);

            bloomBlurEffect.Parameters["blurMag"].SetValue(0.003f);
            bloomBlurEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            bloomBlurEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(lightMapRt.GetTexture(), new Rectangle(0, 0, lightMapBlurredRt.Width, lightMapBlurredRt.Height), Color.White);

            bloomBlurEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            bloomBlurEffect.End();

            graphicsDevice.SetRenderTarget(0, null);
            #endregion Blur light map

            //render into the combined light render target
            graphicsDevice.SetRenderTarget(0, combinedLightRt);
            //clear the render target (this render target will be the result from deferred shading rendering system)
            graphicsDevice.Clear(Color.Black);

            //#2 Combine the light map with the scene map
            //set the light map
            graphicsDevice.Textures[1] = lightMapBlurredRt.GetTexture();
            graphicsDevice.Textures[2] = lightOcclusionRt.GetTexture();

            combineLightEffect.Parameters["ambientLight"].SetValue(Engine.SCMeshManager.AmbientLight);

            //combine the final color with light, to produce the input for post-processing
            combineLightEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            combineLightEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(colorRt.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

            combineLightEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            combineLightEffect.End();

            graphicsDevice.SetRenderTarget(0, null);
        }

        /// <summary>
        /// Draw normal scene by bypassing the bloom post-processing pass.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="input">Input scene buffer</param>
        private void DrawNormalScene(GameTime gameTime, RenderTarget2D input)
        {
            if (isDrawToTexture)
            {
                graphicsDevice.SetRenderTarget(0, dttRt);

                spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);

                // draw a final scene (to texture)
                spriteBatch.Draw(input.GetTexture(), new Rectangle(0, 0, dttRt.Width, dttRt.Height), Color.White);

                spriteBatch.End();

                graphicsDevice.SetRenderTarget(0, null);
            }
            else
            {
                spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);

                // draw final scene
                spriteBatch.Draw(input.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

                spriteBatch.End();
            }
        }

        /// <summary>
        /// Draw the set of post-processing passes.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="input">Input buffer scene</param>
        private void DrawPostProcessing(GameTime gameTime, RenderTarget2D input)
        {
            // Note: combinedLightRt is an input scene buffer.
            // The following code section applies the fixed-code to route the result from anti-aliasing to bloom post-processing. If any additional of post-processing
            //  will be added in the future then one should modify post-processing methods to draw to a separate RenderTarget2D and use it as a next input for another
            //  post-processing.

            // do anti-aliasing (before get for free again in bloom)
            if(isAppliedAntiAliasing)
                antialiasing_postProcessing(gameTime, input);

            // do bloom
            if (isAppliedAntiAliasing && isBloomApplied)
                bloom_postProcessing(gameTime, antialiasingTarget);
            else if (isBloomApplied)
                bloom_postProcessing(gameTime, input);
            else if (isAppliedAntiAliasing)
                DrawNormalScene(gameTime, antialiasingTarget);
            else
                DrawNormalScene(gameTime, input);
        }

        #region Anti-Aliasing (as post-processing)
        /// <summary>
        /// Anti-Aliasing as post-proprocessing.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="input">Input buffer scene</param>
        private void antialiasing_postProcessing(GameTime gameTime, RenderTarget2D input)
        {
            if (antialiasingTechnique == AATechnique.DLAA)
            {
                // set scene sampler
                antialiasingEffect.Parameters["sceneTex"].SetValue(input.GetTexture());
                // pass 0 : Mark edge
                graphicsDevice.SetRenderTarget(0, antialiasingEdgeMaskTarget);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["DLAA"].Passes["P0"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["DLAA"].Passes["P0"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);

                // pass 1 : Edge detect and blur
                // set scene sampler (result from the previous pass)
                antialiasingEffect.Parameters["sceneTex"].SetValue(antialiasingEdgeMaskTarget.GetTexture());

                graphicsDevice.SetRenderTarget(0, antialiasingTarget);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["DLAA"].Passes["P1"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["DLAA"].Passes["P1"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);
            }
                // -- not stable yet --
            else if (antialiasingTechnique == AATechnique.SMAA)
            {
                // set up parameters
                antialiasingEffect.Parameters["colorTex2D"].SetValue(input.GetTexture());
                antialiasingEffect.Parameters["depthTex2D"].SetValue(depthRt.GetTexture());

                // pass 1 : SMAA*EdgeDetection
                graphicsDevice.SetRenderTarget(0, smaaEdgeTexTarget);
                //graphicsDevice.DepthStencilBuffer = smaaEdgeTexDepthStencilBuffer;
                //graphicsDevice.Clear(ClearOptions.Target | ClearOptions.Stencil, Color.TransparentBlack, 1.0f, 0);
                graphicsDevice.Clear(ClearOptions.Target, Color.TransparentBlack, 1.0f, 0);

                // set depth stencil buffer
                /*graphicsDevice.RenderState.StencilEnable = true;
                graphicsDevice.RenderState.StencilPass = StencilOperation.Replace;
                graphicsDevice.RenderState.ReferenceStencil = 1;
                graphicsDevice.RenderState.AlphaBlendEnable = false;
                graphicsDevice.RenderState.AlphaTestEnable = false;
                graphicsDevice.RenderState.DepthBufferEnable = false;*/

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["LumaEdgeDetection"].Passes["LumaEdgeDetection"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["LumaEdgeDetection"].Passes["LumaEdgeDetection"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);

                // pass 2 : SMAABlendingWeightCalculation
                // set up edgeTex
                antialiasingEffect.Parameters["edgesTex2D"].SetValue(smaaEdgeTexTarget.GetTexture());

                graphicsDevice.SetRenderTarget(0, antialiasingTarget);
                //graphicsDevice.DepthStencilBuffer = smaaEdgeTexDepthStencilBuffer;
                graphicsDevice.Clear(ClearOptions.Target, Color.TransparentBlack, 1.0f, 0);

                // set up depth stencil buffer
                /*graphicsDevice.RenderState.StencilEnable = true;
                graphicsDevice.RenderState.StencilPass = StencilOperation.Keep;
                graphicsDevice.RenderState.StencilFunction = CompareFunction.Equal;
                graphicsDevice.RenderState.ReferenceStencil = 1;
                graphicsDevice.RenderState.AlphaBlendEnable = false;
                graphicsDevice.RenderState.AlphaTestEnable = false;
                graphicsDevice.RenderState.DepthBufferEnable = false;*/

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["BlendWeightCalculation"].Passes["BlendWeightCalculation"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["BlendWeightCalculation"].Passes["BlendWeightCalculation"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);
                // set up edgeTex
                /*antialiasingEffect.Parameters["blendTex2D"].SetValue(smaaBlendTexTarget.GetTexture());

                // pass 3 : SMAANeighborhoodBlending 
                graphicsDevice.SetRenderTarget(0, antialiasingTarget);
                graphicsDevice.Clear(Color.Black);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["NeighborhoodBlending"].Passes["NeighborhoodBlending"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["NeighborhoodBlending"].Passes["NeighborhoodBlending"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);*/
            }
            // -- not stable yet --
            else if (antialiasingTechnique == AATechnique.MLAA)
            {
                // set up parameters
                antialiasingEffect.Parameters["colorTex"].SetValue(input.GetTexture());
                antialiasingEffect.Parameters["depthTex"].SetValue(depthRt.GetTexture());

                // pass 1 : Edge Detection (Color)
                antialiasingEffect.Parameters["threshold"].SetValue(0.07f);
                graphicsDevice.SetRenderTarget(0, mlaaEdgeTexTarget);
                graphicsDevice.Clear(Color.TransparentBlack);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["ColorEdgeDetection"].Passes["ColorEdgeDetection"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["ColorEdgeDetection"].Passes["ColorEdgeDetection"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);

                // pass 2 : Blend Weight Calculation
                antialiasingEffect.Parameters["edgesTex"].SetValue(mlaaEdgeTexTarget.GetTexture());
                graphicsDevice.SetRenderTarget(0, antialiasingTarget);
                graphicsDevice.Clear(Color.Black);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["BlendWeightCalculation"].Passes["BlendWeightCalculation"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["BlendWeightCalculation"].Passes["BlendWeightCalculation"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);

                // pass 3 : Neighborhood Blending
                /*antialiasingEffect.Parameters["blendTex"].SetValue(mlaaBlendTexTarget.GetTexture());
                graphicsDevice.SetRenderTarget(0, antialiasingTarget);

                antialiasingEffect.Begin();
                antialiasingEffect.Techniques["NeighborhoodBlending"].Passes["NeighborhoodBlending"].Begin();
                quadRenderer.Draw();
                antialiasingEffect.Techniques["NeighborhoodBlending"].Passes["NeighborhoodBlending"].End();
                antialiasingEffect.End();

                graphicsDevice.SetRenderTarget(0, null);*/
            }
        }
        #endregion Anti-Aliasing (as post-processing)

        #region Bloom Post-processing

        /// <summary>
        /// Bloom post-processing
        /// </summary>
        /// <remarks>Input buffer scene is guarunteed to be intact.</remarks>
        /// <param name="gameTime"></param>
        /// <param name="input">Input render target 2D</param>
        private void bloom_postProcessing(GameTime gameTime, RenderTarget2D input)
        {
            // apply the bloom extract
            graphicsDevice.SetRenderTarget(0, bloomExtractTarget);

            bloomExtractEffect.Parameters["BloomThreshold"].SetValue(bloomSetting.BloomThreshold);

            bloomExtractEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            bloomExtractEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(input.GetTexture(), new Rectangle(0, 0, bloomExtractTarget.Width, bloomExtractTarget.Height), Color.White);

            bloomExtractEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            bloomExtractEffect.End();

            graphicsDevice.SetRenderTarget(0, null);

            // apply the blur effect on the bright area, draw it into the bloom blur render target
            graphicsDevice.SetRenderTarget(0, bloomBlurTarget);

            bloomBlurEffect.Parameters["blurMag"].SetValue(bloomSetting.BlurAmount);
            bloomBlurEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            bloomBlurEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(bloomExtractTarget.GetTexture(), new Rectangle(0, 0, bloomBlurTarget.Width, bloomBlurTarget.Height), Color.White);

            bloomBlurEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            bloomBlurEffect.End();

            graphicsDevice.SetRenderTarget(0, null);

            //combine the two
            if (isDrawToTexture)
            {
                graphicsDevice.SetRenderTarget(0, dttRt);

                //set the blur texture
                graphicsDevice.Textures[1] = bloomBlurTarget.GetTexture();

                combineFinalEffect.Parameters["sceneIntensity"].SetValue(bloomSetting.SceneIntensity);
                combineFinalEffect.Parameters["bloomIntensity"].SetValue(bloomSetting.BloomIntensity);
                combineFinalEffect.Parameters["sceneSaturation"].SetValue(bloomSetting.SceneSaturation);
                combineFinalEffect.Parameters["bloomSaturation"].SetValue(bloomSetting.BloomSaturation);

                combineFinalEffect.Begin();
                spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
                combineFinalEffect.CurrentTechnique.Passes[0].Begin();

                // draw a final scene (to texture)
                spriteBatch.Draw(input.GetTexture(), new Rectangle(0, 0, dttRt.Width,
                  dttRt.Height), Color.White);

                combineFinalEffect.CurrentTechnique.Passes[0].End();
                spriteBatch.End();
                combineFinalEffect.End();

                graphicsDevice.SetRenderTarget(0, null);
            }
            else
            {
                //set the blur texture
                graphicsDevice.Textures[1] = bloomBlurTarget.GetTexture();

                combineFinalEffect.Parameters["sceneIntensity"].SetValue(bloomSetting.SceneIntensity);
                combineFinalEffect.Parameters["bloomIntensity"].SetValue(bloomSetting.BloomIntensity);
                combineFinalEffect.Parameters["sceneSaturation"].SetValue(bloomSetting.SceneSaturation);
                combineFinalEffect.Parameters["bloomSaturation"].SetValue(bloomSetting.BloomSaturation);

                combineFinalEffect.Begin();
                spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
                combineFinalEffect.CurrentTechnique.Passes[0].Begin();

                // draw a final scene
                spriteBatch.Draw(input.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                    graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

                combineFinalEffect.CurrentTechnique.Passes[0].End();
                spriteBatch.End();
                combineFinalEffect.End();
            }
        }
        #endregion Bloom Post-processing

        #region Draw methods for normal lights

        /// <summary>
        /// Draw the effect of directional light into the scene.
        /// </summary>
        /// <param name="lightDirection"></param>
        /// <param name="color"></param>
        private void DrawDirectionalLight(DirectionalLight dlight)
        {
            //set all parameters
            //only individual parameters will be set
            directionalLightEffect.Parameters["lightDirection"].SetValue(dlight.Direction);
            directionalLightEffect.Parameters["lightColor"].SetValue(dlight.Color.ToVector3());
            directionalLightEffect.Parameters["specularIntensity"].SetValue(dlight.SpecularIntensity);
            directionalLightEffect.Parameters["specularPower"].SetValue(dlight.SpecularPower);
            directionalLightEffect.Parameters["lightIntensity"].SetValue(dlight.Intensity);
            directionalLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            directionalLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            directionalLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            directionalLightEffect.Parameters["adjust"].SetValue(screenAdjustment);

            directionalLightEffect.Begin();
            directionalLightEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            directionalLightEffect.CurrentTechnique.Passes[0].End();
            directionalLightEffect.End();
        }

        /// <summary>
        /// Draw the effect of point light into the scene.
        /// </summary>
        /// <param name="lightPosition"></param>
        /// <param name="lightColor"></param>
        /// <param name="lightIntentisy"></param>
        /// <param name="lightSpecularPower"></param>
        /// <param name="lightRadius"></param>
        private void DrawPointLight(PointLight plight)
        {
            //set all parameters
            pointLightEffect.Parameters["LightPosition"].SetValue(plight.Position);
            pointLightEffect.Parameters["LightColor"].SetValue(plight.Color.ToVector3());
            pointLightEffect.Parameters["specularIntensity"].SetValue(plight.SpecularIntensity);
            pointLightEffect.Parameters["specularPower"].SetValue(plight.SpecularPower);
            pointLightEffect.Parameters["LightIntensity"].SetValue(plight.Intensity);
            pointLightEffect.Parameters["LightRadius"].SetValue(plight.Radius);
            pointLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            pointLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            pointLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            pointLightEffect.Parameters["adjust"].SetValue(screenAdjustment);

            //sphere is the unit in radius
            Matrix sphereWorld = Matrix.CreateScale(plight.Radius) * Matrix.CreateTranslation(plight.Position);

            pointLightEffect.Parameters["world"].SetValue(sphereWorld);
            //pointLightEffect.Parameters["view"].SetValue(Engine.CameraManager.View);
            //pointLightEffect.Parameters["projection"].SetValue(Engine.CameraManager.Projection);

            //calculate the length from camera to the light center
            float distance = Vector3.Distance(Engine.CameraManager.Position, plight.Position);
            //if we are inside the light volume then we draw the interior of the sphere
            if (distance < plight.Radius)
                graphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            else
                graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            //also disable the depth buffer (we don't actually draw anything into the scene.)
            graphicsDevice.RenderState.DepthBufferEnable = false;

            //prepare to draw the sphere (assume only one meshpart)
            ModelMesh mesh = sphereModel.Meshes[0];
            ModelMeshPart part = mesh.MeshParts[0];

            pointLightEffect.Begin();
            pointLightEffect.CurrentTechnique.Passes[0].Begin();

            graphicsDevice.VertexDeclaration = part.VertexDeclaration;
            graphicsDevice.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
            graphicsDevice.Indices = mesh.IndexBuffer;
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex,
                part.PrimitiveCount);

            pointLightEffect.CurrentTechnique.Passes[0].End();
            pointLightEffect.End();

            //restore back the cullmode and depth buffer
            graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            graphicsDevice.RenderState.DepthBufferEnable = true;
        }

        /// <summary>
        /// Draw effect of spotlight into the scene. 
        /// </summary>
        /// <param name="slight"></param>
        public void DrawSpotlight(SpotLight slight)
        {
            spotLightEffect.Parameters["lightPosition"].SetValue(slight.Position);
            spotLightEffect.Parameters["lightConeLookAtPos"].SetValue(slight.LookAtPos);
            spotLightEffect.Parameters["lightRange"].SetValue(slight.Range);
            spotLightEffect.Parameters["lightConeAngle"].SetValue(slight.ConeAngle);
            spotLightEffect.Parameters["lightConeDecay"].SetValue(slight.ConeDecay);
            spotLightEffect.Parameters["lightColor"].SetValue(slight.Color.ToVector3());
            spotLightEffect.Parameters["specularIntensity"].SetValue(slight.SpecularIntensity);
            spotLightEffect.Parameters["specularPower"].SetValue(slight.SpecularPower);
            spotLightEffect.Parameters["lightIntensity"].SetValue(slight.Intensity);
            spotLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            spotLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            spotLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            spotLightEffect.Parameters["adjust"].SetValue(screenAdjustment);

            spotLightEffect.Begin();
            spotLightEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            spotLightEffect.CurrentTechnique.Passes[0].End();
            spotLightEffect.End();
        }

        #endregion

        #region Draw methods for shadow lights

        private void DrawDirectionalShadowLight(DirectionalShadowLight dsLight)
        {
            directionalShadowLightEffect.Parameters["lightPosition"].SetValue(dsLight.Position);
            directionalShadowLightEffect.Parameters["lightLookAtPos"].SetValue(dsLight.LookAtPos);
            directionalShadowLightEffect.Parameters["lightRange"].SetValue(dsLight.Range);
            directionalShadowLightEffect.Parameters["lightColor"].SetValue(dsLight.Color.ToVector3());
            directionalShadowLightEffect.Parameters["specularIntensity"].SetValue(dsLight.SpecularIntensity);
            directionalShadowLightEffect.Parameters["specularPower"].SetValue(dsLight.SpecularPower);
            directionalShadowLightEffect.Parameters["lightIntensity"].SetValue(dsLight.Intensity);
            if (dsLight.ShapeLightTexture != null)
            {
                directionalShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(true);
                directionalShadowLightEffect.Parameters["shapeLightMap"].SetValue(dsLight.ShapeLightTexture);
            }
            else
            {
                directionalShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(false);
                directionalShadowLightEffect.Parameters["shapeLightMap"].SetValue((Texture)null);
            }
            directionalShadowLightEffect.Parameters["lightView"].SetValue(dsLight.View);
            directionalShadowLightEffect.Parameters["lightProjection"].SetValue(dsLight.Projection);
            directionalShadowLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            directionalShadowLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            directionalShadowLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            directionalShadowLightEffect.Parameters["shadowMap"].SetValue(shadowMapRt.GetTexture());
            directionalShadowLightEffect.Parameters["adjust"].SetValue(screenAdjustment);
            directionalShadowLightEffect.Parameters["depthBias"].SetValue(shadowDepthBias);

            directionalShadowLightEffect.Begin();
            directionalShadowLightEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            directionalShadowLightEffect.CurrentTechnique.Passes[0].End();
            directionalShadowLightEffect.End();
        }

        /// <summary>
        /// Draw the point shadow light into the scene.
        /// </summary>
        /// <param name="psLight"></param>
        private void DrawPointShadowLight(PointShadowLight psLight)
        {
            //set all parameters
            pointShadowLightEffect.Parameters["LightPosition"].SetValue(psLight.Position);
            pointShadowLightEffect.Parameters["LightColor"].SetValue(psLight.Color.ToVector3());
            pointShadowLightEffect.Parameters["specularIntensity"].SetValue(psLight.SpecularIntensity);
            pointShadowLightEffect.Parameters["specularPower"].SetValue(psLight.SpecularPower);
            pointShadowLightEffect.Parameters["LightIntensity"].SetValue(psLight.Intensity);
            if (psLight.ShapeLightTexture != null)
            {
                pointShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(true);
                pointShadowLightEffect.Parameters["shapeLightMap"].SetValue(psLight.ShapeLightTexture);
            }
            else
            {
                pointShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(false);
                pointShadowLightEffect.Parameters["shapeLightMap"].SetValue((Texture)null);
            }
            pointShadowLightEffect.Parameters["LightRadius"].SetValue(psLight.Radius);
            pointShadowLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            pointShadowLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            pointShadowLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            pointShadowLightEffect.Parameters["shadowMap"].SetValue(shadowMapRt.GetTexture());
            pointShadowLightEffect.Parameters["adjust"].SetValue(screenAdjustment);
            pointShadowLightEffect.Parameters["depthBias"].SetValue(shadowDepthBias);

            //sphere is the unit in radius
            Matrix sphereWorld = Matrix.CreateScale(psLight.Radius) * Matrix.CreateTranslation(psLight.Position);

            pointShadowLightEffect.Parameters["world"].SetValue(sphereWorld);

            //calculate the length from camera to the light center
            float distance = Vector3.Distance(Engine.CameraManager.Position, psLight.Position);
            //if we are inside the light volume then we draw the interior of the sphere
            if (distance < psLight.Radius)
                graphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            else
                graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            //also disable the depth buffer (we don't actually draw anything into the scene.)
            graphicsDevice.RenderState.DepthBufferEnable = false;

            //prepare to draw the sphere (assume only one meshpart)
            ModelMesh mesh = sphereModel.Meshes[0];
            ModelMeshPart part = mesh.MeshParts[0];

            pointShadowLightEffect.Begin();
            pointShadowLightEffect.CurrentTechnique.Passes[0].Begin();

            graphicsDevice.VertexDeclaration = part.VertexDeclaration;
            graphicsDevice.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
            graphicsDevice.Indices = mesh.IndexBuffer;
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex,
                part.PrimitiveCount);

            pointShadowLightEffect.CurrentTechnique.Passes[0].End();
            pointShadowLightEffect.End();

            //restore back the cullmode and depth buffer
            graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            graphicsDevice.RenderState.DepthBufferEnable = true;
        }

        /// <summary>
        /// Draw the spot shadow light into the scene.
        /// </summary>
        private void DrawSpotshadowLight(SpotShadowLight ssLight)
        {
            spotShadowLightEffect.Parameters["lightPosition"].SetValue(ssLight.Position);
            spotShadowLightEffect.Parameters["lightConeLookAtPos"].SetValue(ssLight.LookAtPos);
            spotShadowLightEffect.Parameters["lightRange"].SetValue(ssLight.Range);
            spotShadowLightEffect.Parameters["lightConeAngle"].SetValue(ssLight.ConeAngle);
            spotShadowLightEffect.Parameters["lightConeDecay"].SetValue(ssLight.ConeDecay);
            spotShadowLightEffect.Parameters["lightColor"].SetValue(ssLight.Color.ToVector3());
            spotShadowLightEffect.Parameters["specularIntensity"].SetValue(ssLight.SpecularIntensity);
            spotShadowLightEffect.Parameters["specularPower"].SetValue(ssLight.SpecularPower);
            if (ssLight.ShapeLightTexture != null)
            {
                spotShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(true);
                spotShadowLightEffect.Parameters["shapeLightMap"].SetValue(ssLight.ShapeLightTexture);
            }
            else
            {
                spotShadowLightEffect.Parameters["isShapeLightMapEnabled"].SetValue(false);
                spotShadowLightEffect.Parameters["shapeLightMap"].SetValue((Texture)null);
            }
            spotShadowLightEffect.Parameters["lightIntensity"].SetValue(ssLight.Intensity);
            spotShadowLightEffect.Parameters["lightView"].SetValue(ssLight.View);
            spotShadowLightEffect.Parameters["lightProjection"].SetValue(ssLight.Projection);
            spotShadowLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            spotShadowLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            spotShadowLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            spotShadowLightEffect.Parameters["shadowMap"].SetValue(shadowMapRt.GetTexture());
            spotShadowLightEffect.Parameters["adjust"].SetValue(screenAdjustment);
            spotShadowLightEffect.Parameters["depthBias"].SetValue(shadowDepthBias);

            spotShadowLightEffect.Begin();
            spotShadowLightEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            spotShadowLightEffect.CurrentTechnique.Passes[0].End();
            spotShadowLightEffect.End();
        }

        #endregion

        /// <summary>
        /// Take and save a screenshot to file.
        /// </summary>
        /// <param name="format">Format of file to save.</param>
        public void TakeAndSaveScreenshot(ImageFileFormat format)
        {
            // take a screenshot whenever combineLightRt is not null
            if (combinedLightRt != null)
            {
                string ext = ".jpg";
                // get the postfix of image
                switch (format)
                {
                    case ImageFileFormat.Bmp:
                        ext = ".bmp";
                        break;
                    case ImageFileFormat.Jpg:
                        ext = ".jpg";
                        break;
                    case ImageFileFormat.Png:
                        ext = ".png";
                        break;
                    case ImageFileFormat.Tga:
                        ext = ".tga";
                        break;
                }

                graphicsDevice.ResolveBackBuffer(screenshot);
                if (screenshotNumber < 10)
                    screenshot.Save("realko-0" + screenshotNumber++ + ext, format);
                else
                    screenshot.Save("realko-" + screenshotNumber++ + ext, format);
            }
        }
    }
}