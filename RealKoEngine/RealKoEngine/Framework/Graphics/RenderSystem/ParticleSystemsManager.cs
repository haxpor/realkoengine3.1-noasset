﻿#region File Description
//-----------------------------------------------------------------------------
// ParticleSystemsManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using RealKo.Framework.Graphics.Particle;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// ParticleSytemsManager manages and handle all the particle systems reside in the world, in which all of them are under the control of
    ///  deferred shading render system in another way.
    /// </summary>
    public class ParticleSystemsManager
    {
        /// <summary>
        /// Particles system reside in the world, they are here because they can get the ability to draw with bloom effect.
        /// </summary>
        List<ParticleSystem> particleSystems;
        //Depth comparer
        IComparer<ParticleSystem> depthComparer;

        /// <summary>
        /// Depth value of screen comparer.
        /// </summary>
        /// <remarks>Sort by ascending.</remarks>
        public class DepthSort : IComparer<ParticleSystem>
        {
            int IComparer<ParticleSystem>.Compare(ParticleSystem x, ParticleSystem y)
            {
                return (new CaseInsensitiveComparer()).Compare(x.Depth, y.Depth);
            }
        }

        /// <summary>
        /// Create the particle systems manager.
        /// </summary>
        public ParticleSystemsManager()
        {
            particleSystems = new List<ParticleSystem>();
            depthComparer = new DepthSort();
        }

        /// <summary>
        /// Add new particle system to the system.
        /// </summary>
        /// <param name="particleSystem"></param>
        public void AddParticleSystem(ParticleSystem particleSystem)
        {
            particleSystems.Add(particleSystem);

            //sort by ascending
            particleSystems.Sort(depthComparer);
        }

        /// <summary>
        /// Remove the specified particle system from the system.
        /// </summary>
        /// <param name="particleSystem"></param>
        public void RemoveParticleSystem(ParticleSystem particleSystem)
        {
            particleSystems.Remove(particleSystem);
        }

        /// <summary>
        /// Remove all the particle systems reside in the system.
        /// </summary>
        public void ClearAllParticleSystem()
        {
            particleSystems.Clear();
        }

        /// <summary>
        /// Get the number of particle systems resided in the system.
        /// </summary>
        public int NumberOfParticleSystem
        {
            get {return particleSystems.Count; }
        }

        /// <summary>
        /// Get the list of particle systems.
        /// </summary>
        public List<ParticleSystem> ParticleSystems
        {
            get { return particleSystems; }
        }

    }
}
