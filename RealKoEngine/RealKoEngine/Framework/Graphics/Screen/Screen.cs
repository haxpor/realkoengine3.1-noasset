﻿#region File Description
//-----------------------------------------------------------------------------
// Screen.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// Screen is responsible for drawing the particular area of content on the screen.
    /// This class should be implmented in order to support various type of screen.
    /// In general Screen contains the number of entities insided them, its responsibility is to manage them to update, and draw.
    /// </summary>
    public class Screen : RK_IUpdateable, RK_IDrawable, RK_IPreInitialize
    {
        /// <summary>
        /// Font will be used only for the screen that can be drawed with alpha blending enabled.
        /// </summary>
        protected SpriteFont font;

        protected bool isActive;
        protected int depth;
        protected List<Scene> scenes;

        /// <summary>
        /// Set or get the screen's activeness.
        /// </summary>
        /// <remarks>This will be set only by the ScreenManager.
        /// You should not set this value unless you know what you are doing.</remarks>
        public bool Active
        {
            get { return isActive; }
            set { isActive = value; }
        }

        /// <summary>
        /// Depth value of this screen to be draw.
        /// </summary>
        /// <remarks>The greater of the value, the screen will be more draw on top of others.
        /// The value should be between 0 - 2^sizeof(int).</remarks>
        public int Depth
        {
            get { return depth; }
            set { depth = value; }
        }

        /// <summary>
        /// Get the number of scenes held by this screen.
        /// </summary>
        public int NumberOfScenes
        {
            get
            {
                if (scenes != null)
                    return scenes.Count;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Get the sprite font associated with this screen.
        /// </summary>
        public SpriteFont Font
        {
            get { return font; }
        }

        /// <summary>
        /// Get the scenes list associated with this screen.
        /// </summary>
        public List<Scene> Scenes
        {
            get { return scenes; }
        }

        /// <summary>
        /// Create screen.
        /// </summary>
        /// <remarks>By not to provide any parameter into the constructor then the screen itself cannot draw any text-string.</remarks>
        public Screen()
        {
            this.font = null;

            depth = 0;  //if not set, the screen will be drawed as the base

            //init the scenes list (it should always be 1 at least)
            scenes = new List<Scene>(1);

            //Initialize the screen
            PreInitialize();
        }

        /// <summary>
        /// Create screen.
        /// </summary>
        /// <param name="font">Font used in drawing string.</param>
        public Screen(SpriteFont font)
        {
            this.font = font;

            depth = 0;  //if not set, the screen will be drawed as the base

            //init the scenes list (it should always be 1 at least)
            scenes = new List<Scene>(1);

            //Initialize the screen
            PreInitialize();
        }

        /// <summary>
        /// Create screen.
        /// </summary>
        /// <param name="depth">Depth of this screen to draw.</param>
        /// <param name="font">Font used in drawing string.</param>
        public Screen(SpriteFont font, int depth)
        {
            this.depth = depth;

            //init the scenes list (it should always be 1 at least)
            scenes = new List<Scene>(1);

            //Initialize the screen
            PreInitialize();
        }

        /// <summary>
        /// Get scene from the given index.
        /// </summary>
        /// <param name="index">Index located the position of scene.</param>
        /// <returns>Scene if scens list contain at least one of the scene, otherwise return null.</returns>
        public Scene GetScene(int index)
        {
            if (scenes != null && scenes.Count > 0)
                return scenes[index];
            else
                return null;
        }

        /// <summary>
        /// Add a new scene into the scenes-list held by this screen.
        /// </summary>
        /// <param name="scene"></param>
        public void AddScene(Scene scene)
        {
            if (scenes != null)
                scenes.Add(scene);
        }

        /// <summary>
        /// Initialize the screen.
        /// </summary>
        /// <remarks>The extended class should also call this base method, in order to avoid nothing on screen at first.
        /// But if the caller modifies the activeness value by himself, ignore to call this method can be done.</remarks>
        public virtual void PreInitialize()
        {
            isActive = false;
        }

        /// <summary>
        /// Update the screen.
        /// Cycle through the scens list and update each one.
        /// </summary>
        /// <remarks>This method will be effected if and only if this screen is active.</remarks>
        /// <param name="gameTime">GameTime object.</param>
        public void Update(GameTime gameTime)
        {
            foreach(Scene s in scenes)
            {
                s.Update(gameTime);
            }
        }

        /// <summary>
        /// Draw the content on screen.
        /// Cycle through the scens list and draw each one.
        /// <remarks>This method will be effected if and only if this screen is active.</remarks>
        /// </summary>
        /// <param name="gameTime">GameTime object.</param>
        public void Draw(GameTime gameTime)
        {
            foreach (Scene s in scenes)
            {
                s.Draw(gameTime);
            }
        }
    }
}
