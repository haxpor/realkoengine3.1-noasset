﻿#region File Description
//-----------------------------------------------------------------------------
// ScreenManager.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RealKo.Framework.State;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// Screen manager handle and manage all the screens of the game.
    /// </summary>
    /// <remarks>The screens must be sorted from deepest to the frontmost before adding.
    /// If want to use this class separately from the engine itself, user must modify the code to isolate the reference to the engine.</remarks>
    public class ScreenManager: RK_IUpdateable, RK_IDrawable
    {
        // list of screens to handle
        List<Screen> screenList;
        // state maps to the particular screen
        Dictionary<Screen, List<int>> stateMapping;
        //Depth comparer
        IComparer<Screen> depthComparer;

        /// <summary>
        /// Depth value of screen comparer.
        /// </summary>
        /// <remarks>Sort by ascending.</remarks>
        public class DepthSort : IComparer<Screen>
        {
            int IComparer<Screen>.Compare(Screen x, Screen y)
            {
                return (new CaseInsensitiveComparer()).Compare(x.Depth, y.Depth);
            }
        }

        /// <summary>
        /// Get the list of screens associated with this screen manager.
        /// </summary>
        public List<Screen> Screens
        {
            get { return screenList; }
        }

        /// <summary>
        /// Create the screen manager.
        /// </summary>
        public ScreenManager()
        {
            screenList = new List<Screen>();
            stateMapping = new Dictionary<Screen, List<int>>();
            depthComparer = new DepthSort();
        }

        ~ScreenManager()
        {
            Clear();
        }

        /// <summary>
        /// Update all the screens'content that are currently active.
        /// </summary>
        /// <param name="game">GameTime object</param>
        public void Update(GameTime gameTime)
        {
            bool match = false;
            foreach (Screen s in screenList)
            {
                //change the activeness each screen
                //iterate all the states in screen's state-list
                foreach (int state in stateMapping[s])
                {
                    if (state == Engine.State.CurrentState)
                    {
                        s.Active = true;
                        match = true;
                        break;
                    }
                }
                if(!match)
                    s.Active = false;

                //update the screen if it's active
                if (s.Active)
                    s.Update(gameTime);
            }
        }

        /// <summary>
        /// Draw all the screens' content that are currently active.
        /// </summary>
        /// <param name="gameTime">GameTime object.</param>
        public void Draw(GameTime gameTime)
        {
            foreach (Screen s in screenList)
            {
                //if the screen is active
                // and the current state is matched
                // then draw it
                //Note: Draw from lowest depth to highest depth (already sorted from AddScreen() method)
                foreach (int state in stateMapping[s])
                {
                    if (s.Active && state == Engine.State.CurrentState)
                    {
                        s.Draw(gameTime);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Add the new screen into this Screen Manager, and also map the state to this screen.
        /// </summary>
        /// <param name="screen">Screen to be added.</param>
        /// <param name="state">State to be mapped.</param>
        public void AddScreen(Screen screen, int state)
        {
            //check first if the screen is already there
            if (screenList.Contains(screen) && stateMapping.ContainsKey(screen))
            {
                //add new states into its corresponding list
                stateMapping[screen].Add(state);
            }
            //if it's fresh
            else if (!screenList.Contains(screen) && !stateMapping.ContainsKey(screen))
            {
                screenList.Add(screen);
                List<int> list = new List<int>();
                list.Add(state);
                stateMapping.Add(screen, list);
            }

            //sort by ascending
            screenList.Sort(depthComparer);
        }

        /// <summary>
        /// Add the new screen into this Screen Manager, and map the list of states to this screen.
        /// </summary>
        /// <param name="screen">Screen to be added.</param>
        /// <param name="slist">List of states to be mapped.</param>
        public void AddScreen(Screen screen, List<int> slist)
        {
            //check first if the screen is already there, then replace the old list with new one
            if (screenList.Contains(screen) && stateMapping.ContainsKey(screen))
            {
                //clear the old list
                stateMapping[screen].Clear();

                //replace with the new one
                stateMapping[screen] = slist;
            }
            //if it's fresh
            else if (!screenList.Contains(screen) && !stateMapping.ContainsKey(screen))
            {
                screenList.Add(screen);
                stateMapping.Add(screen, slist);
            }

            //sort by ascending
            screenList.Sort(depthComparer);
        }

        /// <summary>
        /// Remove the specified screen from this Screen Manager.
        /// </summary>
        /// <remarks>Also remove any states mapped to this screen.</remarks>
        /// <param name="screen">Screen to be removed.</param>
        public void RemoveScreen(Screen screen)
        {
            screenList.Remove(screen);
            stateMapping.Remove(screen);
        }

        /// <summary>
        /// Remove all the screens resided on this Screen Manager.
        /// <remarks>This also removes all the state mapping from the list too.</remarks>
        /// </summary>
        public void Clear()
        {
            stateMapping.Clear();
            screenList.Clear();
        }
    }
}
