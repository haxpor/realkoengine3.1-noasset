﻿#region File Description
//-----------------------------------------------------------------------------
// Scene.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework.Graphics;

namespace RealKo.Framework.Graphics
{
    /// <summary>
    /// Purpose of scene class is to separate the content from the logic code.
    /// </summary>
    public class Scene: RK_IUpdateable, RK_IDrawable, RK_IPreInitialize
    {
        /// <summary>
        /// Parent screen that this scene is under.
        /// </summary>
        protected Screen parent;

        /// <summary>
        /// Effect file used for render geometry
        /// </summary>
        /// <remarks></remarks>
        protected Effect effect;

        /// <summary>
        /// Create the new scene.
        /// </summary>
        /// <param name="parent">Parent screen of this scene.</param>
        /// <param name="effect">Effect used for rendering.
        /// Use the one that specified by the engine's render system.</param>
        /// <remarks>If the effect is null, then the underlying system implicitly treated as the normal text-string renderer.</remarks>
        public Scene(Screen parent, Effect effect)
        {
            this.effect = effect;
            this.parent = parent;

            //add this scene into the parent screen
            parent.AddScene(this);

            //internal initialization
            PreInitialize();
        }

        /// <summary>
        /// Internal initialization.
        /// </summary>
        public virtual void PreInitialize()
        {
            //do nothing here
        }

        /// <summary>
        /// Update the scene.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            //do nothing here
        }

        /// <summary>
        /// Draw the scene.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Draw(GameTime gameTime)
        {
            //do nothing here
        }
    }
}
