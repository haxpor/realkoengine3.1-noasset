﻿#region File Description
//-----------------------------------------------------------------------------
// SCMeshNotFoundException.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;

namespace RealKo.Framework.Exception
{
    /// <summary>
    /// Exception when both skydome and clouddome mesh not found.
    /// </summary>
    /// <remarks>Every airport model must have one skydome mesh, and one for clouddome mesh.</remarks>
    public class SCMeshNotFoundException: ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Skydome mesh, or clouddome mesh not found.";
            }
        }

        public override string ToString()
        {
            return Message;
        }
    }
}
