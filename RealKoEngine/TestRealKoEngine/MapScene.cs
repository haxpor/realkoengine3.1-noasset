﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;

namespace TestRealKoEngine
{
    public class MapScene: Scene
    {
        //floor
        Model floorMesh;
        Matrix floorMeshWorld;
        Material floorMeshMaterial;

        //lizard
        Model lizardMesh;
        Matrix lizardMeshWorld;
        Material lizardMesh_RockMaterial;
        Material lizardMesh_EyeMaterial;
        Material lizardMesh_GeoMaterial;

        //ship 1
        Model ship1_Mesh;
        Matrix ship1_MeshWorld;
        Material ship1_MeshMaterial;

        //ship 2
        Model ship2_Mesh;
        Matrix ship2_MeshWorld;
        Material ship2_MeshMaterial;

        float rot = 0f;

        /// <summary>
        /// Create the map scene.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="effect"></param>
        public MapScene(Screen parent, Effect effect)
            : base(parent, effect)
        {
        }

        public override void PreInitialize()
        {
            //floor
            floorMesh = Engine.Content.Load<Model>("Meshes/Others/Ground");
            floorMeshWorld = Matrix.CreateScale(0.5f, 0.5f, 0.5f) * Matrix.CreateTranslation(0f, -2.2f, 0f);
            floorMeshMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            floorMeshMaterial.TextureCoordU = 1f;
            floorMeshMaterial.TextureCoordV = 1f;
            floorMeshMaterial.DiffuseTexture = "Meshes/Others/ground_diffuse";
            floorMeshMaterial.NormalTexture = "Meshes/Others/ground_normal";
            floorMeshMaterial.SpecularTexture = "Meshes/Others/ground_specular";
            floorMeshMaterial.SpecularIntensity = 1.0f;
            floorMeshMaterial.SpecularColor = Color.Coral.ToVector4();
            floorMeshMaterial.SpecularPower = 10.0f;

            //lizard
            lizardMesh = Engine.Content.Load<Model>("Meshes/Others/lizard");
            lizardMeshWorld = Matrix.CreateScale(0.025f, 0.025f, 0.025f) * Matrix.CreateRotationY(MathHelper.Pi) *
                Matrix.CreateTranslation(0, 3f, 0);
            //material
            //rock material
            lizardMesh_RockMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            lizardMesh_RockMaterial.TextureCoordU = 1f;
            lizardMesh_RockMaterial.TextureCoordV = 1f;
            lizardMesh_RockMaterial.DiffuseTexture = "Meshes/Others/rock_diff";
            lizardMesh_RockMaterial.NormalTexture = "Meshes/Others/rock_norm";
            lizardMesh_RockMaterial.SpecularTexture = "Meshes/Others/rock_geo_s";
            lizardMesh_RockMaterial.SpecularIntensity = 20.0f;
            //lizardMesh_RockMaterial.SpecularColor = new Color(0.203999996185303f, 0.163486823439598f, 0.0791520029306412f).ToVector4();
            lizardMesh_RockMaterial.SpecularPower = 2f;

            //lizard geo
            lizardMesh_GeoMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            lizardMesh_GeoMaterial.TextureCoordU = 1.0f;
            lizardMesh_GeoMaterial.TextureCoordV = 1.0f;
            lizardMesh_GeoMaterial.DiffuseTexture = "Meshes/Others/lizard_diff";
            lizardMesh_GeoMaterial.NormalTexture = "Meshes/Others/lizard_norm";
            lizardMesh_GeoMaterial.SpecularTexture = "Meshes/Others/lizard_geo_s";
            lizardMesh_GeoMaterial.SpecularIntensity = 10.0f;
            //lizardMesh_GeoMaterial.SpecularColor = Vector4.One;
            lizardMesh_GeoMaterial.SpecularPower = 10.3380002975464f;

            //lizard eye
            lizardMesh_EyeMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            lizardMesh_EyeMaterial.TextureCoordU = 1.0f;
            lizardMesh_EyeMaterial.TextureCoordV = 1.0f;
            lizardMesh_EyeMaterial.DiffuseTexture = "Meshes/Others/lizardeye_diff";
            lizardMesh_EyeMaterial.NormalTexture = "Meshes/Others/lizardeye_norm";
            lizardMesh_EyeMaterial.SpecularTexture = "Meshes/Others/eye_geo_s";
            lizardMesh_EyeMaterial.SpecularIntensity = 1.0f;
            //lizardMesh_EyeMaterial.SpecularColor = Vector4.One;
            lizardMesh_EyeMaterial.SpecularPower = 58.6940002441406f;

            //ship1
            ship1_Mesh = Engine.Content.Load<Model>("Meshes/Others/ship1");
            ship1_MeshWorld = Matrix.CreateScale(0.25f) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateTranslation(30, 10, 0);
            ship1_MeshMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            ship1_MeshMaterial.TextureCoordU = 1.0f;
            ship1_MeshMaterial.TextureCoordV = 1.0f;
            ship1_MeshMaterial.DiffuseTexture = "Meshes/Others/ship1_c";
            ship1_MeshMaterial.NormalTexture = "Meshes/Others/ship1_n";
            ship1_MeshMaterial.SpecularTexture = "Meshes/Others/ship1_s";
            ship1_MeshMaterial.SpecularPower = 2f;
            ship1_MeshMaterial.SpecularIntensity = 5.0f;

            //ship2
            ship2_Mesh = Engine.Content.Load<Model>("Meshes/Others/ship2");
            ship2_MeshWorld = Matrix.CreateScale(0.25f) * Matrix.CreateRotationX(-MathHelper.PiOver2)
                * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateTranslation(-30, 15, 0);
            ship2_MeshMaterial = new Material(Engine.GraphicsDevice, Engine.Content, effect);
            ship2_MeshMaterial.TextureCoordU = 1.0f;
            ship2_MeshMaterial.TextureCoordV = 1.0f;
            ship2_MeshMaterial.DiffuseTexture = "Meshes/Others/ship2_c";
            ship2_MeshMaterial.NormalTexture = "Meshes/Others/ship2_n";
            ship2_MeshMaterial.SpecularTexture = "Meshes/Others/ship2_s";
            ship2_MeshMaterial.SpecularPower = 2f;
            ship2_MeshMaterial.SpecularIntensity = 5.0f;

            //add a new light
            /*DirectionalLight dlight = new DirectionalLight(Vector3.UnitY, Color.White);
            DirectionalLight dlight2 = new DirectionalLight(Vector3.UnitX, Color.White);
            DirectionalLight dlight3 = new DirectionalLight(-Vector3.UnitX, Color.White);
            DirectionalLight dlight4 = new DirectionalLight(Vector3.UnitZ, Color.White);
            DirectionalLight dlight5 = new DirectionalLight(-Vector3.UnitZ, Color.White);
            Engine.RenderSystem.DirectionalLights.Add(dlight);
            Engine.RenderSystem.DirectionalLights.Add(dlight2);
            Engine.RenderSystem.DirectionalLights.Add(dlight3);
            Engine.RenderSystem.DirectionalLights.Add(dlight4);
            Engine.RenderSystem.DirectionalLights.Add(dlight5);*/
        }

        public override void Update(GameTime gameTime)
        {
            ship1_MeshWorld = Matrix.CreateScale(0.25f) * Matrix.CreateRotationX(rot)
                * Matrix.CreateRotationY(rot) * Matrix.CreateTranslation(30, 15, 0);

            ship2_MeshWorld = Matrix.CreateScale(0.25f) * Matrix.CreateRotationX(-rot)
                * Matrix.CreateRotationY(rot) * Matrix.CreateTranslation(-30, 15, 0);

            rot += 0.01f;

            if (rot > MathHelper.TwoPi)
                rot = 0.0f;
        }

        public override void Draw(GameTime gameTime)
        {
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = false;
            Engine.GraphicsDevice.RenderState.DepthBufferEnable = true;

            //NOTE: The meshIndex and boneIndex can be found in the model's structure-output file.
            //draw floor
            floorMeshMaterial.DrawBasic(gameTime, floorMeshWorld, floorMesh, 0, 1);

            //draw all lizard's meshes
            lizardMesh_RockMaterial.DrawBasic(gameTime, lizardMeshWorld, lizardMesh, 0, 1);
            lizardMesh_GeoMaterial.DrawBasic(gameTime, lizardMeshWorld, lizardMesh, 1, 2);
            lizardMesh_EyeMaterial.DrawBasic(gameTime, lizardMeshWorld, lizardMesh, 2, 3);

            //draw ship1
            ship1_MeshMaterial.DrawBasic(gameTime, ship1_MeshWorld, ship1_Mesh, 0, 0);

            //draw ship2
            ship2_MeshMaterial.DrawBasic(gameTime, ship2_MeshWorld, ship2_Mesh, 0, 0);
        }
    }
}
