﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework;
using RealKo.Framework.Graphics.Particle;

namespace TestRealKoEngine
{
    public class SnowLowParticleSystem : RealKo.Framework.Graphics.Particle.ParticleSystem
    {
        public SnowLowParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            //go to the Base folder of the engine
            settings.TextureName = "Base/Textures/SP_FX_Snowflake";

            settings.MaxParticles = 2000;

            settings.Duration = TimeSpan.FromSeconds(7);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 1;

            settings.MinVerticalVelocity = -3;
            settings.MaxVerticalVelocity = -2;

            //treat wind as the gravity here
            settings.Gravity = Engine.SimulationSettings.Wind;

            settings.EndVelocity = 0.75f;

            settings.MinRotateSpeed = -5;
            settings.MaxRotateSpeed = 5;

            settings.MinStartSize = 1;
            settings.MaxStartSize = 1;

            settings.MinEndSize = 0.25f;
            settings.MaxEndSize = 0.5f;

            settings.MinColor = Color.Snow;
            settings.MaxColor = Color.White;

            settings.SourceBlend = Blend.One;
            settings.DestinationBlend = Blend.One;
            settings.BlendFunction = BlendFunction.Max;
        }
    }
}
