﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RealKo.Framework.Graphics.Particle;

namespace TestRealKoEngine
{
    public class RainLowParticleSystem : RealKo.Framework.Graphics.Particle.ParticleSystem
    {
        public RainLowParticleSystem(GraphicsDevice graphicsDevice, ContentManager content, string name)
            : base(graphicsDevice, content, name)
        {
        }

        protected override void InitializeSettings(ParticleSettings settings)
        {
            //go to the Base folder of the engine
            settings.TextureName = "Base/Textures/SP_FX_Raindrop";

            settings.MaxParticles = 2000;

            settings.Duration = TimeSpan.FromSeconds(3);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 0;

            settings.MinVerticalVelocity = -45;
            settings.MaxVerticalVelocity = -55;

            //treat wind as the gravity here
            settings.Gravity = new Vector3(0, -10, 0);
            settings.IsAffectedByWindChanging = false;

            settings.EndVelocity = 1f;

            settings.MinRotateSpeed = 0;
            settings.MaxRotateSpeed = 0;

            settings.MinStartSize = 10;
            settings.MaxStartSize = 10;

            settings.MinEndSize = 10;
            settings.MaxEndSize = 10;

            settings.SourceBlend = Blend.BlendFactor;
            settings.DestinationBlend = Blend.One;

            settings.BlendFactor = new Color(0.25f, 0.25f, 0.25f, 1.0f);
        }
    }
}
