#region File Description
//-----------------------------------------------------------------------------
// Game.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Graphics.Particle;
using RealKo.Framework.Graphics.Misc;
using RealKo.Framework.Helper;
using RealKo.Framework.Helper.Input;
using RealKo.Framework.Helper.Diag;
using RealKo.Framework.Helper.Util;
using RealKo.Framework.Entity;
using RealKo.Framework.Entity.Type.Manager;
using RealKoContentShared;

namespace TestRealKoEngine
{
    /// <summary>
    /// State information provided for the testing session.
    /// </summary>
    public static class StateInfo
    {
        public enum State
        {
            SIMULATION
        }
    }

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont debugFont;
        KeyboardManager keyManager;

        //MouseManager
        MouseManager mouseManager;

        // will be initially set in code
        public static Vector3 camPos;

        //Framerate measurer
        FrameRateMeasurer frameRate;
        Vector2 frameRatePos = new Vector2(15, 12);

        //Whether we use the fixed camera position or not
        // usually set it to true when you need to test the program remotely as mouse accuracy is not acceptable and will turn testing experience to be very bad
        bool isFixedCamera = true;

        //status of simulation
        bool isPaused = false;
        bool isClouddyAtNight = true;
        bool isDrawToDefaultBuffer = true;
        bool isDebugInfoDrawed = false;  // change to true to draw debugging information

        bool isBackViewDrawed;
        bool isLeftViewDrawed;
        bool isRightViewDrawed;

        bool isAdjustModeEnabled = false;
        bool isFramerateCalculatedAndDrawed = false;

        float rotationX_forFixed = 0.0f; // only used for fixed camera, contains rotation angle X
        float rotationY_forFixed = 0.0f; // only used for fixed camera, contains rotation angle Y

        FogPreset currentFogPreset = FogPreset.DEFAULT;

        //reference to scene
        //SuvarnabhumiScene baseScene;
        //YangonScene baseScene; 
        DonMueangScene baseScene;

        #region Cameras view's properties
        //Binocular Rendertarget
        Texture2D normalSceneTexture;
        Texture2D binocularBackSceneTexture;
        Texture2D binocularLeftSceneTexture;
        Texture2D binocularRightSceneTexture;

        //cameramanager of binocular camera
        CameraManager defaultCameraManager;
        CameraManager binocularBackCameraManager;
        CameraManager binocularLeftCameraManager;
        CameraManager binocularRightCameraManager;

        //number of airplanes drawed on each of the camera view
        int numAirplanesDrawedInDefaultView;
        int numAirplanesDrawedInBackView;
        int numAirplanesDrawedInLeftView;
        int numAirplanesDrawedInRightView;
        #endregion

        //Wrapper client for reading the airplane data packets
        ClientNetAirplaneDataReader clientNetAirplaneReader;
        //Wrapper client for reading the time data
        ClientNetTimeDataReader clientNetTimeReader;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            graphics.MinimumVertexShaderProfile = ShaderProfile.VS_2_0;
            graphics.MinimumPixelShaderProfile = ShaderProfile.PS_3_0;
            
            //we use the content manager provided by the engine instead of the one provided by Game1 class.
            //Note: Engine's content manager is safe to use as its root node is not bound to any folder.
            // Only the downside is that we have to specify the absolute path everytime we load anything.
            //Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            //setup the engine (must be in absolute path)
           /* Engine.SetupEngine(graphics,
                "Content/Config/Simulation/SimulationSettings",
                "Content/Config/Airport/SCMeshes",
                "Content/Config/Airport/AirportOpaqueMeshes",
                "Content/Config/Airport/AirportAlphaMeshes",
                "Content/Config/Airport/AirportLightMeshes",
                "Content/Config/Airport/AirportApproachLightMeshes",
                "Content/Config/TranObj/TransportationObject");*/
            Engine.SetupEngine(graphics,
                "Content/Config/Simulation/SimulationSettings",
                "Content/Config/Airport/SCMeshes",
                "Content/Config/Airport/AirportOpaqueMeshes",
                "Content/Config/Airport/AirportAlphaMeshes",
                "Content/Config/Airport/AirportLightMeshes",
                "Content/Config/Airport/AirportApproachLightMeshes",
                "Content/Config/Airport/AirportPointLightSource",
                "Content/Config/TranObj/TransportationObject");

            //immediately setup the current state of engine
            Engine.State.CurrentState = (int)StateInfo.State.SIMULATION;

            // disable fog
            Engine.FogSettingManager.FogEnabled = false;
            // disable anti-alias
            Engine.RenderSystem.ApplyAntiAliasing = false;

            //set the shadow-depth bias to be used with the current resolution
            //Note: For the experiment, 1280 * 1024 => 0.0001f.
            //      For the experiment, 1024 * 768 => 0.001f.
            // But also note that we must change the shadow depth bias dynamically
            // At the very close we should set the depth bias value to low enough, but when the depth to camera increases we should set it to lower.

            //So in this case, our camera is initially be located closer to the airport thus setting this value to low is the best approach.
            Engine.RenderSystem.ShadowDepthBias = 0.0001f;

            // generic camera positioning
            camPos = XNAUtil.OffsetPoint(Engine.SimulationSettings.InitialCameraPosition.Position / Engine.SimulationSettings.MapToModelScale, 
                true, Engine.SimulationSettings.InitialCameraPosition.IsPosRotated);
            camPos.Y = camPos.Y + 2 * Engine.SimulationSettings.GlobalScale;

            //create the default camera manager for the default view
            //this camera manager will store the old information of Engine's Camera Manager
            defaultCameraManager = new CameraManager();

            //Calculate the offset rotation of the airport
            //Note: Apply this matrix to look at position each camera.
            Vector3 airportOffRotRadians = XNAUtil.ToRadians(ref Engine.SimulationSettings.AirportOffsetRotation);
            Matrix offsetRot = Matrix.CreateFromYawPitchRoll(-airportOffRotRadians.Y, airportOffRotRadians.X, airportOffRotRadians.Z);

            Vector3 unitLookAt = Engine.CameraManager.LookAt - Engine.CameraManager.Position;
            unitLookAt = Vector3.Transform(unitLookAt, offsetRot);
            unitLookAt.Normalize();

            // Set the rotation to camera
            //Engine.CameraManager.LookAt = unitLookAt;
            //Engine.CameraManager.View = Engine.CameraManager.CalculateViewMatrix(Engine.CameraManager.Position, Engine.CameraManager.LookAt, Engine.CameraManager.Up);
            //Engine.CameraManager.InvertedViewProjection = Matrix.Invert(Engine.CameraManager.View * Engine.CameraManager.Projection);

            #region Binocular camera back view setup
            //create the camera manager of binocular camera
            binocularBackCameraManager = new CameraManager();
            binocularBackCameraManager.Position = new Vector3(camPos.X, camPos.Y, camPos.Z + 0.5f);
            binocularBackCameraManager.NearClip = 0.01f;
            binocularBackCameraManager.FarClip = 10000f;
            binocularBackCameraManager.Up = Vector3.UnitY;
            binocularBackCameraManager.LookAt = binocularBackCameraManager.Position + Vector3.Transform(unitLookAt, Matrix.CreateRotationY(MathHelper.ToRadians(180)));
            binocularBackCameraManager.View = Matrix.CreateLookAt(binocularBackCameraManager.Position, binocularBackCameraManager.LookAt, binocularBackCameraManager.Up);
            binocularBackCameraManager.Projection = Matrix.CreatePerspectiveFieldOfView(binocularBackCameraManager.FieldOfView,
                graphics.GraphicsDevice.DisplayMode.AspectRatio, binocularBackCameraManager.NearClip, binocularBackCameraManager.FarClip);
            binocularBackCameraManager.InvertedViewProjection = Matrix.Invert(binocularBackCameraManager.View * binocularBackCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion

            #region Binocular camera left view setup
            //create the camera manager of binocular camera
            binocularLeftCameraManager = new CameraManager();
            binocularLeftCameraManager.Position = new Vector3(camPos.X - 0.5f, camPos.Y, camPos.Z);
            binocularLeftCameraManager.NearClip = 0.01f;
            binocularLeftCameraManager.FarClip = 10000f;
            binocularLeftCameraManager.Up = Vector3.UnitY;
            binocularLeftCameraManager.LookAt = binocularLeftCameraManager.Position + Vector3.Transform(unitLookAt, Matrix.CreateRotationY(MathHelper.ToRadians(90)));
            binocularLeftCameraManager.View = Matrix.CreateLookAt(binocularLeftCameraManager.Position, binocularLeftCameraManager.LookAt, binocularLeftCameraManager.Up);
            binocularLeftCameraManager.Projection = Matrix.CreatePerspectiveFieldOfView(binocularLeftCameraManager.FieldOfView,
                graphics.GraphicsDevice.DisplayMode.AspectRatio, binocularLeftCameraManager.NearClip, binocularLeftCameraManager.FarClip);
            binocularLeftCameraManager.InvertedViewProjection = Matrix.Invert(binocularLeftCameraManager.View * binocularLeftCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion

            #region Binocular camera right view setup
            //create the camera manager of binocular camera
            binocularRightCameraManager = new CameraManager();
            binocularRightCameraManager.Position = new Vector3(camPos.X + 0.5f, camPos.Y, camPos.Z);
            binocularRightCameraManager.NearClip = 0.01f;
            binocularRightCameraManager.FarClip = 10000f;
            binocularRightCameraManager.Up = Vector3.UnitY;
            binocularRightCameraManager.LookAt = binocularRightCameraManager.Position + Vector3.Transform(unitLookAt, Matrix.CreateRotationY(MathHelper.ToRadians(-90)));
            binocularRightCameraManager.View = Matrix.CreateLookAt(binocularRightCameraManager.Position, binocularRightCameraManager.LookAt, binocularRightCameraManager.Up);
            binocularRightCameraManager.Projection = Matrix.CreatePerspectiveFieldOfView(binocularRightCameraManager.FieldOfView,
                graphics.GraphicsDevice.DisplayMode.AspectRatio, binocularRightCameraManager.NearClip, binocularRightCameraManager.FarClip);
            binocularRightCameraManager.InvertedViewProjection = Matrix.Invert(binocularRightCameraManager.View * binocularRightCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //debug font (ready to be used by the scene)
            debugFont = Engine.Content.Load<SpriteFont>("Content/Fonts/Debug");

            //framerate
            frameRate = new FrameRateMeasurer(debugFont, spriteBatch);

            //mousemanager
            mouseManager = new MouseManager(graphics.GraphicsDevice.PresentationParameters.BackBufferWidth,
                graphics.GraphicsDevice.PresentationParameters.BackBufferHeight);
            mouseManager.AlwaysKeepMouseAtCenter = true;
            mouseManager.MouseRelativeEnabled = true;
            //keyboard manager
            keyManager = new KeyboardManager();

            //setup the camera stuff
            Engine.CameraManager.Position = camPos;
            Engine.CameraManager.NearClip = 0.01f;
            Engine.CameraManager.FarClip = 10000f;
            Engine.CameraManager.FieldOfView = MathHelper.ToRadians(60);
            Engine.CameraManager.Projection = Matrix.CreatePerspectiveFieldOfView(Engine.CameraManager.FieldOfView,
                graphics.GraphicsDevice.DisplayMode.AspectRatio, Engine.CameraManager.NearClip, Engine.CameraManager.FarClip);
            //set the projection matrix to the base material
            Engine.BaseMaterial.Parameters["projection"].SetValue(Engine.CameraManager.Projection);

            //setup scene (all the need entities are loaded)
            //Note: It's safe to setup the scene after everything is set up
            SetupScene();

            //safe to create the network stuff section
            //#TODO: Add the network related stuff here
            //bind the client airplane data reader with the port 5001
            clientNetAirplaneReader = new ClientNetAirplaneDataReader(IPAddress.Parse("0.0.0.0"), 5001);
            clientNetAirplaneReader.StartReadingPackets();

            //bind the time data reader with the port 5000
            clientNetTimeReader = new ClientNetTimeDataReader(IPAddress.Parse("0.0.0.0"), 5000);
            clientNetTimeReader.StartReadingPackets();
            //#ENDTODO
        }

        /// <summary>
        /// Setup scene in the game world.
        /// </summary>
        private void SetupScene()
        {
            //#1 For deferred screen
            //create new screen and the test scene
            Screen screen = new Screen();
            DonMueangScene scene2 = new DonMueangScene(screen, Engine.RenderSystem.RenderEffect, keyManager);
            //MapScene scene = new MapScene(screen, Engine.RenderSystem.RenderEffect);
            Engine.DeferredScreenManager.AddScreen(screen, (int)StateInfo.State.SIMULATION);

            //#2 For text2 screen
            Screen text2Screen = new Screen(debugFont);
            Text2Scene text2Scene = new Text2Scene(text2Screen, null, keyManager);
            Engine.Text2ScreenManager.AddScreen(text2Screen, (int)StateInfo.State.SIMULATION);

            //get the reference of scene from the first deferred screen
            baseScene = (DonMueangScene)Engine.DeferredScreenManager.Screens[0].Scenes[0];
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            float ksDiffTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 100f;

            Vector3 lookAhead = new Vector3(0, 0, -1);
            //update the up vector of camera
            Vector3 up = new Vector3(0, 1, 0);

            //if we use the fixed camera position then do this
            if (isFixedCamera)
            {
                Matrix rotation = Matrix.CreateRotationX(rotationX_forFixed) * Matrix.CreateRotationY(rotationY_forFixed);
                //normal transform from mouse's movement
                lookAhead = Vector3.Transform(lookAhead, rotation);

                up = Vector3.Transform(up, rotation);

                // check to look in direction of x and y via key here
                Vector3 move = new Vector3(0, 0, 0);
                if (keyManager.OnKeyDown(Keys.W))
                {
                    move += new Vector3(0, 0, -1);
                }
                if (keyManager.OnKeyDown(Keys.S))
                {
                    move += new Vector3(0, 0, 1);
                }
                if (keyManager.OnKeyDown(Keys.A))
                {
                    move += new Vector3(-1, 0, 0);
                }
                if (keyManager.OnKeyDown(Keys.D))
                {
                    move += new Vector3(1, 0, 0);
                }
                if (keyManager.OnKeyDown(Keys.E))
                {
                    move += new Vector3(0, -1, 0);
                }
                if (keyManager.OnKeyDown(Keys.Q))
                {
                    move += new Vector3(0, 1, 0);
                }
                if (keyManager.OnKeyDown(Keys.Up))
                {
                    rotationX_forFixed += 0.1f;
                }
                if (keyManager.OnKeyDown(Keys.Down))
                {
                    rotationX_forFixed -= 0.1f;
                }
                if (keyManager.OnKeyDown(Keys.Left))
                {
                    rotationY_forFixed += 0.1f;
                }
                if (keyManager.OnKeyDown(Keys.Right))
                {
                    rotationY_forFixed -= 0.1f;
                }

                //update the camera's position
                Vector3 moveAmount = move * ksDiffTime * 1.2f;
                moveAmount = Vector3.Transform(moveAmount, rotation);
                camPos += moveAmount;
            }
            else
            {
                //update camera
                mouseManager.Update(gameTime);

                //update the look ahead vector
                Matrix rotation = Matrix.CreateRotationX(mouseManager.CameraRotY) *
                                                         Matrix.CreateRotationY(mouseManager.CameraRotX);

                //normal transform from mouse's movement
                lookAhead = Vector3.Transform(lookAhead, rotation);

                up = Vector3.Transform(up, rotation);

                //update movement
                Vector3 move = new Vector3(0, 0, 0);
                if (keyManager.OnKeyDown(Keys.W))
                {
                    move += new Vector3(0, 0, -1);
                }
                if (keyManager.OnKeyDown(Keys.S))
                {
                    move += new Vector3(0, 0, 1);
                }
                if (keyManager.OnKeyDown(Keys.A))
                {
                    move += new Vector3(-1, 0, 0);
                }
                if (keyManager.OnKeyDown(Keys.D))
                {
                    move += new Vector3(1, 0, 0);
                }
                if (keyManager.OnKeyDown(Keys.E))
                {
                    move += new Vector3(0, -1, 0);
                }
                if (keyManager.OnKeyDown(Keys.Q))
                {
                    move += new Vector3(0, 1, 0);
                }

                //update the camera's position
                Vector3 moveAmount = move * ksDiffTime;
                moveAmount = Vector3.Transform(moveAmount, rotation);
                camPos += moveAmount;
            }

            lookAhead += camPos;

            //set the position of the camera
            Engine.CameraManager.Position = camPos;
            Engine.BaseMaterial.Parameters["cameraPosition"].SetValue(camPos);

            Engine.CameraManager.View = Matrix.CreateLookAt(camPos, lookAhead, up);
            Engine.CameraManager.Up = up;
            Engine.CameraManager.LookAt = lookAhead;
            Engine.BaseMaterial.Parameters["view"].SetValue(Engine.CameraManager.View);

            //calculate the camera's inverted view-projection, ready to be used by other components
            //so we only calculate this once
            Engine.CameraManager.InvertedViewProjection = Matrix.Invert(
                Engine.CameraManager.View * Engine.CameraManager.Projection);
            Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(Engine.CameraManager.InvertedViewProjection);

            //process the key input
            HandleKeyInput(gameTime);

            //update engine
            Engine.Update(gameTime, isPaused);

            //update the keyboard manager (the key input will be updated after all the screens' scenes associated with the engine has been updated.)
            //thus this way will give the chance for each scene to update itself with key input
            keyManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Key control of binocular camera.
        /// </summary>
        /// <param name="gameTime"></param>
        private void BinocularCameraControl(GameTime gameTime)
        {
            //Note: The perspective view for moving the camera in or out will be based on the binocular's fov.
            //zoom in
            if (keyManager.OnKeyDown(Keys.Up))
            {
                binocularBackCameraManager.Position.Z += 0.1f;
                binocularBackCameraManager.LookAt.Z += 0.1f;
            }
            //zoom out
            if (keyManager.OnKeyDown(Keys.Down))
            {
                binocularBackCameraManager.Position.Z -= 0.1f;
                binocularBackCameraManager.LookAt.Z -= 0.1f;
            }

            //recalculate the view and projection matrix
            binocularBackCameraManager.View = Matrix.CreateLookAt(binocularBackCameraManager.Position, binocularBackCameraManager.LookAt, binocularBackCameraManager.Up);
            binocularBackCameraManager.Projection = Matrix.CreatePerspectiveFieldOfView(binocularBackCameraManager.FieldOfView, 1f,
                binocularBackCameraManager.NearClip, binocularBackCameraManager.FarClip);
            binocularBackCameraManager.InvertedViewProjection = Matrix.Invert(binocularBackCameraManager.View * binocularBackCameraManager.Projection);
        }

        /// <summary>
        /// Process the key input.
        /// </summary>
        /// <param name="gameTime"></param>
        public void HandleKeyInput(GameTime gameTime)
        {
            //exit
            if (keyManager.OnInitialKeyDown(Keys.Escape))
            {
                //kill the client-airplane reader thread
                clientNetAirplaneReader.StopReadingPackets();
                //kill the client-time reader thread
                clientNetTimeReader.StopReadingPackets();

                this.Exit();
            }

            //to full screen
            if (keyManager.OnInitialKeyDown(Keys.F))
            {
                graphics.IsFullScreen = !graphics.IsFullScreen;
                graphics.ApplyChanges();
            }
            //directional light index 0 control
            //if (keyManager.OnInitialKeyDown(Keys.I))
            //    Engine.RenderSystem.DirectionalLights[0].Intensity += 0.01f;

            //if (keyManager.OnInitialKeyDown(Keys.K))
            //    Engine.RenderSystem.DirectionalLights[0].Intensity -= 0.01f;
            // anti-aliasing apply
            if (keyManager.OnInitialKeyDown(Keys.Y))
                Engine.RenderSystem.ApplyAntiAliasing = !Engine.RenderSystem.ApplyAntiAliasing;
            //bloom enabling control
            if (keyManager.OnInitialKeyDown(Keys.B))
                Engine.RenderSystem.ApplyBloom = !Engine.RenderSystem.ApplyBloom;

            //change the x-rate
            if (keyManager.OnInitialKeyDown(Keys.NumPad7) && !Engine.SimulationSettings.IsUpdateOnline)
                Engine.SimulationSettings.AdvTimeRate.CurrentXRate = 1;
            if (keyManager.OnInitialKeyDown(Keys.NumPad8) && !Engine.SimulationSettings.IsUpdateOnline)
                Engine.SimulationSettings.AdvTimeRate.CurrentXRate = 2;
            if (keyManager.OnInitialKeyDown(Keys.NumPad9) && !Engine.SimulationSettings.IsUpdateOnline)
                Engine.SimulationSettings.AdvTimeRate.CurrentXRate = 3;

            //toggle between the SCMesh's movement affect
            if (keyManager.OnInitialKeyDown(Keys.Z))
                Engine.SimulationSettings.IsSCMoveAdvancedTime = !Engine.SimulationSettings.IsSCMoveAdvancedTime;

            //cycle the current active particle effect
            if (keyManager.OnInitialKeyDown(Keys.P) && !Engine.SCMeshManager.BeginChangeToTextureD)
            {
                baseScene.currentEffect++;

                if (baseScene.currentEffect > DonMueangScene.EffectState.None)
                    baseScene.currentEffect = 0;
            }

            //toggle the pause status 
            if (keyManager.OnInitialKeyDown(Keys.RightControl))
                isPaused = !isPaused;
            
            // enter adjust mode (check if target scene has this capability, if not then it has no effect)
            if (keyManager.OnInitialKeyDown(Keys.U))
            {
                isAdjustModeEnabled = !isAdjustModeEnabled;
                baseScene.ToggleAdjustMode(isAdjustModeEnabled);
            }

            //this key just wants to test the time force-settings
            //look out your Home, Insert, PageUp, and End key. Compare them to the wall clock.
            //That's it, you know what I mean. ;)
            //Note: These methods will be suited only for offline-updating.
            if (keyManager.OnInitialKeyDown(Keys.Home))
                Engine.TimeManager.SetTime(0, 0, 0);
            if(keyManager.OnInitialKeyDown(Keys.Insert))
                Engine.TimeManager.SetTime(18, 0, 0);
            if(keyManager.OnInitialKeyDown(Keys.End))
                Engine.TimeManager.SetTime(12, 0, 0);
            if (keyManager.OnInitialKeyDown(Keys.PageUp))
                Engine.TimeManager.SetTime(6, 0, 0);

            //change the direction of wind
            if(keyManager.OnInitialKeyDown(Keys.Tab))
            {
                Vector3 wind = Engine.SimulationSettings.Wind;
                wind.X *= -1f;
                Engine.SimulationSettings.Wind = wind;
            }

            // fog
            // Fog Formulae
            if (keyManager.OnInitialKeyDown(Keys.G))
            {
                Engine.FogSettingManager.FogFormulae++;

                if (Engine.FogSettingManager.FogFormulae > FogFormulae.EXPO2)
                    Engine.FogSettingManager.FogFormulae = FogFormulae.LINEAR;
            }
            // Fog preset
            if (keyManager.OnInitialKeyDown(Keys.H))
            {
                currentFogPreset++;

                if (currentFogPreset > FogPreset.VERY_HIGH)
                    currentFogPreset = FogPreset.DEFAULT;

                // re-init, and update to effect
                Engine.FogSettingManager.InitWithPreset(currentFogPreset);
            }
            // Fog enable/disable
            if (keyManager.OnInitialKeyDown(Keys.J))
            {
                Engine.FogSettingManager.FogEnabled = !Engine.FogSettingManager.FogEnabled;
            }

            //select the bloom preset
            //select from the numpad key
            if (keyManager.OnInitialKeyDown(Keys.NumPad0))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Default;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad1))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Soft;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad2))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Desaturated;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad3))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Saturated;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad4))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Blurry;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad5))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.Subtle;
            else if (keyManager.OnInitialKeyDown(Keys.NumPad6))
                Engine.RenderSystem.BloomSetting = BloomPresetSetting.BloomPreset.InUse;


            //toggle the collision checking
            if (keyManager.OnInitialKeyDown(Keys.RightShift))
            {        
                //set to the basescene
                baseScene.isPerformCollisionChecking = !baseScene.isPerformCollisionChecking;
            }

            //switch between the clouddy/clear sky in the night (0 HRS)
            if (keyManager.OnInitialKeyDown(Keys.Subtract))
            {
                isClouddyAtNight = !isClouddyAtNight;

                if (isClouddyAtNight)
                {
                    Engine.SCMeshManager.CloudDome.TextureA = "Content/Meshes/Airport/SP_ENV_Clouddome_A";
                }
                else
                {
                    //locate to our custome clouddome texture
                    Engine.SCMeshManager.CloudDome.TextureA = "Content/Meshes/Airport/SP_ENV_Clouddome_F";
                }
            }

            //enable/disable the drawing of the binocular
            if (keyManager.OnInitialKeyDown(Keys.Delete))
            {
                isDrawToDefaultBuffer = !isDrawToDefaultBuffer;
            }

            //toggle draw of the back camera's view
            if (keyManager.OnInitialKeyDown(Keys.Down))
            {
                isBackViewDrawed = !isBackViewDrawed;
            }
            //toggle draw of left camera's view
            if (keyManager.OnInitialKeyDown(Keys.Left))
            {
                isLeftViewDrawed = !isLeftViewDrawed;
            }
            //toggle draw of the right camera's view
            if (keyManager.OnInitialKeyDown(Keys.Right))
            {
                isRightViewDrawed = !isRightViewDrawed;
            }

            // toggle debugging draw call
            if (keyManager.OnInitialKeyDown(Keys.V))
                isDebugInfoDrawed = !isDebugInfoDrawed;

            // take a screenshot
            if (keyManager.OnInitialKeyDown(Keys.Enter) && Engine.RenderSystem != null)
                Engine.RenderSystem.TakeAndSaveScreenshot(ImageFileFormat.Png);

            //BinocularCameraControl(gameTime);

            // newly set up dttRt cameras
            if (keyManager.OnInitialKeyDown(Keys.M))
                recalculateDttRtCameras();

            // toggle framerate's calculation
            if (keyManager.OnInitialKeyDown(Keys.LeftShift))
                isFramerateCalculatedAndDrawed = !isFramerateCalculatedAndDrawed;
        }

        /// <summary>
        /// Draw the debug information on screen.
        /// </summary>
        /// <remarks>This method is designed to be used with the resolution of 1280 * 1024 only.</remarks>
        /// <param name="gameTime"></param>
        public void DrawDebugInfo(GameTime gameTime)
        {
            string position = "X: " + camPos.X.ToString("0.##") + " (" + (camPos.X / Engine.SimulationSettings.GlobalScale / Engine.SimulationSettings.MapToModelScale).ToString("0.##") + ")\n";
            position += "Y: " + camPos.Y.ToString("0.##") + " (" + (camPos.Y / Engine.SimulationSettings.GlobalScale / Engine.SimulationSettings.MapToModelScale).ToString("0.##") + ")\n";
            position += "Z: " + camPos.Z.ToString("0.##") + " (" + (camPos.Z / Engine.SimulationSettings.GlobalScale / Engine.SimulationSettings.MapToModelScale).ToString("0.##") + ")\n";

            //use SaveState for safe
            Engine.SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);

            //position
            Engine.SpriteBatch.DrawString(debugFont, position,
                new Vector2(15, 12 * 2), Color.White);

            //defaultl light intensity
            Engine.SpriteBatch.DrawString(debugFont, "Sun Intensity: " + Engine.SCMeshManager.AmbientLight, new Vector2(15, 12*6), Color.White);
            //bloom apply
            Engine.SpriteBatch.DrawString(debugFont, "Bloom: " + Engine.RenderSystem.ApplyBloom.ToString(), new Vector2(15, 12 * 7), Color.White);
            //show the name of bloom settings (if bloom is applied)
            if (Engine.RenderSystem.ApplyBloom)
                Engine.SpriteBatch.DrawString(debugFont, " => " + Engine.RenderSystem.BloomSetting.ToString(), new Vector2(110, 12 * 7),
                    Color.White);

            //other time stuff
            Engine.SpriteBatch.DrawString(debugFont, "SC DeltaTime: " + Engine.SCMeshManager.DeltaTime.ToString(), new Vector2(15, 12 * 9), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Current X-Rate: " + Engine.SimulationSettings.AdvTimeRate.CurrentXRate.ToString() + "x", 
                new Vector2(15, 12 * 10), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "SC Affect: " + Engine.SimulationSettings.IsSCMoveAdvancedTime.ToString(), new Vector2(15, 12 * 11),
                Color.White);

            //special for effect only
            //if either for raining or snow then we will show its corresponding effect level
            if (baseScene.currentEffect == DonMueangScene.EffectState.Rain)
            {
                //raining particle system
                Engine.SpriteBatch.DrawString(debugFont, baseScene.currentEffect.ToString() + " [High: " + baseScene.isRainInHighLevel + "]", new Vector2(15, 12 * 12),
                    Color.White);
            }
            else if (baseScene.currentEffect == DonMueangScene.EffectState.Snow)
            {
                //snow particle system
                Engine.SpriteBatch.DrawString(debugFont, baseScene.currentEffect.ToString() + " [High: " + baseScene.isSnowInHighLevel + "]", new Vector2(15, 12 * 12),
                    Color.White);
            }
            else
            {
                //current particle system
                Engine.SpriteBatch.DrawString(debugFont, baseScene.currentEffect.ToString(), new Vector2(15, 12 * 12),
                    Color.White);
            }
            

            //current wind's strength and direction
            Engine.SpriteBatch.DrawString(debugFont, "Wind: " +Engine.SimulationSettings.Wind.ToString(), new Vector2(15, 12 * 13), Color.White);
            //current wind's change-rate
            Engine.SpriteBatch.DrawString(debugFont, "WindCR: " + Engine.SimulationSettings.WindChangeRate.ToString(), new Vector2(15, 12 * 14),
                Color.White);

            //output some stuff of texture D
            Engine.SpriteBatch.DrawString(debugFont, "Begin change to texture D: " + Engine.SCMeshManager.BeginChangeToTextureD.ToString(),
                new Vector2(15, 12 * 15), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Step up: " + Engine.SCMeshManager.IsChangeToTextureD_StepUp.ToString(),
                new Vector2(15, 12 * 16), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "DDeltaTime: " + Engine.SCMeshManager.DDeltaTime.ToString(),
                new Vector2(15, 12 * 17), Color.White);

            //if the collision between the ships occur then we will draw text to indicate
            if (baseScene.isShipCollided)
            {
                Engine.SpriteBatch.DrawString(debugFont, "Ship Collided: True", new Vector2(15, 12 * 18), Color.White);
            }
            else
            {
                Engine.SpriteBatch.DrawString(debugFont, "Ship Collided: False", new Vector2(15, 12 * 18), Color.White);
            }

            // draw whether AA is enabled
            Engine.SpriteBatch.DrawString(debugFont, "AA Enabled: " + Engine.RenderSystem.ApplyAntiAliasing, new Vector2(15, 12 * 19), Color.White);

            // Fog
            if(Engine.FogSettingManager.FogEnabled)
            {
                Engine.SpriteBatch.DrawString(debugFont, "Fog: " + Engine.FogSettingManager.FogEnabled + ", " + Engine.FogSettingManager.FogFormulae + " [" + currentFogPreset + "]",
                        new Vector2(15, 12 * 20), Color.White);
            }
            else
            {
                Engine.SpriteBatch.DrawString(debugFont, "Fog: " + Engine.FogSettingManager.FogEnabled + " [" + currentFogPreset +"]",
                        new Vector2(15, 12 * 20), Color.White);
            }

            // draw current time
            Engine.SpriteBatch.DrawString(debugFont, "Time: " + Engine.TimeManager.CurrentVirtualTime.Hours + ":" + Engine.TimeManager.CurrentVirtualTime.Minutes + ":" +
                Engine.TimeManager.CurrentVirtualTime.Seconds + ":" + Engine.TimeManager.CurrentVirtualTime.Milliseconds, new Vector2(15, 12 * 21), Color.White);

            //get the boeing 002 already registered by BaseScene from EntityManager
            //TransportationObjectEntity boeing002 = (TransportationObjectEntity)Engine.EntityManager.GetEntityFrom(2);

            /*
            //the current mode status of boeing 002 in BaseScen class
            Engine.SpriteBatch.DrawString(debugFont, "Boeing 002 Status Mode", new Vector2(15, 12 * 20), Color.White);
            //show all information
            Engine.SpriteBatch.DrawString(debugFont, "-Wheel pull out: " + boeing002.IsWheelPullOut, new Vector2(15, 12 * 21), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Window light turn on: " + boeing002.IsWindowLightTurnOn, new Vector2(15, 12 * 22), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Head light turn on: " + boeing002.IsHeadLightTurnOn, new Vector2(15, 12 * 23), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Signal light turn on: " + boeing002.IsSignalLightTurnOn, new Vector2(15, 12 * 24), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Head Ground light turn on: " + boeing002.IsHeadGroundLightTurnOn, new Vector2(15, 12 * 25), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Signal Ground light turn on: " + boeing002.IsSignalGroundLightTurnOn, new Vector2(15, 12 * 26), Color.White);

            //perform collision checking status
            Engine.SpriteBatch.DrawString(debugFont, "Perform collision checking: " + baseScene.isPerformCollisionChecking, new Vector2(15, 12 * 28), Color.White);

            //show the current status of turning on/off of the lights of airport
            Engine.SpriteBatch.DrawString(debugFont, "Airport Lights", new Vector2(15, 12 * 30), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Taxi Light: " + baseScene.taxiAirportLight.IsTurnOn, new Vector2(15, 12 * 31), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Runway01 Light: " + baseScene.runwayAirportLight01.IsTurnOn, new Vector2(15, 12 * 32), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-Runway02 Light: " + baseScene.runwayAirportLight02.IsTurnOn, new Vector2(15, 12 * 33), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-BGround Light: " + baseScene.bgroundAirportLight.IsTurnOn, new Vector2(15, 12 * 34), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "-BSignal Light: " + baseScene.bsignaAirportlLight.IsTurnOn, new Vector2(15, 12 * 35), Color.White);

            //show the current status of the lighting from airport's opaque meshes (only for meshes that have the emissive map)
            Engine.SpriteBatch.DrawString(debugFont, "AirportOM_Light-ON: " + baseScene.isAirportLightTurnOn, new Vector2(15, 12 * 37), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Entity 7009 Light-ON: " + ((AirportOpaqueMeshEntity)Engine.EntityManager.GetEntityFrom(7009)).IsLightTurnOn,
                new Vector2(15, 12 * 38), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Entity 7012 Light-ON: " + ((AirportOpaqueMeshEntity)Engine.EntityManager.GetEntityFrom(7012)).IsLightTurnOn,
                new Vector2(15, 12 * 39), Color.White);

            //show the dtTime of entity 7008 (landscape)
            Engine.SpriteBatch.DrawString(debugFont, "Landscape's baseTextureSet: " + ((AirportOpaqueMeshEntity)(Engine.EntityManager.GetEntityFrom(7008))).BaseTextureSetCode,
                new Vector2(15, 12 * 41), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Landscape's targetTextureSet: " + ((AirportOpaqueMeshEntity)(Engine.EntityManager.GetEntityFrom(7008))).TargetTextureSetCode,
                new Vector2(15, 12 * 42), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "DtTime of landscape: " + ((AirportOpaqueMeshEntity)(Engine.EntityManager.GetEntityFrom(7008))).DtTime,
                new Vector2(15, 12 * 43), Color.White);

            //show the distance to origin of the default camera
            Engine.SpriteBatch.DrawString(debugFont, "Distance-To-Origin: " + baseScene.distanceToOrigin, new Vector2(15, 12 * 44), Color.White);

            //draw the header for the bottom right dttRt
            //if (!isDrawToDefaultBuffer)
            //{
            //    Engine.SpriteBatch.DrawString(debugFont, "DTTRt: Back view", new Vector2(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 300,
            //        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 300), Color.White);
            //}

            //show the status of high level of boeing 004
            Engine.SpriteBatch.DrawString(debugFont, "Boeing 004 in high: " + baseScene.isBoeing004InHigherLevel, new Vector2(15, 12 * 45), Color.White);

            //show the status of the less of airplanes
            Engine.SpriteBatch.DrawString(debugFont, "The less of airplane: " + baseScene.isTheLessOfAirplanesEnable, new Vector2(15, 12 * 46),
                Color.White);

            //show the status of the truck (ID: 4200002)
            Engine.SpriteBatch.DrawString(debugFont, "Truck's HeadLight: " + baseScene.truckEntity.IsHeadLightTurnOn, new Vector2(15, 12 * 48), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Truck's SignalLight: " + baseScene.truckEntity.IsSignalLightTurnOn, new Vector2(15, 12 * 49), Color.White);

            //show the status of the approach lights
            Engine.SpriteBatch.DrawString(debugFont, "Approach Dynamic light: " + baseScene.isApproachDynamicLightTurnOn, new Vector2(15, 12 * 51), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Approach Static light: " + baseScene.isApproachStaticLightTurnOn, new Vector2(15, 12 * 52), Color.White);

            //show the status of cloud level at night (0 HRS)
            Engine.SpriteBatch.DrawString(debugFont, "Clouddy at 0 hours: " + isClouddyAtNight, new Vector2(15, 12 * 54), Color.White);
             */
            Engine.SpriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            //save the original value for camera manager (safe to copy just right now.)
            Engine.CameraManager.CopyTo(defaultCameraManager);

            //clear the number of airplanes drawed on the default buffer
            numAirplanesDrawedInDefaultView = 0;

            //option one (just draw to the default buffer)
            if (isDrawToDefaultBuffer)
            {
                Engine.Draw(gameTime, false, Engine.DTTRtNum.NONE, false);

                //draw what's left in user realm for alpha scene
                //set the depth buffer currently resided on the render system
                Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.DepthBuffer;
                baseScene.DrawAlpha(gameTime);
                //set the depth buffer back to the old one
                Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.OldDepthBuffer;

                //save the number of airplanes drawed on this default buffer
                numAirplanesDrawedInDefaultView = baseScene.numAirplanesDrawed;
            }
            else
            {
                //clear out the number of airplanes drawed on each view of camera
                numAirplanesDrawedInBackView = 0;
                numAirplanesDrawedInLeftView = 0;
                numAirplanesDrawedInRightView = 0;

                //those of the original properties of the camera will be updated in this class's Update() method
                Engine.BaseMaterial.Parameters["cameraPosition"].SetValue(Engine.CameraManager.Position);
                Engine.BaseMaterial.Parameters["view"].SetValue(Engine.CameraManager.View);
                Engine.BaseMaterial.Parameters["projection"].SetValue(Engine.CameraManager.Projection);
                Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(Engine.CameraManager.InvertedViewProjection);

                //draw all the content in the game world held by engine
                Engine.Draw(gameTime, true, Engine.DTTRtNum.ONE, false);

                //## Draw the user (nonstandard scene) here
                //draw on top of the DTTRt1
                Engine.GraphicsDevice.SetRenderTarget(0, Engine.DTTRt1);
                //set the depth buffer currently resided on the render system
                Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.DepthBuffer;
                baseScene.DrawAlpha(gameTime);
                //set the depth buffer back to the old one
                Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.OldDepthBuffer;
                Engine.GraphicsDevice.SetRenderTarget(0, null);
                //## End of drawing the DTTRt1

                //get the texture from the result of draw-call
                normalSceneTexture = Engine.DTTRt1.GetTexture();

                //save the number of airplanes drawed on default view (note all of airplanes are drawed in the user's scene)
                numAirplanesDrawedInDefaultView = baseScene.numAirplanesDrawed;
            }

            //if we choose to draw-to-texture
            if(!isDrawToDefaultBuffer)
            {
                #region Draw the other views of camera

                if (isBackViewDrawed)
                {
                    #region Binocular back view
                    //draw for binocular view
                    //set those parameters into effect
                    Engine.BaseMaterial.Parameters["cameraPosition"].SetValue(binocularBackCameraManager.Position);
                    Engine.BaseMaterial.Parameters["view"].SetValue(binocularBackCameraManager.View);
                    Engine.BaseMaterial.Parameters["projection"].SetValue(binocularBackCameraManager.Projection);
                    Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(binocularBackCameraManager.InvertedViewProjection);

                    //set the position of the binocular's position to the engine's camera manager (affect to the lighting calculation)
                    Engine.CameraManager.Position = binocularBackCameraManager.Position;
                    //also set the view, and projection matrix to the engine's camera manager (affect the calculation of particle)
                    Engine.CameraManager.View = binocularBackCameraManager.View;
                    Engine.CameraManager.Projection = binocularBackCameraManager.Projection;
                    Engine.CameraManager.InvertedViewProjection = binocularBackCameraManager.InvertedViewProjection;

                    //draw
                    Engine.Draw(gameTime, true, Engine.DTTRtNum.TWO, false);

                    //## Draw the user (nonstandard scene) here
                    //draw on top of the DTTRt2
                    Engine.GraphicsDevice.SetRenderTarget(0, Engine.DTTRt2);
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.DepthBuffer;
                    baseScene.DrawAlpha(gameTime);
                    //set the depth buffer back to the old one
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.OldDepthBuffer;
                    Engine.GraphicsDevice.SetRenderTarget(0, null);
                    //## End of drawing the DTTRt2

                    //get the texture from the result of draw-call of binocular's view
                    binocularBackSceneTexture = Engine.DTTRt2.GetTexture();

                    //save the number of airplanes drawed on this view
                    numAirplanesDrawedInBackView = baseScene.numAirplanesDrawed;

                    #endregion
                }

                if (isLeftViewDrawed)
                {
                    #region Binocular left view
                    //draw for binocular view
                    //set those parameters into effect
                    Engine.BaseMaterial.Parameters["cameraPosition"].SetValue(binocularLeftCameraManager.Position);
                    Engine.BaseMaterial.Parameters["view"].SetValue(binocularLeftCameraManager.View);
                    Engine.BaseMaterial.Parameters["projection"].SetValue(binocularLeftCameraManager.Projection);
                    Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(binocularLeftCameraManager.InvertedViewProjection);

                    //set the position of the binocular's position to the engine's camera manager (affect to the lighting calculation)
                    Engine.CameraManager.Position = binocularLeftCameraManager.Position;
                    //also set the view, and projection matrix to the engine's camera manager (affect the calculation of particle)
                    Engine.CameraManager.View = binocularLeftCameraManager.View;
                    Engine.CameraManager.Projection = binocularLeftCameraManager.Projection;
                    Engine.CameraManager.InvertedViewProjection = binocularLeftCameraManager.InvertedViewProjection;

                    //draw
                    Engine.Draw(gameTime, true, Engine.DTTRtNum.THREE, false);

                    //## Draw the user (nonstandard scene) here
                    //draw on top of the DTTRt3
                    Engine.GraphicsDevice.SetRenderTarget(0, Engine.DTTRt3);
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.DepthBuffer;
                    baseScene.DrawAlpha(gameTime);
                    //set the depth buffer back to the old one
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.OldDepthBuffer;
                    Engine.GraphicsDevice.SetRenderTarget(0, null);
                    //## End of drawing the DTTRt3

                    //get the texture from the result of draw-call of binocular's view
                    binocularLeftSceneTexture = Engine.DTTRt3.GetTexture();

                    //save the number of airplanes drawed on this view
                    numAirplanesDrawedInLeftView = baseScene.numAirplanesDrawed;

                    #endregion
                }

                if (isRightViewDrawed)
                {
                    #region Binocular right view
                    //draw for binocular view
                    //set those parameters into effect
                    Engine.BaseMaterial.Parameters["cameraPosition"].SetValue(binocularRightCameraManager.Position);
                    Engine.BaseMaterial.Parameters["view"].SetValue(binocularRightCameraManager.View);
                    Engine.BaseMaterial.Parameters["projection"].SetValue(binocularRightCameraManager.Projection);
                    Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(binocularRightCameraManager.InvertedViewProjection);

                    //set the position of the binocular's position to the engine's camera manager (affect to the lighting calculation)
                    Engine.CameraManager.Position = binocularRightCameraManager.Position;
                    //also set the view, and projection matrix to the engine's camera manager (affect the calculation of particle)
                    Engine.CameraManager.View = binocularRightCameraManager.View;
                    Engine.CameraManager.Projection = binocularRightCameraManager.Projection;
                    Engine.CameraManager.InvertedViewProjection = binocularRightCameraManager.InvertedViewProjection;

                    //draw
                    Engine.Draw(gameTime, true, Engine.DTTRtNum.FOUR, false);

                    //## Draw the user (nonstandard scene) here
                    //draw on top of the DTTRt4
                    Engine.GraphicsDevice.SetRenderTarget(0, Engine.DTTRt4);
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.DepthBuffer;
                    baseScene.DrawAlpha(gameTime);
                    //set the depth buffer back to the old one
                    Engine.GraphicsDevice.DepthStencilBuffer = Engine.RenderSystem.OldDepthBuffer;
                    Engine.GraphicsDevice.SetRenderTarget(0, null);
                    //## End of drawing the DTTRt4

                    //get the texture from the result of draw-call of binocular's view
                    binocularRightSceneTexture = Engine.DTTRt4.GetTexture();

                    //save the number of airplanes drawed on this view
                    numAirplanesDrawedInRightView = baseScene.numAirplanesDrawed;

                    #endregion
                }


                #endregion

                #region Actually draw onto the screen buffer
                //now actually draw onto the default buffer (we must draw them here, in which the COLOR0 will not be modified prior each separate draw-call.)
                Engine.SpriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.SaveState);

                if (!isDrawToDefaultBuffer)
                {
                    //draw the normal scene texture
                    Engine.SpriteBatch.Draw(normalSceneTexture, new Rectangle(0, 0, Engine.GraphicsDevice.PresentationParameters.BackBufferWidth,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight), Color.White);
                }

                if (isBackViewDrawed)
                {
                    //draw the binocular back scene texture
                    Engine.SpriteBatch.Draw(binocularBackSceneTexture, new Rectangle(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 256, 256, 256), Color.White);
                }
                if (isLeftViewDrawed)
                {
                    //draw the binocular left scene texture
                    Engine.SpriteBatch.Draw(binocularLeftSceneTexture, new Rectangle(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 256 * 2, 256, 256), Color.White);
                }

                if (isRightViewDrawed)
                {
                    //draw the binocular right scene texture
                    Engine.SpriteBatch.Draw(binocularRightSceneTexture, new Rectangle(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 256 * 3, 256, 256), Color.White);
                }
                Engine.SpriteBatch.End();
                #endregion

                #region Draw the text indicates each view port on the right

                //begin the spritebatch
                Engine.SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);

                if (isBackViewDrawed)
                {
                    Engine.SpriteBatch.DrawString(debugFont, "DTTRt: Back view, " + numAirplanesDrawedInBackView, new Vector2(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 15), Color.White);
                }

                if (isLeftViewDrawed)
                {
                    Engine.SpriteBatch.DrawString(debugFont, "DTTRt: Left view, " + numAirplanesDrawedInLeftView, new Vector2(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 256 - 15), Color.White);
                }

                if (isRightViewDrawed)
                {
                    Engine.SpriteBatch.DrawString(debugFont, "DTTRt: Right view, " + numAirplanesDrawedInRightView, new Vector2(Engine.GraphicsDevice.PresentationParameters.BackBufferWidth - 256,
                        Engine.GraphicsDevice.PresentationParameters.BackBufferHeight - 256 * 2 - 15), Color.White);
                }

                //end the spritebatch
                Engine.SpriteBatch.End();

                #endregion          
            }

            #region Draw current time and some flags
            //current time (draw the left thing)

            // -- *change these lines to draw on the right side of the screen --
            //begin the spritebatch
            /*Engine.SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);
            Engine.SpriteBatch.DrawString(debugFont, "Time: " +
                Engine.TimeManager.CurrentVirtualTime.Hours.ToString() + " : " +
                Engine.TimeManager.CurrentVirtualTime.Minutes.ToString() + " : " +
                Engine.TimeManager.CurrentVirtualTime.Seconds.ToString() + " : " +
                Engine.TimeManager.CurrentVirtualTime.Milliseconds.ToString(), new Vector2(15, 12 * 2), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "DTT Enabled: " + !isDrawToDefaultBuffer, new Vector2(15, 12 * 3), Color.White);
            Engine.SpriteBatch.DrawString(debugFont, "Airplanes Drawed: " + numAirplanesDrawedInDefaultView, new Vector2(15, 12 * 4), Color.White);
            Engine.SpriteBatch.End();*/
            #endregion

            //set back the default camera manager's properties into the Engine's Camera Manager
            defaultCameraManager.CopyTo(Engine.CameraManager);

            //From this point on, the drawing process will draw any content on top of the deferred/text2 screen.
            //draw something over
            if(isDebugInfoDrawed)
                DrawDebugInfo(gameTime);

            // draw text indicating that we're in adjust mode (only if entered adjust mode)
            Engine.SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);
            if (isAdjustModeEnabled)
            {
                Engine.SpriteBatch.DrawString(debugFont, "- Adjust Mode -",
                    new Vector2(graphics.GraphicsDevice.Viewport.Width / 2 - debugFont.MeasureString("- Adjust Mode -").X / 2,
                        graphics.GraphicsDevice.Viewport.Height - 100), Color.White);
            }
            Engine.SpriteBatch.End();

            //calculate the framerate
            if(isFramerateCalculatedAndDrawed)
                frameRate.CalculateFrameRateAndDrawIt(gameTime, frameRatePos);

            base.Draw(gameTime);
        }

        /// <summary>
        /// Recalculate DttRt cameras.
        /// </summary>
        /// <remarks>This will give flexible set up to users to manually position and rotate the camera around and set up 3 other DttRt cameras.</remarks>
        private void recalculateDttRtCameras()
        {
            // Calculate lookat unit vector for the main camera
            Vector3 unitLookAtMain = Engine.CameraManager.LookAt - Engine.CameraManager.Position;
            unitLookAtMain.Normalize();

            #region Binocular camera back view setup
            //create the camera manager of binocular camera
            binocularBackCameraManager.Position = new Vector3(camPos.X, camPos.Y, camPos.Z + 0.5f);
            binocularBackCameraManager.LookAt = binocularBackCameraManager.Position + Vector3.Transform(unitLookAtMain, Matrix.CreateRotationY(MathHelper.ToRadians(180)));
            binocularBackCameraManager.View = Matrix.CreateLookAt(binocularBackCameraManager.Position, binocularBackCameraManager.LookAt, binocularBackCameraManager.Up);
            binocularBackCameraManager.InvertedViewProjection = Matrix.Invert(binocularBackCameraManager.View * binocularBackCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion

            #region Binocular camera left view setup
            //create the camera manager of binocular camera
            binocularLeftCameraManager.Position = new Vector3(camPos.X - 0.5f, camPos.Y, camPos.Z);
            binocularLeftCameraManager.LookAt = binocularLeftCameraManager.Position + Vector3.Transform(unitLookAtMain, Matrix.CreateRotationY(MathHelper.ToRadians(90)));
            binocularLeftCameraManager.View = Matrix.CreateLookAt(binocularLeftCameraManager.Position, binocularLeftCameraManager.LookAt, binocularLeftCameraManager.Up);
            binocularLeftCameraManager.InvertedViewProjection = Matrix.Invert(binocularLeftCameraManager.View * binocularLeftCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion

            #region Binocular camera right view setup
            //create the camera manager of binocular camera
            binocularRightCameraManager.Position = new Vector3(camPos.X + 0.5f, camPos.Y, camPos.Z);
            binocularRightCameraManager.LookAt = binocularRightCameraManager.Position + Vector3.Transform(unitLookAtMain, Matrix.CreateRotationY(MathHelper.ToRadians(-90)));
            binocularRightCameraManager.View = Matrix.CreateLookAt(binocularRightCameraManager.Position, binocularRightCameraManager.LookAt, binocularRightCameraManager.Up);
            binocularRightCameraManager.InvertedViewProjection = Matrix.Invert(binocularRightCameraManager.View * binocularRightCameraManager.Projection);
            //the setting to the effect file will be done immediately before the drawing process of binocular camera (see Draw() method below.)
            #endregion
        }
    }
}
