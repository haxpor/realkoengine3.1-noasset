﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Helper.Util;

namespace TestRealKoEngine
{
    /// <summary>
    /// This class is only used for testing.
    /// It represents the real truck entity, which will be implemented in the near future.
    /// 
    /// Do not rely on this sourcecode, as it will be primarily for change in the near future.
    /// </summary>
    public class TruckEntity : Entity3D
    {
        //alpha meshes
        protected int hglightMeshIndex = 1;
        protected int hlightMeshIndex = 2;
        protected int elightMeshIndex = 0;

        //opaque meshes
        protected int bodyworkMeshIndex = 3;

        protected int boneIndex = 6;

        //material stuff
        protected Material opaqueMaterial;
        protected Material lm1Material;
        protected Material lm2Material;
        protected Model model;

        //light control status
        //if head light is turned on then "ground head-light must be turned on too.", due to the truck must be on the ground.
        protected bool isHeadLightTurnOn;
        protected bool isSignalLightTurnOn;

        #region Lights Properties

        /// <summary>
        /// Get or set the flag indicates the status of head light of the truck.
        /// </summary>
        public bool IsHeadLightTurnOn
        {
            get { return isHeadLightTurnOn; }
            set { isHeadLightTurnOn = value; }
        }

        /// <summary>
        /// Get or set the flag indicates the status of signal light of the truck.
        /// </summary>
        public bool IsSignalLightTurnOn
        {
            get { return isSignalLightTurnOn; }
            set { isSignalLightTurnOn = value; }
        }

        #endregion

        /// <summary>
        /// Create a new entity of ship.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeId"></param>
        /// <param name="name"></param>
        public TruckEntity(uint id, string name)
            : base(id, 5000000, name)
        {
            model = Engine.Content.Load<Model>("Content/Meshes/Others/Truck/TR_Firetruck");

            //create the material initially
            opaqueMaterial = new Material(Engine.GraphicsDevice, Engine.Content, Engine.RenderSystem.RenderEffect);
            opaqueMaterial.DiffuseTexture = "Content/Meshes/Others/Truck/TR_Cars_D";

            //[Use the precompiled shader file from the Base content folder]
            //create the lm1 material
            lm1Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/TransportationObjectAlphaLM1Render"));
            lm1Material.CacheOriginalBoneTransforms(model);
            //cannot resuse the texture from airport's stuff, as the content processor will detect missing.
            lm1Material.DiffuseTexture = "Content/Meshes/Others/Truck/SP_BLight";

            //create the lm2 meterial
            lm2Material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.Content.Load<Effect>("Base/Effects/TransportationObjectAlphaLM2Render"));
            lm2Material.CacheOriginalBoneTransforms(model);
            lm2Material.DiffuseTexture = "Content/Meshes/Others/Truck/SP_BLight";

            //set the properties of the ship
            scale *= Engine.SimulationSettings.GlobalScale;

            //at this point, we should set the accumulated model's bounding sphere (for real airplane)
            //thus the need of convenient method from XNAUtil class is in need.
            originalBoundingSphere = XNAUtil.CreateBoundingSphereFrom(model);
        }

        /// <summary>
        /// Update this ship entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            //calculate the world matrix
            world = Matrix.CreateScale(scale) *
                    Matrix.CreateRotationX(rotation.X) *
                    Matrix.CreateRotationY(rotation.Y) *
                    Matrix.CreateRotationZ(rotation.Z) *
                    Matrix.CreateTranslation(position);

            //also transform the boundingsphere too
            boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
        }

        /// <summary>
        /// Draw this ship entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            opaqueMaterial.DrawBasic(gameTime, world, model, bodyworkMeshIndex, boneIndex);
        }

        /// <summary>
        /// Draw the alpha stuff.
        /// </summary>
        /// <param name="gameTime"></param>
        public void DrawAlpha(GameTime gameTime)
        {
            if (isHeadLightTurnOn)
            {
                //draw head light mesh
                lm1Material.DrawBasic(gameTime, world, model, hlightMeshIndex, boneIndex);

                //draw the ground head-light
                lm1Material.DrawBasic(gameTime, world, model, hglightMeshIndex, boneIndex);
            }

            if (isSignalLightTurnOn)
            {
                //draw the signal light
                lm2Material.Effect.Parameters["time"].SetValue((float)gameTime.TotalGameTime.TotalSeconds * 3.14f);
                lm2Material.DrawBasic(gameTime, world, model, elightMeshIndex, boneIndex);
            }
        }
    }
}
