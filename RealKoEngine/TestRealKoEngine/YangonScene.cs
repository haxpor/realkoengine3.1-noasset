﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Graphics.Particle;
using RealKo.Framework.Helper.Util;
using RealKo.Framework.Helper.Input;

namespace TestRealKoEngine
{
    /// <summary>
    /// This is a base scene for Yangon Airport.
    /// </summary>
    public class YangonScene : Scene
    {
        //flag indicates whether or not the ships are collides
        public bool isShipCollided = false;
        //perform collision checking or not
        public bool isPerformCollisionChecking = false;
        //turn on/off the light of each mesh of airport
        public bool isAirportLightTurnOn = true;
        //level up/down the height of boeing 004
        public bool isBoeing004InHigherLevel = false;
        //enable the updating and drawing for the less of airplanes' entity
        public bool isTheLessOfAirplanesEnable = false;
        //turn on/off the dynamic approach light
        public bool isApproachDynamicLightTurnOn = false;
        //turn on/off the static approach light
        public bool isApproachStaticLightTurnOn = true;

        //spotlight index 0 rotate status (by enable this value, the spot shadow light index 1 will be disabled, otherwise enabled.)
        public bool isSpotShadowLightIndex0_Rotate = true;
        public bool isSpotShadowLightIndex1_Enable = false;

        //number of airplanes drawed
        public int numAirplanesDrawed = 0;

        //distance to origin
        public float distanceToOrigin;

        //Particle system stuff
        public enum EffectState
        {
            SmokePlume,
            Snow,
            Rain,
            Fog,
            Dusk,
            Fire,
            None
        };

        public EffectState currentEffect = EffectState.None;
        EffectState preEffect = EffectState.None;

        //individual particle system
        ParticleSystem smokePlumeParticleSystem;
        ParticleSystem snowParticleSystem;
        ParticleSystem rainParticleSystem;
        ParticleSystem rainSplashParticleSystem;
        ParticleSystem fogParticleSystem;
        ParticleSystem duskParticleSystem;
        ParticleSystem fireParticleSystem;
        //particle system that used specially for collision detection (response part)
        ParticleSystem explosionParticleSystem;

        //user's custom particle system
        ParticleSystem snowLowParticleSystem;
        ParticleSystem rainingLowParticleSystem;
        ParticleSystem fogLowParticleSystem;

        //Ship entity (just for test)
        //[ShipEntity 1-2 are used for the user's own customization of object class creation without using the predefine object format from engine]
        //ShipEntity shipEntity1, shipEntity2;

        //Truck entity (also exposed it to main Game1.cs class)
        //public TruckEntity truckEntity;

        //Airplane entity
        TransportationObjectEntity boeing001, boeing002, boeing003, boeing004;
        //Airplane that controlled the position from server app
        TransportationObjectEntity bc005, bc006;
        TransportationObjectEntity[] boeings;
        const int NUMBER_OF_THE_LESS_AIRPLANE = 125;

        //Reference keyboard manager from Game class
        KeyboardManager keyManager;

        //light mesh reference from the entity loaded by EntityManager
        //these must be known by the user at the editting of the config file. (The engine never know that information.)
        //Public statement will allow Game Class to see the information, and control the lights.
        //public AirportStaticLightMeshEntity taxiAirportLight;
        //public AirportSignalLightMeshEntity runwayAirportLight01;
        //public AirportSignalLightMeshEntity runwayAirportLight02;
        //public AirportStaticLightMeshEntity bgroundAirportLight;
        //public AirportSignalLightMeshEntity bsignaAirportlLight;

        Texture2D shapeLight1, shapeLight2;

        #region Specific code variables

        // #1 Reused position 
        Vector3 particle_pos;   // reused variabled for initialize airplane's position, and particle's position

        #endregion Specific code variables

        #region Effect controller level

        //make it public in order for Game1.cs to show it on the debugging information string
        public bool isRainInHighLevel = false;
        public bool isSnowInHighLevel = false;

        #endregion

        #region Adjust mode

        AirportOpaqueMeshEntity refScaleMesh;
        const uint REF_SCALE_MESH_ID = 7029;
    
        // save states
        bool isFogEnabled;

        #endregion Adjust mode

        /// <summary>
        /// Create the map scene 2.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="effect"></param>
        /// <param name="keyManager"></param>
        public YangonScene(Screen parent, Effect effect, KeyboardManager keyManager)
            : base(parent, effect)
        {
            this.keyManager = keyManager;
        }

        public override void PreInitialize()
        {
            //load the shapelight maps
            shapeLight1 = Engine.Content.Load<Texture2D>("Content/Textures/spotlight");
            shapeLight2 = Engine.Content.Load<Texture2D>("Content/Textures/carlight");

            // ref scale
            refScaleMesh = (AirportOpaqueMeshEntity)Engine.EntityManager.GetEntityFrom(REF_SCALE_MESH_ID);
            refScaleMesh.Material.LightOcclusion = 1.0f;
            refScaleMesh.Active = false;    // constrain to start with false, consistent with Game1.cs

            //airplane
            boeing001 = new TransportationObjectEntity(1, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(1));
            boeing002 = new TransportationObjectEntity(2, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(5));
            boeing003 = new TransportationObjectEntity(3, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(4));
            boeing004 = new TransportationObjectEntity(4, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(4));

            //bc005 = new TransportationObjectEntity(5, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(4));
            //bc006 = new TransportationObjectEntity(6, Engine.TransportationObjectInfoManager.GetTransportationObjectFrom(4));

            boeings = new TransportationObjectEntity[NUMBER_OF_THE_LESS_AIRPLANE];
            // default is 6-62 (60-62 is trucks-type)
            int aBottomId = 63; // upper id of transportation objects to random and add into the scene
            int aUpperId = 63;  // lower id of transportation objects to random and add into the scene
            for (int i = 0; i < boeings.Length - 1; i++)
            {
                boeings[i] = new TransportationObjectEntity((uint)i + 7,
                    Engine.TransportationObjectInfoManager.GetTransportationObjectFrom((uint)XNAUtil.RandomBetween(aBottomId, aUpperId)));

                //set individual properties
                boeings[i].IsWheelPullOut = false;
                boeings[i].IsSignalLightTurnOn = true;
                boeings[i].IsHeadLightTurnOn = false;
                boeings[i].IsHeadGroundLightTurnOn = false;
                boeings[i].IsSignalGroundLightTurnOn = false;
                boeings[i].IsWindowLightTurnOn = true;

                // disable rotational mesh
                //boeings[i].IsRotMesh1_Paused = false;
                //boeings[i].IsRotMesh2_Paused = false;
                //boeings[i].IsRotMesh3_Paused = false;

                boeings[i].Rotation = new Vector3(0, MathHelper.PiOver2, 0);

                //random the position across the map with
                //reuse the particle_pos for this occasion only.
                particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-70, 70),
                            0f,
                            XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-70, 70));
                //particle_pos = new Vector3(0, 10, 0);     // this line is only used for testing rotational mesh of transportation object
                boeings[i].Position = particle_pos;

                //register to the entitymanager
                Engine.EntityManager.RegisterEntity(boeings[i]);

                boeings[i].Active = isTheLessOfAirplanesEnable;
            }
            // post-set the position to one of boeing -> used this to test "translation", "rotation" of the model + rotatable meshes
            boeings[boeings.Length - 2].Position = new Vector3(26, 0f, -26);
            {
                int i = boeings.Length - 1;
                boeings[i] = new TransportationObjectEntity((uint)i + 7,
                    Engine.TransportationObjectInfoManager.GetTransportationObjectFrom((uint)XNAUtil.RandomBetween(aBottomId, aUpperId)));

                //set individual properties
                boeings[i].IsWheelPullOut = false;
                boeings[i].IsSignalLightTurnOn = true;
                boeings[i].IsHeadLightTurnOn = false;
                boeings[i].IsHeadGroundLightTurnOn = false;
                boeings[i].IsSignalGroundLightTurnOn = false;
                boeings[i].IsWindowLightTurnOn = true;

                // disable rotational mesh
                boeings[i].IsRotMesh1_Paused = false;
                boeings[i].IsRotMesh2_Paused = false;
                boeings[i].IsRotMesh3_Paused = false;

                boeings[i].Rotation = new Vector3(0, MathHelper.PiOver2, 0);

                //random the position across the map with
                //reuse the particle_pos for this occasion only.
                particle_pos = new Vector3(50, 0, -51);
                //particle_pos = new Vector3(0, 10, 0);     // this line is only used for testing rotational mesh of transportation object
                boeings[i].Position = particle_pos;

                //register to the entitymanager
                Engine.EntityManager.RegisterEntity(boeings[i]);

                boeings[i].Active = isTheLessOfAirplanesEnable;
            }

            //modify the scale for even larger
            //boeing001.Scale *= 5;
            //boeing002.Scale *= 5f;
            boeing003.Scale *= 0.05f;

            //set the initial status mode of each airplane
            boeing001.IsWheelPullOut = true;
            boeing001.IsSignalLightTurnOn = true;
            boeing001.IsHeadLightTurnOn = true;
            boeing001.IsHeadGroundLightTurnOn = true;
            boeing001.IsSignalGroundLightTurnOn = true;
            boeing001.IsWindowLightTurnOn = true;

            boeing002.IsWheelPullOut = true;
            boeing002.IsSignalLightTurnOn = true;
            boeing002.IsHeadLightTurnOn = true;
            boeing002.IsHeadGroundLightTurnOn = true;
            boeing002.IsSignalGroundLightTurnOn = true;
            boeing002.IsWindowLightTurnOn = true;

            boeing003.IsWheelPullOut = true;
            boeing003.IsSignalLightTurnOn = true;
            boeing003.IsHeadLightTurnOn = false;
            boeing003.IsHeadGroundLightTurnOn = false;
            boeing003.IsSignalGroundLightTurnOn = false;
            boeing003.IsWindowLightTurnOn = true;

            boeing004.IsWheelPullOut = false;
            boeing004.IsSignalLightTurnOn = true;
            boeing004.IsHeadLightTurnOn = false;
            boeing004.IsHeadGroundLightTurnOn = false;
            boeing004.IsSignalGroundLightTurnOn = false;
            boeing004.IsWindowLightTurnOn = true;

            /*bc005.IsWheelPullOut = false;
            bc005.IsSignalLightTurnOn = true;
            bc005.IsHeadLightTurnOn = false;
            bc005.IsHeadGroundLightTurnOn = false;
            bc005.IsSignalGroundLightTurnOn = false;
            bc005.IsWindowLightTurnOn = true;

            bc006.IsWheelPullOut = false;
            bc006.IsSignalLightTurnOn = true;
            bc006.IsHeadLightTurnOn = false;
            bc006.IsHeadGroundLightTurnOn = false;
            bc006.IsSignalGroundLightTurnOn = false;
            bc006.IsWindowLightTurnOn = true;*/

            //set the position of airplane
            boeing001.Position = new Vector3(30, 0, 0);
            boeing002.Position = new Vector3(-30, 0, 0);
            boeing003.Position = new Vector3(0, 0, -41);
            boeing004.Position = new Vector3(0, 0, -30);

            //set the direction of heading to the tower for all airplanes
            boeing001.Rotation = new Vector3(0, MathHelper.PiOver2, 0);
            boeing002.Rotation = new Vector3(0, MathHelper.PiOver2, 0);
            boeing003.Rotation = new Vector3(0, MathHelper.PiOver2, 0);
            boeing004.Rotation = new Vector3(0, MathHelper.PiOver2, 0);

            //the position, and rotation of bc005, and bc006 will be set from the server app

            //enable all the lights on airport
            //we can do the following or just setting individually for each entity that we reference to.
            //Note: Individual settings will be done individually next. (See the code below at this method.)
            foreach (AirportStaticLightMeshEntity asl in Engine.EntityManager.GetAirportStaticLightEntityList())
            {
                asl.IsTurnOn = true;
            }
            foreach (AirportSignalLightMeshEntity asgl in Engine.EntityManager.GetAirportSignalLightEntityList())
            {
                asgl.IsTurnOn = true;
            }
            foreach (AirportApproachLightMeshEntity adl in Engine.EntityManager.GetAirportApproachLightEntityList())
            {
                adl.IsTurnOn = true;
            }

            //enable the light of all airport's opaque mesh
            foreach (AirportOpaqueMeshEntity aom in Engine.EntityManager.GetAirportOpaqueEntityList())
            {
                aom.IsLightTurnOn = isAirportLightTurnOn;
            }

            //set the flag accroding to the value set in the foreach loop
            isApproachStaticLightTurnOn = true; //for all meshes
            isApproachDynamicLightTurnOn = true; //for all meshes

            //get the light mesh from EntityManager
            //taxiAirportLight = (AirportStaticLightMeshEntity)Engine.EntityManager.GetEntityFrom(10001);
            //runwayAirportLight01 = (AirportSignalLightMeshEntity)Engine.EntityManager.GetEntityFrom(10002);
            //runwayAirportLight02 = (AirportSignalLightMeshEntity)Engine.EntityManager.GetEntityFrom(10003);
            //bsignaAirportlLight = (AirportSignalLightMeshEntity)Engine.EntityManager.GetEntityFrom(10004);
            //bgroundAirportLight = (AirportStaticLightMeshEntity)Engine.EntityManager.GetEntityFrom(10005);

            #region Experiment with the directional light, to simulate the sun-light
            //Engine.RenderSystem.DirectionalShadowLights.Add(new DirectionalShadowLight(Vector3.One, Vector3.Zero, 10000, Color.White, 1.0f, 1.0f,
            //    1.0f, null,
            //    Matrix.CreateLookAt(Vector3.One, Vector3.Zero, Vector3.UnitX),
            //    Matrix.CreateOrthographicOffCenter(-10f, 10f, -10f, 10f, 0.1f, 10.0f)));
            #endregion

            //individual particle system
            smokePlumeParticleSystem = new SmokePlumeParticleSystem(Engine.GraphicsDevice, Engine.Content, "SmokePlume");
            snowParticleSystem = new SnowParticleSystem(Engine.GraphicsDevice, Engine.Content, "Snow");
            rainParticleSystem = new RainParticleSystem(Engine.GraphicsDevice, Engine.Content, "Rain");
            rainSplashParticleSystem = new RainSplashParticleSystem(Engine.GraphicsDevice, Engine.Content, "RainSplash");
            fogParticleSystem = new FogParticleSystem(Engine.GraphicsDevice, Engine.Content, "Fog");
            duskParticleSystem = new DuskParticleSystem(Engine.GraphicsDevice, Engine.Content, "Dusk");
            fireParticleSystem = new FireParticleSystem(Engine.GraphicsDevice, Engine.Content, "Fire");
            //particle for collision detection (response part)
            explosionParticleSystem = new ExplosionParticleSystem(Engine.GraphicsDevice, Engine.Content, "Explosion");

            //user's custom particle system
            snowLowParticleSystem = new SnowLowParticleSystem(Engine.GraphicsDevice, Engine.Content, "SnowLow");
            rainingLowParticleSystem = new RainParticleSystem(Engine.GraphicsDevice, Engine.Content, "RainingLow");
            fogLowParticleSystem = new FogLowParticleSystem(Engine.GraphicsDevice, Engine.Content, "FogLow");

            //set the fog's duration to short live (override 100 seconds live from the framework)
            fogParticleSystem.Duration = TimeSpan.FromSeconds(4);

            //set the depth for each of the particle system
            fogParticleSystem.Depth = 1;
            duskParticleSystem.Depth = 2;
            fireParticleSystem.Depth = 3;
            explosionParticleSystem.Depth = 4;
            smokePlumeParticleSystem.Depth = 5;
            snowParticleSystem.Depth = 6;
            rainParticleSystem.Depth = 7;
            rainSplashParticleSystem.Depth = 8;

            //set user's particle system's depth
            snowLowParticleSystem.Depth = 6;
            rainingLowParticleSystem.Depth = 7;
            fogLowParticleSystem.Depth = 1;

            //set the behaviour
            fireParticleSystem.IsAffectedByWindChanging = false;

            /*
            //ship
            shipEntity1 = new ShipEntity(4200000, "Test Ship1", 0, 0, 1);
            shipEntity2 = new ShipEntity(4200001, "Test Ship2", 0, 0, 2);
            //we dont enable them at first
            shipEntity1.Active = false;
            shipEntity2.Active = false;

            //truck
            truckEntity = new TruckEntity(4200002, "Truck");
            //enable all the lights of truck
            truckEntity.IsHeadLightTurnOn = true;
            truckEntity.IsSignalLightTurnOn = true;

            //set the ships' position
            shipEntity1.Position = new Vector3(-43, 1, 32);
            shipEntity2.Position = new Vector3(-43, 1, 32);

            //set the truck's position
            truckEntity.Position = new Vector3(-43, 0.01f, -119);

            //set some rotation angle
            shipEntity2.Rotation = new Vector3(-MathHelper.PiOver2, 0, 0);

            //set initial rotation angle for truck
            truckEntity.Rotation = new Vector3(0, -MathHelper.PiOver2, 0);
            */

            //add the particle systems into the engine
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(smokePlumeParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(snowParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(rainParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(rainSplashParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(fogParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(duskParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(explosionParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(fireParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(snowLowParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(rainingLowParticleSystem);
            Engine.RenderSystem.ParticleSystemsManager.AddParticleSystem(fogLowParticleSystem);

            //add the ship entity into the entitymanager
            //Engine.EntityManager.RegisterEntity(shipEntity1);
            //Engine.EntityManager.RegisterEntity(shipEntity2);

            //add the airplane entity into the entitymanager
            //Engine.EntityManager.RegisterEntity(boeing001);
            //Engine.EntityManager.RegisterEntity(boeing002);
            Engine.EntityManager.RegisterEntity(boeing003);
            //Engine.EntityManager.RegisterEntity(boeing004);

            //register the bc005, bc006 (this is the result from virtual command sent prior before the updating of airplane)
            //Engine.EntityManager.RegisterEntity(bc005);
            //Engine.EntityManager.RegisterEntity(bc006);

            // -- Remember to pull these lines back in when SCMesh is ready --
            /*
            #region For debugging to show-off the shadow casting (This section is not the standard or flexible way to be done in the actual release)
            //set the turn-on flag of building-ground airport's light individually
            //bgroundAirportLight.IsTurnOn = false;
            //set the turn-on flag of highway mesh's emissive light
            AirportOpaqueMeshEntity am = (AirportOpaqueMeshEntity)Engine.EntityManager.GetEntityFrom(7012);
            am.IsLightTurnOn = false;
            //set the flag of spot shadow light index 1
            Engine.RenderSystem.SpotShadowLights[1].IsEnabled = isSpotShadowLightIndex1_Enable;
            #endregion
             */
        }

        private void UpdateKeyInput(GameTime gameTime)
        {
            //key checking
            //turn on/off the light of all airport's opaque meshes
            if (keyManager.OnInitialKeyDown(Keys.RightAlt))
            {
                //isAirportLightTurnOn = !isAirportLightTurnOn;

                //now set to all the airport's opaque meshes
                //foreach (AirportOpaqueMeshEntity aom in Engine.EntityManager.GetAirportOpaqueEntityList())
                //{
                //    aom.IsLightTurnOn = isAirportLightTurnOn;
                //}
            }

            //turn on/off the runway light (blue)
            if (keyManager.OnInitialKeyDown(Keys.LeftAlt))
            {
                AirportStaticLightMeshEntity am = (AirportStaticLightMeshEntity)Engine.EntityManager.GetEntityFrom(10001);
                am.IsTurnOn = !am.IsTurnOn;
            }
            //turn on/off the runway light (yellow)
            if (keyManager.OnInitialKeyDown(Keys.LeftControl))
            {
                AirportStaticLightMeshEntity am = (AirportStaticLightMeshEntity)Engine.EntityManager.GetEntityFrom(10002);
                am.IsTurnOn = !am.IsTurnOn;
            }

            //For all the airport's meshes entity
            //change to the different textureset, also change the weather effect too.
            if (keyManager.OnInitialKeyDown(Keys.F9))
            {
                //change all opaque meshes of airport to daytextureset
                foreach (AirportOpaqueMeshEntity am in Engine.EntityManager.GetAirportOpaqueEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.DayTextureSet;
                }

                //change all alpha meshes of airport to daytextureset
                foreach (AirportAlphaMeshEntity am in Engine.EntityManager.GetAirportAlphaEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.DayTextureSet;
                }

                //stop the SCMesh effect
                Engine.SCMeshManager.BeginChangeToTextureD = false;

                //change the weather effect to None
                currentEffect = EffectState.None;
            }
            if (keyManager.OnInitialKeyDown(Keys.F10))
            {
                //change all opaque meshes of airport to raintextureset
                foreach (AirportOpaqueMeshEntity am in Engine.EntityManager.GetAirportOpaqueEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.RainingTextureSet;
                }

                //change all alpha meshes of airport to raintextureset
                foreach (AirportAlphaMeshEntity am in Engine.EntityManager.GetAirportAlphaEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.RainingTextureSet;
                }

                //if not start yet, then start
                if (!Engine.SCMeshManager.BeginChangeToTextureD)
                {
                    //change the SCMesh to response to it
                    Engine.SCMeshManager.BeginChangeToTextureD = true;

                    //get the currently used effect
                    preEffect = currentEffect;

                    //set the effect to raining
                    currentEffect = YangonScene.EffectState.Rain;
                }
            }
            if (keyManager.OnInitialKeyDown(Keys.F11))
            {
                //change all opaque meshes of airport to snowtextureset
                foreach (AirportOpaqueMeshEntity am in Engine.EntityManager.GetAirportOpaqueEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.SnowTextureSet;
                }

                //change all alpha meshes of airport to snowtextureset
                foreach (AirportAlphaMeshEntity am in Engine.EntityManager.GetAirportAlphaEntityList())
                {
                    am.TargetTextureSetCode = TextureSetCode.SnowTextureSet;
                }

                //stop the SCMesh effect
                Engine.SCMeshManager.BeginChangeToTextureD = false;

                //change the weather effect to Snow
                currentEffect = EffectState.Snow;
            }

            //enable/disable the less of airplanes
            if (keyManager.OnInitialKeyDown(Keys.PageDown))
            {
                isTheLessOfAirplanesEnable = !isTheLessOfAirplanesEnable;

                //set the activeness to all of the less of airplanes
                foreach (TransportationObjectEntity a in boeings)
                    a.Active = isTheLessOfAirplanesEnable;
            }

            //switch the effect of raining and snow to either high or low level
            //switch the raining level
            if (keyManager.OnInitialKeyDown(Keys.F7))
            {
                isRainInHighLevel = !isRainInHighLevel;
            }
            //switch the snow level
            if (keyManager.OnInitialKeyDown(Keys.F8))
            {
                isSnowInHighLevel = !isSnowInHighLevel;
            }
        }

        /// <summary>
        /// Clipping the plane if the camera is far away from the ground.
        /// This method will solve simply the problem of depth-fighting of the ground light below the building.
        /// </summary>
        /// <remarks>User need to implement the more advanced technique here, this method just to show and guide the way.</remarks>
        /// <param name="gameTime"></param>
        private void UpdateCameraNearClip(GameTime gameTime)
        {
            distanceToOrigin = Vector3.Distance(Engine.CameraManager.Position, Vector3.Zero);

            //should be above 2 for safety
            //Note: For the experiment, 1280 * 1024 => 2f.
            //      For the experiment, 1024 * 768 => 5f.
            if (Engine.CameraManager.Position.Y > 50f)
                Engine.CameraManager.NearClip = 2f;
            else
                Engine.CameraManager.NearClip = 0.1f;

            //update the shadow depth bias
            if (Engine.CameraManager.Position.Y >= 1f)
                Engine.RenderSystem.ShadowDepthBias = 0.001f;
            else
                Engine.RenderSystem.ShadowDepthBias = 0.0001f;

            //now we must recalculate the projection matrix to be used in the rendering process
            Engine.CameraManager.Projection = Engine.CameraManager.CalculateProjectionMatrix(Engine.GraphicsDevice.DisplayMode.AspectRatio);
            //set to the parameters inside the base material (effect file)
            Engine.BaseMaterial.Parameters["projection"].SetValue(Engine.CameraManager.Projection);

            //also recalculate the inverted view-projection too (as we perform this after the one in Game1.cs)
            Engine.CameraManager.InvertedViewProjection = Engine.CameraManager.CalculateInvertedViewProjectionMatrix();
            //set to the parameters inside the base material (effect file)
            //Engine.BaseMaterial.Parameters["invertedViewProjection"].SetValue(Engine.CameraManager.InvertedViewProjection);
        }

        /// <summary>
        /// Perform checking to occlude all lights and shadow lights in the scene.
        /// </summary>
        /// <remarks>This method need to be fixed.</remarks>
        private void OccludeLightsAndShadowLights()
        {
            //#DEBUG#/
            for (int i = 0; i < Engine.RenderSystem.PointLights.Count; i++)
            {
                PointLight p = Engine.RenderSystem.PointLights[i];
                p.IsEnabled = XNAUtil.AngleCheck(Engine.CameraManager.View.Forward, p.Position - Engine.CameraManager.Position,
                    MathHelper.ToRadians(90));
            }

            //Normal lights (not occlude directional lights)
            //for pointlights
            /*foreach (PointLight p in Engine.RenderSystem.PointLights)
            {
                p.IsEnabled = XNAUtil.AngleCheck(Engine.CameraManager.View.Forward, p.Position - Engine.CameraManager.Position,
                    MathHelper.ToRadians(90));
            }*/

            //for spotlights
            foreach (SpotLight s in Engine.RenderSystem.Spotlights)
            {
                s.IsEnabled = XNAUtil.AngleCheck(Engine.CameraManager.View.Forward, s.Position - Engine.CameraManager.Position,
                    MathHelper.ToRadians(90));
            }

            //Shadow lights
            //Note: To occlude the shadow light, we must take consideration of range because even if we face in opposite direction of the light,
            // its light and casting-shadow can still affect the scene as we are looking.
            //for directional lights
            foreach (DirectionalShadowLight ds in Engine.RenderSystem.DirectionalShadowLights)
            {
                ds.IsEnabled =
                    XNAUtil.AngleCheck(Engine.CameraManager.View.Forward, ds.Position - Engine.CameraManager.Position,
                    MathHelper.ToRadians(90)) ||
                    Vector3.Distance(Engine.CameraManager.Position, ds.Position) <= ds.Range;
            }

            //for spotlights
            foreach (SpotShadowLight s in Engine.RenderSystem.SpotShadowLights)
            {
                s.IsEnabled =
                    XNAUtil.AngleCheck(Engine.CameraManager.View.Forward, s.Position - Engine.CameraManager.Position,
                    MathHelper.ToRadians(90)) ||
                    Vector3.Distance(Engine.CameraManager.Position, s.Position) <= s.Range;
            }
        }

        /// <summary>
        /// Update point lights intensity.
        /// </summary>
        /// <param name="gameTime"></param>
        /*private void UpdatePointLightsIntensity(GameTime gameTime)
        {
            float intensity = 1.0f - Engine.RenderSystem.DirectionalLights[0].Intensity;

            foreach (PointLight p in Engine.RenderSystem.PointLights)
            {
                p.Color = new Color(intensity, intensity, intensity);
            }
        }*/

        public override void Update(GameTime gameTime)
        {
            //OccludeLightsAndShadowLights();

            //UpdatePointLightsIntensity(gameTime);

            //update the key input
            UpdateKeyInput(gameTime);

            //Engine.RenderSystem.DirectionalShadowLights[0].Intensity = Engine.RenderSystem.DirectionalLights[0].Intensity;

            //increate the rotation angle a little each frame
            //Vector3 r1 = shipEntity1.Rotation;
            //r1.X += 0.001f; r1.X = r1.X % MathHelper.TwoPi;
            //r1.Z += 0.001f; r1.Z = r1.Z % MathHelper.TwoPi;
            //shipEntity1.Rotation = r1;

            //Vector3 r2 = shipEntity2.Rotation;
            //r2.X += 0.001f; r2.X = r2.X % MathHelper.TwoPi;
            //r2.Z += 0.001f; r2.Z = r2.Z % MathHelper.TwoPi;
            //shipEntity2.Rotation = r2;

            //rotate the position of ship entity1
            //shipEntity1.Position = Vector3.Transform(shipEntity1.Position, Matrix.CreateRotationY(0.01f));

            //move boeing 003 higher if not perform collision detection
            if (!isPerformCollisionChecking)
            {
                //get the position of boeing 004
                Vector3 boeing003_Position = boeing003.Position;

                //move smoothly to the top
                // 
                //boeing003_Position.Y = MathHelper.Lerp(boeing003_Position.Y, 8, 0.004f);

                //set back the position to boeing 004
                boeing003.Position = boeing003_Position;
            }
            //move to the default level
            else
            {
                //get the position of boeing 003
                Vector3 boeing003_Position = boeing003.Position;

                //move smoothly to the top
                boeing003_Position.Y = MathHelper.Lerp(boeing003_Position.Y, -0.16f, 0.004f);

                //set back the position to boeing 003
                boeing003.Position = boeing003_Position;
            }

            //move boeing 004 higher if we say so
            if (isBoeing004InHigherLevel)
            {
                //get the position of boeing 004
                Vector3 boeing004_Position = boeing004.Position;

                //move smoothly to the top
                boeing004_Position.Y = MathHelper.Lerp(boeing004_Position.Y, 15, 0.004f);

                //set back the position to boeing 004
                boeing004.Position = boeing004_Position;
            }
            else
            {
                //get the position of boeing 003
                Vector3 boeing004_Position = boeing004.Position;

                //move smoothly to the top
                boeing004_Position.Y = MathHelper.Lerp(boeing004_Position.Y, 2, 0.004f);

                //set back the position to boeing 003
                boeing004.Position = boeing004_Position;
            }

            // testing update the position of airplane
            /*Vector3 pos = boeings[boeings.Length - 1].Position;
            Vector3 amount = Game1.OffsetPoint(new Vector3(1f, 0, 0), true);
            pos += amount;
            boeings[boeings.Length - 1].Position = pos;

            Vector3 rotation = boeings[boeings.Length - 1].Rotation;
            rotation.X += 0.02f;
            rotation.Y += 0.04f;
            rotation.Z += 0.05f;
            boeings[boeings.Length - 1].Rotation = rotation;*/

            //update the ships
            //shipEntity1.Update(gameTime);
            //shipEntity2.Update(gameTime);

            //update the truck
            //truckEntity.Update(gameTime);

            //rotate the position of the boeing003
            //boeing003.Position = Vector3.Transform(boeing003.Position, Matrix.CreateRotationY(0.01f));
            //rotate as circle and rotate by itself for z-axis
            //boeing002.Position = Vector3.Transform(boeing002.Position, Matrix.CreateRotationY(0.01f));
            //boeing002.Rotation = new Vector3(0.001f * (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds, 
            //    0, 0.001f * (float)Engine.TimeManager.CurrentVirtualTime.TotalMilliseconds);

            //rotate the position of the boeing004
            boeing004.Position = Vector3.Transform(boeing004.Position, Matrix.CreateRotationY(0.01f));

            //update the airplane (actual update to world matrix)
            boeing001.Update(gameTime);
            boeing002.Update(gameTime);
            boeing003.Update(gameTime);
            boeing004.Update(gameTime);
            //bc005.Update(gameTime);
            //bc006.Update(gameTime);

            //update all the less of airplanes
            if (isTheLessOfAirplanesEnable)
            {
                foreach (TransportationObjectEntity a in boeings)
                    a.Update(gameTime);

                // test "translation" + "rotation" of the model + rotable meshes
                Vector3 pos = boeings[boeings.Length - 2].Position;
                pos.X += 0.01f;
                pos.Y += 0.01f;
                boeings[boeings.Length - 2].Position = pos;

                // test at least 2 axis
                Vector3 rot = boeings[boeings.Length - 2].Rotation;
                rot.Y += 0.05f;
                rot.X += 0.05f;
                boeings[boeings.Length - 2].Rotation = rot;
            }

            //THIS IS THE SECTION THAT USER SHOULD IMPLEMENT THE COLLISION DETECTION.
            //ALL OF THE ALGORITHM IS UP TO USER'S IMPLEMENTATION.
            //check for boeings' collision
            //perform the coarse collision checking
            if (isPerformCollisionChecking)
            {
                //reduce the radius of boeing (to get the even more close collision)
                BoundingSphere b2 = boeing002.BoundingSphere;
                b2.Radius *= 0.8f;
                boeing002.BoundingSphere = b2;

                BoundingSphere b3 = boeing003.BoundingSphere;
                b3.Radius *= 0.8f;
                boeing003.BoundingSphere = b3;

                if (boeing002.BoundingSphere.Intersects(boeing003.BoundingSphere))
                {
                    //now we should do the finer collision checking
                    //get the hull mesh's bounding sphere from boeing 002
                    BoundingSphere hull002 = boeing002.Model.Meshes[(int)boeing002.TransportationObjectInfo.BodyMIndexes.Hull.X].BoundingSphere;
                    //get the hull mesh's bounding sphere from boeing 003
                    BoundingSphere hull003 = boeing003.Model.Meshes[(int)boeing003.TransportationObjectInfo.BodyMIndexes.Hull.X].BoundingSphere;

                    //transform both bounding sphere
                    hull002 = XNAUtil.TransformBoundingSphere(hull002, boeing002.World);
                    hull003 = XNAUtil.TransformBoundingSphere(hull003, boeing003.World);

                    //check for collision
                    if (hull002.Intersects(hull003))
                    {
                        //set the position of projectile of explosion paricle system
                        if (!isShipCollided)
                        {
                            for (int i = 0; i < 250; i++)
                            {
                                smokePlumeParticleSystem.AddParticle(boeing002.Position, new Vector3(0, -10, 0));

                                //to achieve more realistic effect (2 times of smokeplume's particles)
                                //for (int j = 0; j < 2; j++)
                                fireParticleSystem.AddParticle(boeing002.Position, new Vector3(2, 1, 3));
                            }
                        }

                        isShipCollided = true;
                    }
                    else
                        isShipCollided = false;
                }
                else
                    isShipCollided = false;
            }
            //END OF COLLISION DETECTION CHECKING

            UpdateCameraNearClip(gameTime);

            //add some particles each frame into appropriate particle system
            AddParticles(gameTime);
        }

        /// <summary>
        /// Add new particles for the active particle system.
        /// </summary>
        /// <param name="gameTime"></param>
        public void AddParticles(GameTime gameTime)
        {
            //Calculate the matrix used to rotate based on the offset of the airport
            Matrix offsetRot = Matrix.CreateFromYawPitchRoll(Engine.AirportOffseted_Rotation.Y, Engine.AirportOffseted_Rotation.X,
                           Engine.AirportOffseted_Rotation.Z);

            switch (currentEffect)
            {
                case EffectState.SmokePlume:
                    //add a new particle each frame (thus 60 particles per frame)
                    smokePlumeParticleSystem.AddParticle(
                            Vector3.Transform(new Vector3(9, 0, 35) + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                    break;
                case EffectState.Snow:
                    if (isSnowInHighLevel)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100), 50,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100));
                            snowParticleSystem.AddParticle(
                                   Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100), 50,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100));
                            snowLowParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }
                    }
                    break;
                case EffectState.Rain:
                    if (isRainInHighLevel)
                    {
                        //raining individual
                        for (int i = 0; i < 20; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100), 90,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 200));
                            rainParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }

                        //rainsplash
                        for (int i = 0; i < 20; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-60, 60), 0.1f,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-60, 60));
                            rainSplashParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }
                    }
                    else
                    {
                        //raining individual
                        for (int i = 0; i < 5; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100), 90,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 200));
                            rainingLowParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }

                        //rainsplash
                        for (int i = 0; i < 10; i++)
                        {
                            particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100), 0.1f,
                                XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-100, 100));
                            rainSplashParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                        }
                    }

                    break;
                case EffectState.Fog:
                    for (int i = 0; i < 3; i++)
                    {
                        particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-250, 200),
                            25 - XNAUtil.RandomBetween(5, 20),
                            XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-200, 200));
                        fogLowParticleSystem.AddParticle(
                                Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                    }
                    break;
                case EffectState.Dusk:
                    for (int i = 0; i < 6; i++)
                    {
                        particle_pos = new Vector3(XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-250, 200),
                            10 - XNAUtil.RandomBetween(7, 9),
                            XNAUtil.RandomUnitDirection() * XNAUtil.RandomBetween(-200, 200));
                        duskParticleSystem.AddParticle(
                                    Vector3.Transform(particle_pos + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                    }
                    break;
                case EffectState.Fire:
                    smokePlumeParticleSystem.AddParticle(
                        Vector3.Transform(new Vector3(-40, 0, -6) + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                    fireParticleSystem.AddParticle(
                        Vector3.Transform(new Vector3(-40, 0, -6) + Engine.AirportOffseted_Position, offsetRot), Vector3.Zero);
                    break;
            }

            //special for collision (if occurred)
            //only for other particle systems effect rather than the explosion manager effect
            if (isShipCollided)
            {
                explosionParticleSystem.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            //(no need to draw each airplane separately as we register them into the engine, and the engine will take care the drawing process
            // for us.)

            //set the number of airplanes drawed initially
            numAirplanesDrawed = 0;

            //draw truck
            //truckEntity.Draw(gameTime);

            //draw all the airplanes here
            foreach (TransportationObjectEntity a in Engine.EntityManager.GetAirplaneEntityList())
            {
                //we check the activeness first, just to reduce the overhead from view frustum's culling.
                if (a.Active)
                {
                    ContainmentType ct = Engine.CameraManager.ViewFrustum.Contains(a.BoundingSphere);
                    if (ct == ContainmentType.Contains || ct == ContainmentType.Intersects)
                    {
                        a.IsCulled = false;
                        a.Draw(gameTime);
                        numAirplanesDrawed++;
                    }
                    else
                        a.IsCulled = true;
                }
            }
        }

        /// <summary>
        /// Draw all the alpha stuff.
        /// </summary>
        /// <remarks>This method shuould be called after Engine's Draw() method is called successfully.</remarks>
        /// <param name="gameTime"></param>
        public void DrawAlpha(GameTime gameTime)
        {
            //prepare to draw in alpha mode
            //we should not do like below, if we have non-standard model need to be rendered in the scene.
            //the switching of states in graphics card can hurt the performance, thus reducing framerates.
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = true;
            Engine.GraphicsDevice.RenderState.DepthBufferEnable = true;
            Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = false;
            Engine.GraphicsDevice.RenderState.BlendFunction = BlendFunction.Add;
            Engine.GraphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
            Engine.GraphicsDevice.RenderState.DestinationBlend = Blend.One;
            Engine.GraphicsDevice.RenderState.CullMode = CullMode.None;

            //truckEntity.DrawAlpha(gameTime);

            //restore the renderstates of graphicsdevice
            Engine.GraphicsDevice.RenderState.PointSpriteEnable = false;
            Engine.GraphicsDevice.RenderState.PointSizeMax = 256;
            Engine.GraphicsDevice.RenderState.AlphaBlendEnable = false;
            Engine.GraphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
            Engine.GraphicsDevice.RenderState.BlendFactor = Color.White;
            Engine.GraphicsDevice.RenderState.SourceBlend = Blend.One;
            Engine.GraphicsDevice.RenderState.DestinationBlend = Blend.One;
            Engine.GraphicsDevice.RenderState.AlphaTestEnable = false;
            Engine.GraphicsDevice.RenderState.AlphaFunction = CompareFunction.Always;
            Engine.GraphicsDevice.RenderState.DepthBufferEnable = true;
            Engine.GraphicsDevice.RenderState.DepthBufferWriteEnable = true;
        }

        /// <summary>
        /// Toggle adjust mode.
        /// </summary>
        /// <remarks>Rolled out this method to allow externals to call as it alter the simulation's pausing state.</remarks>
        /// <param name="enable">Whether adjust mode is enabled or not.</param>
        public void ToggleAdjustMode(bool enable)
        {
           refScaleMesh.Active = enable;

           if (enable)
            {
               // save previous states
                isFogEnabled = Engine.FogSettingManager.FogEnabled;

                // disable fog
                Engine.FogSettingManager.FogEnabled = false;
            }
            else
            {
                // set states back to normal
                Engine.FogSettingManager.FogEnabled = isFogEnabled;
            }
        }
    }
}
