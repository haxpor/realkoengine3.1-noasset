﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Helper.Util;

namespace TestRealKoEngine
{
    /// <summary>
    /// This class is only used for testing.
    /// It represents the real airplane entity, which will be implemented in the near future.
    /// 
    /// Do not rely on this sourcecode, as it will be primarily for change in the near future.
    /// </summary>
    public class ShipEntity: Entity3D
    {
        protected int meshIndex;
        protected int boneIndex;
        protected Material material;
        protected Model model;

        /// <summary>
        /// Create a new entity of ship.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeId"></param>
        /// <param name="name"></param>
        public ShipEntity(uint id, string name, int meshIndex, int boneIndex, int shipType)
            : base(id, 5000000, name)
        {
            //create the material initially
            material = new Material(Engine.GraphicsDevice, Engine.Content, Engine.RenderSystem.RenderEffect);

            if (shipType == 1)
            {
                model = Engine.Content.Load<Model>("Content/Meshes/Others/ship1");

                material.DiffuseTexture = "Content/Meshes/Others/ship1_c";
                material.NormalTexture = "Content/Meshes/Others/ship1_n";
                material.SpecularTexture = "Content/Meshes/Others/ship1_s";
            }
            else if (shipType == 2)
            {
                model = Engine.Content.Load<Model>("Content/Meshes/Others/ship2");

                material.DiffuseTexture = "Content/Meshes/Others/ship2_c";
                material.NormalTexture = "Content/Meshes/Others/ship2_n";
                material.SpecularTexture = "Content/Meshes/Others/ship2_s";
            }

            //specular intensity will be grabbed from the texture, but for specular power, we must set it manually
            //if we set the specular intensity even if we already have the specular texture, the value gotten from the texture will be
            // multiplied with this SpecularIntensity value also.
            // The proper value should be in [0,1]. The higher or lower value will be clampped in that range, thus no effect.
            material.SpecularIntensity = 1f;

            //set the properties of the ship
            scale *= 0.04f;

            //set the mesh and bone index
            this.meshIndex = meshIndex;
            this.boneIndex = boneIndex;

            //at this point, we should set the accumulated model's bounding sphere (for real airplane)
            //thus the need of convenient method from XNAUtil class is in need.
            originalBoundingSphere = XNAUtil.CreateBoundingSphereFrom(model);
        }

        /// <summary>
        /// Update this ship entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            //calculate the world matrix
            world = Matrix.CreateScale(scale) *
                    Matrix.CreateRotationX(rotation.X) *
                    Matrix.CreateRotationY(rotation.Y) *
                    Matrix.CreateRotationZ(rotation.Z) *
                    Matrix.CreateTranslation(position);

            //also transform the boundingsphere too
            boundingSphere = XNAUtil.TransformBoundingSphere(originalBoundingSphere, world);
        }

        /// <summary>
        /// Draw this ship entity.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            material.DrawBasic(gameTime, world, model, meshIndex, boneIndex);
        }
    }
}
