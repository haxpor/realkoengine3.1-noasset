﻿#region File Description
//-----------------------------------------------------------------------------
// ClientSerializedNetworkWrapper.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TestRealKoEngine
{
    /// <summary>
    /// This generic class for all of the data exists in the network,
    /// it is the wrapper class for RealKo App, just to handle receiving/sending the data on the network to server side.
    /// </summary>
    /// <remarks>Do not use this class, as it is used for experiment/example only. User must adapt and extend the functionality as need.</remarks>
    public class ClientSerializedNetworkWrapper
    {
        //protected Socket socket;
        protected UdpClient socket;

        //memory and buffer stuff
        protected MemoryStream ms;
        protected BinaryReader binReader;
 
        //but note that this is when the airplane's packet is in 26 bytes long.
        /// <summary>
        /// Create the client's network wrapper.
        /// </summary>
        public ClientSerializedNetworkWrapper(IPAddress bindAddress, int port)
        {
            //create new memory stream
            ms = new MemoryStream();
            //create binary reader in UTF-8 format
            binReader = new BinaryReader(ms, Encoding.UTF8);

            //bind socket to the assigned address
            //socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //socket.Bind(new IPEndPoint(bindAddress, port));
            socket = new UdpClient(new IPEndPoint(bindAddress, port));
        }

        /// <summary>
        /// Read the available packets from the receiving stream.
        /// </summary>
        /// <remarks>The specific data format must implement this method to correctly read the data.
        /// If the user's implementation invovles multithreading issue, then user should know that this method no longer need to be called
        /// externally.</remarks>
        protected virtual void ReadPackets()
        {
            //do nothing here
        }
    }
}
