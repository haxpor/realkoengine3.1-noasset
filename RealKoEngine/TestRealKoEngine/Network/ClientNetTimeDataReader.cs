﻿#region File Description
//-----------------------------------------------------------------------------
// ClientNetTimeDataReader.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Threading;
using Microsoft.Xna.Framework;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;

namespace TestRealKoEngine
{
    /// <summary>
    /// Time data reader.
    /// </summary>
    /// <remarks>This class need to be enhanced in the future. The purpose of this class is just to guid the user how to write the network app.</remarks>
    public class ClientNetTimeDataReader : ClientSerializedNetworkWrapper
    {
        //the thread stuff with our own flag to indicate the aliveness of the thread.
        Thread thread;
        bool isAlive = true;

        #region Temp variables
        /// <summary>
        /// Temporary variable used to read the byte-array from the stream.
        /// </summary>
        byte[] bytes;
        #endregion

        /// <summary>
        /// Create the time data reader.
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <param name="port"></param>
        public ClientNetTimeDataReader(IPAddress bindAddress, int port)
            : base(bindAddress, port)
        {
            //bind the method to the thread
            thread = new Thread(new ThreadStart(ReadPackets));
            thread.IsBackground = true;

            //we have 5 bytes to read for time data packet
            bytes = new byte[5];
        }

        /// <summary>
        /// Start reading packets process.
        /// </summary>
        public void StartReadingPackets()
        {
            //start the reading-packets thread (no need to call ReadPackets() method again)
            thread.Start();
        }

        /// <summary>
        /// Stop reading packets process.
        /// </summary>
        public void StopReadingPackets()
        {
            //wake it up first time
            thread.Interrupt();
            //now kill it
            thread.Abort();
            //set our own flag, and actually kill it
            isAlive = false;
        }

        /// <summary>
        /// Implement the specific part of reading time data format.
        /// </summary>
        /// <remarks>This method will be run concurrently along with the main thread. (Multithreading)
        /// This method is also the generic method, which can handle any amount of size of packets. The server will determine the number of
        ///  packets sent to this client app.</remarks>
        protected override void ReadPackets()
        {
            try
            {
                //by using our own flag, the termination of this thread will be correctly handled.
                while (isAlive)
                {
                    //delay according to the XNA's mechanism of Updating the app.
                    //Refer to the XNA Game Studio Documentation.
                    System.Threading.Thread.Sleep(16);

                    //read data (blocking call until the available data has comed)
                    IPEndPoint refHost = new IPEndPoint(IPAddress.Any, 0);
                    //only 1 packet will be sent for time packet
                    byte[] readBuffer = socket.Receive(ref refHost);

                    //read whole to bytes
                    ms.Position = 0;
                    ms.Write(readBuffer, 0, readBuffer.Length);
                    ms.Position = 0;

                    //read the data packet
                    bytes = binReader.ReadBytes(5);

                    //now set the time for online-updating
                    Engine.TimeManager.SetTime(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4]);
                }
            }
            catch (Exception ex)
            {
                //error occur due to thread
            }
        }
    }
}
