﻿#region File Description
//-----------------------------------------------------------------------------
// DataFormat.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;

namespace TestRealKoEngine
{
    /// <summary>
    /// Data packet for the actual real airplane data.
    /// </summary>
    /// <remarks>This data format will be used entirely for one port-channel.</remarks>
    public class AirplaneDataPacket
    {
        public ushort ID;
        public float X;
        public float Y;
        public float Z;
        public float PitchAngle;
        public float YawAngle;
        public float RollAngle;

        //to notify other for this packet's size
        public const int Size = sizeof(ushort) + 6 * sizeof(float);
    }

    /// <summary>
    /// Time data packet.
    /// </summary>
    /// <remarks>This data format will be used entirely for one port-channel.</remarks>
    public class TimeDataPacket
    {
        public byte Hours;
        public byte Minutes;
        public byte Seconds;
        public byte ElapsedSeconds;
        public byte ElapsedMilliseconds;

        public const int Size = sizeof(byte) * 5;
    }
}
