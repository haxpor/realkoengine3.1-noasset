﻿#region File Description
//-----------------------------------------------------------------------------
// ClientNetAirplaneDataReader.cs
//
// RealKo Engine
// Copyright (C) - 2009. All rights reserved.
// 
// Author: Haxpor (haxpor@gmail.com)
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Threading;
using Microsoft.Xna.Framework;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;

namespace TestRealKoEngine
{
    /// <summary>
    /// Airplane data reader.
    /// </summary>
    /// <remarks>This class need to be enhanced in the future. The purpose of this class is just to guid the user how to write the network app.</remarks>
    public class ClientNetAirplaneDataReader : ClientSerializedNetworkWrapper
    {
        //the thread stuff with our own flag to indicate the aliveness of the thread.
        Thread thread;
        bool isAlive = true;

        /// <summary>
        /// Create the airplane data reader.
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <param name="port"></param>
        public ClientNetAirplaneDataReader(IPAddress bindAddress, int port)
            : base(bindAddress, port)
        {
            //bind the method to the thread
            thread = new Thread(new ThreadStart(ReadPackets));
            thread.IsBackground = true;
        }

        /// <summary>
        /// Start reading packets process.
        /// </summary>
        public void StartReadingPackets()
        {
            //start the reading-packets thread (no need to call ReadPackets() method again)
            thread.Start();
        }

        /// <summary>
        /// Stop reading packets process.
        /// </summary>
        public void StopReadingPackets()
        {
            //wake it up first time
            thread.Interrupt();
            //now kill it
            thread.Abort();
            //set our own flag, and actually kill it
            isAlive = false;
        }

        /// <summary>
        /// Implement the specific part of reading airplane data format.
        /// </summary>
        /// <remarks>This method will be run concurrently along with the main thread. (Multithreading)
        /// This method is also the generic method, which can handle any amount of size of packets. The server will determine the number of
        ///  packets sent to this client app.</remarks>
        protected override void ReadPackets()
        {
            try
            {
                //by using our own flag, the termination of this thread will be correctly handled.
                while (isAlive)
                {
                    //delay according to the XNA's mechanism of Updating the app.
                    //Refer to the XNA Game Studio Documentation.
                    System.Threading.Thread.Sleep(16);

                    //read data (blocking call until the available data has comed)
                    //int numBytesRead = socket.Receive( readBuffer, 0, readBuffer.Length-1, SocketFlags.None, out error);
                    IPEndPoint refHost = new IPEndPoint(IPAddress.Any, 0);
                    byte[] readBuffer = socket.Receive(ref refHost);

                    //int numPackets = numBytesRead / AirplaneDataPacket.Size;
                    int numPackets = readBuffer.Length / AirplaneDataPacket.Size;

                    //read whole to bytes
                    ms.Position = 0;
                    ms.Write(readBuffer, 0, readBuffer.Length);
                    ms.Position = 0;

                    for (int i = 1; i <= numPackets; i++)
                    {
                        //now set the data to each particle airplane according to airplane-id
                        //we will access the airplane entity via the dictionary implemented with hash-table for fast accessing
                        TransportationObjectEntity ae = (TransportationObjectEntity)Engine.EntityManager.GetEntityFrom(binReader.ReadUInt16());

                        //set the position
                        Vector3 position = ae.Position;
                        position.X = binReader.ReadSingle();
                        position.Y = binReader.ReadSingle();
                        position.Z = binReader.ReadSingle();
                        ae.Position = position;

                        //set the rotation angle
                        Vector3 rotation = ae.Rotation;
                        rotation.X = binReader.ReadSingle();
                        rotation.Y = binReader.ReadSingle();
                        rotation.Z = binReader.ReadSingle();
                        ae.Rotation = rotation;
                    }
                }
            }
            catch (Exception ex)
            {
                //error occur due to thread
            }
        }
    }
}
