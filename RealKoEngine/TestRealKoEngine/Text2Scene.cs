﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RealKo.Framework;
using RealKo.Framework.Graphics;
using RealKo.Framework.Entity;
using RealKo.Framework.Graphics.Particle;
using RealKo.Framework.Helper.Util;
using RealKo.Framework.Helper.Input;

namespace TestRealKoEngine
{
    /// <summary>
    /// Use this class for learning purpose, and experiment only.
    /// </summary>
    public class Text2Scene : Scene
    {
        string textShow;
        Texture2D texture1, texture2;
        //keyManager reference from the Game class
        KeyboardManager keyManager;

        /// <summary>
        /// Create the map scene 2.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="effect"></param>
        /// <param name="keyboardManager"></param>
        public Text2Scene(Screen parent, Effect effect, KeyboardManager keyboardManager)
            : base(parent, effect)
        {
            this.keyManager = keyboardManager;
        }

        public override void PreInitialize()
        {
            texture1 = Engine.Content.Load<Texture2D>("Content/Meshes/Airplane/AP_XLight");
            texture2 = Engine.Content.Load<Texture2D>("Content/Meshes/Airplane/AP_BOE_747_E000");
        }

        public override void Update(GameTime gameTime)
        {
            //checking the key input for arbitrary test key
            if (keyManager.OnKeyDown(Keys.L))
                textShow = "Key L is pressed.";
            else
                textShow = "Key L is not pressed.";
        }

        public override void Draw(GameTime gameTime)
        {
            Engine.SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState);
            //test the draw text-string
            Vector2 dimTextShow = parent.Font.MeasureString(textShow);
            Engine.SpriteBatch.DrawString(parent.Font, textShow, new Vector2(Engine.GraphicsDevice.Viewport.Width - dimTextShow.X, 
                100), Color.White);

            //test draw the texture stuff (alpha texture)
            Engine.SpriteBatch.Draw(texture1, new Rectangle(Engine.GraphicsDevice.Viewport.Width - 200, 0, 100, 100), Color.White);
            //test draw the texture stuff (opaque texture)
            Engine.SpriteBatch.Draw(texture2, new Rectangle(Engine.GraphicsDevice.Viewport.Width - 100, 0, 100, 100), Color.White);
            Engine.SpriteBatch.End();
        }
    }
}
